%% Implementation of Alpha-trimmed Mean filter
% data     - input sample of data to be processed (vector)
% win_size - filter window size
% a_value  - value of trimming parameter, in percents, possible values are
%    from 0 (filter equals to Moving Average) to 49 (filter equals to Moving Median)
% rFilt    - output filtered signal, its length is equal to the length of
%    "data", i.e. the function filters the whole signal at once.
% Last review - 13/03/17 by Alexey Roienko, a.roienko@it-jim.com

function rFilt = atm_filter(data, win_size, a_value)

if nargin ~= 3
    error('Function needs three arguments!')
elseif win_size < 0
    error('Wrong value of Window Size!')
elseif a_value > 49 || a_value < 0
    error('Wrong value of Trimmed parameter!')
else
    %% Init part
    L       = length(data);    
    rFilt   = zeros(size(data));
    trimPar = round(win_size * a_value/100);
    start   = 1+trimPar;
    finish  = win_size - trimPar;

    %% Main part
    for l=1:L
        if l==1
            rFilt(l) = data(l);
        elseif l<win_size
            rFilt(l) = mean(data(1:l));
        else            
            temp = sort(data(l-win_size+1:l));            
            rFilt(l) = mean(temp(start:finish));
        end
    end    
end

