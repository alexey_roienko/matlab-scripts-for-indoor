%% Mean Absolute Deviation
% input - vector or matrix of input data
% output - value or vector of mean absolute deviation values
function output = mae(input)

[~, Y] = size(input);
mean_values = mean(input);

output = zeros(1,Y);
for y=1:Y
    output(y) = mean(abs(input(:,y) - mean_values(y)));
end