%% Read RSSI beacons data from the input data matrix
% Two modes are supported:
% 1. Read all beacons data from the input data matrix. In this case only
% one input argument is necessary - input data matrix.
% 2. Read only selected beacons data from the input data matrix. In this case
% two input arguments are necessary - input data matrix and vector of 
% selected beacons in each row of input data matrix.
% !Note that currently only one beacon can be selected in each row of input 
% data matrix.
%
% Input arguments:
% data - Input data matrix - should be a cell matrix of the form [filesN, beaconsN]
% (not necessary) selected_beacons - vector of selected beacon numbers for each "data"-row
%
% Output:
% rssi - Output RSSI data matrix - is a cell matrix of the form [filesN, beaconsN] 
%   for the operation mode 1 if filesN>1;
%   represents matrix of double values of the form [packetsN, beaconsN] for 
%   the operation mode 1 if filesN>1 and the operation mode 2.

function rssi = get_rssi(varargin)

data = varargin{1};
if isempty(data)
    error('There is an empty input data matrix!');        
else
    %% Init
    [filesN, beaconsN] = size(data);
    if nargin == 2
        selected_beacons = varargin{2};
    end

    %% Data extration
    if nargin == 2
        [~, sel_beac_N] = size(selected_beacons);
        rssi = cell(filesN, sel_beac_N);
        for jf=1:filesN
            beacon = data{jf, selected_beacons(jf)};            
            for sb=1:sel_beac_N                
                rssi{jf, sb} = zeros(length(beacon),1);
                for p=1:length(beacon)
                    rssi{jf, sb}(p,1) = beacon{p}.RSSI;
                end
            end
        end
    else
        rssi = cell(filesN, beaconsN);
        for jf=1:filesN
            for b=1:beaconsN
                beacon = data{jf, b};
                rssi{jf, b} = zeros(length(beacon),1);
                for p=1:length(beacon)
                    rssi{jf, b}(p,1) = beacon{p}.RSSI;
                end                
            end
        end
    end
end


















