function [rFiltered, vEst] = alpha_beta_filter(rMeasured, a, b, Ts)

persistent rPred vPred
if isempty(rPred)
    rPred = rMeasured;
end
if isempty(vPred)
    vPred = 0;
end

rFiltered = rPred + a*(rMeasured - rPred);
vEst  = vPred + (b/Ts)*(rFiltered - rPred);

rPred = rFiltered + vEst*Ts;
vPred = vEst;