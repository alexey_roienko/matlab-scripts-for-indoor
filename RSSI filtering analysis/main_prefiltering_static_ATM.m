%% RSSI prefiltering investigation, ATM-filter analysis
clc, close all
clear

addpath([pwd '/datafiles/'], [pwd '/functions/'])
figN = 1;
other_beacons_enabled = 'false';
if strcmp(other_beacons_enabled, 'true')
    beaconsN = 4;
    MAC_selected = 'A:AD';
else
    beaconsN = 1;
end


%% Load data files
f_content   = dir([pwd '/datafiles/']);
f_start     = 3;
json_files  = cell(length(f_content)-f_start+1, beaconsN);
packets     = zeros(length(f_content)-f_start+1, 1);
if strcmp(other_beacons_enabled, 'true')
    beacon_index = zeros(length(f_content)-f_start+1, 1);
end

for f=f_start:length(f_content)
    disp(['Processing ' f_content(f).name ' ...']);
    json_files(f-f_start+1, :) = loadjson(f_content(f).name);    
    if strcmp(other_beacons_enabled, 'true')
        for b=1:beaconsN
            beacon_MAC_address = json_files{f-f_start+1, b}{1,1}.MACaddress;
            MACend = beacon_MAC_address(end-3:end);
            if strcmp(MACend, MAC_selected)
                beacon_index(f-f_start+1) = b;
                break;
            end
        end
        packets(f-f_start+1) = length(json_files{f-f_start+1, beacon_index(f-f_start+1)});
    else
        packets(f-f_start+1) = length(json_files{f-f_start+1});
    end
end


%% RSSI extraction from files
if strcmp(other_beacons_enabled, 'true')
    raw_rssi_values = get_rssi(json_files, beacon_index);
else
    raw_rssi_values = get_rssi(json_files);
end


%% Mean, MSE and MAE values for initial data
MX_raw_rssi_values  = zeros(1, length(json_files));
MSE_raw_rssi_values = zeros(1, length(json_files));
MAE_raw_rssi_values = zeros(1, length(json_files));
for jf=1:length(json_files)
    MX_raw_rssi_values(jf)  = mean(raw_rssi_values{jf, 1}(1:packets(jf)));
    MSE_raw_rssi_values(jf) = (var(raw_rssi_values{jf, 1}(1:packets(jf))))^.5;
    MAE_raw_rssi_values(jf) = mae(raw_rssi_values{jf, 1}(1:packets(jf)));
end


%% RSSI filtering
AT_parameters = [3 30; 5 20; 5 40; 7 10; 7 30; 7 40];
[ATpairs, ~] = size(AT_parameters);
filt_rssi_values = cell(length(json_files), ATpairs);
for jf=1:length(json_files)
    for ap=1:ATpairs
        filt_rssi_values{jf, ap} = zeros(size(raw_rssi_values{jf, 1}));
        filt_rssi_values{jf, ap}(1:packets(jf)) = atm_filter(raw_rssi_values{jf, 1}(1:packets(jf)), ...
            AT_parameters(ap,1), AT_parameters(ap,2));
    end
end


%% Accuracy estimation by derivation MSE and MAE values
mse_results = zeros(1+ATpairs, length(json_files));
mae_results = zeros(1+ATpairs, length(json_files));
for jf=1:length(json_files)
    mse_results(1, jf) = MSE_raw_rssi_values(jf);
    mae_results(1, jf) = MAE_raw_rssi_values(jf);
    for ap=1:ATpairs
        mse_results(ap+1, jf) = (var(filt_rssi_values{jf, ap}(1:packets(jf))))^0.5;        
        mae_results(ap+1, jf) = mae(filt_rssi_values{jf, ap}(1:packets(jf)));
    end
end


%% Depict raw and filtered RSSI signals
legend_strs = cell(ATpairs+1,1);
legend_strs{1} = 'raw RSSI';
for jf=1:length(json_files)
    %% General view
    figure(figN);
    
    t = 1:packets(jf);
    plot(t, raw_rssi_values{jf, 1}(t,1), 'LineWidth', 1); hold on;
    for ap=1:ATpairs
        plot(t, filt_rssi_values{jf, ap}(t,1));        
        legend_strs{ap+1} = ['WS=' num2str(AT_parameters(ap,1)) ', \alpha=' num2str(AT_parameters(ap,2))];
    end
    legend(legend_strs, 'Location', 'NorthWest');    
    str = ['Distance to beacon ' num2str(jf) 'm, Level 8'];
    title(str);
        
    max_val = max(raw_rssi_values{jf, 1}(t,1));
    min_val = min(raw_rssi_values{jf, 1}(t,1));
    dlt     = 2; %dBm
    
    xlim([t(1) t(end)]);
    ylim([min_val-dlt max_val+dlt]);
    
    xlabel('Samples');
    ylabel('RSSI values');
    
    set(gcf, 'Position', [15 450 1840 530]);
    saveas(gcf, [pwd '/figs/fig' num2str(jf) '_full.png'], 'png');
    close(figN);
    figN = figN + 1;
    
    
    %% Detailed view
    figure(figN);
    if packets(jf)>200
        t = 100:200;
    elseif packets(jf)>100
        t = 50:100;
    else
        t = 7:50;
    end
    plot(t, raw_rssi_values{jf, 1}(t,1), 'LineWidth', 1); hold on;
    for ap=1:ATpairs
        plot(t, filt_rssi_values{jf, ap}(t,1));        
        legend_strs{ap+1} = ['WS=' num2str(AT_parameters(ap,1)) ', \alpha=' num2str(AT_parameters(ap,2))];
    end
    legend(legend_strs, 'Location', 'NorthWest');    
    str = ['Distance to beacon ' num2str(jf) 'm, Level 8'];
    title(str);
        
    max_val = max(raw_rssi_values{jf, 1}(t,1));
    min_val = min(raw_rssi_values{jf, 1}(t,1));
    dlt     = 2; %dBm
    
    xlim([t(1) t(end)]);
    ylim([min_val-dlt max_val+dlt]);
    
    xlabel('Samples');
    ylabel('RSSI values');
    
    set(gcf, 'Position', [15 450 1840 530]);
    saveas(gcf, [pwd '/figs/fig' num2str(jf) '_part.png'], 'png');
    close(figN);
    figN = figN + 1;
end
















