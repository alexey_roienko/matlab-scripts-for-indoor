%% Analysis of RSSI filtering algorithm known as Moving Average for static user position 
clc, close all
clear

addpath([pwd '/datafiles/'], [pwd '/functions/'])
figN = 1;
other_beacons_enabled = 'false';
if strcmp(other_beacons_enabled, 'true')
    beaconsN = 4;
    MAC_selected = 'A:AD';
else
    beaconsN = 1;
end


%% Load data files
f_content   = dir([pwd '/datafiles/']);
f_start     = 3;
json_files  = cell(length(f_content)-f_start+1, beaconsN);
packets     = zeros(length(f_content)-f_start+1, 1);
if strcmp(other_beacons_enabled, 'true')
    beacon_index = zeros(length(f_content)-f_start+1, 1);
end

for f=f_start:length(f_content)
    disp(['Processing ' f_content(f).name ' ...']);
    json_files(f-f_start+1, :) = loadjson(f_content(f).name);    
    if strcmp(other_beacons_enabled, 'true')        
        for b=1:beaconsN
            beacon_MAC_address = json_files{f-f_start+1, b}{1,1}.MACaddress;
            MACend = beacon_MAC_address(end-3:end);
            if strcmp(MACend, MAC_selected)
                beacon_index(f-f_start+1) = b;
                break;
            end
        end
        packets(f-f_start+1) = length(json_files{f-f_start+1, beacon_index(f-f_start+1)});
    else
        packets(f-f_start+1) = length(json_files{f-f_start+1});
    end
end


%% RSSI extraction from files
if strcmp(other_beacons_enabled, 'true')
    raw_rssi_values = get_rssi(json_files, beacon_index);
else
    raw_rssi_values = get_rssi(json_files);
end


%% Mean, MSE and MAE values for initial data
MX_raw_rssi_values  = zeros(1, length(json_files));
MSE_raw_rssi_values = zeros(1, length(json_files));
MAE_raw_rssi_values = zeros(1, length(json_files));
for jf=1:length(json_files)
    MX_raw_rssi_values(jf)  = mean(raw_rssi_values{jf, 1}(1:packets(jf)));
    MSE_raw_rssi_values(jf) = (var(raw_rssi_values{jf, 1}(1:packets(jf))))^.5;
    MAE_raw_rssi_values(jf) = mae(raw_rssi_values{jf, 1}(1:packets(jf)));
end


%% RSSI filtering
WS = [3 5 7 11 15];
filt_rssi_values = cell(length(json_files), length(WS));
for jf=1:length(json_files)
    for ws=1:length(WS)
        filt_rssi_values{jf, ws} = zeros(size(raw_rssi_values{jf, 1}));
        filt_rssi_values{jf, ws}(1:packets(jf)) = ma_filter(raw_rssi_values{jf, 1}(1:packets(jf)), WS(ws));
    end
end


%% Accuracy estimation by derivation MSE and MAE values
mse_results = zeros(1+length(WS), length(json_files));
mae_results = zeros(1+length(WS), length(json_files));
for jf=1:length(json_files)
    mse_results(1, jf) = MSE_raw_rssi_values(jf);
    mae_results(1, jf) = MAE_raw_rssi_values(jf);
    for ws=1:length(WS)
        mse_results(ws+1, jf) = (var(filt_rssi_values{jf, ws}(1:packets(jf))))^0.5;        
        mae_results(ws+1, jf) = mae(filt_rssi_values{jf, ws}(1:packets(jf)));
    end
end


%% Depict raw and filtered RSSI signals
legend_strs = cell(length(WS)+1,1);
legend_strs{1} = 'raw RSSI';
for jf=1:length(json_files)
    %% General view
    figure(figN);
    
    t = 1:packets(jf);
    plot(t, raw_rssi_values{jf, 1}(t,1)); hold on;
    for ws=1:length(WS)
        plot(t, filt_rssi_values{jf, ws}(t,1));        
        legend_strs{ws+1} = ['WS=' num2str(WS(ws))];
    end
    legend(legend_strs, 'Location', 'NorthWest');    
    str = ['Distance to beacon ' num2str(jf+1) 'm, Level 8'];
    title(str);
        
    max_val = max(raw_rssi_values{jf, 1}(t,1));
    min_val = min(raw_rssi_values{jf, 1}(t,1));
    dlt     = 2; %dBm
    
    xlabel('Samples');
    ylabel('Raw RSSI values');
    
    xlim([t(1) t(end)]);
    ylim([min_val-dlt max_val+dlt]);
    set(gcf, 'Position', [15 450 1840 530]);
    saveas(gcf, [pwd '/figs/fig' num2str(jf) '_full.png'], 'png');
    close(figN);
    figN = figN + 1;
    
    
    %% Detailed view
%     figure(figN);
%     if packets(jf)>200
%         t = 100:200;
%     elseif packets(jf)>100
%         t = max(WS):100;
%     else
%         t = 11:50;
%     end
%     plot(t, raw_rssi_values{jf, 1}(t,1)); hold on;
%     for ws=1:length(WS)
%         plot(t, filt_rssi_values{jf, ws}(t,1));        
%         legend_strs{ws+1} = ['WS=' num2str(WS(ws))];
%     end
%     legend(legend_strs, 'Location', 'NorthWest');
%     %str = ['Log-file "' f_content(jf+2).name '"'];
%     str = ['Distance to beacon ' num2str(jf) 'm, Level 8'];
%     title(str);
%         
%     max_val = max(raw_rssi_values{jf, 1}(t,1));
%     min_val = min(raw_rssi_values{jf, 1}(t,1));
%     dlt     = 2; %dBm
%     
%     xlabel('Samples');
%     ylabel('Raw RSSI values');

%     xlim([t(1) t(end)]);
%     ylim([min_val-dlt max_val+dlt]);
%     set(gcf, 'Position', [15 450 1840 530]);
%     saveas(gcf, [pwd '/figs/fig' num2str(jf) '_part.png'], 'png');
%     close(figN);
%     figN = figN + 1;
end
















