%% Calibration of TX power, case 1, only one beacon is active
clc, close all
clear

addpath([pwd '/datafiles/'], [pwd '/functions/'])
figN = 1;
other_beacons_enabled = 'false';
if strcmp(other_beacons_enabled, 'true')
    beaconsN = 4;
    MAC_selected = 'A:AD';
else
    beaconsN = 1;
end


%% Load data files
f_content   = dir([pwd '/datafiles/']);
f_start     = 3;
json_files  = cell(length(f_content)-f_start+1, beaconsN);
packets     = zeros(length(f_content)-f_start+1, 1);
if strcmp(other_beacons_enabled, 'true')
    beacon_index = zeros(length(f_content)-f_start+1, 1);
end

for f=f_start:length(f_content)
    disp(['Processing ' f_content(f).name ' ...']);
    json_files(f-f_start+1, :) = loadjson(f_content(f).name);    
    if strcmp(other_beacons_enabled, 'true')        
        for b=1:beaconsN
            beacon_MAC_address = json_files{f-f_start+1, b}{1,1}.MACaddress;
            MACend = beacon_MAC_address(end-3:end);
            if strcmp(MACend, MAC_selected)
                beacon_index(f-f_start+1) = b;
                break;
            end
        end
        packets(f-f_start+1) = length(json_files{f-f_start+1, beacon_index(f-f_start+1)});
    else
        packets(f-f_start+1) = length(json_files{f-f_start+1});
    end
end


%% RSSI extraction from files
if strcmp(other_beacons_enabled, 'true')
    raw_rssi_values = get_rssi(json_files, beacon_index);
else
    raw_rssi_values = get_rssi(json_files);
end


%% Mean, MSE and MAE values for initial data
MX_raw_rssi_values  = zeros(1, length(json_files));
MSE_raw_rssi_values = zeros(1, length(json_files));
MAE_raw_rssi_values = zeros(1, length(json_files));
for jf=1:length(json_files)
    MX_raw_rssi_values(jf)  = mean(raw_rssi_values{jf, 1}(1:packets(jf)));
    MSE_raw_rssi_values(jf) = (var(raw_rssi_values{jf, 1}(1:packets(jf))))^.5;
    MAE_raw_rssi_values(jf) = mae(raw_rssi_values{jf, 1}(1:packets(jf)));
end


%% RSSI filtering
% MAF
MA_WS = [3 5];
% ATMF
AT_parameters = [7 10; 5 20];
[ATpairs, ~] = size(AT_parameters);
% ABF
AB_parameters = [0.3 0.3; 0.2 0.3];
Ts = 0.42;   % 0.420 sec.
[ABpairs, ~] = size(AB_parameters);
generalN = length(MA_WS) + ATpairs + ABpairs;
% filtering cycle
filt_rssi_values = cell(length(json_files), generalN);
for jf=1:length(json_files)
    % MAF
    start = 1;
    finish = length(MA_WS);
    for par=start:finish
        filt_rssi_values{jf, par} = zeros(size(raw_rssi_values{jf, 1}));
        filt_rssi_values{jf, par}(1:packets(jf)) = ma_filter(raw_rssi_values{jf, 1}(1:packets(jf)), MA_WS(par));
    end
    % ATMF
    start  = finish + 1;
    finish = finish + ATpairs;
    index = 1;
    for par=start:finish
        filt_rssi_values{jf, par} = zeros(size(raw_rssi_values{jf, 1}));
        filt_rssi_values{jf, par}(1:packets(jf)) = atm_filter(raw_rssi_values{jf, 1}(1:packets(jf)), ...
            AT_parameters(index,1), AT_parameters(index,2));
        index = index + 1;
    end
    % ABF
    start  = finish + 1;
    finish = finish + ABpairs;
    index = 1;
    for par=start:finish
        filt_rssi_values{jf, par} = zeros(size(raw_rssi_values{jf, 1}));
        filt_rssi_values{jf, par}(1:packets(jf)) = ab_filter(raw_rssi_values{jf, 1}(1:packets(jf)), ...
            AB_parameters(index,1), AB_parameters(index,2), Ts);
        index = index + 1;
    end
end


%% Accuracy estimation by derivation MSE and MAE values
mse_results = zeros(1+generalN, length(json_files));
mae_results = zeros(1+generalN, length(json_files));
for jf=1:length(json_files)
    mse_results(1, jf) = MSE_raw_rssi_values(jf);
    mae_results(1, jf) = MAE_raw_rssi_values(jf);
    for gN=1:generalN
        mse_results(gN+1, jf) = (var(filt_rssi_values{jf, gN}(1:packets(jf))))^0.5;        
        mae_results(gN+1, jf) = mae(filt_rssi_values{jf, gN}(1:packets(jf)));
    end
end


%% Depict raw and filtered RSSI signals
legend_strs = cell(generalN+1,1);
legend_strs{1} = 'raw RSSI';
for jf=1:length(json_files)
    %% General view
    figure(figN); 
    
    t = 1:packets(jf);
    plot(t, raw_rssi_values{jf, 1}(t,1)); hold on;
    % MAF
    start = 1;
    finish = length(MA_WS);
    for par=start:finish
        plot(t, filt_rssi_values{jf, par}(t,1));        
        legend_strs{par+1} = ['WS=' num2str(MA_WS(par))];
    end
    % ATMF
    start  = finish + 1;
    finish = finish + ATpairs;
    index = 1;
    for par=start:finish
        plot(t, filt_rssi_values{jf, par}(t,1));        
        legend_strs{par+1} = ['WS=' num2str(AT_parameters(index,1)) ', \alpha=' num2str(AT_parameters(index,2))];
        index = index + 1;
    end
    % ABF
    start  = finish + 1;
    finish = finish + ABpairs;
    index = 1;
    for par=start:finish
        plot(t, filt_rssi_values{jf, par}(t,1));
        legend_strs{par+1} = ['\alpha=' num2str(AB_parameters(index,1)) ', \beta=' num2str(AB_parameters(index,2))];
        index = index + 1;
    end    
    legend(legend_strs, 'Location', 'NorthWest');    
    str = ['Distance to beacon ' num2str(jf) 'm, Level 8'];
    title(str);
        
    max_val = max(raw_rssi_values{jf, 1}(t,1));
    min_val = min(raw_rssi_values{jf, 1}(t,1));
    dlt     = 2; %dBm
    
    xlim([t(1) t(end)]);
    ylim([min_val-dlt max_val+dlt]);
    
    xlabel('Samples');
    ylabel('RSSI values');
    
    set(gcf, 'Position', [15 450 1840 530]);
    saveas(gcf, [pwd '/figs/fig' num2str(jf) '_full.png'], 'png');
    close(figN);
    figN = figN + 1;
    
    
    %% Detailed view
    figure(figN);
    if packets(jf)>200
        t = 100:200;
    elseif packets(jf)>100
        t = 50:100;
    else
        t = 7:50;
    end
    plot(t, raw_rssi_values{jf, 1}(t,1)); hold on;
    % MAF
    start = 1;
    finish = length(MA_WS);
    for par=start:finish
        plot(t, filt_rssi_values{jf, par}(t,1));        
        legend_strs{par+1} = ['WS=' num2str(MA_WS(par))];
    end
    % ATMF
    start  = finish + 1;
    finish = finish + ATpairs;
    index = 1;
    for par=start:finish
        plot(t, filt_rssi_values{jf, par}(t,1));        
        legend_strs{par+1} = ['WS=' num2str(AT_parameters(index,1)) ', \alpha=' num2str(AT_parameters(index,2))];
        index = index + 1;
    end
    % ABF
    start  = finish + 1;
    finish = finish + ABpairs;
    index = 1;
    for par=start:finish
        plot(t, filt_rssi_values{jf, par}(t,1));
        legend_strs{par+1} = ['\alpha=' num2str(AB_parameters(index,1)) ', \beta=' num2str(AB_parameters(index,2))];
        index = index + 1;
    end  
    legend(legend_strs, 'Location', 'NorthWest');    
    str = ['Distance to beacon ' num2str(jf) 'm, Level 8'];
    title(str);
        
    max_val = max(raw_rssi_values{jf, 1}(t,1));
    min_val = min(raw_rssi_values{jf, 1}(t,1));
    dlt     = 2; %dBm
    
    xlim([t(1) t(end)]);
    ylim([min_val-dlt max_val+dlt]);
    
    xlabel('Samples');
    ylabel('RSSI values');
    
    set(gcf, 'Position', [15 450 1840 530]);
    saveas(gcf, [pwd '/figs/fig' num2str(jf) '_part.png'], 'png');
    close(figN);
    figN = figN + 1;
end
















