%% Initialization file for tMain.m script
%% Author:
%  Yevhen Chervoniak, eugenecher94@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  29/08/17

% settings.packets       = 'RSSI';              % set the type of packets to analyse display
settings.packets       = 'dist';              % set the type of packets to analyse display
% settings.displayMode   = 'raw only';          % set the groups of packets to show
settings.displayMode   = 'all';               % set the groups of packets to show
settings.beaconsN      = length(beaconsMACs); % number of beacons
settings.timeout       = 1.5;                 % seconds, time when lost packets are recovered
settings.deltaTs       = 10;                  % msec., defines minimal possible difference between close timestamps of separate beacons
settings.enableFilt    = 'true';              % recovered RSSI filtering
settings.showRecovRssi = 'true';              % show recovered RSSI
if settings.enableFilt
    settings.filter = 'Mean';                 % type of filter for filtering recovered RSSI
    switch settings.filter
        case 'Mean'
            settings.WS = 3;                  % size of filter window
    end
end
settings.debugInfo = 'false';                 % show current timestamp