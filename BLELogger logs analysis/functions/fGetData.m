%% Function extracts the RSSI values and timestamps from the input data matrix
%  which is had got from downloading a json log-file.
%% Input
% data - structure consisting of content of json-file
% beacMAClist - 1 x n cell of beacons MAC-adddresses, where n is number 
% of beacons
%% Output
% rssi - 1 x n cell of arrays of rssi values of each beacon, where n is
% number of beacons
% ts - 1 x n cell of arrays of timestamp values of each beacon, where n is
% number of beacons
% indices - 1 x n array, the number of array element corresponds to the 
% number of beacon in data structure, the value of this element corresponds
% to the number of beacon in beacMAClist
%% Author:
%  Alexey Roienko, a.roienko@it-jim.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  04/05/17


function [rssi, ts] = fGetData(data, beacMAClist)

beaconsN = length(data);
indecesN = length(beacMAClist);
indices = zeros(1, indecesN);

%% Finding beacons order in the log
for bN = 1:beaconsN
    currentMAC = data{bN}{1}.MACaddress;
    for n=1:indecesN
        if strcmpi(currentMAC, beacMAClist{n})
            indices(n) = bN;
            break;
        end
    end
end

%% Reading RSSI and timestamp values
rssi = cell(1, indecesN);
ts   = cell(1, indecesN);
for bN = 1:indecesN
    if indices(bN)~=0
        bNpacks  = length(data{indices(bN)});
        rssi{bN} = zeros(bNpacks, 1);
        ts{bN}   = zeros(bNpacks, 1);
        for pN = 1:bNpacks
            rssi{bN}(pN) = data{indices(bN)}{pN}.RSSI;         
            ts{bN}(pN)   = data{indices(bN)}{pN}.TimeStamp;         
        end
    end
end
