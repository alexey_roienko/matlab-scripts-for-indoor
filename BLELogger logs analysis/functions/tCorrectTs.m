%% Function for correction those packet timestamps which are not equal but 
%  very closed to unique timestamp values.
%  Function rewrites the initial timestamp values with the closest unique
%  ones.
%% Input
% initTs - 1 x n cell of timestamps of each beacon, n - number of beacons
% uniqueTs - double array of all unique timestamps recorded by the logger
% deltaTs - defines minimal possible difference between close timestamps of separate beacons
%% Output
% output - 1 x n structure of corrected timestamps of all beacons, n -
% number of beacons
%% Author:
%  Alexey Roienko, a.roienko@it-jim.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  04/05/17

function output = tCorrectTs(initTs, uniqueTs, deltaTs)

beaconsN = length(initTs);
output   = cell(1, beaconsN);
for bN=1:beaconsN
    if ~isempty(initTs{bN})
        bNpacks = length(initTs{bN});
        output{bN} = zeros(bNpacks, 1);
        for pN=1:bNpacks
            currentTS = initTs{bN}(pN);
            output{bN}(pN) = findClosedValue(currentTS, uniqueTs, deltaTs);
        end
    end
end



function answer = findClosedValue(curTS, unTS, deltaTS)

tsN = length(unTS);
for i=1:tsN
    if (curTS > unTS(i)-deltaTS) && (curTS < unTS(i)+deltaTS)
        answer = unTS(i);
        break;
    end
end





