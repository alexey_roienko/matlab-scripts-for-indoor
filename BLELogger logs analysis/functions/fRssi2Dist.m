%% Function provides estimation of distances to the selected beacons 
%  using beacon parameters Damp and TXpower

function output = fRssi2Dist(beacons)

beaconsN = length(beacons);
output   = zeros(1, beaconsN);

if beaconsN > 1
    for bN = 1:beaconsN
        output(bN) = 10^(0.1*(beacons{bN}.txpower - beacons{bN}.RSSI) / beacons{bN}.damp);
    end
else
    output = 10^(0.1*(beacons.txpower - beacons.RSSI) / beacons.damp);
end