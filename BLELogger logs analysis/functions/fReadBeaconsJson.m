%% The function extracts beacons data from map description json-file
%% Input
% path - char array indicating path to the json-file
% type - char array indicating the group of data being extracted
%% Output
% output - structure of data. Its type depends on type of extracted data
%% Author:
%  Yevhen Chervoniak
%  Corrected:
%  Alexey Roienko, a.roienko@it-jim.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  04/05/17

function output = fReadBeaconsJson(path, type)

persistent runN

%% Initialization
f_content = dir([pwd path]);
f_index   = 3;


%% Loading JSON file
if isempty(runN)
    disp(['Processing ' f_content(f_index).name ' ...']);
    runN = 0;
end
data = loadjson([path f_content(f_index).name]);
beacons_data = data{1}.beacons;
beaconsN = length(beacons_data);


%% Choose the necessary information
switch type
    case 'MAC'
        beacOrderedMACs = cell(1, beaconsN);
        for bN=1:beaconsN
            beacOrderedMACs{bN} = beacons_data{bN}.macAddress;
        end
        output = beacOrderedMACs;
        
    case 'beacons_params'
        coords = cell(1, beaconsN);
        for bN=1:beaconsN
            coords{bN}.x = beacons_data{bN}.x;
            coords{bN}.y = beacons_data{bN}.y;
            coords{bN}.z = beacons_data{bN}.z;
            coords{bN}.damp    = beacons_data{bN}.damp;
            coords{bN}.txpower = beacons_data{bN}.txpower;
        end
        output = coords;
        
    case 'map'        
        mapParams.title     = data{1}.description;
        mapParams.width     = data{1}.width;
        mapParams.height    = data{1}.height;
        mapParams.pixelSize = data{1}.pixelSize;
        output = mapParams;
end






