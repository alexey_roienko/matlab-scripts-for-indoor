%% Function interpolates packets, that were lost during recordings
%% INPUT:
% rawPack - 1 x n cell containing packet arrays, received from each beacon,
% n - number of beacons
% unTimestamps - array of all unique timestmps, that were recorded
% beacPack - m x n double array indicating reception (1) or loss (0) of
% packets at current timestamp, m - number of unique timestamps, n -
% number of beacons
% indices - 1 x n array, the value of array element corresponds to the 
% number of beacon in data structure, the number of this element corresponds
% to the number of beacon in beacMAClist
% settings - structure, contains all necessary info for system operation
%% OUTPUT:
% allPack - 1 x n cell containing both received and recovered packets
% (combined into 1 array) from each beacon, n - number of beacons
% recovPack - 1 x n cell containing recovered packets, n - number of
% beacons
% recovBeacPack - m x n double array indicating reception (1) or loss (0)
% of packets (including recovered ones) at current timestamp, m - number
% of unique timestamps, n - number of beacons
% allTime - 1 x n cell containing timestamps of both received and recovered
% packets, n - number of beacons
% recovTime - 1 x n cell containing timestamps of recovered packets,
% n - number of beacons
%% Authors:
% Yevhen Chervoniak, eugenecher94@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  03/07/17

function [allPack, recovPack, recovBeacPack, allTime, recovTime] = fPackRecovery(rawPack, unTimestamps, beacPack, settings)
%% Init
allPack       = cell(1, settings.beaconsN);
allTime       = cell(1, settings.beaconsN);
recovPack     = cell(1, settings.beaconsN);
recovTime     = cell(1, settings.beaconsN);
recovBeacPack = beacPack;

ts         = 1;
counters1  = zeros(1, settings.beaconsN); % counter of rawPack samples
counters2  = zeros(1, settings.beaconsN); % counter of recovPack samples
counters3  = zeros(1, settings.beaconsN); % counter of allPack samples

cBuffer = struct([]);
for bN=1:settings.beaconsN
    cBuffer(bN).input  = zeros(settings.WS, 1);
    cBuffer(bN).output = 0;
    cBuffer(bN).index  = 1;
    cBuffer(bN).reset  = settings.timeout;
    cBuffer(bN).overflow = 0;
end

%% Body
while (ts <= length(unTimestamps))
    % Extracting beacons' packets values for the current timestamp
    if strcmpi(settings.debugInfo, 'true')
        disp(['current timestamp = ' num2str(unTimestamps(ts))]);        
    end
    
    for bN = 1:settings.beaconsN
        if (~isempty(rawPack{bN})) && (beacPack(ts, bN) == 1)
            % Increase counters
            counters1(bN) = counters1(bN) + 1;
            counters3(bN) = counters3(bN) + 1;
            % Write the values down into output arrays
            cBuffer(bN).input(cBuffer(bN).index) = rawPack{bN}(counters1(bN));
            allPack{bN}(counters3(bN)) = rawPack{bN}(counters1(bN));
            allTime{bN}(counters3(bN)) = unTimestamps(ts);
            % Increase cBuffer index
            cBuffer(bN).index = cBuffer(bN).index + 1;
            % Reset timer
            cBuffer(bN).reset = settings.timeout;
            % If cBuffer is overflown...
            if cBuffer(bN).index > settings.WS
                cBuffer(bN).index = 1;
                cBuffer(bN).overflow = 1;
            end
        elseif (~isempty(rawPack{bN})) && (beacPack(ts, bN) == 0) ...
                && ts > 1 && recovBeacPack(ts-1, bN) == 1
            % Increase counters
            counters2(bN) = counters2(bN) + 1;
            counters3(bN) = counters3(bN) + 1;
            % Packet is lost. Need to decrease timer value
            deltaT = (unTimestamps(ts)-unTimestamps(ts-1))/1000;
            cBuffer(bN).reset = cBuffer(bN).reset - deltaT;
            % If timeout...
            if cBuffer(bN).reset <= 0
                % Reset buffer and timer
                counters2(bN) = counters2(bN) - 1;
                counters3(bN) = counters3(bN) - 1;
                cBuffer(bN).index  = 1;
                cBuffer(bN).reset  = settings.timeout;
                cBuffer(bN).input  = zeros(settings.WS, 1);
                cBuffer(bN).overflow = 0;
            else
                switch settings.filter
                    case 'Mean'
                        if cBuffer(bN).overflow == 0
                            cBuffer(bN).output = mean(cBuffer(bN).input(1:cBuffer(bN).index-1));
                        else
                            cBuffer(bN).output = mean(cBuffer(bN).input);
                        end
                end
                % Write the values down into output arrays
                recovPack{bN}(counters2(bN)) = cBuffer(bN).output;
                recovTime{bN}(counters2(bN)) = unTimestamps(ts);
                allPack{bN}(counters3(bN))   = cBuffer(bN).output;
                allTime{bN}(counters3(bN))   = unTimestamps(ts);
                % Set the packet as recieved
                recovBeacPack(ts, bN) = 1;
            end
        end
    end
    ts = ts + 1;
end





