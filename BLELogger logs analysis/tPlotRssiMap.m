%% Plotting of RSSI map
%% Author:
%  Yevhen Chervoniak, eugenecher94@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  29/08/17

p = line(0, 0);
for bN=1:settings.beaconsN
    if ~isempty(allTime{bN})
        num(1:length(correctTime{bN})) = bN;
        p(bN) = plot(correctTime{bN}/1000, num, '.', 'MarkerSize', 25, 'LineWidth', 1);
        
        % Check display mode
        if strcmpi(settings.displayMode, 'all')
            clear num;
            num(1:length(recovTime{bN})) = bN;
            plot(recovTime{bN}/1000, num, 'o', 'MarkerSize', 7, 'LineWidth', 1,...
                'Color', get(p(bN), 'Color'));
            for ts = 1:length(allTime{bN})
                index = ((-1)^(ts))*0.2;
                text(allTime{bN}(ts)/1000-0.3, bN+index, num2str(round(allRssi{bN}(ts)*10)/10),...
                    'Color', 'black', 'FontSize', 10) ;
            end
        else
            for ts = 1:length(correctTime{bN})
                index = ((-1)^(ts))*0.2;
                text(correctTime{bN}(ts)/1000-0.3, bN+index, num2str(round(rawRssi{bN}(ts)*10)/10),...
                    'Color', 'black', 'FontSize', 10) ;
            end
        end
        clear num;
    end
end

%% Editing the figure
title(['Available packets from beacons for ' logCaption num2str(f-f_start+1)]);
xlabel('Time, sec.');
ylabel('Beacons number (on the json-file)');
ax = gca;
ax.XTick = 0:timend/1000;
ax.YTick = 0:settings.beaconsN+1;
xlim([0 timend/1000]);
ylim([0 settings.beaconsN+1]);
ax.YGrid = 'on';
ax.GridLineStyle = '--';
ax.GridAlpha = 0.5;
box on;


