%% Initialization
clc, close all;
clear;

addpath([pwd '/logs/'], [pwd '/beacons_data/'], [pwd '/functions/']);
figN = 1;
f_start = 3;

logCaption  = 'Trajectory #';
beaconsMACs = fReadBeaconsJson('/beacons_data/', 'MAC');
beaconsParams = fReadBeaconsJson('/beacons_data/', 'beacons_params');
tGetSettings;

%% Cycle over all log-files available in the "logs" subfolder
f_content = dir([pwd '/logs/']);
for f = f_start%:length(f_content)
    %% Extrat data from the log-file
    clc;
    disp(['Processing ' f_content(f).name ' ...']);
    
    data = loadjson(f_content(f).name);
    [rawRssi, time] = fGetData(data, beaconsMACs);

    
    %% Displaying warning if one or more beacons are absent in the log
    for bN = 1:settings.beaconsN
        if isempty(rawRssi{bN})
            disp(['Warning! Beacon #' num2str(bN) ' is absent in the log!']);
        end
    end

    
    %% Find max time sample
    temp = zeros(1, settings.beaconsN);
    for bN = 1:settings.beaconsN
        if ~isempty(time{bN})
            temp(bN) = time{bN}(end);
        end
    end
    % Round up to thousand
    timend = ceil(max(temp)/1000)*1000; 
    clear temp;

    
    %% Find all unique timestamps
    for bN = 1:settings.beaconsN
        if ~isempty(time{bN})
            bNpacks = length(time{bN});
            for pN = 1:bNpacks
                k = time{bN}(pN);
                if k == 0
                    k = 1;
                end
                tempTime(k) = k;
            end
        end
    end

    k = 0;
    temp = zeros(1, 1000);
    for i = 1:length(tempTime)
        if tempTime(i) > 0
            k = k+1;
            if k == 1
                temp(k) = tempTime(i);
            elseif k > 1 && tempTime(i) - temp(k-1) > settings.deltaTs
                temp(k) = tempTime(i);
            elseif k > 1 && tempTime(i) - temp(k-1) <= settings.deltaTs
                k = k - 1;
            end
        end
    end
    unTimestamps = temp(1:k);
    
    
    %% Timestamps correction
    correctTime = tCorrectTs(time, unTimestamps, settings.deltaTs);
    
    
    %% Finding available packets from all beacons
    beacPack = zeros(length(unTimestamps), settings.beaconsN);
    for bN=1:settings.beaconsN
        if ~isempty(time{bN})
            for pN=1:length(correctTime{bN})
                index = find(unTimestamps == correctTime{bN}(pN));
                beacPack(index, bN) = 1;
            end
        end
    end
        
    clear temp tempTime index;
    
    
    if strcmpi(settings.packets, 'RSSI')
        % RSSI interpolation
        [allRssi, recovRssi, recovBeacPack, allTime, recovTime] = ...
            fPackRecovery(rawRssi, unTimestamps, beacPack, settings);
    else
        % Calculate distances
        rawDist   = cell(1, settings.beaconsN);
        for i = 1:settings.beaconsN
            for j = 1:length(rawRssi{i})
                beacon.RSSI = rawRssi{i}(j);
                beacon.txpower = beaconsParams{i}.txpower;
                beacon.damp = beaconsParams{i}.damp;
                rawDist{i}(j) = fRssi2Dist(beacon);
            end
        end
        [allDist, recovDist, recovBeacPack, allTime, recovTime] = ...
            fPackRecovery(rawDist, unTimestamps, beacPack, settings);
    end

    %% Plot the data
    unTimestamps = unTimestamps./1000;
    figure(figN);
    % Depicting timestamps dotted lines
    hold on;
    
    for i = 1:length(unTimestamps)
        plot([unTimestamps(i) unTimestamps(i)], [0 settings.beaconsN+1], 'k:');
    end
    % Depicting points for packets available
    if strcmpi(settings.packets, 'RSSI')
        tPlotRssiMap;
    else
        tPlotDistMap;
    end
    
    figN = figN + 1;
end


