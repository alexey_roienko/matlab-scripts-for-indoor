classdef CONVERTOR
%% Author:
%  Feliks Sirenko, sirenkofelix@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  21/05/17
%% Class definition
%CONVERTOR Contains functions to operatre with quaternions and Euler angles
    
    %% Public properties
    properties (Access = public)
        quaternion;                           %% q = [0 0 0 0] - quaternion
        angles;                               %% angles = [pitch roll yaw]
    end
    
    %% Object Initializer
    methods (Access = public)        
        
        function obj = Convertor(varargin)
            % quaternion = [0 0 0 0] is an array [:,4]
            % angles = [pitch roll yaw] pitch, roll and yaw are arrays;
            for i=1:2:nargin
                if strcmpi(varargin{i}, 'quaternion')
                    obj.quaternion = varargin{i+1};
                elseif strcmpi(varargin{i}, 'angles')
                    obj.angles = varargin{i+1};
                else
                    error('Invalid argument');
                end
            end
        end
        
    end
    
    %% Public methods
    methods (Access = public)
        
        function angles = fGetAnglesFromQuaternion(obj)
            %% fGETANGLESFROMQUATERNION Converts quaternion to Euler angles in degrees
            % INPUTS:
            % obj.quaternion - object property, who must be set prior to the call of the function.
            % OUTPUTS:
            % angles - array of angles [pitch roll yaw] in DEG (ATTENTION!!! Angles are not an object property)
            % REQUIREMENTS:
            % fQuaternConj(quaternion);
            % fQuatern2euler(quaternion);
  
            angles = fQuatern2euler(fQuaternConj(obj.quaternion)) * 180 / pi;           
        end
        
        function accGlobal = fFromLocalToGlobal(obj, accLocal)
            %% fFROMLOCALTOGLOBAL Converts accelerations from the local CS to the global CS
            % INPUTS:
            % accLocal - array of accelerations in the mobile phone coordinate system accLocal = [ax, ay, az] in gravity units.
            % obj.quaternion - object property, who must be set prior to the call of the function.
            % OUTPUTS:
            % accGlobal - array of accelerations in global coordinate system accGlobal = [ax, ay, az] in gravity units.
            % REQUIRES:
            % fQuaternRotate ([ax ay az],fQuaternConj (quaternion))
            % fQuaternConj (quaternion)
            
            %accGlobal = fQuaternRotate([accLocal(:,1), accLocal(:,2), accLocal(:,3)], fQuaternConj(obj.quaternion));
            accGlobal = fQuaternRotate([accLocal(:,1), accLocal(:,2), accLocal(:,3)], obj.quaternion);
        end
        
        function quaternionAr = fGetQuaterionFromAngles(obj)
            % FGETQUATERNIONFROMANGLES Converts Euler angles in degrees to quaternion
            % INPUTS:
            % obj.angles - object property, who must be set prior to the call of the function.
            % OUTPUTS:
            % quaternionAr - quaternion representation of mobile position in a global CS
            % REQUIRES:
            % angle2quat - standard
            % degtorad - standard
            %quaternionAr = angle2quat(degtorad(obj.angles(:,1)), degtorad(obj.angles(:,2)), degtorad(obj.angles(:,3)),'XYZ');

            quaternionAr = angle2quat(degtorad(obj.angles(:,3)), ...
                                      degtorad(obj.angles(:,1)), ...
                                      degtorad(obj.angles(:,2)), 'ZXY');
        end
    end
end


%% Standalone functions
function euler = fQuatern2euler(q)
    %% fQUATERN2EULER Function that converts euler angles to quaterion
    % INPUTS:
    % q - quaternion or array of quaternions like q = [0 0 0 0];
    % phi - pitch angle
    % theta - roll angle
    % psi - yaw angle
    % OUTPUTS:
    % euler - set or sets of euler angles, where
    % phi - pitch angle
    % theta - roll angle
    % psi - yaw angle
    R(1,1,:) = 2.*q(:,1).^2-1+2.*q(:,2).^2;
    R(2,1,:) = 2.*(q(:,2).*q(:,3)-q(:,1).*q(:,4));
    R(3,1,:) = 2.*(q(:,2).*q(:,4)+q(:,1).*q(:,3));
    R(3,2,:) = 2.*(q(:,3).*q(:,4)-q(:,1).*q(:,2));
    R(3,3,:) = 2.*q(:,1).^2-1+2.*q(:,4).^2;

    phi = atan2(R(3,2,:), R(3,3,:) );
    theta = -atan(R(3,1,:) ./ sqrt(1-R(3,1,:).^2) );
    psi = atan2(R(2,1,:), R(1,1,:) );

    euler = [phi(1,:)' theta(1,:)' psi(1,:)'];
end

function qConj = fQuaternConj(q)
    %% fQUATERNCONJ - function who finds conjugated quaternion
    % INPUTS:
    % q - quaternion or array of quaternions like q = [0 0 0 0];
    % OUTPUTS:
    % qConj - conjugated quaternion or aray of conjugated quaternions
    qConj = [q(:,1) -q(:,2) -q(:,3) -q(:,4)];
end

function v = fQuaternRotate(v, q)
    %% fQUATERNROTATE Function that turns local CS to make it mate global CS
    % INPUTS:
    % v - parameter, who must be transformed from local to global CS (e.g.accelerations, velocities, angles)
    % q - quaternion q = [0 0 0 0];
    % OUTPUTS:
    % v - transformed parameter in global CS
    
%     [row, ~] = size(v); 
%     v0XYZ = fQuaternProd(fQuaternProd(q, [zeros(row, 1) v]), fQuaternConj(q)); 

    [row, ~] = size(v);    
    d     = fQuaternProd(q, [zeros(row, 1) v]);
    v0XYZ = fQuaternProd(d, fQuaternConj(q));
    v = v0XYZ(:, 2:4); 
end


%% QUATERNPROD Calculated the production of 2 quaternions
%   a - quaternion a = [0 0 0 0];
%   b - quaternion b = [0 0 0 0];
function ab = fQuaternProd(a, b)    
    ab(:,1) = a(:,1).*b(:,1) - a(:,2).*b(:,2) - a(:,3).*b(:,3) - a(:,4).*b(:,4);
    ab(:,2) = a(:,1).*b(:,2) + a(:,2).*b(:,1) + a(:,3).*b(:,4) - a(:,4).*b(:,3);
    ab(:,3) = a(:,1).*b(:,3) - a(:,2).*b(:,4) + a(:,3).*b(:,1) + a(:,4).*b(:,2);
    ab(:,4) = a(:,1).*b(:,4) + a(:,2).*b(:,3) - a(:,3).*b(:,2) + a(:,4).*b(:,1);
end


