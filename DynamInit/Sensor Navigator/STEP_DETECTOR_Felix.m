%STEPDETECTOR Summary of this class goes here
% Detailed explanation goes here

classdef STEP_DETECTOR_Felix < handle
    
properties
    accPrevVal;         % absolute acceleration value at previous time sample        
    accAvg;             % current averaged absolute acceleration value
    wAccAVG;            % weight of a new measurement in the averaged acceleration value
    stepAmp;            % current averaged step amplitude value
    wStepAmp;           % weight of a new derived step amplitude in the averaged step amplitude value
    wStepTime;          % weight of a new step duration in the averaged step duration
    stepDuration;       % averaged steps duration
    coefCT;             % time safety factor (see TR)
    coefCMM;            % amplitude threshold factor (see TR)
    coefA;              % amplitude threshold (see TR)
    coefCmax;           % default maximal value of absolute acceleration (or amplitude)
    coefCmin;           % default minimal value of absolute acceleration (or amplitude)
    ampMax;             % current amplitude maximal value inside current step
    ampMin;             % current amplitude minimal value inside current step        
    tStart;             % timestamp when current step detection is started        
end    
    
    
methods (Access = public)

    %% Class constructor
    function obj = STEP_DETECTOR_Felix(varargin)
        %% Reading input parameters
        for i = 1:2:nargin
            if strcmpi(varargin{i}, 'accGlobAbsFirst')
                obj.accPrevVal = varargin{i+1};    
            elseif strcmpi(varargin{i}, 'INIStruct')
                ini = varargin{i+1};
            else
                error('Invalid argument');
            end
        end

        %% Define internal parameters
        obj.accAvg       = ini.Sensor_stepDetectorF.initAccAVG;
        obj.wAccAVG      = ini.Sensor_stepDetectorF.wAccAVG; 
        obj.stepAmp      = ini.Sensor_stepDetectorF.initStepAmp;
        obj.wStepAmp     = ini.Sensor_stepDetectorF.wStepAmp;
        obj.wStepTime    = ini.Sensor_stepDetectorF.wStepTime;
        obj.stepDuration = ini.Sensor_stepDetectorF.initStepTime;
        obj.coefCT       = ini.Sensor_stepDetectorF.coefCT;
        obj.coefCMM      = ini.Sensor_stepDetectorF.coefCMM;
        obj.coefA        = ini.Sensor_stepDetectorF.coefA;

        %% Setup initial values of detector parameters
        obj.coefCmax     = -100;
        obj.coefCmin     = 100;
        obj.ampMax       = obj.coefCmax;
        obj.ampMin       = obj.coefCmin;
        obj.tStart       = 0;
    end
    
    
    %% Class main method. Provides flag for current situation in step detection 
    %  as well as debug info.
    % INPUT:
    % t      - sec., timestamp of 'acc' value
    % acc    - grav.units, current absolute acceleration value in global coordinate system
    % OUTPUT:
    % flag   - string, detector state flag
    % output - matrix, contains detector internal paramter values, for
    %          debugging and logging
    function [flag, output] = getSteps(obj, t, acc)
        % Average acceleration
        obj.accAvg = ((1 - obj.wAccAVG) * obj.accAvg + obj.wAccAVG * acc);

        % Evaluate step condition
        stepCond = (acc > obj.accAvg) && (obj.accPrevVal < obj.accAvg);

        if stepCond
            if obj.tStart ~= 0 
                if t - obj.tStart > obj.coefCT * obj.stepDuration
                    obj.tStart = 0;
                    obj.ampMin = obj.coefCmin;
                    obj.ampMax = obj.coefCmax;
                    flag = 'STEP_RESET';
                else
                    if obj.ampMax - obj.ampMin > obj.coefA * obj.stepAmp
                        if obj.ampMax - obj.ampMin < obj.coefCMM
                            obj.stepAmp = ((1 - obj.wStepAmp) * obj.stepAmp + obj.wStepAmp * (obj.ampMax - obj.ampMin));
                        else
                            obj.stepAmp = ((1 - obj.wStepAmp) * obj.stepAmp + obj.wStepAmp * obj.coefCMM);
                        end   
                        obj.stepDuration = ((1 - obj.wStepTime) * obj.stepDuration + obj.wStepTime * (t - obj.tStart));

                        obj.tStart = t;
                        obj.ampMin = obj.ampMax;
                        obj.ampMax = obj.coefCmax;
                        flag = 'STEP_END_START';

                    else
                        obj.tStart = t;
                        obj.ampMin = obj.coefCmin;
                        obj.ampMax = obj.coefCmax;
                        flag = 'STEP_RESET_START';
                    end
                end
            else
                obj.tStart = t;
                obj.ampMin = obj.coefCmin;
                obj.ampMax = obj.coefCmax;
                flag = 'STEP_START';
            end
        else
            if obj.tStart == 0 
                flag = 'STEP_NONE';                    
            elseif t - obj.tStart > obj.coefCT * obj.stepDuration
                obj.tStart = 0;
                flag = 'STEP_RESET';
            else
                if obj.ampMax < acc
                    obj.ampMax = acc;
                end
                if obj.ampMin > acc
                    obj.ampMin = acc;
                end
                flag = 'STEP_IN';
            end
        end
        % PostProcessing
        obj.accPrevVal = acc;

        output = [obj.accAvg, double(stepCond), obj.tStart, obj.stepAmp, obj.ampMax, obj.ampMin, obj.stepDuration];
    end
end

end