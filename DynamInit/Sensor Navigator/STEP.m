classdef Step
%% Author:
%  Feliks Sirenko, sirenkofelix@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  21/05/17
%% Class definition
%STEP Class implements a step detection algorithm

    %% Public properties %%
    properties (Access = public)
        samplePeriod; % Sample period
        cutoffHigh;   % Cutoff frequency for highpass Butterworth filter
        cutoffLow;    % Cutoff frequency for lowpass Butterworth filter
        stationaryIndicator;    % Boundary of stationary 
    end
    
    %% Public methods %%
    methods (Access = public)    
        %% Object initializer
         function obj = STEP(varargin)
                for i = 1:2:nargin
                    if  strcmp(varargin{i}, 'samplePeriod')
                        obj.samplePeriod = varargin{i+1};
                    elseif  strcmp(varargin{i}, 'cutoffLow')
                        obj.cutoffLow = varargin{i+1};
                     elseif  strcmp(varargin{i}, 'stationaryIndicator')
                        obj.stationaryIndicator = varargin{i+1};
                    else
                        error('Invalid argument');
                    end
                end;
         end;
    end;
    
    %% Public methods %%
    methods (Access = public)
        function result = fDetectSteps(obj, acc, filterFlag)
        %% fDETECTSTEPS Core function responsible for the step detection
        % INPUTS:
        % obj.samplePeriod - object property, who must be set prior to the call of the function.
        % obj.cutoffHigh - object property, who must be set prior to the call of the function.
        % obj.cutoffLow - object property, who must be set prior to the call of the function.
        % obj.stationaryIndicator - object property, who must be set prior to the call of the function.
        % OUTPUTS:
        % stat - binaty array of states (0 - motion, 1 - stationary)
        % REQUIREMENTS:
        % obj.funcButterworthFilter_High(accel);
        % obj.funcButterWorthFilter_Low(acc_norm);
        % obj.fPlotRawAndStationary (t, ax, ay, az, anorm, stationary)
            
            if strcmpi(filterFlag, 'on')
                accX = obj.fButterWorthFilter_Low(acc(:,1));
                accY = obj.fButterWorthFilter_Low(acc(:,2));
                accZ = obj.fButterWorthFilter_Low(acc(:,3));
            end
            accX = acc(end,1);
            accY = acc(end,2);
            accZ = acc(end,3);

            % Compute accelerometer magnitude
            acc_norm = (accX^2 + accY^2 + accZ^2).^0.5;
            acc_norm(acc_norm < 0) = 0;           
            % Threshold detection:
            % 1 - point is stationary, 0 - point is not stationary
            stat = acc_norm < obj.stationaryIndicator;
            stat = stat(end);

            result = [accX accY accZ stat];
        end
    end
    
    %% Private methods %%
    methods (Access = public)
        function param = fButterWorthFilter_Low(obj, param)
        %% fBUTTERWORTHFILTER_LOW Highpass Butterworth filter
        % INPUTS:
        % param - acceleartion that must be filtered
        % obj.samplePeriod - object property, who must be set prior to the call of the function.
        % obj.cutoffLow - object property, who must be set prior to the call of the function.
        % OUTPUTS:
        % param - filtered acceleartion
            [b, a] = butter(8, (2 * obj.cutoffLow)/(1/obj.samplePeriod), 'low' ); 
            param = filter(b, a, param);
            %param = filtfilt(b, a, param); 
        end        
    end
end

