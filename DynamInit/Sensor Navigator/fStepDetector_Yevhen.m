%% Function detects the moments of user activity (motion)
%% INPUT:
%  input - n x 1 double vector of all received z-acceleration packets, 
%  n - number of packets
%% OUTPUT:
%  steps - m x 3 array of information about steps, m - number of steps
%  detected, each raw has the following format:
%  step start (number of sample) - step finish (number o sample) - length
%  of step (finish - start)
%  stationary - n x 1 logical vector - 'false' if current acceleration 
%  sample belongs to step, 'true' if it is not
%% Author:
%  Yevhen Chervoniak
%% Owner:
%  IT-Jim Company
%% Last update:
%  08/08/17

function [steps, stationary] = fStepDetector_notApproved(input)

%% Initialization
L       = length(input);
zDeriv  = zeros(L, 1);
zDeriv2 = zeros(L, 1);
stationary = true(L, 1);
stepNum  = 1;
dt       = 0.03;

%% Calculate derivatives
for t = 1:L
    if t > 1
        zDeriv(t)  = (input(t) - input(t-1))/dt;
        zDeriv2(t) = (zDeriv(t) - zDeriv(t-1))/dt;
    end
end

%% Set constants
zDerLim    = max(zDeriv)*0.25;
zDer2Lim   = max(zDeriv2)*0.15;
stepLength = 24;
stepQuart  = stepLength/4;

%% Detect steps
%Initialize
for t = 1:L
    if zDeriv(t) > zDerLim && abs(zDeriv2(t)) < zDer2Lim
        steps(1, 1) = t;
        stationary(t, 1) = false;
        break;
    end
end

% Continue
for t = steps(1, 1)+1:L
    if zDeriv(t) > zDerLim && abs(zDeriv2(t)) < zDer2Lim
        if t - steps(stepNum, 1) > stepQuart && t - steps(stepNum, 1) <= stepLength
            steps(stepNum, 2) = t;
            steps(stepNum, 3) = steps(stepNum, 2) - steps(stepNum, 1);            
            stationary(steps(stepNum, 1):steps(stepNum, 2)) = false;
            stepNum = stepNum + 1;            
            steps(stepNum, 1) = t;
        end
        if t - steps(stepNum, 1) > stepLength
            steps(stepNum, 1) = t;
        end
    end
end
if steps(end, 3) == 0
    steps = steps(1:end-1, :);
end

end