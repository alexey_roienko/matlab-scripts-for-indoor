%% Function implements averaging sliding window filtering
% input   - data vector or matrix
% WS      - sliding window size
% modeStr - string described one of two modes:
%           "round" - data filtering using rounding input samples
%           "other str or empty" - ordinary filtering

function output = fASWFiltering2(input, WS, modeStr)

%% Initialization
[X, Y] = size(input);
output = zeros(X, Y);
fifoSize = 0;
counter = 1;

%% Format definition
if ~(X==1 || Y==1)    
    fifo_vector = zeros(WS, Y);
else
    fifo_vector = zeros(WS, 1);
end

if nargin==3
    if strcmpi(modeStr, 'round')
        input = round(input);
    else
        warning(['Undefined function parameter - ' modeStr]);
        return
    end
end

%% Filtering
if ~(X==1 || Y==1)
    for t = 1:X
        if counter > WS
            counter = 1;
        end
        % Angles transform from [-180; 180] plain into [0; 360] / [0; 360 plain]
        if t > 1
            for i = 1:Y
                if input(t, i) - input(t-1, i) > 180
                    input(t, i) = input(t, i) - 360;
                elseif input(t, i) - input(t-1, i) < -180
                    input(t, i) = input(t, i) + 360;
                end
            end
        end
        
        % Filling the buffer
        fifo_vector(counter, :) = input(t, :);
        for i = 1:length(fifo_vector(1, :))
            for j = 1:length(fifo_vector(:, 1))
                if fifo_vector(j, i) == 0
                    break;
                end
                fifoSize = j;
            end
        end
        output(t, :) = mean(fifo_vector(1:fifoSize, :), 1);
        
        % Inverse transform of angles
        for i = 1:Y
            if output(t, i) > 180
                output(t, i) = -360 + output(t, i);
            elseif output(t, i) < -180
                output(t, i) = 360 + output(t, i);
            end
        end
        
        % Increase counter
        counter = counter + 1;
    end
else
    for t = 1:X
        if counter > WS
            counter = 1;
        end
        
        % Angles transform from [-180; 180] plain into [0; 360] / [0; 360 plain]
        if t > 1
            if input(t) - input(t-1) > 180
                input(t) = input(t) - 360;
            elseif input(t) - input(t-1) < -180
                input(t) = input(t) + 360;
            end
        end
        
        % Filling the buffer
        fifo_vector(counter) = input(t);
        for i = 1:length(fifo_vector)
            if fifo_vector(i) == 0
                break;
            end
            fifoSize = i;
        end
        output(t) = mean(fifo_vector(1:fifoSize));
        
        % Inverse transform of angles
        if output(t) > 180
            output(t) = -360 + output(t);
        elseif output(t) < -180
            output(t) = 360 + output(t);
        end
        
        % Increase counter
        counter = counter + 1;
    end
end
end