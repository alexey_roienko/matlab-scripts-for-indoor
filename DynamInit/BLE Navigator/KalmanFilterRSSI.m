classdef KalmanFilterRSSI < handle
    %KALMANFILTERRSSI Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = public)
        beacNumb
        Q; 
        R;
        P_init;
        X_many;
        timeout1;
        timeout2;
    end
    
    properties (Access = private)
        H = [1 0];
        P; 
        P_many;
        timer;
        X;
    end
    
    methods  (Access = public)
        function obj = KalmanFilterRSSI(varargin)
            for i = 1:2:nargin
                if  strcmpi(varargin{i}, 'P_init'), obj.P_init = varargin{i+1};
                elseif strcmpi(varargin{i}, 'Q'), obj.Q = varargin{i+1};
                elseif strcmpi(varargin{i}, 'R'), obj.R = varargin{i+1};
                elseif strcmpi(varargin{i}, 'beacNumb'), obj.beacNumb = varargin{i+1};
                elseif strcmpi(varargin{i}, 'timeout1'), obj.timeout1 = varargin{i+1}; 
                elseif strcmpi(varargin{i}, 'timeout2'), obj.timeout2 = varargin{i+1};
                else error('Invalid argument');
                end
            end;
                
                obj.X_many = zeros( 2, obj.beacNumb);
                obj.P_many = zeros( 4, obj.beacNumb);
                obj.timer = zeros( 1, obj.beacNumb);
         end
        
        function output = FilterMany(obj, dt, Z)
            %% TIMEOUT IMPLEMENTATION
            for i = 1 : length(Z)                       % iterate over all beacons
                if Z(i) == 0                            % if the measurement is not available
                    obj.timer(i) = obj.timer(i) + dt;   % add dt to timer, who accumulates time from the last reading
                else
                    obj.timer(i) = 0;                   % if measurement came, then reset the timer
                end
            end
                
            for i=1 : length(Z)
                if obj.X_many(1,i) ~= 0 && sum(obj.P_many(:, i)) ~= 0 && obj.timer(i) <= obj.timeout1 % The evaluation was made at least once (default)
                    obj.X = obj.X_many(:,i);  % Getting state vector from buffer for all beacons
                    obj.P(1,1) = obj.P_many(1,i);
                    obj.P(1,2) = obj.P_many(2,i);
                    obj.P(2,1) = obj.P_many(3,i);
                    obj.P(2,2) = obj.P_many(4,i);

                    obj.FilterOne(dt, Z(i));   % Put state vector to buffer
                    obj.X_many(:,i) = obj.X;
                    obj.P_many(1,i) = obj.P(1,1);
                    obj.P_many(2,i) = obj.P(1,2);
                    obj.P_many(3,i) = obj.P(2,1);
                    obj.P_many(4,i) = obj.P(2,2);
                elseif Z(i) ~= 0 && obj.X_many(1,i) == 0 && sum(obj.P_many(:, i)) == 0 % First measurement appeared 
                    obj.X_many(:,i) = [Z(i) 0]';                                          
                    obj.P_many(1,i) = obj.P_init(1,1);
                    obj.P_many(2,i) = obj.P_init(1,2);
                    obj.P_many(3,i) = obj.P_init(2,1);
                    obj.P_many(4,i) = obj.P_init(2,2);
                elseif obj.timer(i) > obj.timeout2
                    obj.X_many(:,i) = [0 0]';
                    obj.P_many(:,i) = [0 0 0 0]';
                end           
           end
            output = obj.X_many(1,:);           
        end       
        
        
        function FilterOne(obj, dt, Z)
        % Predition stage 
        A = [1 dt; 0 1];

            % Predict new coord
            obj.X = A * obj.X;
            obj.P = A * obj.P * A' + obj.Q;
            
            % Correction stage
            if Z ~= 0
                K = obj.P * obj.H' / (obj.H * obj.P * obj.H' + obj.R);
            else
                K = [0;0];
            end
            
            obj.X = obj.X + K * (Z - obj.H * obj.X);
            obj.P = (eye(2) - K * obj.H) * obj.P; 
        end
    end
end

