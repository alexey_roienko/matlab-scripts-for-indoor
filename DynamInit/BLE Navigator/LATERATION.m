classdef LATERATION   
    methods (Access = public, Static)
                
        %% Function provides beacon numbers with maximal RSSI values for current timestamp.        
        %  INPUT:
        %  rssiValues - vector, 1xbN, RSSI values, possibly at the output
        %               of the RSSI filter, dBm (bN - beacons number);
        %  posBeaconN - int, number of beacons used for tri/multilateration;
        %  debug      - boolean, if TRUE then some additional info will
        %               be printed in the Command Window
        %  OUTPUT:
        %  output     - vector, 1 x posBeaconN, contains a struct with 
        %               numbers of beacons with maximal RSSI and its RSSI values
        %  result     - boolean, if TRUE then beacons have successfully
        %               been found; FALSE - if the number of active beacons 
        %               is less then 'posBeaconN'.
        function [output, result] = fGetTripletStandard(rssiValues, posBeaconN, debug)            
            
            if nnz(rssiValues) < posBeaconN
                output = struct();
                result = false;
                warning('Error! Not enough available RSSI from beacons!');
                return;
            else
                infValue = 1e10;
                tempRssiValues = rssiValues;
                tempRssiValues(tempRssiValues == 0) = -infValue;
                [~, B] = sort(tempRssiValues, 'descend');
                
                output.beacNO = B(1:posBeaconN);
                output.RSSI = rssiValues(output.beacNO);
                result = true;
                if debug
                    disp(['fGetTripletStandard: selected beacons ' int2str(output.beacNO)]);
                end
            end
        end
        
        
        %% Function provides beacon numbers with RSSI values for current 
        %  timestamp. Adaptive algorithm is used. It takes into account 
        %  distances from a beacon with maximal RSSI to other becaons and
        %  checks angles of a triangle built of the selected beacons.
        %  INPUT:
        %  rssiValues - vector, 1xbN, RSSI values, possibly at the output
        %               of the RSSI filter, dBm (bN - beacons number);
        %  INI        - reference to the project INI-file structure;
        %  debug      - boolean, if TRUE then some additional info will
        %               be printed in the Command Window
        %  beacCoord  - array of structs, 1xbN; each structure contains 
        %               .x, .y and .z fields providing position coordinates
        %               of each beacon;
        %  varargin:  - additional arguments; applied for test LOGGING only;
        %   {1}       - cell array, 1xbN, contains info about beacon IDs
        %               in the form of MajorMinor;
        %   {2}       - double, contains info about current timestamp;
        %   {3}       - fileID, reference to the test file 1;
        %   {4}       - fileID, reference to the test file 2;
        %  OUTPUT:
        %  output     - vector, 1 x posBeaconN, contains a struct with 
        %               numbers of beacons with maximal RSSI and its RSSI values
        %  result     - boolean, if TRUE then beacons have successfully
        %               been found; FALSE - if the number of active beacons 
        %               is less then 'posBeaconN'.
        function [output, result] = fGetTripletsAdvanced(rssiValues, INI, debug, ...
                                                         beacCoord, varargin)
                 
             posBeaconN = INI.BLE.lateratBeacNO;
             infValue = 1e10;
                       
             if nnz(rssiValues) < posBeaconN
                 output = struct();
                 result = false;
                 warning('Error! Not enough available RSSI from beacons!');
                 return;
             else
                % Logging
                if ~isempty(varargin)
                    beaconsID = varargin{1};
                    timestamp = varargin{2};
                    fID_T4 = varargin{3};
                    fID_T5 = varargin{4};                    
                end
                    
                tempRssiValues = rssiValues;
                tempRssiValues(tempRssiValues == 0) = -infValue;
                [~, posMax] = max(tempRssiValues);

                rssiMod = tempRssiValues;
                d = zeros(1, length(rssiMod));
                for i = 1:length(tempRssiValues)
                    if (i ~= posMax) && (tempRssiValues(i) ~= -infValue)
                        d(i) = sqrt((beacCoord{1,i}.x - beacCoord{1,posMax}.x)^2 + ...
                                    (beacCoord{1,i}.y - beacCoord{1,posMax}.y)^2 + ...
                                    (beacCoord{1,i}.z - beacCoord{1,posMax}.z)^2);
                        rssiMod(i) = d(i)^(0.2) * tempRssiValues(i);
                    end
                end
                
                if INI.BLE_testfiles.write_CheckTriplets
                    % write modified RSSI
                    fprintf(fID_T4, '%.3f\t', timestamp);
                    for i = 1:length(rssiMod)
                        fprintf(fID_T4, '%.3f\t', rssiMod(i));
                    end
                    fprintf(fID_T4, '\n\t\t');
                    % Write distances
                    for i = 1:length(d)
                        fprintf(fID_T4, '%.3f\t', d(i));
                    end
                    fprintf(fID_T4, '\n');
                end
                
                [~, B] = sort(rssiMod, 'descend');
                selectedBeacons = B(1:posBeaconN);
                
                k = posBeaconN + 1;
                
                
                %% Procedure for checking beacon triangle angles
                anglesSelected = false;
                while ~anglesSelected
                    if debug
                        disp(['fGetTripletAdvanced: selected beacons ' int2str(selectedBeacons)]);
                    end

                    if LATERATION.isAngleCorrect3D(INI.BLE_filters.anglesTH, ...
                                                 beacCoord{1, selectedBeacons(1)},...
                                                 beacCoord{1, selectedBeacons(2)},...
                                                 beacCoord{1, selectedBeacons(3)},...
                                                 debug)                                 
                        
                        output.beacNO = selectedBeacons;
                        output.RSSI = rssiValues(selectedBeacons);
                        
                        anglesSelected = true;
                        result = true;
                    else
                        %% At least one of triangle angles is incorrect                        
                        if INI.BLE_testfiles.write_CheckTriplets
                            fprintf(fID_T5, '%.3f\t', timestamp);
                            for bN = 1:posBeaconN
                                majMin = beaconsID{selectedBeacons(bN)};
                                fprintf(fID_T5, '%s\t', majMin(6:end));
                            end
                            fprintf(fID_T5, '\n');                        
                        end
                        
                        if (rssiMod(B(k)) ~= -infValue)
                            % if there is one more combination,...
                            selectedBeacons(posBeaconN) = B(k);
                            if debug
                                disp('Changing triplet...');
                            end
                            k = k + 1;
                            
                            anglesSelected = false;
                            result = false;
                        else
                            % we have already checked all available
                            % combinations, and if nothing is OK, return to
                            % the initial one
                            output.beacNO = B(1:posBeaconN);
                            output.RSSI = rssiValues(B(1:posBeaconN));
                            
                            anglesSelected = true;
                            result = true;
                        end
                    end
                end
            end            
        end
        
        
        function [output, result] = fGetNDist(readings, numb, indicator)
            if nnz(readings) > numb
                tempArray = readings;
                tempArray(tempArray <= 0.3 ) = 999;
                [A, B] =sort(tempArray, 'ascend');
                output.DIST = A(1:numb);
                output.beacNO = B(1:numb);
                result = true;
                if indicator
                    disp(['Selected beacons are : ' int2str(output.beacNO)]);
                end
            else
                output = NaN;
                result = false;
            end            
        end
        
        
        function output = fRssi2Dist(beacons)
        	beaconsN = length(beacons);
            output   = zeros(1, beaconsN);
            for bN = 1:beaconsN
%                 if beacons{bN}.RSSI < beacons{bN}.txpower
%                     output(bN) = 1;
%                 else
%                     output(bN) = 10^(0.1*(beacons{bN}.txpower - beacons{bN}.RSSI) / beacons{bN}.damp);
%                 end
                output(bN) = 10^(0.1*(beacons{bN}.txpower - beacons{bN}.RSSI) / beacons{bN}.damp);
                if output(bN) < 1
                    output(bN) = 1;
                end
            end
        end
       
       
       function estRXCoords = fTrilateration(beacons, DistMatrix, userH)
            distN = length(DistMatrix);
            beaconsN = length(beacons);

            if (distN ~= beaconsN)
                error('Number of reference points and distances are different!');

            elseif (distN ~= 3)||(beaconsN ~= 3)
                error('Number of reference points and/or distances differs from 3!');
            else
                RefPoints = zeros(3,beaconsN);
                for bN=1:beaconsN
                    RefPoints(1,bN) = beacons{bN}.x;
                    RefPoints(2,bN) = beacons{bN}.y;
                    RefPoints(3,bN) = 0;%beacons{bN}.z - userH;
                end

                %% Step 1 - Calculate A, B, C, Xs and Ys coefficients for trilateration formulas
                A = RefPoints(1,1)^2 + RefPoints(2,1)^2 - DistMatrix(1)^2;
                B = RefPoints(1,2)^2 + RefPoints(2,2)^2 - DistMatrix(2)^2;
                C = RefPoints(1,3)^2 + RefPoints(2,3)^2 - DistMatrix(3)^2;

                Xs_array(1) = RefPoints(1,1) - RefPoints(1,3);
                Xs_array(2) = RefPoints(1,2) - RefPoints(1,1);
                Xs_array(3) = RefPoints(1,3) - RefPoints(1,2);

                Ys_array(1) = RefPoints(2,1) - RefPoints(2,3);
                Ys_array(2) = RefPoints(2,2) - RefPoints(2,1);
                Ys_array(3) = RefPoints(2,3) - RefPoints(2,2);

                %% Step 2 - Calculate Receiver X ...
                num(1)   = A*Ys_array(3) + B*Ys_array(1) + C*Ys_array(2);
                denom(1) = 2*(RefPoints(1,1)*Ys_array(3) + ...
                              RefPoints(1,2)*Ys_array(1) + ...
                              RefPoints(1,3)*Ys_array(2));
                estRXCoords(1) = num(1)/denom(1);

                %% ... and Y coordinates
                num(2)   = A*Xs_array(3) + B*Xs_array(1) + C*Xs_array(2);
                denom(2) = 2*(RefPoints(2,1)*Xs_array(3) + ...
                              RefPoints(2,2)*Xs_array(1) + ...
                              RefPoints(2,3)*Xs_array(2));
                estRXCoords(2) = num(2)/denom(2);     
            end
        end 
        
        
        function [N1, N2] = fMultilateration(beacons, DistMatrix, userH)
            beaconsN = length(beacons);
            distN = length(DistMatrix);

            if (distN ~= beaconsN)
                error('Number of reference points and distances are different');
            end

            A = zeros(beaconsN, 4);
            b = zeros(beaconsN, 1);
            for bN=1:beaconsN
                x = beacons{bN}.x; 
                y = beacons{bN}.y;
                z = beacons{bN}.z - userH;

                A(bN, :) = [1 -2*x  -2*y  -2*z]; 
                b(bN, 1) = DistMatrix(bN)^2-x^2-y^2-z^2;
            end

            if beaconsN == 3
                Xp = A\b;  % Gaussian elimination

                xp = Xp(2:4, :);
                Z = null(A,'r');
                z = Z(2:4, :);
                if rank(A)==3
                    %Polynom coeff.
                    a2 = z(1)^2 + z(2)^2 + z(3)^2 ;
                    a1 = 2*(z(1)*xp(1) + z(2)*xp(2) + z(3)*xp(3))-Z(1);
                    a0 = xp(1)^2 +  xp(2)^2+  xp(3)^2-Xp(1);
                    p = [a2 a1 a0];
                    t = roots(p);

                    %Solutions
                    N1 = Xp + t(1)*Z;
                    N2 = Xp + t(2)*Z;
                end

            elseif beaconsN > 3
                % Solution without Weights Matrix
                Xpdw = pinv(A)*b; 
                % the matrix  inv(A'*A)*A' or inv(A'*C*A)*A'*C or pinv(A)
                % depends only on the reference points
                % it could be computed only once
                N1 = Xpdw;
                N2 = N1;
            end

            N1 = real(N1(2:4, 1));
            N2 = real(N2(2:4, 1));
        end
    end  
    
    methods (Access = private, Static)
        
        function angles = getAngleByCoord2D( A , B , C)
            vectorAB = [B(1)-A(1)  B(2)-A(2)];
            vectorAC = [C(1)-A(1)  C(2)-A(2)];
            vectorCB = [B(1)-C(1)  B(2)-C(2)];
            
            AB_AC = acosd((vectorAB(1) * vectorAC(1) + vectorAB(2) * vectorAC(2)) / ...
            (sqrt(vectorAB(1)^2 + vectorAB(2)^2) * sqrt(vectorAC(1)^2 + vectorAC(2)^2)));

            AB_CB = acosd((vectorAB(1) * vectorCB(1) + vectorAB(2) * vectorCB(2)) / ...
            (sqrt(vectorAB(1)^2 + vectorAB(2)^2) * sqrt(vectorCB(1)^2 + vectorCB(2)^2)));
        
        	AC_CB = acosd((vectorAC(1) * vectorCB(1) + vectorAC(2) * vectorCB(2)) / ...
            (sqrt(vectorAC(1)^2 + vectorAC(2)^2) * sqrt(vectorCB(1)^2 + vectorCB(2)^2)));

            angles = [AB_AC AB_CB 180-AC_CB];

%             str = sprintf('Angles: %.1f, %.1f, %.1f\n', angles);
%             disp(str);
        end
        
        function angles = getAngleByCoord3D(A, B, C)
            vectAB = [B(1)-A(1)  B(2)-A(2)  B(3)-A(3)];
            vectAC = [C(1)-A(1)  C(2)-A(2)  C(3)-A(3)];
            vectCB = [B(1)-C(1)  B(2)-C(2)  B(3)-C(3)];
            
            AB_AC = acosd((vectAB(1)*vectAC(1) + vectAB(2)*vectAC(2) + vectAB(3)*vectAC(3)) / ...
            (sqrt(vectAB(1)^2+vectAB(2)^2+vectAB(3)^2) * sqrt(vectAC(1)^2+vectAC(2)^2+vectAC(3)^2)));

            AB_CB = acosd((vectAB(1)*vectCB(1) + vectAB(2)*vectCB(2) + vectAB(3)*vectCB(3)) / ...
            (sqrt(vectAB(1)^2+vectAB(2)^2+vectAB(3)^2) * sqrt(vectCB(1)^2+vectCB(2)^2+vectCB(3)^2)));
        
        	AC_CB = acosd((vectAC(1)*vectCB(1) + vectAC(2)*vectCB(2) + vectAC(3)*vectCB(3)) / ...
            (sqrt(vectAC(1)^2+vectAC(2)^2+vectAC(3)^2) * sqrt(vectCB(1)^2+vectCB(2)^2+vectCB(3)^2)));

            angles = [AB_AC AB_CB 180-AC_CB];

%             str = sprintf('Angles: %.1f, %.1f, %.1f\n', angles);
%             disp(str);
        end
        
        function output = isAngleCorrect2D(anglesTH, beacA , beacB , beacC, debug)
            angles = LATERATION.getAngleByCoord2D([beacA.x beacA.y], ...
                [beacB.x beacB.y], [beacC.x beacC.y]);
            
            if max(angles) > anglesTH
                output = false;
                str2 = 'isAngleCorrect = FALSE';
            else
                output = true;
                str2 = 'isAngleCorrect = TRUE';
            end
            
            if debug
                str1 = ['Current angles are ' num2str(angles)];
                disp(str1);
                disp(str2);
            end
        end
        
        function output = isAngleCorrect3D(anglesTH, beacA , beacB , beacC, debug)
            angles = LATERATION.getAngleByCoord3D([beacA.x beacA.y beacA.z], ...
                [beacB.x beacB.y beacB.z], [beacC.x beacC.y beacC.z]);
            
            if max(angles) > anglesTH
                output = false;
                str2 = 'isAngleCorrect = FALSE';
            else
                output = true;
                str2 = 'isAngleCorrect = TRUE';
            end
            
            if debug
                str1 = ['Current angles are ' num2str(angles)];
                disp(str1);
                disp(str2);
            end
        end
    end
end