classdef DistanceLimiter < handle
    % DISTANCECORRECTOR Summary of this class goes here    
    %   Detailed explanation goes here
    properties
        prevX;      % Xtemp = [x y] Previous state
        prevT;      % t_temp - previous time
        timeLim     % X coord for limiter interpolation
        distLim     % Y coord for limiter interpolation
    end
    
    methods
        function obj = DistanceLimiter(varargin)
            %% Class constructor
            for i = 1:2:nargin
                if strcmpi(varargin{i}, 'timeLim')
                    obj.timeLim = varargin{i+1};
                elseif strcmpi(varargin{i}, 'distLim')
                    obj.distLim = varargin{i+1};
                else
                    error('Invalid argument');
                end
            end
        end
        
        
        %% Main method of the class
        %  Application of limiter to distance between previous point and
        %  current
        function limitedX = applyLimiter(obj, t, X, debug)            
            if isempty(obj.prevX)
                obj.prevX = X;
                limitedX = X;
                if debug
                    disp('The first point for Limiter.');
                    disp(['Coords are: X = ', num2str(X(1,1)), ', Y = ' , num2str(X(2,1))]);
                end
            else                
                dt   = t - obj.prevT;
                dist = sqrt((obj.prevX(1,1) - X(1,1))^2 + (obj.prevX(2,1) - X(2,1))^2);
                
                %lim  = interp1(obj.timeLim, obj.distLim, dt);
                lim = obj.findNearestDist(dt);
                
                if dist > lim
                    if debug
                        disp('Limitation should be done.');
                        disp(['Initial coords: X = ', num2str(X(1,1)), ', Y = ' , num2str(X(2,1))]);
                    end
                    
                    dx = X(1,1) - obj.prevX(1,1);
                    dy = X(2,1) - obj.prevX(2,1);
                    
                    temp = zeros(size(X));
                    
                    dx_new = dx * lim / dist;
                    dy_new = dy * lim / dist;
                    
                    temp(1,1) = obj.prevX(1,1) + dx_new;
                    temp(2,1) = obj.prevX(2,1) + dy_new;
                    
%                     if debug
%                         disp('Debug info:');
%                         str = sprintf('%.4f %.4f %.4f %.4f %.4f %.4f %.4f', ...
%                             dt, dx, dy, dist, lim, dx_new, dy_new);
%                         disp(str);
%                     end
                    
                    if debug
                        disp(['Corrected coords: X = ', num2str(temp(1,1)), ...
                              ', Y = ', num2str(temp(2,1))]);
                    end
                else
                    temp = X;
                    if debug
                        disp('Limitation is not necessary.');
                        disp(['Coords are: X = ', num2str(temp(1,1)), ...
                              ', Y = ', num2str(temp(2,1))]);
                    end
                end
                obj.prevX = temp;
                limitedX = temp;
            end            
            obj.prevT = t; 
        end
    end
    
    
    methods (Access = private)
        %% Function interpolates the table of the object, i.e. limDist = f(t)
        %  for the dt set by user
        % INPUT: 
        % dt      - time interval between current and previous sample
        % OUTPUT:
        % limDist - interpolated value of distance for limiter
        function limDist = findNearestDist(obj, dt)
            % find the boundaries for current dt in the table
            i = 1;
            while (i <= length(obj.timeLim)) && (obj.timeLim(i) < dt)
                i = i + 1;
            end
            
            if i == 1
                % if dt is less than the first table point,...
                coefK1 = obj.distLim(i) / obj.timeLim(i);
                limDist = coefK1 * dt;
            elseif i > length(obj.timeLim)
                % if dt is greater than the last table point,...
                coefK1 = (obj.distLim(i-1) - obj.distLim(i-2))/(obj.timeLim(i-1) - obj.timeLim(i-2));
                limDist = coefK1 * (dt - obj.timeLim(i-1)) + obj.distLim(i-1);
            else
                % ordinary case
                coefK1 = (obj.distLim(i) - obj.distLim(i-1))/(obj.timeLim(i) - obj.timeLim(i-1));
                limDist = coefK1 * (dt - obj.timeLim(i-1)) + obj.distLim(i-1);
            end
        end
    end
end


