classdef DIPlotter < handle
    methods (Static)
        function plotSeededParticles(particlesPositions, pixelSize)
            xCoords = particlesPositions(:,1)/pixelSize;
            yCoords = particlesPositions(:,2)/pixelSize;
            plotCoordInit = plot(xCoords, yCoords, ...
                'LineStyle', 'none', 'Marker', 'o', 'markersize', 5 ,...
                'MarkerFaceColor', 'y', 'MarkerEdgeColor', 'k');
            title('Seeding');
            pause();
            delete(plotCoordInit);
        end
        
        
        function varargout = plotTrajectories(posCur, posPrev, pixelSize, shouldDelete)
            plotParticle = zeros(length(posCur), 1);
            posCurPS  = posCur / pixelSize;
            posPrevPS = posPrev / pixelSize;
            for i = 1:size(posCur, 1)
                plotParticle(i) = plot([posPrevPS(i,1) posCurPS(i,1)], [posPrevPS(i,2) posCurPS(i,2)],...
                    'LineStyle', '-', 'Marker', 'o', 'Color', 'b', 'MarkerSize', 5 ,...
                    'MarkerFaceColor', 'b' , 'MarkerEdgeColor', 'k');
            end
            title('Model of displacement');
            if shouldDelete
                pause();
                delete(plotParticle);
            else
                varargout{1} = plotParticle;
            end
        end
        
        
        function plotSurvivedParticles(posCur, posPrev, weights, pixelSize)
            trajectoriesRef = DIPlotter.plotTrajectories (posCur, posPrev, pixelSize, false);
            posCurPS = posCur / pixelSize;
            plotDied = zeros(length(weights), 1);
            plotDiedEmpty = true;
            for pN=1:length(weights)
                if 0 == weights(pN)
%                     plotDied(pN) = scatter(posCurPS(pN,1), posCurPS(pN,2), 14, 'o', 'filled', ...
%                         'MarkerFaceColor', 'r', 'MarkerEdgeColor' , 'r');
                    plotDied(pN) = plot(posCurPS(pN,1), posCurPS(pN,2), 'Marker', 'o', ...
                        'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'r', 'MarkerSize', 5);
                    plotDiedEmpty = false;
                end
            end
            title('Survived and Died Particles');
            pause();
            delete(trajectoriesRef);
            if ~plotDiedEmpty
                delete(plotDied);
            end
        end
       
        
        function plotProbableArea(center, radius, pixelSize)
            THETA = linspace(0,2 * pi, 1000);
            RHO   = ones(1,1000) * radius / pixelSize;
            [X,Y] = pol2cart(THETA, RHO);
            X = X + center(1) / pixelSize;
            Y = Y + center(2) / pixelSize;
            circleArea = area(X, Y, 'FaceColor', 'r', 'EdgeColor', 'r', ...
                              'FaceAlpha',0,'EdgeAlpha',.3);
            pause();
            delete(circleArea);
        end
        
        
         function plotFinalPosition(x, y, shouldDelete, pixelSize)
            xCoords = x / pixelSize;
            yCoords = y / pixelSize;
            plotFinalPos = plot(xCoords, yCoords,...
                'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', 10 ,...
                'MarkerFaceColor', 'c', 'MarkerEdgeColor', 'k');
            title('At the end of Dynamic Initialization');
            pause();
            if shouldDelete
                delete(plotFinalPos);
            end
        end
    end
end

