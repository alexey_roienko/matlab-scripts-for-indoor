classdef DynamicInitializer < handle
    %DYNAMICINITIALIZER Class who accumulates all methods for dynamic
    %initialization
    
    properties
        map;             % map
        mask;            % binary mask
        INI;             % reference to config settings
        maskTable;       % mask file
        pixelSize;       % pixel size
        sensorCoords;    % positions determined by the Sensor part of SDK        
        bleCoords;       % positions determined by the BLE part of SDK
        sensorAbsTS;     % system value when the first Accelerometer value was received, msec.
        bleAbsTS;        % system value when the first BLE package was received, msec.
    end
    
    properties (Access = private)
        coords;          % array of particles current coordinates
        coordsPrev;      % array of particles previous coordinates
        weights;         % particles weights
        currParticlesN;  % current number of particles
        meshSteps;       % map mesh steps
        sensorDeltas;    % coords increments calculated using sensorUserCoords
    end
    
    methods (Access = public)
        function obj = DynamicInitializer(varargin)
            %% Object initializer
            for i = 1:2:nargin
                if strcmpi(varargin{i}, 'map'), obj.map = varargin{i+1};
                elseif strcmpi(varargin{i}, 'mask'), obj.mask = varargin{i+1};
                elseif strcmpi(varargin{i}, 'INI'), obj.INI = varargin{i+1};
                elseif strcmpi(varargin{i}, 'maskTable'), obj.maskTable = varargin{i+1};
                elseif strcmpi(varargin{i}, 'pixelSize'), obj.pixelSize = varargin{i+1};
                elseif strcmpi(varargin{i}, 'sensorCoords'), obj.sensorCoords = varargin{i+1};
                elseif strcmpi(varargin{i}, 'sensorAbsTS'), obj.sensorAbsTS = varargin{i+1};
                elseif strcmpi(varargin{i}, 'bleCoords'), obj.bleCoords = varargin{i+1};
                elseif strcmpi(varargin{i}, 'bleAbsTS'), obj.bleAbsTS = varargin{i+1};
                else
                    error('Invalid argument');
                end
            end
            obj.meshSteps = [obj.INI.general.mesh2StepX obj.INI.general.mesh2StepY];
            obj.prepareInputData();
        end
        
                
        %% MAIN function of the class
        function [t, pos] = getPosition(obj)
            %% STEP 1: Seeding of the initial generation of particles
            obj.getInitialGeneration(obj.INI.DI_debug.printDebugInfo);
            
            realInitParticlesN = sum(obj.weights);
            
            if obj.INI.DI_debug.showInitialSeeding
                DIPlotter.plotSeededParticles(obj.coordsPrev, obj.pixelSize);
            end
            if obj.INI.DI_testfiles.writeTestFiles
                fidO2 = fopen('test files/tDIafterSeeding.dat', 'w');
                fprintf(fidO2, 'ParticleNo [X Y]\n');
                for n=1:size(obj.coordsPrev, 1)
                    fprintf(fidO2, '%d \t%.8f \t%.8f\n', n, obj.coordsPrev(n,:));
                end
                fclose(fidO2);
                
                fidO3 = fopen('test files/tDIparticles.dat', 'w');
                fprintf(fidO2, 'ParticleNo [X Y]');
            end

            
            %% MAIN LOOP
            for t = 2:size(obj.sensorDeltas, 1)
                %% STEP 1: User position is changed according to the accumulated displacement
                obj.incrementParticlesCoords(obj.sensorDeltas(t,2:3), obj.INI.PF.modelSTD);
                    
                if obj.INI.DI_debug.showDisplacements
                    DIPlotter.plotTrajectories(obj.coords, obj.coordsPrev, obj.pixelSize, true);
                end
                
                    
                %% STEP 2: Remove particles that have not survived
                obj.checkParticles();

                if obj.INI.DI_debug.showSurvivedParticles
                    DIPlotter.plotSurvivedParticles(obj.coords, obj.coordsPrev, obj.weights, obj.pixelSize);
                end
                    
                obj.renewParticleGen();
                    
                if obj.INI.DI_debug.plotCircle
                    [center, radius]= obj.calcCircleParameters(realInitParticlesN);
                    DIPlotter.plotProbableArea(center, radius, obj.pixelSize);
                end
                if obj.INI.DI_testfiles.writeTestFiles
                    fprintf(fidO3,'\nTimestamp = %.8f\n', obj.sensorDeltas(t,1));
                    for n=1:size(obj.coordsPrev, 1)
                        fprintf(fidO3, '%d \t%.8f \t%.8f\n', n, obj.coordsPrev(n,:));
                    end
                end
                
                
                %% STEP 3: Escape point
                %  If the initialization time is over or the number of
                %  particles remained is less than 5% from initial number
                 if (length(obj.coordsPrev) < 0.05 * realInitParticlesN) || ...
                     (obj.sensorDeltas(t,1) > obj.INI.DI.timeForInit)

                    pos = obj.getParticleClustersPositions();

                    DIPlotter.plotFinalPosition(pos(:,1), pos(:,2), true, obj.pixelSize);

                    if ~isempty(obj.bleCoords)
                        %% STEP 6: Selecting one point over entire collection.
                        % Here we have a set of metrics determined in the
                        % method 'getFinalPositions()'. From this
                        % set we select the only position which is the closest to the 
                        % beacon readings.

                        % Selecting the beacon readings right before the
                        % 'obj.timeSamples(t,1)', which corresponds to the time of
                        % dynamic initializaion final
                        for m = 1:size(obj.bleCoords, 1)
                            if (obj.bleCoords(m,1) > obj.sensorDeltas(t,1)) && ...
                               (obj.bleCoords(m-1,1) < obj.sensorDeltas(t,1))

                                refBeaconPos = obj.bleCoords(m-1,2:3);
                                break;
                            end
                        end

                        %% Select the final DI position
                        pos = obj.getFinalPosition(pos, refBeaconPos);

                        DIPlotter.plotFinalPosition(pos(1,1), pos(1,2), false, obj.pixelSize);      
                    end
                    break;
                end
            end
            
            if obj.INI.DI_testfiles.writeTestFiles
                fclose(fidO3);
            end
        end        
    end
    
    
    
    methods(Access = private)
        
        %% Function calculates deltas for sensorUserCoords input data and 
        %  timestamps for non-zero delta values.
        function prepareInputData(obj)
            if isempty(obj.bleCoords)
                %% Test case - seeding all over the building map
                % Timestamps are calculated starting from the beginning
                tStartSensor = 2;                
            else
                %% Test case - seeding around the BLE reading
                % Find the first sensor timestamp after the first ble timestamp
                % Here we suppose that Acceleration (as well as Angles) data usually
                % start arriving much earlier then BLE data
                tStartSensor = 1;
                diff = (obj.bleAbsTS - obj.sensorAbsTS)/10^3;
                while obj.sensorCoords(tStartSensor,1) < (obj.bleCoords(1,1)+diff)
                    tStartSensor = tStartSensor + 1;
                end
                if tStartSensor == 1
                    tStartSensor = 2;
                end
                % Timestamps are calculated starting from the 'tStartSensor'
            end
                        
            tSensor = 1;                
            deltas  = zeros(size(obj.sensorCoords));
            for n = tStartSensor : size(obj.sensorCoords, 1)
                if (abs(obj.sensorCoords(n,2) - obj.sensorCoords(n-1,2)) > 1e-10) || ...
                   (abs(obj.sensorCoords(n,3) - obj.sensorCoords(n-1,3)) > 1e-10)
                    tSensor = tSensor + 1;
                    deltas(tSensor, 1)   = obj.sensorCoords(n,1);
                    deltas(tSensor, 2:3) = obj.sensorCoords(n,2:3) - obj.sensorCoords(n-1,2:3);
                end
            end
            
            if obj.INI.DI_testfiles.writeTestFiles
                fidO1 = fopen('test files/tDIsensorDeltas.dat', 'w');
                fprintf(fidO1, 'Timestamp [X Y]\n');
                for n=1:tSensor
                    fprintf(fidO1, '%.8f \t%.8f \t%.8f\n', deltas(n,:));
                end
                fclose(fidO1);
            end
            obj.sensorDeltas = deltas(1:tSensor, :);
        end
        
        
        %% Function seeds initial number of particles on the map.
        %  The center of seeding is defined by whether the parameter
        %  obj.beacons is set or not. If it is set, then the seeding center
        %  is at [beacons(1).x beacons(1).y]. Otherwise, the whole map will be seeded.
        function getInitialGeneration(obj, debug)
            %% STEP 1: Define the center of seeding zone
            if isempty(obj.bleCoords)
                % Define size of the mesh vertices for each axis
                mtSize = length(obj.maskTable);
                nx = floor(mtSize/obj.INI.general.mesh2Rows);
                ny = obj.INI.general.mesh2Rows;
                % Define minimal and maximal index values in maskTable
                lowerBoundary = [0 0];
                upperBoundary = [lowerBoundary(1) + (nx-1)*obj.INI.general.mesh2StepX ...
                                 lowerBoundary(2) + (ny-1)*obj.INI.general.mesh2StepY];
            else
                initCenter = obj.bleCoords(1, 2:3);
                seedRadius = obj.INI.DI.seedingRadius;
            end
            
            %% STEP 2: Seed particles
            if obj.INI.DI_debug.testMode
                posVariations1 = dlmread('DItestModeFiles/stage1seeding5000ptcls.dat', ' ');
                fIndex = 1;
            end
            
            pN = 1;
            while pN <= obj.INI.DI.initParticlesN
                if isempty(obj.bleCoords)
                    if obj.INI.DI_debug.testMode
                        coordPrev = posVariations1(fIndex,1:2).* upperBoundary;
                        fIndex = fIndex + 1;
                    else
                        coordPrev = rand(1,2).* upperBoundary;
                    end
                else
                    if obj.INI.DI_debug.testMode
                        coordPrev = initCenter + (posVariations1(fIndex,1:2) * 2 * seedRadius - seedRadius);
                        fIndex = fIndex + 1;
                    else
                        coordPrev = initCenter + (rand(1,2) * 2 * seedRadius - seedRadius);
                    end
                end
                
                if ~CORRECTOR.fIsNotOnTheMap(coordPrev, obj.meshSteps, obj.INI.general.mesh2Rows, length(obj.maskTable))
                    if ~CORRECTOR.fIsInTheBlackRegion(coordPrev, obj.meshSteps, obj.INI.general.mesh2Rows, obj.maskTable)
                        obj.coordsPrev(pN,:) = coordPrev;
                        obj.weights(pN,1) = 1;
                        pN = pN + 1;
                    elseif debug
                        disp('Seeding: Particle in the black region');
                    end
                elseif debug
                    disp('Seeding: Particle is outside the map')
                end
            end
            
            %% Define the final number of particles seeded
            obj.currParticlesN = length(obj.weights);
        end
        
        
        %% Function increments each particle coordinates.
        %  INPUT:
        %  incr - vector(2,1), X and Y increment values of a user, meters
        %  disp - model STD, defines the dispersion from the original X or
        %         Y value, meters
        function incrementParticlesCoords(obj, incr, modelStd)
            if obj.INI.DI_debug.testMode
                posVariations2 = dlmread('DItestModeFiles/stage2incrementing1000ptcls.dat', ' ');
            end
            
            obj.coords = zeros(obj.currParticlesN, 2);
            for i = 1:obj.currParticlesN                
                if obj.INI.DI_debug.testMode
                    % X coordinate
                    obj.coords(i,1) = obj.coordsPrev(i,1) + incr(1) + posVariations2(i,1)*modelStd;
                    % Y coordinate
                    obj.coords(i,2) = obj.coordsPrev(i,2) + incr(2) + posVariations2(i,2)*modelStd;
                else
                    % X coordinate
                    obj.coords(i,1) = obj.coordsPrev(i,1) + incr(1) + randn(1,1)*modelStd;
                    % Y coordinate
                    obj.coords(i,2) = obj.coordsPrev(i,2) + incr(2) + randn(1,1)*modelStd;
                end
            end
        end
        
        
        %% Function checks each particle for three conditions and remove those ones
        %  which are out of the map, or on the black mask region or crossing the wall
        function checkParticles(obj)
            % Check particles for being out of the map, on the black mask
            % region or crossing the wall. If one of this conditions is true, 
            % then particle weight is equal to 0.
            for i = 1:obj.currParticlesN
                % Check is on the map...
                if ~CORRECTOR.fIsNotOnTheMap(obj.coords(i,:), obj.meshSteps, ...
                                             obj.INI.general.mesh2Rows, length(obj.maskTable))
                    % Check is in the allowed region...
                    if ~CORRECTOR.fIsInTheBlackRegion(obj.coords(i,:), obj.meshSteps, ...
                                                      obj.INI.general.mesh2Rows, obj.maskTable)
                        % Check is wall crossed...
                        if ~CORRECTOR.fIsWallBeingCrossed2(obj.coordsPrev(i,:), obj.coords(i,:), ...
                                                           obj.meshSteps, obj.INI.general.mesh2Rows, obj.maskTable)
                            obj.weights(i) = 1;
                        else
                            obj.weights(i) = 0;
                        end
                    else
                        obj.weights(i) = 0;
                    end
                else
                    obj.weights(i) = 0;
                end
            end
        end
        
        
        %% Function creates new particles generation
        function renewParticleGen(obj)
            obj.coordsPrev = zeros(1,2);
            k = 1;
            for pN = 1:obj.currParticlesN
                if obj.weights(pN) == 1
                    obj.coordsPrev(k, :) = obj.coords(pN,:);
                    k = k + 1;
                end
            end            
            % Change the number of available particles and their weights
            obj.currParticlesN = k - 1;
            obj.weights = ones(obj.currParticlesN, 1);         
            obj.coords  = zeros(obj.currParticlesN, 2);
        end
        
        
        %% Function calculates the center and radius of undefined user position circle
        function [center, radius] = calcCircleParameters(obj, initParticlesN)
            center = [mean(obj.coordsPrev(:,1)) mean(obj.coordsPrev(:,2))];
            distMax = max((obj.coordsPrev(:,1) - center(1)).^2 + (obj.coordsPrev(:,2) - center(2)).^2);
            distMin = min((obj.coordsPrev(:,1) - center(1)).^2 + (obj.coordsPrev(:,2) - center(2)).^2);
            radius = (size(obj.coordsPrev, 1)/initParticlesN)^0.5 * (sqrt(distMax) - sqrt(distMin)) + sqrt(distMin);            
        end     
        
        
        function finalCoords = getParticleClustersPositions(obj)
            k = 1;
            superCounter = 0;
            while superCounter < (obj.currParticlesN ^2)                
                escapeCriterion = obj.currParticlesN;
                if obj.currParticlesN >=2
                    for i = 2:2:obj.currParticlesN
                        prevDistance = sqrt((obj.coordsPrev(i,1) - obj.coordsPrev(i-1, 1))^2 +...
                                            (obj.coordsPrev(i,2) - obj.coordsPrev(i-1, 2))^2);
                        % '2' means margin for averaging, i.e. 2 meters
                        if prevDistance < 2
                            obj.coordsPrev(k,:) = [(obj.coordsPrev(i,1) + obj.coordsPrev(i-1, 1))/2 ...
                                                   (obj.coordsPrev(i,2) + obj.coordsPrev(i-1, 2))/2];
                        else
                            obj.coordsPrev(k,:) = [obj.coordsPrev(i-1,1) obj.coordsPrev(i-1,2)];
                            k = k + 1;
                            obj.coordsPrev(k,:) = [obj.coordsPrev(i,1) obj.coordsPrev(i,2)];
                        end
                        k = k + 1;
                    end
                else
                    obj.coordsPrev(1,:) = [obj.coordsPrev(1, 1) obj.coordsPrev(1, 2)];
                    k = 2;
                end
                
                % Leaving only the averaged particles considering their number
                if rem(obj.currParticlesN, 2) == 0
                    obj.coordsPrev = obj.coordsPrev(1 : k-1,:);
                else
                    obj.coordsPrev = [obj.coordsPrev(1 : k-1,:);obj.coordsPrev(obj.currParticlesN,:)];
                end
                
                % for stability
                obj.coordsPrev = obj.coordsPrev(randperm(size(obj.coordsPrev,1)),:);
                %
                obj.currParticlesN = length(obj.coordsPrev(:,1));
                k = 1;
                
                if escapeCriterion ~= obj.currParticlesN
                    superCounter = 1;
                else
                    superCounter = superCounter + 1;
                end
            end
            finalCoords = obj.coordsPrev;
        end
        
        
        function output = getFinalPosition(~, DIPositions, refBeaconPos)
            [~, qw] = sortrows(sqrt(DIPositions(:,1) - refBeaconPos(1)).^2 + ...
                                   (DIPositions(:,2) - refBeaconPos(2)).^2);
            output = DIPositions(qw(1),:);
        end
    end
end

