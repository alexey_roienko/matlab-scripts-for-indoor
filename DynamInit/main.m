clc, clear, close all;

addpath('COMMON classes', 'COMMON classes\jsonlab', 'logs', 'map');
addpath('DYNAMIC INIT');

%% ========================= SETTINGS ============================ %
% Possible variants: BLE, Sensor, PF, DI
workingMode = 'DI';
% Date of the log-files
logDateTime = '2017-12-20_19-18-35';

% Read 'config.ini'
[INI, mapParams, beaconsParams, beaconsID, mapImage, mapMask] = ...
    READER.fReadAllSettingsBLE('config.ini');
% Rewrite parameter values, which correspond to meshes, in INI entity
READER.fCorrectMeshData(INI);

% Load maskTable files, greater mesh for Mesh Corrector
if INI.correctors.mapCorrection || INI.correctors.meshCorrection ...
      || INI.correctors.wallsCorrection
    maskTable1 = load([pwd INI.general.pathMasktable1]);
    maskTable2 = load([pwd INI.general.pathMasktable2]);
end


%% ========================= MAIN PART =========================== %
if strcmpi(workingMode, 'BLE')
    %% BLE Navigator section 
    addpath('BLE Navigator');
    main_BLE;

elseif strcmpi(workingMode, 'Sensor')
    %% Sensor Navigator section 
    addpath('Sensor Navigator');
    % With Activity Detector
%     main_Sensor_1;
    % With Felix's Step Detector through the 'isStationary'
%     main_Sensor_2;
    % With Felix's Step Detector as it is implemented in SDK'
    main_Sensor_3;

elseif strcmpi(workingMode, 'PF')    
    %% Particle Filter Navigator section
    addpath('BLE Navigator', 'Sensor Navigator', 'PF Navigator');
    
    % Run BLE Navigator
    main_BLE;
    
    % Run Sensor Navigator
    % SN should start from the third BLE estimated point
    % The 3rd point is due to the latency in SDK code
    INI.Sensor.initialX = bleUserCoords(INI.PF_debug.startSNindex, 2);
    INI.Sensor.initialY = bleUserCoords(INI.PF_debug.startSNindex, 3);
    main_Sensor_3;
    
    % Run PF Navigator
    main_Fusion;
    
elseif strcmpi(workingMode, 'DI')
    %% Particle Filter Navigator section
    addpath('BLE Navigator', 'Sensor Navigator', 'DYNAMIC INIT');
    
    % Run BLE Navigator
    main_BLE;
    
    % Run Sensor Navigator
    % SN should start from the third BLE estimated point
    % The 3rd point is due to the latency in SDK code
    INI.Sensor.initialX = bleUserCoords(INI.DI_debug.startSNindex, 2);
    INI.Sensor.initialY = bleUserCoords(INI.DI_debug.startSNindex, 3);
    main_Sensor_3;
    
    % Run DI
    main_DI;
    
else
    error('Unknown Working Mode!');
end






