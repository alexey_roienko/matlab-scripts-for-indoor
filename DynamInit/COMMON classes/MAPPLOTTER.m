%% Author:
%  Alexey Roienko (a.roienko@it-jim.com) and 
%  Feliks Sirenko, sirenkofelix@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  21/09/17
classdef MAPPLOTTER
    %Class is intended to draw the map of the test room and additional
    %information on it, like beacons, sensor trajectory etc.
    %   Overview of main class fields and methods will be here
    
    properties (Access = private)
        mapFigRef;
        pxSize;
    end
    
    methods (Access = public)
        %% Object Initializer
        %  Draw the map as an image
        function obj = MAPPLOTTER(mapImage, varargin)
            % Draw the map as an image
            %obj.mapFigRef = figure('units', 'normalized', 'outerposition', [0.05 0.05 0.95 0.95]);
            obj.mapFigRef = figure();
%             set(gca, 'XTick', []); 
%             set(gca, 'YTick', []);
            imshow(mapImage);
%             hold on;
            
            % If the title of the map is received as the second input parameter
            switch nargin
                case 2
                    title(varargin{1}, 'FontSize', 16);
                case 3
                    obj.pxSize = varargin{1};
                    title(varargin{2}, 'FontSize', 16);
            end
        end
        
        
        %% Depicts the line and its starting and ending points on the current map
        function lineRef = mPlotLine(obj, startPoint, endPoint, lineColor, markerColor, ...
                                     markerSize, dispName)
            figure(obj.mapFigRef);
            lineRef = line([startPoint(1) endPoint(1)], [startPoint(2) endPoint(2)], ...
                           'LineStyle', '-', 'Color', lineColor,...
                           'Marker', 'o', 'MarkerFaceColor', markerColor,...
                           'MarkerSize', markerSize, 'DisplayName', dispName);
        end
        
        
        %% Method allows representing results obtained by Matlab and acquired by 
        %  UserApp (taken from log-file)
        %  Main point is that timestamps are likely to be different, there
        %  is a latency for UserApp data comparing to Matlab results
        function mPlotCompareMLvsUA(obj, mlData, uaData, mlDataTs, uaDataTs)
            
            %% Calculate delay of uaData comparing to mData
            timeDif = (uaDataTs - mlDataTs)/1000;
            if ~(timeDif >= 0)
                error('Difference between data timestamps is negative!');
            end
            
            figure(obj.mapFigRef);
            hold on;
            mlDataIndex = 1;
            for t=1:length(uaData(:,1))
                currentTS = uaData(t,1) + timeDif;
                while (mlDataIndex <= length(mlData(:,1))) && (mlData(mlDataIndex, 1) < currentTS)
                    mlDataIndex = mlDataIndex + 1;
                end
                
                if mlDataIndex >= length(mlData(:,1))
                    break;
                end
                
                if t~=1
                    % Plot uaData
                    obj.mPlotLine(uaData(t-1, 2:3)/obj.pxSize, uaData(t, 2:3)/obj.pxSize, ...
                                  'red', 'red', 8);
                    % Plot mlData
                    obj.mPlotLine(prevMlPoint/obj.pxSize, mlData(mlDataIndex-1, 2:3)/obj.pxSize,...
                                  'blue', 'blue', 5);                    
                end
                
                if mlDataIndex == 1
                    prevMlPoint = mlData(mlDataIndex, 2:3);
                else
                    prevMlPoint = mlData(mlDataIndex-1, 2:3);
                end
            end
        end
        
        
        %% Method allows representing results obtained by Matlab and acquired by 
        %  CPP Navigator (taken from log-file)
        %  Main point is that timestamps are similar, there
        %  is almost no latency for CPP data comparing to Matlab results
        function mPlotCompareMLvsCPP(obj, mlData, cppData)
            
            if length(cppData(:,1)) > length(mlData(:,1))
                dataL = length(mlData(:,1));
            else
                dataL = length(cppData(:,1));
            end
            
            for t=2:dataL
                if t == dataL
                    % Plot uaData
                    cppRef = obj.mPlotLine(cppData(t-1, 2:3)/obj.pxSize, cppData(t, 2:3)/obj.pxSize, ...
                                           'red', 'red', 8, 'SDK coords');
                    % Matlab Data
                    mlRef = obj.mPlotLine(mlData(t-1, 2:3)/obj.pxSize, mlData(t, 2:3)/obj.pxSize,...
                                          'blue', 'blue', 5, 'MatLab coords');
                    axis image
                else
                    % Plot uaData
                    obj.mPlotLine(cppData(t-1, 2:3)/obj.pxSize, cppData(t, 2:3)/obj.pxSize, ...
                                  'red', 'red', 8, 'SDK coords');
                    % Matlab Data
                    obj.mPlotLine(mlData(t-1, 2:3)/obj.pxSize, mlData(t, 2:3)/obj.pxSize,...
                                  'blue', 'blue', 5, 'MATLab coords');
                    axis image
                end                
%                 str = sprintf('n=%d, t = %.3f, ML = (%.3f; %.3f), UA = (%.3f; %.3f)\n', ...
%                     t, cppData(t,1), mlData(t,2:3), cppData(t,2:3));
%                 disp(str);
%                 pause();
            end
            legend([cppRef mlRef], 'Location', 'NorthWest');
        end
        
    end
end




