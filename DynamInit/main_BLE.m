

%% ============= READING DATA from log-files ============== %
[bleData, bleDataAbsTs] = READER.read([pwd '\logs\input_ble_packets-' logDateTime '.txt'],...
                                    INI, beaconsID);
packetsNumber = length(bleData(:,1));

%% ================ End of Test Files Headers ================ % 
if INI.BLE_debug.showRSSIMap
     PLOTTER.fShowRSSIMap(bleData, 'Raw RSSI');
end
%% ===================== END DEBUG ===================== % 


predictPackN = ceil(bleData(end,1)/INI.BLE.beaconsCycle) + 1;

%% ======================= RSSI KALMAN FILTRATION =========================%
if INI.BLE_filters.RSSIKalmanFilter
    P = [100 0; 0 100];        % covariance matrix         
    Q = [0.001 0; 0 0.001];    % model error covariance [0.002 0; 0 0.002]
    R = 5;                     % measurement error covariance 0.11
    objKalman = KalmanFilterRSSI ('P_init', P,  'Q', Q,  'R', R,...
                                  'beacNumb', INI.BLE.beaconsN,...
                                  'timeout1', INI.BLE.timeout1,...
                                  'timeout2', INI.BLE.timeout2);
    
    KalmanFilteredData = zeros(predictPackN, size(bleData, 2));
    addedTSindex = 1;
    kalmanIndex = 1;
    for pN=1:packetsNumber
        if pN==1
            dt = 0;
        else
            dt = bleData(pN,1)-bleData(pN-1,1);
        end
        
        if dt > INI.BLE.beaconsCycle + (0.5*INI.BLE.beaconsCycle)
            % abnormal situation, some timestamps are missing by receiver
            realTS = round(dt / INI.BLE.beaconsCycle) - 1;
            rssiValues = KalmanFilteredData(kalmanIndex-1, 2:end);
            for i = 1:realTS
                % set proper timestamp
                KalmanFilteredData(kalmanIndex, 1) = bleData(pN-1,1) + i*INI.BLE.beaconsCycle;
                addedTS(addedTSindex) = kalmanIndex;
                addedTSindex = addedTSindex + 1;
                % double the last RSSI values
                KalmanFilteredData(kalmanIndex, 2:end) = rssiValues;
                kalmanIndex = kalmanIndex + 1;
            end
        end
        % normal situation, timestamp is correct
        KalmanFilteredData(kalmanIndex, 1) = bleData(pN,1);
        KalmanFilteredData(kalmanIndex, 2:end) = objKalman.FilterMany(dt, bleData(pN,2:end));
        kalmanIndex = kalmanIndex + 1;
    end
else
    KalmanFilteredData = bleData;
end


%% ======================= DEBUG ======================= % 
if INI.BLE_debug.showRSSIMap
    PLOTTER.fShowRSSIMap(KalmanFilteredData, 'Kalman Filtered RSSI');
end
if INI.BLE_debug.plotBeaconsOnMap
    logFileName = ['input_ble_packets-' logDateTime '.txt'];
    mapFig = PLOTTER.PlotMap(mapImage, mapParams, logFileName,...
                             mapParams.pixelSize, beaconsParams);
end
%% ===================== END DEBUG ===================== % 


%% Initialization of KalmanXY Filter
if INI.BLE_filters.XYKalmanFilter
    P = [10 0; 0 10];              % How sure I am in initial position
    Q = [0.5 0; 0 0.5];            % What mistake can I make when predicting a new one position
    R = eye(3) * 15;               % Distribute weights between the model and the measurements
    objKalmanXY = KalmanFilterXY('P_init', P, 'Q', Q, 'R', R);
end


%% Initialization of Distance Limiter class
if INI.BLE_filters.distanceLimiter
    objDistLimiter = DistanceLimiter('timeLim', INI.BLE.limiterX, 'distLim', INI.BLE.limiterY);
end


%% Initialization of KalmanXY Filter%% Main cycle
bleUserCoords   = zeros(length(KalmanFilteredData(:,1)), 3);
SelectedBeacons = struct('beacNO', [], 'RSSI', []);
addedTSindex = 1;

for t = 1:length(KalmanFilteredData(:,1))
     bleUserCoords(t,1) = KalmanFilteredData(t,1);
     
     %% Choice of Beacon Triplet
     if INI.BLE_filters.tripletsFilter
         if INI.BLE_testfiles.write_CheckTriplets
             [SelectedBeacons(t), isSuccessfully] = ...
                 LATERATION.fGetTripletsAdvanced( ...
                     KalmanFilteredData(t, 2:end), INI, ...
                     INI.BLE_debug.tripletsFunction, beaconsParams, ...
                     beaconsID, KalmanFilteredData(t, 1), fID_T4, fID_T5);
         else
             [SelectedBeacons(t), isSuccessfully] = ...
                 LATERATION.fGetTripletsAdvanced( ...
                      KalmanFilteredData(t, 2:end), INI, ...
                      INI.BLE_debug.tripletsFunction, beaconsParams);
         end
         
     else
        [SelectedBeacons(t), isSuccessfully] = ...
            LATERATION.fGetTripletStandard( KalmanFilteredData(t, 2:end), ...
                                            INI.BLE.lateratBeacNO, false);
     end    
     %% ===================== END DEBUG ===================== %
     
     
     if isSuccessfully
        %% If beacons have been succesfully chosen, then ...
        beaconsLaterat = cell(1,INI.BLE.lateratBeacNO);
        for j=1:INI.BLE.lateratBeacNO
            beacon = beaconsParams{1,SelectedBeacons(t).beacNO(j)};
            beacon.RSSI = SelectedBeacons(t).RSSI(j);
            beaconsLaterat{j} = beacon; 
        end     

        distancesLaterat = LATERATION.fRssi2Dist(beaconsLaterat);     

        %% Choose the positioning algorithm
        if strcmpi(INI.BLE.posMethod, 'Trilateration')
            bleUserCoords(t,2:3) = LATERATION.fTrilateration(...
                beaconsLaterat, distancesLaterat, INI.general.userHeight);
            
        elseif strcmpi(INI.BLE.posMethod, 'Multilateration')
            bleUserCoords(t,2:3) = LATERATION.fMultilateration(...
                beaconsLaterat, distancesLaterat, INI.general.userHeight);               
        else
            error('Error in Lateration method');
        end
        %% ===================== END DEBUG ===================== %
        
        
        %% Apply Map Correction
        if INI.correctors.mapCorrection || INI.correctors.meshCorrection
            temp = CORRECTOR.fMapCorrector(bleUserCoords(t,2:3), ...
                     [INI.general.mesh2StepX  INI.general.mesh2StepY],...
                     INI.general.mesh2Rows,  maskTable2);
            bleUserCoords(t,2:3) = temp;
        end
        
        
        %% Apply XY Kalman Filter
        if INI.BLE_filters.XYKalmanFilter
            if exist('addedTS','var') && (addedTSindex <= length(addedTS))...
                                      && (addedTS(addedTSindex) == t)
                bleUserCoords(t,2:3) = bleUserCoords(t-1,2:3);
                addedTSindex = addedTSindex + 1;
            else
                bleUserCoords(t,2:3) = objKalmanXY.FilterByKalman(bleUserCoords(t,2:3),...
                    [beaconsLaterat{1,1}.x beaconsLaterat{1,1}.y],...
                    [beaconsLaterat{1,2}.x beaconsLaterat{1,2}.y],...
                    [beaconsLaterat{1,3}.x beaconsLaterat{1,3}.y]);

                %% Apply Map Correction once again after filter application
                if INI.correctors.mapCorrection || INI.correctors.meshCorrection
                    temp = CORRECTOR.fMapCorrector(bleUserCoords(t,2:3), ...
                             [INI.general.mesh2StepX  INI.general.mesh2StepY],...
                              INI.general.mesh2Rows, maskTable2);
                    bleUserCoords(t,2:3) = temp;
                end 
            end
        end
        
        %% Apply Distance Limiter
        if INI.BLE_filters.distanceLimiter
            temp = objDistLimiter.applyLimiter(bleUserCoords(t,1), bleUserCoords(t,2:3)', ...
                INI.BLE_debug.distanceLimiterInfo);
            if INI.BLE_filters.XYKalmanFilter
                objKalmanXY.setX(temp);
            end
            bleUserCoords(t,2:3) = temp;
        end

        %% ================= End writing to Test Files  ================= %     
        %% ===================== END DEBUG ===================== %
        
        
        %% Apply Mesh Correction
        if INI.correctors.meshCorrection
            temp = CORRECTOR.fMeshCorrector(bleUserCoords(t,2:3), ...
                         [INI.general.mesh1StepX  INI.general.mesh1StepY],...
                          INI.general.mesh1Rows,  maskTable1);
            bleUserCoords(t,2:3) = temp;
        end
    end
end

