classdef DistanceLimiter2 < handle
    % DISTANCECORRECTOR Summary of this class goes here    
    %   Detailed explanation goes here
    properties
        prevX;      % Xtemp = [x y] Previous state
        prevT;      % t_temp - previous time
        limCoef     % coefficient of a DL: L_lim = limCoef * delta_t
    end
    
    methods
        function obj = DistanceLimiter2(varargin)
            %% Class constructor
            for i = 1:2:nargin
                if strcmpi(varargin{i}, 'limiterCoef')
                    obj.limCoef = varargin{i+1};
                else
                    error('Invalid argument');
                end
            end
        end
        
        
        %% Main method of the class
        %  Application of limiter to distance between previous point and
        %  current
        function outCoords = applyLimiter(obj, t, X, debug)            
            if isempty(obj.prevX)
                obj.prevX = X;
                outCoords = X;
                if debug
                    disp('The first point for Limiter.');
                    disp(['Output coords are: X = ', num2str(X(1,1)), ', Y = ', num2str(X(2,1))]);
                end
            else                
                dt      = t - obj.prevT;
                dist    = sqrt((obj.prevX(1,1) - X(1,1))^2 + (obj.prevX(2,1) - X(2,1))^2);                
                limDist = obj.limCoef * dt;
                                
                if dist > limDist
                    if debug
                        disp('Limitation should be done.');
                        disp(['Initial coords: X = ', num2str(X(1,1)), ', Y = ' , num2str(X(2,1))]);
                    end
                    dx   = X(1,1) - obj.prevX(1,1);
                    dy   = X(2,1) - obj.prevX(2,1);
                    
                    temp = zeros(size(X));
                    
                    dx_new = dx * limDist / dist;
                    dy_new = dy * limDist / dist;
                    
                    temp(1,1) = obj.prevX(1,1) + dx_new;
                    temp(2,1) = obj.prevX(2,1) + dy_new;

                    if debug
                        disp(['Corrected coords: X = ', num2str(temp(1,1)), ...
                                              ', Y = ', num2str(temp(2,1))]);
                    end
                else
                    temp = X;
                    if debug
                        disp('Limitation is not necessary.');
                        disp(['Output coords are: X = ', num2str(temp(1,1)), ...
                                               ', Y = ', num2str(temp(2,1))]);
                    end
                end
                obj.prevX = temp;
                outCoords  = temp;
            end            
            obj.prevT = t; 
        end
    end
      
end


