classdef KalmanFilterXY < handle
    %KALMANFILTERPMODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = public)
        P_init      % covariance error prediction
        Q           % covariance of the process noise
        R           % covariance of the measurement noise
    end
    
	properties (Access = private)
        P; 
        X;
    end
    
    methods (Access = public)
        
        %% Object initializer
        % Initializes the KalmanFilterXY object
        function obj = KalmanFilterXY(varargin)            
            for i = 1:2:nargin
                if strcmp(varargin{i}, 'P_init')
                    obj.P_init = varargin{i+1};  
                elseif  strcmp(varargin{i}, 'Q')
                    obj.Q = varargin{i+1};
                elseif  strcmp(varargin{i}, 'R')
                    obj.R = varargin{i+1};  
                else
                    error('Invalid argument');
                end
           end
        end
        
        
        %% Setter - Sets the parameters of state vector
        function setX(obj, coords)            
            obj.X = coords;
        end
        
        
        %% Pure KalmanXY filtration without any limiters
        % Model equation X = A * X, where
        % X - state vector X = [x y]'
        % A - matrix model        
        function output = FilterByKalman(obj, meas, beaconA, beaconB, beaconC)           
           isFirst = false;
           
           % If it's the first call of the filter...
           if isempty(obj.X)
               obj.X = meas';
               if isempty(obj.P)
                   obj.P = obj.P_init;
               end
               isFirst = true;
           end
           
           if ~isFirst
               A = eye(2);
               obj.X = A * obj.X;

               % Ahead Error Covariance
               obj.P = A * obj.P * A' + obj.Q;

               % Kalman Gain
               dAdx = (meas(1) - beaconA(1))/ sqrt((meas(1) - beaconA(1))^2 + (meas(2) - beaconA(2))^2);
               dAdy = (meas(2) - beaconA(2))/ sqrt((meas(1) - beaconA(1))^2 + (meas(2) - beaconA(2))^2);
               dBdx = (meas(1) - beaconB(1))/ sqrt((meas(1) - beaconB(1))^2 + (meas(2) - beaconB(2))^2);
               dBdy = (meas(2) - beaconB(2))/ sqrt((meas(1) - beaconB(1))^2 + (meas(2) - beaconB(2))^2);
               dCdx = (meas(1) - beaconC(1))/ sqrt((meas(1) - beaconC(1))^2 + (meas(2) - beaconC(2))^2);
               dCdy = (meas(2) - beaconC(2))/ sqrt((meas(1) - beaconC(1))^2 + (meas(2) - beaconC(2))^2);

               H = [dAdx dAdy;...
                    dBdx dBdy;...
                    dCdx dCdy];

               K = obj.P * H' / (H * obj.P * H' + obj.R);
               % Estimation update
               D = H * meas';

               obj.X = obj.X + K *(D - H * obj.X);

               %Error Covariance
               obj.P = (eye(2) - K * H) * obj.P;
           end           
           output = obj.X;
        end
    end   
end

