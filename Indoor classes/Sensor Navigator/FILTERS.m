classdef FILTERS < handle
    methods (Access = public, Static)
        
        function output = fASWFilteringMany(input, WS, modeStr)
            output = zeros(length(input),4);
            output(:,1) = input(:,1);
            if nargin == 3
                output(:,2) = FILTERS.aswFiltering(input(:,2), WS, modeStr);
                output(:,3) = FILTERS.aswFiltering(input(:,3), WS, modeStr);
                output(:,4) = FILTERS.aswFiltering(input(:,4), WS, modeStr);
            else
                output(:,2) = FILTERS.aswFiltering(input(:,2), WS);
                output(:,3) = FILTERS.aswFiltering(input(:,3), WS);
                output(:,4) = FILTERS.aswFiltering(input(:,4), WS);
            end            
        end
        
        
        function angles = fMapOrientationCorrection(angles, mapAngle)                
            for i=1:length(angles)    
                temp = angles(i,4) - mapAngle;
                if temp > 180
                    temp = -360 + temp;
                elseif temp < -180
                    temp = 360 + temp;
                end
                angles(i,4) = temp;
            end
        end
        
        
        function output = fButterWorthFilter(input, a, b)
            output = zeros(length(input), 3);
            output(:,1) = filter(b, a, input(:,1)');
            output(:,2) = filter(b, a, input(:,2)');
            output(:,3) = filter(b, a, input(:,3)' - 1) + 1;
        end
    end
    
    methods (Access = private, Static)
        function output = aswFiltering(input, WS, modeStr)
            %% Function implements averaging sliding window filtering
            % input   - data vector or matrix
            % WS      - sliding window size
            % modeStr - string described one of two modes:
            %           "round" - data filtering using rounding input samples
            %           "other str or empty" - ordinary filtering

            output = aswFiltering2(input, WS, modeStr);
%             [X, Y] = size(input);
%             output = zeros(X, Y);
% 
%             counter = WS;
% 
%             if ~(X==1 || Y==1)    
%                 fifo_vector = zeros(WS, Y);
%             else
%                 N = length(input);
%                 fifo_vector = zeros(WS, 1);
%             end
% 
%             if nargin==3
%                 if strcmpi(modeStr, 'round')
%                     input = round(input);
%                 else                    
%                     warning(['Undefined function parameter - ' modeStr]);
%                     return
%                 end        
%             end
% 
%             if ~(X==1 || Y==1)
%                 output(1:WS-1,:) = input(1:WS-1,:);
%                 fifo_vector(1:WS-1, :) = input(1:WS-1,:);
%                 for p=WS:X
%                     if counter <= WS
%                         fifo_vector(counter, :) = input(p,:);
%                         counter = counter + 1;
%                     else
%                         counter = 1;
%                         fifo_vector(counter, :) = input(p,:);
%                     end    
%                     output(p,:) = mean(fifo_vector,1);
%                 end
%             else
%                 output(1:WS-1) = input(1:WS-1);
%                 fifo_vector(1:WS-1) = input(1:WS-1);
%                 for p=WS:N
%                     if counter <= WS
%                         fifo_vector(counter) = input(p);
%                         counter = counter + 1;
%                     else
%                         counter = 1;
%                         fifo_vector(counter) = input(p);
%                     end    
%                     output(p) = mean(fifo_vector);
%                 end
%             end
        end
    end
end

