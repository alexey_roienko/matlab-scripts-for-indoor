%% Function implements averaging sliding window filtering
% input   - data vector or matrix
% WS      - sliding window size
% modeStr - string described one of two modes:
%           "round" - data filtering using rounding input samples
%           "other str or empty" - ordinary filtering

function output = fASWFiltering2(input, WS, modeStr)

%% Initialization
[X, Y] = size(input);
output = zeros(X, Y);
fifoSize = 0;
counter = 1;

%% Format definition
if ~(X==1 || Y==1)    
    fifo_vector = zeros(WS, Y);
else
    fifo_vector = zeros(WS, 1);
end

if nargin==3
    if strcmpi(modeStr, 'round')
        input = round(input);
    else
        warning(['Undefined function parameter - ' modeStr]);
        return
    end
end

%% Filtering
if ~(X==1 || Y==1)
    for x = 1:X
        if counter > WS
            counter = 1;
        end
        % Angles transform from [-180; 180] plain into [0; 360] / [0; 360 plain]
        if x > 1
            for y = 1:Y
                if input(x, y) - input(x-1, y) > 180
                    input(x, y) = input(x, y) - 360;
                elseif input(x, y) - input(x-1, y) < -180
                    input(x, y) = input(x, y) + 360;
                end
            end
        end
        
        % Filling the buffer
        fifo_vector(counter, :) = input(x, :);
        for i = 1:length(fifo_vector(1, :))
            for j = 1:length(fifo_vector(:, 1))
                if fifo_vector(j, i) == 0
                    break;
                end
                fifoSize = j;
            end
        end
        output(x, :) = mean(fifo_vector(1:fifoSize, :), 1);
        
        % Inverse transform of angles
        for i = 1:Y
            if output(x, i) > 180
                output(x, i) = -360 + output(x, i);
            elseif output(x, i) < -180
                output(x, i) = 360 + output(x, i);
            end
        end
        
        % Increase counter
        counter = counter + 1;
    end
else
    for x = 1:X
        if counter > WS
            counter = 1;
        end
        
        % Angles transform from [-180; 180] plain into [0; 360] / [0; 360 plain]
        if x > 1
            if input(x) - input(x-1) > 180
                input(x) = input(x) - 360;
            elseif input(x) - input(x-1) < -180
                input(x) = input(x) + 360;
            end
        end
        
        % Filling the buffer
        fifo_vector(counter) = input(x);
        for i = 1:length(fifo_vector)
            if fifo_vector(i) == 0
                break;
            end
            fifoSize = i;
        end
        output(x) = mean(fifo_vector(1:fifoSize));
        
        % Inverse transform of angles
        if output(x) > 180
            output(x) = -360 + output(x);
        elseif output(x) < -180
            output(x) = 360 + output(x);
        end
        
        % Increase counter
        counter = counter + 1;
    end
end
end