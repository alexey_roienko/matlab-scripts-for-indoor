__gensimrand__ generates simulated randoms for the particle filter and DI

Simulated randoms are generated for _N_ particles (I curently use _N=1000_): 

Two types of output files are generated: `inc` files are included directly in the `simrand.cpp`
(compile time) in the `src/SimRand` directory, and
the plain `dat` files are to be used anywhere (e.g. MatLab). The PF and DI should use the data `simrandPI[i]`  (for example) for particle i.
If there is not enough data (_Np > N_), then we wrap around by taking `simrandPI[i % N]`.
Here _N_ is the number of simulated randoms, and _Np_ is the number of particles in PF or DI.
Wrapping around is not good for `simrandI`, see below.


* `simrandPI` : Random 2D points (x, y) with x, y uniformly distributed in [-1, 1]. Used for initialization. 

* `simrandPP` : Random 2D points (x, y) with x, y normally distributed with _mu_ = 0, _sigma_ = 1. Used for movement.

* `simrandBeta` : Random doubles in [0, 1]. Used for resampling in PF only.

* `simrandI` : Random ints in [0, N-1]. Used for selecting particle number in the PF resampling.  
Note: we should actually use _i % Np_, where _Np_ is the current number of particles. This only gives proper random distribution
if _Np <= N_ and _N_ is divisible by _Np_. For example, if _N=1000_, we can use _Np = 20, 40, 50, 100, 200, 250, 500, 1000_. 
Any other number _Np_ will give somewhat wrong distribution of  `simrandI`, the code will still work.