classdef PARTICLE_FILTER
    methods (Access = public, Static)
        
        %% Method creates array similar to that created from A.Grechnev log.
        %  Contains info in the form [timestamp, deltaX, deltaY, bleX, bleY],
        %  where 'timestamp' - moment of time when results from BLE Navigator are obtained;
        %  'deltaX', 'deltaY' - increments in correcponding coordinate
        %  calculated by Sensor Navigator;
        %  'bleX', 'bleY' - corrdinates obtained by BLE Navigator
        %% INPUT:
        %  bleCoords    - vector, Nx3, coordinates estimated by 'main_BLE.m', in meters, (X,Y)
        %  bleAbsTs     - system value when the first BLE package was received, msec.
        %  sensorCoords - vector, Mx3, coordinates estimated by 'main_Sensor.m', in meters, (dX,dY)
        %  sensorAbsTs  - system value when the first Accelerometer value was received, msec.
        %% OUTPUT:
        %  outputData   - vector, Nx5, [timestamp, deltaX, deltaY, bleX, bleY], in meters, (X,Y)
        function outputData = fPrepareInputData(bleCoords, bleAbsTS, sensorCoords, sensorAbsTS)
            
            %% Find the first sensor timestamp after the first ble timestamp
            % Here we suppose that Acceleration (as well as Angles) data usually
            % start arriving much earlier then BLE data
            tStartSensor = 1;
            diff = (bleAbsTS - sensorAbsTS)/10^3;
            while sensorCoords(tStartSensor,1) < (bleCoords(1,1)+diff)
                tStartSensor = tStartSensor + 1;
            end
            
            %% Cycle over all ble samples, calculating deltaX and deltaY from sensors            
            outputData        = zeros(size(bleCoords, 1), 5);
            temp              = zeros(1,2);
            outputData(1,1)   = bleCoords(1,1);
            outputData(1,4:5) = bleCoords(1,2:3);            
            tSensor           = tStartSensor;
            for tBle = 2:size(bleCoords, 1)
                while (tSensor <= size(sensorCoords,1)) && (sensorCoords(tSensor,1) < (bleCoords(tBle,1) + diff))
                    temp(1) = temp(1) + (sensorCoords(tSensor,2) - sensorCoords(tSensor-1,2));
                    temp(2) = temp(2) + (sensorCoords(tSensor,3) - sensorCoords(tSensor-1,3));
                    tSensor = tSensor + 1;
                end
                outputData(tBle,1)   = bleCoords(tBle,1);
                outputData(tBle,2:3) = temp;
                outputData(tBle,4:5) = bleCoords(tBle,2:3);
                
                temp = zeros(1,2);                
            end
        end
        
        
        %% Main method which implements Particle Filter.
        %% INPUT:
        %  prevParticleCoords - vector, Nx2, contains coordinates of each particle, in meters, (X,Y)
        %  bleCoords          - vector, 1x2, coordinates estimated by BLE Navigator 
        %                       for current timestamp, in meters, (X,Y)
        %  sensorCoords       - vector, 1x2, [deltaX; deltaY] estimated by Sensor Navigator 
        %                       for current timestamp, in meters, (X,Y)
        %  INI                - structure, contains 'config' information 
        %  maskTable2         - vector, Mx1, 'maskTable2' array (smaller mesh)
        %% OUTPUT:
        %  userCoords           - vector, 1x2, user coordinates estimated by 
        %                         Particle Filter, in meters, (X,Y)
        %  alivedParticleCoords - vector, Nx2, contains coordinates of alived particles, in meters, (X,Y)
        %  newParticleCoords    - vector, Nx2, contains coordinates of all new particles,
        %                         both dead and alived, in meters, (X,Y)
        function [userCoords, alivedParticleCoords, newParticleCoords] = fGetCoordinates(...
                prevParticleCoords, bleCoords, sensorCoords, INI, meshParams, maskTable2)
                
            maskTable2RowsN   = meshParams.fineMesh(3);
            newParticleCoords = zeros(INI.PF.particlesNo, 2);
            
            if INI.PF_debug.testMode
                currentDir = pwd;
                cd('..\Indoor classes\PFtestModeFiles\simrand_reference_data');
                posVariations2 = dlmread([pwd '\simrandpp.dat'], ' ');
                cd(currentDir)
            end
                        
            %% STEP ONE %%
            for pNO = 1:INI.PF.particlesNo
                if INI.PF_debug.testMode
                    % Model: X                
                    newParticleCoords(pNO, 1) = prevParticleCoords(pNO, 1) + sensorCoords(1,1) + ...
                        posVariations2(pNO,1) * INI.PF.modelSTD;
                    % Model: Y
                    newParticleCoords(pNO, 2) = prevParticleCoords(pNO, 2) + sensorCoords(1,2) + ...
                        posVariations2(pNO,2) * INI.PF.modelSTD;
                else
                    % Model: X                
                    newParticleCoords(pNO, 1) = prevParticleCoords(pNO, 1) + sensorCoords(1,1) + ...
                        randn(1,1) * INI.PF.modelSTD;
                    % Model: Y
                    newParticleCoords(pNO, 2) = prevParticleCoords(pNO, 2) + sensorCoords(1,2) + ...
                        randn(1,1) * INI.PF.modelSTD;
                end
            end
            
        
            %% STEP TWO %%
            mtSize         = length(maskTable2);
            mesh2Steps     = [meshParams.fineMesh(1) meshParams.fineMesh(2)];
            weightsAllowed = ones(INI.PF.particlesNo, 1);
            for pNO = 1:INI.PF.particlesNo
                if (INI.correctors.mapCorrection || INI.correctors.meshCorrection || ...
                       INI.correctors.wallsCorrection) && ...
                       CORRECTOR.fIsNotOnTheMap(newParticleCoords(pNO,:), ...
                                       mesh2Steps, maskTable2RowsN, mtSize)                    
                    weightsAllowed(pNO, 1) = 0;
                end
                if (weightsAllowed(pNO, 1) ~= 0) && INI.correctors.meshCorrection && ...
                       CORRECTOR.fIsInTheBlackRegion(newParticleCoords(pNO,:),...
                                   mesh2Steps, maskTable2RowsN, maskTable2)
                    weightsAllowed(pNO, 1) = 0;
                end
                if (weightsAllowed(pNO, 1) ~= 0) && INI.correctors.wallsCorrection && ...
                       CORRECTOR.fIsWallBeingCrossed2(prevParticleCoords(pNO, :), ...
                       newParticleCoords(pNO, :), mesh2Steps, maskTable2RowsN, maskTable2)
                    weightsAllowed(pNO, 1) = 0;
                end
            end
            
            % If all particles are outside the map, then correct all of them
            % by meshCorrector with small mesh(#2)
            corrParticleCoords = zeros(size(newParticleCoords));
            if sum(weightsAllowed) < 1
                for pNO = 1:INI.PF.particlesNo
                    if INI.correctors.mapCorrection
                        corrParticleCoords(pNO,:) = ...
                            CORRECTOR.fMapCorrector(newParticleCoords(pNO,:), meshParams, maskTable2);
                    end
                    if INI.correctors.wallsCorrection
                        corrParticleCoords(pNO,:) = ...
                            CORRECTOR.fWallCorrector2(prevParticleCoords(pNO,:), ...
                            newParticleCoords(pNO,:), INI, maskTable2);
                    end
                    if INI.correctors.meshCorrection
                        corrParticleCoords(pNO,:) = ...
                            CORRECTOR.fMeshCorrector(newParticleCoords(pNO,:), meshParams, maskTable2, 'fineMesh');
                    end
                end
                weightsAllowed = ones(INI.PF.particlesNo, 1);
            else
                corrParticleCoords = newParticleCoords;
            end
            
            % Calculate weights for particles with weightsAllowed(pNO)=1 only
            weights = zeros(INI.PF.particlesNo, 1);
            for pNO = 1:INI.PF.particlesNo
                if 1 == weightsAllowed(pNO)
                    w(1,1) = -(corrParticleCoords(pNO, 1) - bleCoords(1))^2 / (2*INI.PF.weightSTD^2);
                    w(1,2) = -(corrParticleCoords(pNO, 2) - bleCoords(2))^2 / (2*INI.PF.weightSTD^2);
                    weights(pNO, 1) = w(1,1) + w(1,2);
                end
            end
                        
            % Weights calculation
            maxVal = max(weights);
            weightsMod = zeros(length(weights),1);
            for pNO = 1:INI.PF.particlesNo
                if weightsAllowed(pNO, 1) == 1
                    weightsMod(pNO,1) = (1/(2*pi)/INI.PF.weightSTD^2) * exp(weights(pNO) - maxVal);
                end
            end
            
            weightsSum  = sum(weightsMod);
            weightsNorm = weightsMod / weightsSum;
                        
            %% STEP THREE %
            alivedParticleCoords = PARTICLE_FILTER.fResample(corrParticleCoords, ...
                                      weightsNorm, INI.PF_debug.testMode);
            
            userCoords = [mean(alivedParticleCoords(:,1)) mean(alivedParticleCoords(:,2))];
        end 
    end
    
    
    methods (Access = private, Static)
        function pNewCoords = fResample(pOldCoords, pWeights, testMode)            
            pNumber = length(pWeights);            
            if testMode
                currentDir = pwd;
                cd('..\Indoor classes\PFtestModeFiles');
                index = dlmread([pwd '\thirdStageParticleNumber.txt'], ' ') + 1;
                cd('simrand_reference_data');
                randComp = dlmread([pwd '\simrandbeta.dat'], ' ');
                cd(currentDir)
            else
                index = randi([1 pNumber], 1, 1);
            end
            
            k = 1;
            pNewCoords = zeros(pNumber, 2);
            for i=1:numel(pWeights)
                if testMode
                    pBeta = 2 * max(pWeights) * randComp(i);
                else
                    pBeta = 2 * max(pWeights) * rand(1);
                    %pBeta = pBeta + 2 * max(pWeights) * rand(1,1);
                end
                
                while pBeta > pWeights(index)
            		pBeta = pBeta - pWeights(index);
                    if (index > pNumber - 1)
                        index = 1;
                    else
                        index = index + 1;
                    end            		
                end
                pNewCoords(k,:) = pOldCoords(index,:);
                k = k + 1;
            end
        end        
    end
end

