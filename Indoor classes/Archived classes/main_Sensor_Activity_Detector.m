%% Script uses binary ACTIVITY detector for tracking user position.
%  Namely, 'isStationary' detector is implemented. Our first version of
%  Sensor Navigator.


%% ============= READING DATA from log-files ============== %
[accelLocalData, accelDataAbsTs] = READER.read([pwd '\logs\input_accel-' logDateTime '.txt'], INI);
[anglesData, anglesDataAbsTs]    = READER.read([pwd '\logs\input_angles-' logDateTime '.txt'], INI);
L1              = length(accelLocalData);
L2              = length(anglesData);
timestamps      = accelLocalData(:,1);
objConvertor    = CONVERTOR();
if INI.Sensor_debug.comparePositionUAOnline
    [UAData, uaDataAbsTs] = READER.read([pwd '\logs\output_position_ua-' logDateTime '.txt'], INI);
end


%% Transform the accel values to uniform view depending on the platform
if strcmpi(INI.general.logsSource, 'SensorLogger') ...
   && strcmpi(INI.general.deviceOS, 'Android')    
    accelLocalData(:,2) = accelLocalData(:,2) / 9.81;
    accelLocalData(:,3) = accelLocalData(:,3) / 9.81;
    accelLocalData(:,4) = accelLocalData(:,4) / 9.81;

elseif strcmpi(INI.general.logsSource, 'SensorLogger') ...
       && strcmpi(INI.general.deviceOS, 'iOS')    
    accelLocalData(:,2) = accelLocalData(:,2) * (-1);
    accelLocalData(:,3) = accelLocalData(:,3) * (-1);
    accelLocalData(:,4) = accelLocalData(:,4) * (-1);
end


%% Angles filtering but for Android only
anglesFiltData = anglesData;
if (INI.Sensor.anglesFiltering) && strcmpi(INI.general.deviceOS, 'Android')
    disp('Filtering raw angles...');
    % X axis
    anglesFiltData(:,2) = fASWFiltering2(anglesData(:,2), INI.Sensor.awfWS);
    % Y axis
    anglesFiltData(:,3) = fASWFiltering2(anglesData(:,3), INI.Sensor.awfWS);
    % Z axis
    anglesFiltData(:,4) = fASWFiltering2(anglesData(:,4), INI.Sensor.awfWS);
end


%% ======================= DEBUG ======================= % 
% ============= Creation of Test Files Headers ============== %
% Writing the filtered Angles to the file for testing 
if (INI.Sensor_testfiles.writeFilteredAngles) && strcmpi(INI.general.deviceOS, 'Android')
    fid = fopen('test files/tSNfilteredAngles.txt', 'w');
    fprintf(fid, 'Timestamp  Pitch\t\tRoll\t\tYaw\n');
    for n=1:L1
        fprintf(fid, '%.5f\t %.5f\t %.5f\t %.5f\n', anglesFiltData(n,:));
    end
    fclose(fid);
end
% ================ End of Test Files Headers ================ % 
%% ===================== END DEBUG ===================== % 


%% Map Angle Correction
disp('Correcting YAW angle according to MAP ANGLE...');
anglesCorrData = anglesFiltData;
for n=1:L1
    anglesCorrData(n,4) = anglesFiltData(n,4) - INI.Sensor.MapOrientationAngle;
    if anglesCorrData(n,4) > 180
        anglesCorrData(n,4) = -360 + anglesCorrData(n,4);
    elseif anglesCorrData(n,4) < -180
        anglesCorrData(n,4) = 360 + anglesCorrData(n,4);
    end    
end


%% ======================= DEBUG ======================= % 
figN = 1;
if INI.Sensor_debug.showLocalAccel
    figure(figN);
    plot(1:L1, accelLocalData(:,2), 1:L1, accelLocalData(:,3), 1:L1, accelLocalData(:,4));
    legend('X', 'Y', 'Z');
    title('Local accelerations');
    grid on
    figN = figN + 1;
end
if INI.Sensor_debug.showInitialFilteredAngles
    figure(figN);
    plot(1:L2, anglesData(:,2), 1:L2, anglesData(:,3), 1:L2, anglesData(:,4),...
         1:L2, anglesFiltData(:,2), 1:L2, anglesFiltData(:,3), 1:L2, anglesFiltData(:,4), ...
         1:L2, anglesCorrData(:,4));
    legend('pitch', 'roll', 'yaw',...
           'pitch filtered', 'roll filtered', 'yaw filtered',...
           'yaw corrected');
    title('Initial and Filtered angles (local)');
    grid on
    figN = figN + 1;
end
%% ===================== END DEBUG ===================== % 


%% ----------------------- GENERAL CYCLE ---------------------- %%
disp('Converting from LOCAL to GLOBAL...');
accelGlobalData      = zeros(size(accelLocalData));
accelGlobalData(:,1) = accelLocalData(:,1);
for n = 1:L1
    objConvertor.angles = [anglesCorrData(n,2) anglesCorrData(n,3) anglesCorrData(n,4)];
    % Convert from Euler angles to quaternion
    objConvertor.quaternion = objConvertor.fGetQuaterionFromAngles();
    % Converting acceleration from local CS to global CS
    accelGlobalData(n, 2:4) = objConvertor.fFromLocalToGlobal(...
        [accelLocalData(n,2) accelLocalData(n,3) accelLocalData(n,4)]);
    % Transformation from RIGHT-HAND TO LEFT-HAND coordinate system
 %   accGlobal(2) = -accGlobal(2);
end



%% ======================= DEBUG ======================= % 
if INI.Sensor_debug.showGlobalAccel
    figure(figN);
    plot(1:L1, accelGlobalData(:,2), 1:L1, accelGlobalData(:,3), 1:L1, accelGlobalData(:,4));
    legend('X', 'Y', 'Z');
    title('Accelerations global');
    grid on
    figN = figN + 1;
end
%% ===================== END DEBUG ===================== % 


%% Calculate Butterworth filter coefficients
if INI.Sensor.useButterworthFilter
    % Calculate filter coefficients
    disp('Calculating Butterworth filter coefficients...');    
    [b, a] = butter(INI.Sensor.filterOrder,...
                   (2*INI.Sensor.fCutoff) / (1/INI.Sensor.samplePeriod), 'low');
    
    % Apply the Butterworth filter
    accelGlobalFiltData      = zeros(size(accelGlobalData));
    accelGlobalFiltData(:,1) = accelGlobalData(:,1);    
    accelGlobalFiltData(:,2) = filter(b, a, accelGlobalData(:,2));
    accelGlobalFiltData(:,3) = filter(b, a, accelGlobalData(:,3));
    accelGlobalFiltData(:,4) = filter(b, a, accelGlobalData(:,4) - 1);
    accelGlobalFiltData(:,4) = accelGlobalFiltData(:,4) + 1;    
else
    accelGlobalFiltData = accelGlobalData;
end


%% Calculate the module of the Global Acceleration
accelGlobalAbsFiltData      = zeros(length(accelGlobalFiltData(:,1)), 2);
accelGlobalAbsFiltData(:,1) = accelGlobalFiltData(:,1);
for n=1:L1
    %%                                         !!!!!!!! Check this with CPP !!!!!!!!!!
    accelGlobalAbsFiltData(n,2) = sqrt(accelGlobalFiltData(n,2)^2 + ...
                                       accelGlobalFiltData(n,3)^2 + ...
                                       accelGlobalFiltData(n,4)^2);
end


%% Activity Detection (later will be changed on Step Detection)
%%                                             !!!!!!!! Check this with CPP !!!!!!!!!!
isStationary = accelGlobalAbsFiltData(:,2) < INI.Sensor.stationaryTH;
if INI.Sensor_debug.showGlobalFilteredAccel
    figure(figN);
    plot(1:L1, accelGlobalFiltData(:,2), ...
         1:L1, accelGlobalFiltData(:,3), ...
         1:L1, accelGlobalFiltData(:,4),...
         1:L1, isStationary, ...
         1:L1, accelGlobalAbsFiltData(:,2), ...
         1:L1, INI.Sensor.stationaryTH * ones(L1,1));
    legend('filtered(a_x)', 'filtered(a_y)', 'filtered(a_z)', 'isStationary', ...
           'filtered(abs(a3))', 'Threshold');
    title('Filtered Global Accelerations');
    grid on
    figN = figN + 1;
end



%% ======================= DEBUG ======================= % 
% Plots the map of the office (coordinates in pixels)
if INI.Sensor_debug.showTrajectory
    mapPlotter = MAPPLOTTER(mapImage, 'Obtained trajectory (SN)');
end
%% ===================== END DEBUG ===================== % 



%% ====================== TRACKING ========================= %%
finalTime             = accelGlobalFiltData(end, 1);
timeStepN             = ceil(finalTime / INI.Sensor.timeStep);
sensorUserCoords      = zeros(timeStepN, 3);
sensorUserCoords(1,:) = [0 INI.Sensor.initialX INI.Sensor.initialY];

vel      = zeros(timeStepN, 3);
vel(1,:) = [0 0 0];
pos      = zeros(timeStepN, 3);
pos(1,:) = [0 INI.Sensor.initialX INI.Sensor.initialY];
deltaXY  = zeros(timeStepN, 3);


%% ======================= DEBUG ======================= % 
if INI.Sensor_debug.comparePositionUAOnline
    mapPlotter = MAPPLOTTER(mapImage,  mapParams.pixelSize);
    tIndex = 2;
    while accelGlobalFiltData(tIndex,1) < INI.Sensor.TdifUAandAcc
        tIndex = tIndex + 1;
    end
    UAshowIndex = 2;
    tShow = accelGlobalFiltData(tIndex-1, 1) + INI.Sensor.timeStep;
else
    tShow = 1;
end
%% ===================== END DEBUG ===================== % 

for n = 2:L1
    % Modification 1
    deltaT       = accelGlobalFiltData(n,1) - accelGlobalFiltData(n-1,1);
    pos(n,1)     = accelGlobalFiltData(n,1);
    deltaXY(n,1) = accelGlobalFiltData(n,1);
    if 1 == isStationary(n)        
        pos(n,2) = pos(n-1, 2); 
        pos(n,3) = pos(n-1, 3);        
    else
        % Coordinate axis transformation to axis of the map
        if anglesCorrData(n,4) > 0
            ang = anglesCorrData(n,4) - 180;
        else
            ang = anglesCorrData(n,4) + 180;
        end
        
        dist = INI.Sensor.userAverageVelocity * deltaT;
        addX = dist * sin(degtorad(ang));
        addY = dist * cos(degtorad(ang));
        
        deltaXY(n,2:3) = [addX addY];       
        
        pos(n,2) = pos(n-1,2) + addX; 
        pos(n,3) = pos(n-1,3) + addY;
    end
    
    
    %% ============== MapCorrector =============== %%
    if INI.correctors.mapCorrection || INI.correctors.meshCorrection
        temp = CORRECTOR.fMapCorrector(pos(n,2:3), ...
                         [INI.general.mesh2StepX INI.general.mesh2StepY],...
                          INI.general.mesh2Rows, maskTable2);
        pos(n,2:3) = temp;
    end
    
    
    %% ============== WallsCorrector =============== %%    
    if INI.correctors.wallsCorrection
%         if n>856
%             disp(['n=' num2str(n)]);
%         end
        temp1 = CORRECTOR.fWallCorrector1(pos(n-1,2:3), ...
            pos(n,2:3), INI, maskTable2);        
        pos(n,2:3) = temp1;
    end
    
    sensorUserCoords(n,:) = pos(n,:);    
    
    
    %% ============== MeshCorrector =============== %%    
    %% !!!!!!!!!!! Comment the second part of the condition for looking at 
    %  the real internal trajectory (i.e. ' %|| INI.correctors.wallsCorrection ')
    
    if INI.correctors.meshCorrection || INI.correctors.wallsCorrection
        temp2 = CORRECTOR.fMeshCorrector(sensorUserCoords(n,2:3), ...
                         [INI.general.mesh1StepX INI.general.mesh1StepY],...
                          INI.general.mesh1Rows, maskTable1);
        sensorUserCoords(n,2:3) = temp2;
    end
    
        
    %% ======================= DEBUG ======================= % 
    if INI.Sensor_debug.showTrajectory        
        mapPlotter.mPlotLine(sensorUserCoords(n-1,2:3) / mapParams.pixelSize, ...
                             sensorUserCoords(n,2:3) / mapParams.pixelSize, ...
                             'blue', 'blue', 5);
        
%         if n > 334
%             disp(['n=' num2str(n)]);
%             pause
%         end
    end
    
    % If it is time to show the position, then ...
    if (tShow - accelGlobalFiltData(n,1) < 0)
        % If it is necessary to check the UA data, then ...
        if INI.Sensor_debug.comparePositionUAOnline
            if UAshowIndex <= length(UAData(:,1))
                mapPlotter.mPlotLine(UAData(UAshowIndex-1, 2:3) / mapParams.pixelSize, ...
                                     UAData(UAshowIndex,   2:3) / mapParams.pixelSize, ...
                                     'red', 'red', 8);                                 
                mapPlotter.mPlotLine(sensorUserCoords(n-1,2:3) / mapParams.pixelSize, ...
                                       sensorUserCoords(n,2:3) / mapParams.pixelSize, ...
                                       'blue', 'blue', 5);
                pause
            end
            UAshowIndex = UAshowIndex + 1;
        end
        % This is done to draw a line on the plot
        tShow = tShow + INI.Sensor.timeStep;
    end
    %% ======================= END DEBUG ======================= % 
    
end



%% ======================= DEBUG ======================= %
if INI.Sensor_debug.comparePositionCPP        
    cppData = READER.read([pwd '\logs\output_position_sensor_cpp-' logDateTime '.txt'], INI);
    mapPlotter = MAPPLOTTER(mapImage, mapParams.pixelSize, 'Comparison Matlab vs. CPP results (SN)');
    mapPlotter.mPlotCompareMLvsCPP(sensorUserCoords, cppData);

elseif INI.Sensor_debug.comparePositionUA
    if ~INI.Sensor_debug.comparePositionUAOnline
        [UAData, uaDataAbsTs] = READER.read([pwd '\logs\output_position_ua-' logDateTime '.txt'], INI);
    end
    mapPlotter = MAPPLOTTER(mapImage, mapParams.pixelSize, 'Comparison Matlab vs. UA results (SN)');
    mapPlotter.mPlotCompareMLvsUA(sensorUserCoords, UAData, bleDataTs, uaDataAbsTs);
end
%% ===================== END DEBUG ===================== %





