%fID = fopen('logs/step_test/out.txt', 'r');
%sdkResults = textscan(fID, '%f %f %s');
fID = fopen('logs/stepnav_test2/out_stepdetect.txt', 'r');
sdkResults = textscan(fID, '%f %f %f %f %f %s %f %f %s');
fclose(fID);

stepsT      = zeros(L2, 3);
stepsT(:,1) = sdkResults{1,1};
stepsT(:,3) = sdkResults{1,2};

isStationaryT      = zeros(L2, 2);
isStationaryT(:,1) = stepsT(:,1);
isStationaryT(1,2) = 1;
k = 1;

compareFlags = ones(L1,1);

for n=2:L1
    flag = sdkResults{1,9}(n,1);
    if strcmpi(flag, 'STEP_RESET_START')        
        stepsT(n,2) = -0.5;        
        for i = k:n
            isStationaryT(i,2) = 1;
        end
        k = n;
    elseif strcmpi(flag, 'STEP_END_START')
        stepsT(n,2) = 1;
        isStationaryT(n,2) = 1;
        k = n;
    elseif strcmpi(flag, 'STEP_RESET')
        stepsT(n,2) = 0;
        for i = k : n
            isStationaryT(i,2) = 1;
        end
        k = n; 
    elseif strcmpi(flag, 'STEP_START')
        stepsT(n,2) = 2;
        k = n;
    elseif strcmpi(flag, 'STEP_NONE')
        stepsT(n,2) = -1;
        isStationaryT(n,2) = 1;
        for i = k : n
            isStationaryT(i,2) = 1;
        end
        k = n;
    elseif strcmpi(flag, 'STEP_IN')
        stepsT(n,2) = 0.5;
    end
    
    if stepsT(n,2)~=steps(n,2)
        compareFlags(n) = 0;
    end
end

% figure();
% plot(1:L1, compareFlags);
% ylim([-1 2]);
