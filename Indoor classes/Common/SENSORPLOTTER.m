classdef SENSORPLOTTER < handle
    %PLOTTER Summary of this class goes here
    %   Detailed explanation goes here      
    methods (Access = public, Static)
                
        %%
        function plotIMUReadings(signal, figureName, yLabel, leg, varargin)
            figure('units','normalized','outerposition',[0.05 0.05 .9 .9]);            
            fSize = 14;            
            limits = zeros(4,2);
            
            limits(1,:) = [0 signal(end,1)];
            
            if exist('varargin', 'var')                
                limits(2,:) = varargin{1};
                limits(3,:) = varargin{1};
                limits(4,:) = varargin{1};
            end
                
            for s=2:4
                if ~exist('varargin', 'var')
                    minVal = min(signal(:,s));
                    maxVal = max(signal(:,s));
                    if maxVal > abs(minVal)
                        delta = 0.2*maxVal;
                        limits(s,:) = [-maxVal-delta maxVal+delta];
                    else
                        delta = 0.2*abs(minVal);
                        limits(s,:) = [minVal-delta abs(minVal)+delta];
                    end
                end
                subplot(3,1,s-1)
                plot(signal(:, 1), signal(:, s));
                legend(leg{s-1});
                if s==2
                    title(figureName);
                end
                xlabel('Time, sec.');
                ylabel(yLabel);
                xlim(limits(1,:));
                ylim(limits(s,:));
                set(gca,'fontsize',fSize);
                grid on;
                grid minor;
            end
%             pause;
%             close;
        end
        
        
        %%
        function fPlotStationary(ar, threshold)
             figure('units', 'normalized', 'outerposition', [0.05 0.05 .95 .95])
             plot(ar(:,1), ar(:,2), 'r', 'DisplayName', 'a_x');
             hold on;
             plot(ar(:,1), ar(:,3), 'g', 'DisplayName', 'a_y');             
             plot(ar(:,1), ar(:,4), 'b', 'DisplayName', 'a_z' );             
             plot(ar(:,1), ar(:,5), 'LineWidth', 2, 'DisplayName', 'a_abs');             
             plot(ar(:,1), ar(:,6), 'k', 'LineWidth', 2 ,'DisplayName', 'stationary');
             plot(ar(:,1), threshold*ones(size(ar(:,1))), 'c', 'LineWidth', 1.5 ,'DisplayName', 'threshold');
             title('Stationary Periods' );
             xlabel('Time (s)' );
             ylabel('a m/s^2' );
             grid on;
             grid minor;
             legend('show');             
             pause;
             close;
        end
       
        
        %%
        function fPlotSimpleApproach (mapFigRef, coord, coord_prev)
            figure(mapFigRef);
            line([coord_prev(1) coord(1)], [coord_prev(2) coord(2)], ...
                'LineStyle', '-', 'Marker', 'o', 'MarkerFaceColor', 'red',...
                'MarkerSize', 5, 'DisplayName', 'Current coordinates');
            pause(0.01);
        end
        
        
        %%
        function fPlotStepDetector2(steps, accelGlob, isStationary)
            figure('units', 'normalized', 'outerposition', [0.05 0.05 .95 .95])
            title('Detailed Figure of Step Detector Operation');
            grid on, grid minor
            set(gca,'fontsize',12);
            hold on;
            
            % Count number of steps
            stepsCounter = 0;
            for i = 1:length(steps)
               if  steps(i,2) == 1
                   stepsCounter = stepsCounter + 1;
               end
            end
            
            plot(steps(:,1), steps(:,3), 'g', 'LineWidth', 2, 'DisplayName', 'abs(accel)');
            plot(steps(:,1), steps(:,2), 'r', 'LineWidth', 3, 'DisplayName', 'flags');
            plot(accelGlob(:,1), accelGlob(:,2), 'c', 'DisplayName', 'a_x(t)');
            plot(accelGlob(:,1), accelGlob(:,3), 'm', 'DisplayName', 'a_y(t)');
            plot(accelGlob(:,1), accelGlob(:,4), 'b', 'DisplayName', 'a_z(t)');
            plot(isStationary(:,1), .5*isStationary(:,2) + 1.3, 'k',...
                            'LineWidth', 3, 'DisplayName', 'isStationary');
            xlim([-5.5 steps(end,1)+3]);
            
            leftShift = -5;
            text(leftShift, 2,   'STEP START',       'Color','r');
            text(leftShift, 1,   'STEP END START',   'Color','r');
            text(leftShift, .5,  'STEP IN',          'Color','r');
            text(leftShift, 0,   'STEP RESET',       'Color','r');
            text(leftShift, -.5, 'STEP RESET START', 'Color','r');
            text(leftShift, -1,  'STEP NONE',        'Color','r');
            
            text(steps(length(steps), 1), 1.3, 'MOVING',   'Color', 'k');
            text(steps(length(steps), 1), 1.8, 'STANDING', 'Color', 'k');
            
            xLim = get(gca, 'xlim');            
            yLim = get(gca, 'ylim');            
            text((xLim(1)+xLim(2))/2.5, yLim(1)+0.2, ['STEPS = ', int2str(stepsCounter)],...
                 'Color', 'b', 'FontSize', 16);
            
            xlabel('Time, sec.');
            ylabel('Acceleration, grav.units');
                        
            legend('Location', 'NorthEastOutside');
%             pause, close;
        end
        
    end
end
