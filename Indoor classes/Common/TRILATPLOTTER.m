classdef TRILATPLOTTER < handle
    %PLOTTER Summary of this class goes here
    %   Detailed explanation goes here      
    methods (Access = public, Static)
        
        %%
        function fShowRSSIMap(RSSI, name)            
            %% Plotting the data
            figure('units','normalized','outerposition',[0.05 0.05 0.95 0.95])
            hold on;
            timestamps = RSSI(:,1)';  
            hold on;
            for i = 1:length(timestamps)
                    plot([timestamps(i) timestamps(i)], [0 14], 'k:');
            end
            
            % Depicting points for packets available
            sizeRSSI = size(RSSI);
            for beac = 2:sizeRSSI(2)
                for read = 1:sizeRSSI(1)
                    if RSSI(read, beac)~=0
                        scatter(RSSI(read,1), beac-1, 'filled', 'MarkerEdgeColor', [0 .5 .5], 'MarkerFaceColor',[0 .7 .7]);
                        text(RSSI(read,1), beac-1+0.3, num2str(roundn(RSSI(read,beac), -1)) , 'Color', 'black', 'FontSize', 8) ;
                    end                     
                end
            end    
            title(['\color{magenta}\fontsize{16}' name]);
            T = get(gca,'tightinset');
            set(gca,'position',[T(1) T(2) 1-T(1)-T(3) 1-T(2)-T(4)]);
            xlabel('Time, sec.');
            ylabel('Beacons number in json-file)');
            ax = gca;
            ax.XTick = ceil(min(timestamps))-1 : ceil(max(timestamps));
            ax.YTick = 0:sizeRSSI(2);
            xlim([(ceil(min(timestamps))-1) ceil(max(timestamps))]);
            ylim([0 sizeRSSI(2)]);
            ax.YGrid = 'on';
            ax.GridLineStyle = '--';
            ax.GridAlpha = 0.5;
            box on;     
            pause();
            close;
        end
        
        
        %%
        function fShowRSSITimeScale1(RSSI, name)
            figure('units','normalized','outerposition',[0.05 0.05 0.95 0.95])
            hold on;

            for i = 2:length(RSSI(1,:))
                plot(RSSI(:,1), RSSI(:,i), 'LineWidth' , 3, 'DisplayName' , ['BEAC' int2str(i-1)] );
            end
            legend('Location','northeast');
            title(['\color{magenta}\fontsize{16}' name]);
            T = get(gca,'tightinset');
            set(gca,'position',[T(1) T(2) 1-T(1)-T(3) 1-T(2)-T(4)]);
            xlabel('Time, sec.');
            ylabel('Beacons number in json-file)');
            ax = gca;
            ax.XTick = 0:ceil(max(RSSI(:,1)));
            ylim([-100 -60]);
            xlim([0 ceil(max(RSSI(:,1)))]);
            ax.YGrid = 'on';
            ax.GridLineStyle = '--';
            ax.GridAlpha = 0.5;
            box on;

            pause();
            close;
        end
        
        
        function fShowRSSITimeScale2(RSSI, beacN, name)
            figure('units','normalized', 'outerposition', [0.05 0.05 0.9 0.9])
            hold on;

            if beacN == 0
                % depict all
                for i=2 : length(RSSI(1,:))
                    plot(RSSI(:,1), RSSI(:,i), 'LineWidth' , 3, 'DisplayName' , ['BEAC' int2str(i-1)] );
                end
                legend('Location', 'northeast');
                title(['\color{black}\fontsize{16}' name]);
            else
                % depict the mentioned beacon only
                % Representation with gaps when the package is absent
                for t=1:length(RSSI(:,1))
                    if (RSSI(t,beacN+1)~=0)
                        if (t~=1)
                            if (RSSI(t-1,beacN+1)~=0)
                                line([RSSI(t-1,1) RSSI(t,1)], [RSSI(t-1,beacN+1) RSSI(t,beacN+1)], 'LineWidth', 3);
                            else
                                dt = (RSSI(t,1) - RSSI(t-1,1))*0.3;
                                line([RSSI(t,1)-dt RSSI(t,1)+dt], [RSSI(t,beacN+1) RSSI(t,beacN+1)], 'LineWidth', 3);
                            end
                        end                        
                        %'DisplayName', ['BEAC' int2str(beacN)]);
                    end
                end
                title(['\color{black}\fontsize{16}' [name ', beacon #' num2str(beacN)]]);
            end
            xlabel('Time, sec.');
            ylabel('RSSI values, dBm');
            
%             T = get(gca,'tightinset');
%             set(gca,'position',[T(1) T(2) 1-T(1)-T(3) 1-T(2)-T(4)]);
            
            ax = gca;
            ax.XTick = 0:2:ceil(max(RSSI(:,1)));
            ylim([-100 -60]);
            xlim([0 ceil(max(RSSI(:,1)))]);
            
            ax.YGrid = 'on';
            ax.GridLineStyle = '--';
            ax.GridAlpha = 0.5;
            box on;

%             pause();
%             close;
        end
        
        
        function fShowRSSIComparison(RSSIbefore, RSSIafter, beacN, name)
            figure('units','normalized', 'outerposition', [0.05 0.05 0.9 0.9])
            hold on;

            if beacN == 0
                warning('Point out the Beacon Number! #1 beacon will be used for now.');
            end
            
            % Representation BEFORE with gaps when the package is absent
            for t=1:length(RSSIbefore(:,1))
                if (RSSIbefore(t,beacN+1)~=0)
                    if (t~=1)
                        if (RSSIbefore(t-1,beacN+1)~=0)
                            lineBefRef = line([RSSIbefore(t-1,1) RSSIbefore(t,1)], [RSSIbefore(t-1,beacN+1) RSSIbefore(t,beacN+1)], ...
                                'LineWidth', 3, 'Color', 'b', 'DisplayName', 'Raw RSSI');                            
                        else
                            dt = (RSSIbefore(t,1) - RSSIbefore(t-1,1))*0.3;
                            lineBefRef = line([RSSIbefore(t,1)-dt RSSIbefore(t,1)+dt], [RSSIbefore(t,beacN+1) RSSIbefore(t,beacN+1)], ...
                                'LineWidth', 3, 'Color', 'b', 'DisplayName', 'Raw RSSI');
                        end
                    end
                end
            end
            
            % Representation AFTER with gaps when the package is absent
            for t=1:length(RSSIafter(:,1))
                if (RSSIafter(t,beacN+1)~=0)
                    if (t~=1)
                        if (RSSIafter(t-1,beacN+1)~=0)
                            lineAftRef = line([RSSIafter(t-1,1) RSSIafter(t,1)], [RSSIafter(t-1,beacN+1) RSSIafter(t,beacN+1)], ...
                                'LineWidth', 3, 'Color', 'r', 'DisplayName', 'Filtered RSSI');
                        else
                            dt = (RSSIafter(t,1) - RSSIafter(t-1,1))*0.3;
                            lineAftRef = line([RSSIafter(t,1)-dt RSSIafter(t,1)+dt], [RSSIafter(t,beacN+1) RSSIafter(t,beacN+1)], ...
                                'LineWidth', 3, 'Color', 'r', 'DisplayName', 'Filtered RSSI');
                        end
                    end
                end
            end
            
            title(['\color{black}\fontsize{16}' [name ', beacon #' num2str(beacN)]]);
            xlabel('Time, sec.');
            ylabel('RSSI values, dBm');
            legend([lineBefRef lineAftRef]);
                        
            ax = gca;
            ax.XTick = 0:2:ceil(max(RSSIafter(:,1)));
            ylim([-100 -60]);
            xlim([0 ceil(max(RSSIafter(:,1)))]);
            
            ax.YGrid = 'on';
            ax.GridLineStyle = '-';
            ax.GridAlpha = 0.5;
            box on;

%             pause();
%             close;
        end
        
        
        %%
        function mapFigRef = PlotMapWithBeacons(varargin)
            mapFigRef = figure('units', 'normalized', 'outerposition', [0.05 0.05 0.95 0.95]);
            set(gca,'XTick',[]);
            set(gca,'YTick',[]);
            imshow(varargin{1,1});
            hold on;
            if nargin > 1
                title(varargin{1,3}, 'FontSize',16);
                xlabel(['Processing ' varargin{1,3} ' ...'], 'FontSize',16 );
                pix = varargin{1,4};
                beac = varargin{1,5};
                for i = 1: length(beac)
                    plot(beac{1,i}.x/pix, beac{1,i}.y/pix, 'LineStyle', 'none',...
                        'Marker', 'o', 'MarkerFaceColor', 'yellow', 'markersize', 10,...
                        'DisplayName', strcat('B', i));
                    text(beac{1,i}.x / pix - 30, beac{1,i}.y / pix, ['B' int2str(i)], 'FontSize', 14);
                end
            end
        end
        
        
        %%
        function PlotBeaconCirclesRSSI(figRef, userCoords, BeacParam, distToUser, pix, mapImage) 
            figure(figRef);
            hold on
                        
            %% Plot Selected Beacons
            XY = zeros(length(BeacParam),2);
            
            for i = 1:length(BeacParam)
                XY(i,1) = BeacParam{1,i}.x;
                XY(i,2) = BeacParam{1,i}.y;
            end
            
            selBeac = plot(XY(:,1)/pix, XY(:,2)/pix,...
                           'LineStyle', 'none', 'Marker', 'o', ...
                           'MarkerFaceColor', 'r', 'markersize', 30);
           
            %% Plot RSSI of Selected Beacons 
            captions = zeros(length(BeacParam),1);
            
            for i = 1:length(BeacParam)
                captions(i,1) = BeacParam{1,i}.RSSI;
            end
        
            texts = text(XY(:,1)/pix-40, XY(:,2)/pix, int2str(captions(:,1)), 'FontSize', 14);
                
            circle = zeros(length(distToUser), 1);
            for i = 1:length(distToUser)
                ang=0:0.01:2*pi; 
               	xp=distToUser(i) * cos(ang);
               	yp=distToUser(i) * sin(ang);
               	circle(i) = plot((XY(i,1)+xp)/pix, (XY(i,2)+yp)/pix);
            end
            
            coords = plot(userCoords(1)/pix, userCoords(2)/pix, 'LineStyle', 'none',...
                          'Marker', 'o', 'MarkerFaceColor', 'b', 'markersize', 20);
%             xlim([1 length(mapImage(1, :, 1))]);
%             ylim([1 length(mapImage(:, 1, 1))]);
            axis tight
            
            pause();
            
            delete(selBeac);
            delete(circle);
%             delete(coords);
            delete(texts);
        end
        
    end
end
