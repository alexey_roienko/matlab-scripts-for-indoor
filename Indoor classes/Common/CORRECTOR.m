%% Author:
%  Feliks Sirenko, sirenkofelix@gmail.com
%  Eugene Chervonyak
%  Alexey Roienko, a.roienko@it-jim.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  21/05/17
%% Class definition
%  The class contains methods who check the correctness of a trajectory 
%  Namely, MAP corrections (if the point is out of map bounds)
%  MESH corrections (marker is positioned in mesh vertices only)
%  WALL corrections (defines whether the particle crosses the wall or not)

classdef CORRECTOR
    %% Public methods
    methods (Static, Access = public)
        
        %% Function provides MAP coordinates correction using "masktable.out" file
        %  which is based on mesh representation of navigation area.
        %% INPUT:
        %  rawCoords     - vector, 1x2, raw values of estimated coordinates, in meters, (X,Y)
        %  meshSteps     - vector, 1x2, values of mesh steps along two axes, in meters (X, Y)
        %  rowsN         - number of rows in masktable.out
        %  maskTable     - vector, Nx1, contains indecies for transformation of
        %                  mesh vertices according to the map mask
        %% OUTPUT:
        %  correctedXY   - vector, 1x2, corrected values of estimated coordinates, in meters, (X,Y)
        function correctedXY = fMapCorrector(rawCoords, meshParams, maskTable, meshType)
            
            if strcmpi(meshType, 'fineMesh')
                meshSteps = [meshParams.fineMesh(1) meshParams.fineMesh(2)];
                rowsN     =  meshParams.fineMesh(3);
            elseif strcmpi(meshType, 'coarseMesh')
                meshSteps = [meshParams.coarseMesh(1) meshParams.coarseMesh(2)];
                rowsN     =  meshParams.coarseMesh(3);
            else
                error('fMapCorrector: Undefined Mesh Type!');
            end
            
            % Define size of the mesh vertices for each axis
            mtSize = length(maskTable);
            nx = floor(mtSize/rowsN);
            ny = rowsN;            
            % Define minimal and maximal index values in maskTable
            lowerBoundary = [0 0];
            upperBoundary = [lowerBoundary(1) + (nx-1)*meshSteps(1) ...
                             lowerBoundary(2) + (ny-1)*meshSteps(2)];
%             disp(['upper: [', num2str(upperBoundary(1)), ', ', ...
%                   num2str(upperBoundary(2)), ']']);
            
            % Make correction if necessary on X axis coordinate
            if rawCoords(1) < lowerBoundary(1)
                correctedXY(1) = lowerBoundary(1);
            elseif rawCoords(1) > upperBoundary(1)
                correctedXY(1) = upperBoundary(1);
            else
                correctedXY(1) = rawCoords(1);
            end
            
            % Make correction if necessary on Y axis coordinate
            if rawCoords(2) < lowerBoundary(2)
                correctedXY(2) = lowerBoundary(2);
            elseif rawCoords(2) > upperBoundary(2)
                correctedXY(2) = upperBoundary(2);
            else
                correctedXY(2) = rawCoords(2);
            end
            
            % Finally, if one of the coordinates (or both of them) were
            % outside the map, do the MESH correction with the same
            % meshtable for excluding the situation when point is put on
            % the map border
%             if (rawCoords(1) <= lowerBoundary(1))||(rawCoords(2) <= lowerBoundary(2))...
%                    ||(rawCoords(1) >= upperBoundary(1))||(rawCoords(2) >= upperBoundary(2))
%                 correctedXY = CORRECTOR.fMeshCorrector(tempCoords, meshSteps, rowsN, maskTable);
%             else
%                 correctedXY = tempCoords;
%             end
        end
        
        
        %% Function provides MESH coordinates correction using "masktable.out" file
        %  which is based on mesh representation of navigation area.
        %% INPUT:
        %  rawCoords     - vector, 1x2, raw values of estimated coordinates, in meters, (X,Y)
        %  meshSteps     - vector, 1x2, values of mesh steps along two axes, in meters (X, Y)
        %  rowsN         - number of rows in masktable.out
        %  maskTable     - vector, Nx1, contains indecies for transformation of
        %                  mesh vertices according to the map mask
        %% OUTPUT:
        %  correctedXY   - vector, 1x2, corrected values of estimated coordinates, in meters, (X,Y)
        function correctedXY = fMeshCorrector(rawCoords, meshParams, maskTable, meshType)
            
            if strcmpi(meshType, 'fineMesh')
                meshSteps = [meshParams.fineMesh(1) meshParams.fineMesh(2)];
                rowsN     =  meshParams.fineMesh(3);
            elseif strcmpi(meshType, 'coarseMesh')
                meshSteps = [meshParams.coarseMesh(1) meshParams.coarseMesh(2)];
                rowsN     =  meshParams.coarseMesh(3);
            else
                error('fMapCorrector: Undefined Mesh Type!');
            end
            
            % Correct coords on Map at first
            %mapCorrXY = CORRECTOR.fMapCorrector(rawCoords, meshSteps, rowsN, maskTable);
            mapCorrXY = rawCoords;
            
            % define minimal and maximal index values in maskTable
            mtSize = length(maskTable);
            nx = floor(mtSize/rowsN);
            ny = rowsN;            
            % Define minimal and maximal index values in maskTable
            lowerBoundary = [0 0];
            
            % Find the mesh node nearest to inPos (iX = 0 .. nx-1, iY = 0..ny-1)
            nX = round((mapCorrXY(1) - lowerBoundary(1))/meshSteps(1));
            if nX<0
                nX = 0;
            elseif nX > nx-1
                nX = nx-1;
            end
            nY = round((mapCorrXY(2) - lowerBoundary(2))/meshSteps(2));
            if nY<0
                nY = 0;
            elseif nY > ny-1
                nY = ny-1;
            end            
            nXmod = nX;
            nYmod = nY;
            
            % Calculate index in maskTable
            index = int32(nXmod*ny + nYmod);
            
            % Correction stage
            value = maskTable(index+1);
            
            % Apply masktable for any case
            nXmod2 = floor(value / rowsN);
            nYmod2 = value - nXmod2*rowsN;
            correctedXY(1)  = nXmod2 * meshSteps(1);
            correctedXY(2)  = nYmod2 * meshSteps(2);
        end
        
        
        %% Function provides WALL coordinates correction using "masktable2.out" file
        %  which is based on mesh 2 representation of navigation area (smaller cells).
        %  This approach uses Wall Correction method proposed by F.Sirenko and A.Roienko.
        %% INPUT:
        %  prevCoords  - vector, 1x2, coordinates of previous estimated point, in meters, (X,Y)
        %  newCoords   - vector, 1x2, coordinates of new estimated point, in meters, (X,Y)
        %  INI         - structure, containing config parameters of a system        
        %  maskTable2  - vector, Nx1, contains indeces for transformation of
        %                  mesh vertices according to the map mask
        %% OUTPUT:
        %  correctedXY   - vector, 1x2, corrected values of estimated 
        %                  coordinates taking into account walls location, in meters, (X,Y)
        function correctedXY = fWallCorrector1(prevCoords, newCoords, meshParams, maskTable2)
            % Define meshSteps variable...
            meshSteps = [meshParams.fineMesh(1) meshParams.fineMesh(2)];
            
            % Start checking...
            if CORRECTOR.fIsWallBeingCrossed1(prevCoords, newCoords, meshSteps, meshParams.fineMesh(3), maskTable2)
                % If there's a WallCrossing, let's change new point coords
                % and try to check once more...
                if ~CORRECTOR.fIsWallBeingCrossed1(prevCoords, [prevCoords(1) newCoords(2)], ...
                        meshSteps, meshParams.fineMesh(3), maskTable2)
                    tempCoords = [prevCoords(1) newCoords(2)];
                    correctedXY = tempCoords;
                elseif ~CORRECTOR.fIsWallBeingCrossed1(prevCoords, [newCoords(1) prevCoords(2)], ...
                        meshSteps, meshParams.fineMesh(3), maskTable2)
                    tempCoords = [newCoords(1) prevCoords(2)];
                    correctedXY = tempCoords;
                else
                    correctedXY = prevCoords;                    
                end
            else
                correctedXY = newCoords;
            end
        end
        
        
        %% Function provides WALL coordinates correction using "masktable2.out" file
        %  which is based on mesh 2 representation of navigation area (smaller cells).
        %  This approach uses Wall Correction method proposed by A.Grechnev.
        %% INPUT:
        %  prevCoords  - vector, 1x2, coordinates of previous estimated point, in meters, (X,Y)
        %  newCoords   - vector, 1x2, coordinates of new estimated point, in meters, (X,Y)
        %  INI         - structure, containing config parameters of a system        
        %  maskTable2  - vector, Nx1, contains indeces for transformation of
        %                  mesh vertices according to the map mask
        %% OUTPUT:
        %  correctedXY   - vector, 1x2, corrected values of estimated 
        %                  coordinates taking into account walls location, in meters, (X,Y)
        function correctedXY = fWallCorrector2(prevCoords, newCoords, meshParams, maskTable2)
            % Define meshSteps variable...
            meshSteps = [meshParams.fineMesh(1) meshParams.fineMesh(2)];
            
            % Start checking...
            if CORRECTOR.fIsWallBeingCrossed2(prevCoords, newCoords, meshSteps, ...
                                         meshParams.fineMesh(3), maskTable2)
                % If there's a WallCrossing, let's change new point coords
                % and try to check once more...
                if ~CORRECTOR.fIsWallBeingCrossed2(prevCoords, [prevCoords(1) newCoords(2)], ...
                        meshSteps, meshParams.fineMesh(3), maskTable2)
                    tempCoords = [prevCoords(1) newCoords(2)];
                    correctedXY = tempCoords;
                elseif ~CORRECTOR.fIsWallBeingCrossed2(prevCoords, [newCoords(1) prevCoords(2)], ...
                        meshSteps, meshParams.fineMesh(3), maskTable2)
                    tempCoords = [newCoords(1) prevCoords(2)];
                    correctedXY = tempCoords;
                else
                    correctedXY = prevCoords;                    
                end
            else
                correctedXY = newCoords;
            end
        end
        
        
        %% Function checks whether the particle is on the map or not        
        %% INPUT:
        %  rawCoords - vector, 1x2, raw values of estimated coordinates, in meters, (X,Y)
        %  meshSteps - vector, 1x2, values of mesh steps along two axes, in meters (X, Y)
        %  rowsN     - number of rows in masktable.out
        %  mtSize    - number which defines the length of "masktable.out" file
        %% OUTPUT:
        %  answer    - boolean, the answer on the question in the method's header
        function answer = fIsNotOnTheMap(rawCoords, meshSteps, rowsN, mtSize)
            % Define size of the mesh vertices for each axis
            nx = floor(mtSize/rowsN);
            ny = rowsN;
            
            % Define minimal and maximal index values in maskTable
            lowerBoundary = [0 0];
            upperBoundary = [lowerBoundary(1) + (nx-1)*meshSteps(1) ...
                             lowerBoundary(2) + (ny-1)*meshSteps(2)];
            
            % Check the boundaries
            if (rawCoords(1) < lowerBoundary(1))||(rawCoords(1) > upperBoundary(1))||...
               (rawCoords(2) < lowerBoundary(2))||(rawCoords(2) > upperBoundary(2))
                answer = true;
            else
                answer = false;
            end
        end
        
        
        %% Function checks whether the particle is on the black region of the map or not
        %% INPUT:
        %  rawCoords - vector, 1x2, raw values of estimated coordinates, in meters, (X,Y)
        %  meshSteps - vector, 1x2, values of mesh steps along two axes, in meters (X, Y)
        %  rowsN     - number of rows in masktable.out
        %  maskTable - vector, Nx1, contains indecies for transformation of
        %              mesh vertices according to the map mask
        %% OUTPUT:
        %  answer    - boolean, the answer on the question in the method's header
        function answer = fIsInTheBlackRegion(rawCoords, meshSteps, rowsN, maskTable)
            % Indecies of the RAW point in 2D mesh
            nX = round(rawCoords(1) / meshSteps(1));
            nY = round(rawCoords(2) / meshSteps(2));
            
            % Calculate index in maskTable
            index = int32(nX*rowsN + nY);

            % Correction stage
            value = int32(maskTable(index+1));            
            answer = false;
            
            if value ~= index
                answer = true;            
            end
        end
        
        
        %% Function checks whether the next particle position crosses 
        %  the wall on the map or not. This is the version without conversion to pixels.
        %  This algorithm is written by F.Sirenko and A.Roienko.        
        %% INPUT:
        %  point1    - vector, 1x2, previous particle position, in meters, (X,Y)
        %  point2    - vector, 1x2, next particle position, in meters, (X,Y)
        %  meshSteps - vector, 1x2, values of mesh steps along two axes, in meters (X, Y)
        %  rowsN     - number of rows in masktable.out
        %  maskTable - vector, Nx1, contains indecies for transformation of
        %              mesh vertices according to the map mask
        %% OUTPUT:
        %  answer    - boolean, the answer on the question in the method's header
        function answer = fIsWallBeingCrossed1(point1, point2, meshSteps, rowsN, maskTable)
            
            answer   = false;
            endPoint = false;
            
            % If there is the same point
            if point1 == point2
                % Check it, just to be sure it is not in the forbidden region
                if CORRECTOR.fIsInTheBlackRegion(point2, meshSteps, rowsN, maskTable)
                    answer = true;
                end                
            else            
                % if dx > dy (45 degree boundary lines)
                if abs(point2(1)-point1(1)) > abs(point2(2)-point1(2))
                    % setting up an increment for proper pixels checking
                    incr = meshSteps(1)/2;
                    if point2(1) - point1(1) < 0
                        incr = incr * (-1);
                    end

                    % calculating the slope between two points
                    if point2(1) ~= point1(1)
                        slope = (point2(2) - point1(2)) / (point2(1) - point1(1));                   

                        % Look through the each pixel of the line between two
                        % points and check if it appears on the black region of the mask
                        for x = point1(1) : incr : point2(1)
                            if abs(slope) > 1e-10
                                % If slope does not equal to 0,...
                                tempCoordY = slope*(x-point1(1)) + point1(2);
                            else
                                tempCoordY = point1(2);
                            end
                            tempCoordX = x;                            
                            tempPoint  = [tempCoordX tempCoordY];

                            if CORRECTOR.fIsInTheBlackRegion(tempPoint, meshSteps, rowsN, maskTable)
                                answer = true;
                                break;
                            end                    
                            if x == point2(1)
                                endPoint = true;
                            end
                        end
                    else
                        error('Denominator (X coords) is equal to 0!');
                    end
                    
                    % If the end point was not checked, the check will be
                    % performed manually
                    if (~endPoint) && (~answer)
                        if CORRECTOR.fIsInTheBlackRegion(point2, meshSteps, rowsN, maskTable)
                            answer = true;
                        end
                    end

                % if dx <= dy (45 degree boundary lines)
                else
                    % setting up an increment for proper pixels checking
                    if point2(2) - point1(2) >= 0
                        incr = meshSteps(2)/2;
                    else
                        incr = -meshSteps(2)/2;
                    end

                    % calculating the slope between two points
                    if point2(2) ~= point1(2)
                        slope = (point2(1) - point1(1)) / (point2(2) - point1(2));
                        
                        % Look through the each pixel of the line between two
                        % points and check if it appears on the black region of the mask
                        for y = point1(2) : incr : point2(2)
                            if abs(slope) > 1e-10
                                % If slope does not equal to 0,...
                                tempCoordX = slope*(y-point1(2)) + point1(1);
                            else
                                % If slope==0, i.e. X coordinates are the same,...
                                tempCoordX = point1(1);      
                            end
                            tempCoordY = y;
                            tempPoint  = [tempCoordX tempCoordY];

                            if CORRECTOR.fIsInTheBlackRegion(tempPoint, meshSteps, rowsN, maskTable)
                                answer = true;
                                break;
                            end                    
                            if y == point2(2)
                                endPoint = true;
                            end
                        end                        
                    else
                        error('Denominator (Y coords) is equal to 0!');
                    end
                    
                    % If the end point was not checked, the check will be
                    % performed manually
                    if (~endPoint) && (~answer)
                        if CORRECTOR.fIsInTheBlackRegion(point2, meshSteps, rowsN, maskTable)
                            answer = true;
                        end
                    end
                end
            end
        end
        
        
        %% Function checks whether the next particle position crosses 
        %  the wall on the map or not. This is the version without conversion to pixels.
        %  This algorithm is written by A.Grechnev.
        %% INPUT:
        %  point1    - vector, 1x2, previous particle position, in meters, (X,Y)
        %  point2    - vector, 1x2, next particle position, in meters, (X,Y)
        %  meshSteps - vector, 1x2, values of mesh steps along two axes, in meters (X, Y)
        %  rowsN     - number of rows in masktable.out
        %  maskTable - vector, Nx1, contains indecies for transformation of
        %              mesh vertices according to the map mask
        %% OUTPUT:
        %  answer    - boolean, the answer on the question in the method's header
        function answer = fIsWallBeingCrossed2(point1, point2, meshSteps, rowsN, maskTable)
            
            answer = false;
            % If there is the same point
            if point1 == point2
                % Check it, just to be sure it is not in the forbidden region
                if CORRECTOR.fIsInTheBlackRegion(point2, meshSteps, rowsN, maskTable)
                    answer = true;
                end
            else
                %  Line length
                lineLen = sqrt((point2(1) - point1(1))^2 + (point2(2) - point1(2))^2);
                xi = meshSteps(1)/2;
                ksiP = 0;
                while 1
                    t = (ksiP * xi) / lineLen;
                    inPosX = point1(1) + (point2(1) - point1(1)) * t;
                    inPosY = point1(2) + (point2(2) - point1(2)) * t;

                    if (t >= 1.0)
                        if CORRECTOR.fIsInTheBlackRegion(point2, meshSteps, rowsN, maskTable)
                            answer = true;                            
                        end
                        break;
                    elseif CORRECTOR.fIsInTheBlackRegion([inPosX, inPosY], meshSteps, rowsN, maskTable)
                        answer = true;
                        break;
                    end
                    ksiP = ksiP + 1;
                end
            end
        end
        
        
        %% Function checks whether the point is on the black region of the map or not
        %  Differ from method "fIsInTheBlackRegion" in units of 'rawCoords'; 
        %  here they are in pixels.
        %% INPUT:
        %  rawCoords - vector, 1x2, raw values of estimated coordinates, in pixels, (X,Y)        
        %  mSteps    - vector, 1x2, values of mesh steps along two axes, in pixels (X,Y)
        %  rowsN     - number of rows in masktable.out
        %  maskTable - vector, Nx1, contains indecies for transformation of
        %              mesh vertices according to the map mask
        %% OUTPUT:
        %  answer    - boolean, the answer on the question in the method's header
        function answer = fIsInTheBlackRegionPix(rawCoords, mSteps, rowsN, maskTable)
            % Indecies of the RAW point in 2D mesh
            nX = round(rawCoords(1) / mSteps(1));
            nY = round(rawCoords(2) / mSteps(2));
            
            % Calculate index in maskTable
            index = int32(nX*rowsN + nY);

            % Correction stage
            value = int32(maskTable(index+1));
            answer = false;
            
            if value ~= index
                answer = true;
            end
        end
        
    end
end