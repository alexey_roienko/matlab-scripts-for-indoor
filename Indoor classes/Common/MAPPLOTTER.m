%% Author:
%  Alexey Roienko (a.roienko@it-jim.com) and 
%  Feliks Sirenko, sirenkofelix@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  21/09/17
classdef MAPPLOTTER
    %Class is intended to draw the map of the test room and additional
    %information on it, like beacons, sensor trajectory etc.
    %   Overview of main class fields and methods will be here
    
    properties (Access = private)
        mapFigRef;
        pxSize;
    end
    
    methods (Access = public)
        %% Object Initializer
        %  Draw the map as an image
        function obj = MAPPLOTTER(mapImage, varargin)
            % Draw the map as an image
            %obj.mapFigRef = figure('units', 'normalized', 'outerposition', [0.05 0.05 0.95 0.95]);
            obj.mapFigRef = figure();
%             set(gca, 'XTick', []); 
%             set(gca, 'YTick', []);
            imshow(mapImage);
%             hold on;
            
            % If the title of the map is received as the second input parameter
            switch nargin
                case 2
                    title(varargin{1}, 'FontSize', 16);
                case 3
                    obj.pxSize = varargin{1};
                    title(varargin{2}, 'FontSize', 16);
            end
        end
        
        
        %% Depicts the line and its starting and ending points on the current map
        function varargout = mPlotLine(obj, startPoint, endPoint, lineColor, markerColor, ...
                                     markerSize, dispName)
            figure(obj.mapFigRef);
            if nargout == 1
                varargout{1} = line([startPoint(1) endPoint(1)], [startPoint(2) endPoint(2)], ...
                                   'LineStyle', '-', 'Color', lineColor,...
                                   'Marker', 'o', 'MarkerFaceColor', markerColor,...
                                   'MarkerSize', markerSize, 'DisplayName', dispName);
            else
                line([startPoint(1) endPoint(1)], [startPoint(2) endPoint(2)], ...
                   'LineStyle', '-', 'Color', lineColor,...
                   'Marker', 'o', 'MarkerFaceColor', markerColor,...
                   'MarkerSize', markerSize, 'DisplayName', dispName);
            end
        end
        
        
        %% Method allows representing results obtained by Matlab and acquired by 
        %  CPP Navigator (taken from log-file)
        %  Main point is that timestamps are similar, there
        %  is almost no latency for CPP data comparing to Matlab results
        function mPlotCompareMLvsCPP(obj, mlData, cppData, videoRecord)
            
            % Start animation recording and create an avi-file
            if videoRecord
                videoRecorder = VideoWriter('Wrong calibration');
                videoRecorder.FrameRate = 40;
                open(videoRecorder);
            end

            if length(cppData(:,1)) > length(mlData(:,1))
                dataL = length(mlData(:,1));
            else
                dataL = length(cppData(:,1));
            end
            
            for t=2:dataL-1
                % Plot cppData if the previous and current points are different
                if ~((cppData(t-1,2) == cppData(t,2)) && (cppData(t-1,3) == cppData(t,3)))
                    obj.mPlotLine(cppData(t-1, 2:3)/obj.pxSize, cppData(t, 2:3)/obj.pxSize, ...
                                  'red', 'red', 8, 'SDK coords');
                end
                % Plot Matlab Data if the previous and current points are different
                if ~((mlData(t-1,2) == mlData(t,2)) && (mlData(t-1,3) == mlData(t,3)))
                    obj.mPlotLine(mlData(t-1, 2:3)/obj.pxSize, mlData(t, 2:3)/obj.pxSize,...
                                  'blue', 'blue', 5, 'MATLab coords');
                end
                axis image
                
                if videoRecord
                    currFrame = getframe(gcf);
                    writeVideo(videoRecorder, currFrame); 
                end
                
%                 str = sprintf('n=%d, t = %.3f, ML = (%.3f; %.3f), UA = (%.3f; %.3f)\n', ...
%                     t, cppData(t,1), mlData(t,2:3), cppData(t,2:3));
%                 disp(str);
%                 pause();
            end
            
            % Plot uaData and receive a reference to the line
            cppRef = obj.mPlotLine(cppData(dataL-1, 2:3)/obj.pxSize, cppData(dataL, 2:3)/obj.pxSize, ...
                                   'red', 'red', 8, 'SDK coords');
            % Plot Matlab Data and receive a reference to the line
            mlRef = obj.mPlotLine(mlData(dataL-1, 2:3)/obj.pxSize, mlData(dataL, 2:3)/obj.pxSize,...
                                  'blue', 'blue', 5, 'MatLab coords');
            axis image
            
            if videoRecord
                currFrame = getframe(gcf);
                writeVideo(videoRecorder, currFrame); 
                close(videoRecorder);
            end
            
            legend([cppRef mlRef], 'Location', 'SouthEast');
        end
        
        
        %% Method allows representing results obtained by Matlab and acquired by 
        %  CPP Navigator (taken from log-file)
        %  Main point is that timestamps are similar, there
        %  is almost no latency for CPP data comparing to Matlab results
        function mPlotCPPresults(obj, cppData, videoRecord)
            
            % Start animation recording and create an avi-file
            if videoRecord
                videoRecorder = VideoWriter('Wrong calibration');
                videoRecorder.FrameRate = 40;
                open(videoRecorder);
            end

            dataL = length(cppData(:,1));
            
            for t=2:dataL-1
                % Plot cppData if the previous and current points are different
                if ~((cppData(t-1,2) == cppData(t,2)) && (cppData(t-1,3) == cppData(t,3)))
                    obj.mPlotLine(cppData(t-1, 2:3)/obj.pxSize, cppData(t, 2:3)/obj.pxSize, ...
                                  'red', 'red', 8, 'SDK coords');
                end
                axis image
                
                if videoRecord
                    currFrame = getframe(gcf);
                    writeVideo(videoRecorder, currFrame); 
                end
                
%                 str = sprintf('n=%d, t = %.3f, ML = (%.3f; %.3f), UA = (%.3f; %.3f)\n', ...
%                     t, cppData(t,1), mlData(t,2:3), cppData(t,2:3));
%                 disp(str);
%                 pause();
            end
            
            % Plot uaData and receive a reference to the line
            cppRef = obj.mPlotLine(cppData(dataL-1, 2:3)/obj.pxSize, cppData(dataL, 2:3)/obj.pxSize, ...
                                   'red', 'red', 8, 'SDK coords');
            axis image
            
            if videoRecord
                currFrame = getframe(gcf);
                writeVideo(videoRecorder, currFrame); 
                close(videoRecorder);
            end
            
            legend(cppRef, 'Location', 'SouthEast');
        end
        
        
        %% Method allows representing results obtained by Matlab and acquired by 
        %  UserApp (taken from log-file)
        %  Main point is that timestamps are likely to be different, there
        %  is a latency for UserApp data comparing to Matlab results
        function mPlotCompareMLvsUA(obj, mlData, uaData, mlDataTs, uaDataTs)
            
            %% Calculate delay of uaData comparing to mData
            timeDif = (uaDataTs - mlDataTs)/1000;
            if ~(timeDif >= 0)
                error('Difference between data timestamps is negative!');
            end
            
            figure(obj.mapFigRef);
            hold on;
            mlDataIndex = 1;
            for t=1:length(uaData(:,1))
                currentTS = uaData(t,1) + timeDif;
                while (mlDataIndex <= length(mlData(:,1))) && (mlData(mlDataIndex, 1) < currentTS)
                    mlDataIndex = mlDataIndex + 1;
                end
                
                if mlDataIndex >= length(mlData(:,1))
                    break;
                end
                
                
                if t~=1
                    % Plot uaData
                    obj.mPlotLine(uaData(t-1, 2:3)/obj.pxSize, uaData(t, 2:3)/obj.pxSize, ...
                                  'red', 'red', 8, 'SDK coords');
                    % Plot mlData
                    obj.mPlotLine(prevMlPoint/obj.pxSize, mlData(mlDataIndex-1, 2:3)/obj.pxSize,...
                                  'blue', 'blue', 5, 'MATLab coords');                    
                else
                    % Plot uaData
                    obj.mPlotLine(uaData(t, 2:3)/obj.pxSize, uaData(t, 2:3)/obj.pxSize, ...
                                  'red', 'red', 8, 'SDK coords');
                    % Plot mlData
                    obj.mPlotLine(mlData(mlDataIndex-1, 2:3)/obj.pxSize, mlData(mlDataIndex-1, 2:3)/obj.pxSize,...
                                  'blue', 'blue', 5, 'MATLab coords');
                end
                axis image
                                
                if mlDataIndex == 1
                    prevMlPoint = mlData(mlDataIndex, 2:3);
                else
                    prevMlPoint = mlData(mlDataIndex-1, 2:3);
                end
                
                pause
            end
        end
        
    end
end




