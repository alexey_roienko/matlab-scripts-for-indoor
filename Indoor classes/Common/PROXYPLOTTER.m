classdef PROXYPLOTTER
    methods (Access = public, Static)
        
        function PlotAllTrajectories(edges, pixelSize, color, shouldDelete, size, titleStr)
            edgesN    = length(edges);
            plotArray = zeros(edgesN,1);
            textArray = zeros(edgesN,1);
            
            for i = 1:edgesN
                plotArray(i) = plot([edges{1,i}.beac1X edges{1,i}.beac2X]./pixelSize,...
                    [edges{1,i}.beac1Y edges{1,i}.beac2Y]./pixelSize, 'Color', color, 'LineWidth',size);
                if edges{1,i}.beac1X < edges{1,i}.beac2X
                    x = edges{1,i}.beac1X + (edges{1,i}.beac2X - edges{1,i}.beac1X) / 2;
                else
                    x = edges{1,i}.beac2X + (edges{1,i}.beac1X - edges{1,i}.beac2X) / 2;
                end
                if edges{1,i}.beac1Y < edges{1,i}.beac2Y
                    y = edges{1,i}.beac1Y + (edges{1,i}.beac2Y - edges{1,i}.beac1Y) / 2;
                else
                    y = edges{1,i}.beac2Y + (edges{1,i}.beac1Y - edges{1,i}.beac2Y) / 2;
                end
                
                textArray(i) = text(x/pixelSize-50, y/pixelSize-20, ['Line ' num2str(edges{1,i}.edgeN) ...
                    '(' num2str(edges{1,i}.beac1N) '  ' num2str(edges{1,i}.beac2N) '  ' ...
                        sprintf('%.2f', edges{1,i}.dist) ')'], 'Color' , 'blue', 'FontSize', 8);                
%                 pause(0.1);
            end
%             pause();
            if shouldDelete
                delete(plotArray);
                delete(textArray);
            end
            
            title(titleStr);
        end
        
        
        function PlotSelectedCut(graph, beacons, pix)
            textArray = zeros(length(beacons),2);
            plotCut = plot([graph.beac1X graph.beac2X]./pix,...
                           [graph.beac1Y graph.beac2Y]./pix, 'r', 'LineWidth', 3 );
            for i = 1:length(beacons)
                textArray(i,1) = text(beacons{1,i}.x / pix - 20, beacons{1,i}.y / pix - 60, ...
                    num2str(beacons{1,i}.RSSI(beacons{1,i}.RSSI ~= -inf)), 'Color' , 'red', 'FontSize', 10);
                textArray(i,2) = text(beacons{1,i}.x / pix - 20, beacons{1,i}.y / pix - 30, ...
                    num2str(beacons{1,i}.relRSSI(beacons{1,i}.relRSSI ~= -inf)), 'Color' , 'red', 'FontSize', 10);
            end
%             pause();
            delete(plotCut);
            delete(textArray);
        end
        
        
        function PlotCirclesAndCrossings(beac, allCross, trueCross, pix)
            circle = zeros(length(beac), 1);
            for i = 1:length(beac)
                ang=0:0.01:2*pi; 
               	xp=beac{1,i}.dist * cos(ang);
               	yp=beac{1,i}.dist * sin(ang);
               	circle(i) = plot((beac{1,i}.x+xp)/pix, (beac{1,i}.y+yp)/pix);
            end
            allCrossAr = zeros(length(allCross), 1);
            for i = 1 : length(allCross)
               allCrossAr(i) = plot(allCross{1,i}(1,1)/pix, allCross{1,i}(1,2)/pix,...
                                    'Marker', 'o', 'MarkerFaceColor', 'b', 'MarkerSize', 3);               
            end
            trueCrossAr = zeros(length(trueCross), 1);
            for i = 1 : length(trueCross)
               trueCrossAr(i) = plot(trueCross{1,i}(1,1)/pix, trueCross{1,i}(1,2)/pix,...
                                    'Marker', 'o', 'MarkerFaceColor', 'g', 'MarkerSize', 5);
            end
            pause();
            delete(allCrossAr);
            delete(trueCrossAr);
            delete(circle);            
        end
        
        
        function PlotPositionsAndNumbers(coords, pix, step)
            if ~isnan(coords)
                userCoords = plot(coords(1,1)/pix, coords(1,2)/pix,...
                    'Marker', 'o', 'MarkerFaceColor', 'r', 'MarkerSize', 16);
                texts = text(coords(1,1)/pix - 20, coords(1,2)/pix, num2str(step),...
                    'Color', 'y', 'FontSize', 10);
%                  pause();
%                  delete(userCoords);
%                  delete(texts);
            end
        end
    end
end

