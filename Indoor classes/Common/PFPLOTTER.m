classdef PFPLOTTER < handle
    %PLOTTER Summary of this class goes here
    %   Detailed explanation goes here      
    methods (Access = public, Static)
                
        %%
        function fPlotOnMapPF(PFuserCoords, bleCoords, pixelSize, ...
                              particlesPrev, varargin)
                          
            metrics = plot(PFuserCoords(:,1)/pixelSize, PFuserCoords(:,2)/pixelSize,...
                              'LineStyle', 'none', 'Marker', 'o', ...
                              'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'r',...
                              'MarkerSize', 11, 'DisplayName', 'User position');
            
            beacon  = plot(bleCoords(1)/pixelSize, bleCoords(2)/pixelSize,...
                             'LineStyle', 'none', 'Marker', 'o', ...
                             'MarkerFaceColor', 'c', 'MarkerEdgeColor', 'k', ...
                             'MarkerSize', 8, 'DisplayName', 'BLE position');
            
            switch length(varargin)
                case 1
                    particlesAlived = varargin{1}(:,:);
                    alivP   = plot(particlesAlived(:,1)/pixelSize, particlesAlived(:,2)/pixelSize,...
                                  'LineStyle', 'none', 'Marker', 'o', ...
                                  'MarkerFaceColor', 'g', 'MarkerEdgeColor', 'k', ...
                                  'MarkerSize', 5, 'DisplayName', 'New particles');
                case 2
                    particlesAlived = varargin{1}(:,:);
                    particlesAllNew = varargin{2}(:,:);
                    allNewP = plot(particlesAllNew(:,1)/pixelSize, particlesAllNew(:,2)/pixelSize,...
                                  'LineStyle', 'none', 'Marker', 'o', ...
                                  'MarkerFaceColor', 'b', 'MarkerEdgeColor', 'k', ...
                                  'MarkerSize', 7, 'DisplayName', 'All new particles');
                    alivP   = plot(particlesAlived(:,1)/pixelSize, particlesAlived(:,2)/pixelSize,...
                                  'LineStyle', 'none', 'Marker', 'o', ...
                                  'MarkerFaceColor', 'g', 'MarkerEdgeColor', 'g', ...
                                  'MarkerSize', 4, 'DisplayName', 'Alived particles');
            end
            
            prevP = plot(particlesPrev(:,1)/pixelSize, particlesPrev(:,2)/pixelSize,...
                          'LineStyle', 'none', 'Marker', 'o', ...
                          'MarkerFaceColor', 'm', 'MarkerEdgeColor', 'k', ...
                          'MarkerSize', 5, 'DisplayName', 'Old particles');
            
%             caption = text(200, 1800, ['t = ',num2str(round(time, 2)), 'c'], ...
%                            'Color', 'red', 'FontSize', 10);
            axis image
            
            switch length(varargin)
                case 1
                    legend([alivP prevP metrics beacon], 'Location', 'SouthWest');
                case 2
                    legend([allNewP alivP prevP metrics beacon], 'Location', 'SouthWest');
                otherwise
                    legend([prevP metrics beacon], 'Location', 'SouthWest');
            end
            
            pause();
            
            switch length(varargin)
                case 1
                    delete(alivP);
                case 2
                    delete(alivP);
                    delete(allNewP);
            end
            
            delete(prevP);
            delete(beacon);
            delete(metrics);
%             delete(caption);
        end
                
    end
end
