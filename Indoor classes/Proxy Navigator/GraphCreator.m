classdef GraphCreator < handle
%GRAPHCREATOR Summary of this class goes here
%   Detailed explanation goes here
    
methods (Access = public)
    %% Object Constructor
    function obj = GraphCreator()
    end


    %% Method gets the vertices and the edges of the graph,
    %  who describes the available trajectories for a user
    function finalEdges = getGraph(obj, beacons, meshParams, maskTable2, maxLength, CRIT_ANGLE)
        
        %% mesh2Rows, maskTable2, meshSteps,
        
        % STAGE 1: SEARCH FOR ALL POSSIBLE EDGES WITHOUT REPETITIONS, BELOW THE LIMIT
        initialEdges = obj.getAllEdges(beacons, meshParams, maskTable2, maxLength);
%         NewPlotterAlex.PlotAllTrajectories(edges, pix, 'red', 1, 1, '');        


        % STAGE 2: SORT edges BY THE distance in descent order
        sortedEdges = obj.sortFoundEdges(initialEdges);
%         NewPlotterAlex.PlotAllTrajectories(edges, pix, 'green', 0, 1, '');
        

        % STAGE 3: FIND NEIGHBOURS BY VERTEX
        % In the array "neighbours" in every cell you see the numbers
        % of vertexes that are in contact with the base vertex
        neighbours = obj.getNeighbourVertexes(sortedEdges, length(beacons));

        % DebugInfo
%         for i=1:length(neighbours)
%             clearVarMessage = ['Vertex: ', num2str(i), '   ', 'Neighbours: '];
%             for j=1:length(neighbours{1,i})
%                 clearVarMessage = strcat(clearVarMessage, num2str(neighbours{1,i}(j)), '-');
%             end
%             disp(clearVarMessage);
%             clearvars message;
%         end
        % EndDebug


        % STAGE 4: BUILD TRIANGLES
        edgesN = length(sortedEdges);
        triangleN = 1;
        k = 1;
        triangles = {};
        for edgeN = 1:edgesN
            sortedEdges{1,edgeN}.discarded = false;
        end

        % Forming triangles
        for edgeN = 1:edgesN
            trVertex1 = sortedEdges{1,edgeN}.beac1N;
            trVertex2 = sortedEdges{1,edgeN}.beac2N;
            
            neighbours1 = neighbours{1, trVertex1};
            neighbours2 = neighbours{1, trVertex2};
            trVerteces3 = intersect(neighbours1, neighbours2);
            
            if ~isempty(trVerteces3)
                for i = 1:length(trVerteces3)
                    if ~obj.isTriangleSideDisabled(sortedEdges, [trVertex1 trVertex2 trVerteces3(i)])
                        % Calculate the length of each triangle side 
                        % Edge 'ij'
                        side12 = sqrt((beacons{1,trVertex1}.x - beacons{1,trVertex2}.x)^2 + ...
                                      (beacons{1,trVertex1}.y - beacons{1,trVertex2}.y)^2);
                        % Edge 'ik'
                        side13 = sqrt((beacons{1,trVertex1}.x - beacons{1,trVerteces3(i)}.x)^2 + ...
                                      (beacons{1,trVertex1}.y - beacons{1,trVerteces3(i)}.y)^2);
                        % Edge 'jk'
                        side23 = sqrt((beacons{1,trVertex2}.x - beacons{1,trVerteces3(i)}.x)^2 + ...
                                      (beacons{1,trVertex2}.y - beacons{1,trVerteces3(i)}.y)^2);
                        % First contiguous to side12 angle
                        angleBetween12And13 = 180/pi * acos((side12^2 + side13^2 - side23^2) / (2 * side12 * side13));
                        % Second contiguos to side12 angle
                        angleBetween12And23 = 180/pi * acos((side12^2 + side23^2 - side13^2) / (2 * side12 * side23));
                        % Only opposite to side12 angle
                        angleBetween13And23 = 180/pi * acos((side13^2 + side23^2 - side12^2) / (2 * side13 * side23));

                        triangles{1,k} = [trVertex1, trVertex2, trVerteces3(i),...
                                          side12, side13, side23,...
                                          angleBetween12And13, angleBetween12And23, angleBetween13And23];
                        k = k + 1;
                    end
                end
            end
            
            if (~isempty(triangles)) && obj.shouldEdgeBeDiscarded(triangles, CRIT_ANGLE)
                sortedEdges{1,edgeN}.discarded = true;                
            end
            k = 1;
            triangles = {};
        end

        % Formatting the output
        finalEdges = {};
        k = 1;
        for i = 1:edgesN
            if ~sortedEdges{1,i}.discarded
                finalEdges{1,k} = sortedEdges{1,i};
                k = k + 1;
            end
        end
    end
    
	
	%%
    function output = signBeaconsCoords(obj, initGraph, beacParams, debug)
        edgesN = length(initGraph);
        maxBeacN = length(beacParams);
        
        output = cell(1, edgesN);
        for en=1:edgesN
            edge = initGraph{1,en};
            if ~((edge.beac1N > maxBeacN) || (edge.beac2N > maxBeacN))
                beac1 = beacParams{1, edge.beac1N};
                beac2 = beacParams{1, edge.beac2N};

                edge.beac1X = beac1.x;   edge.beac1Y = beac1.y;
                edge.beac2X = beac2.x;   edge.beac2Y = beac2.y;

                dx = beac1.x - beac2.x;
                dy = beac1.y - beac2.y;

                edge.angle = calcEdgeAngle(obj, dx, dy, edge.dist);

                if debug
                    dist = sqrt(dx^2 + dy^2);
                    disp('================== CHECK GRAPH DIST READ ===================');
                    fprintf('Calculated Dist = %.3f, from file = %.3f\n\n', dist, edge.dist);
                end

                output{1,en} = edge;
            else
                %error('GraphCreatorG.signBeaconsCoords: unknown BEACON number!');
                
                edge.beac1X = 0;   edge.beac1Y = 0;
                edge.beac2X = 0;   edge.beac2Y = 0;

                dx = beac1.x - beac2.x;
                dy = beac1.y - beac2.y;

                edge.angle = 0;
                
                if debug
                    dist = 0;
                    disp('================== CHECK GRAPH DIST READ ===================');
                    fprintf('Calculated Dist = %.3f, from file = %.3f\n\n', dist, edge.dist);
                end

                output{1,en} = edge;
            end
        end
    end
	
    
    % Description: Given vertices and edge connections for a graph (represented by
    %       either adjacency matrix or edge list) and edge costs that are greater
    %       than zero, this function computes the shortest path from one or more
    %       starting nodes to one or more termination nodes.
    %
    % Author:
    %       Joseph Kirk
    %       jdkirk630@gmail.com
    %
    % Date: 02/27/15
    %
    % Release: 2.0
    %
    % Inputs:
    %     [AorV] Either A or V where
    %         A   is a NxN adjacency matrix, where A(I,J) is nonzero (=1)
    %               if and only if an edge connects point I to point J
    %               NOTE: Works for both symmetric and asymmetric A
    %         V   is a Nx2 (or Nx3) matrix of x,y,(z) coordinates
    %     [xyCorE] Either xy or C or E (or E3) where
    %         xy  is a Nx2 (or Nx3) matrix of x,y,(z) coordinates (equivalent to V)
    %               NOTE: only valid with A as the first input
    %         C   is a NxN cost (perhaps distance) matrix, where C(I,J) contains
    %               the value of the cost to move from point I to point J
    %               NOTE: only valid with A as the first input
    %         E   is a Px2 matrix containing a list of edge connections
    %               NOTE: only valid with V as the first input
    %         E3  is a Px3 matrix containing a list of edge connections in the
    %               first two columns and edge weights in the third column
    %               NOTE: only valid with V as the first input
    %     [SID] (optional) 1xL vector of starting points
    %         if unspecified, the algorithm will calculate the minimal path from
    %         all N points to the finish point(s) (automatically sets SID = 1:N)
    %     [FID] (optional) 1xM vector of finish points
    %         if unspecified, the algorithm will calculate the minimal path from
    %         the starting point(s) to all N points (automatically sets FID = 1:N)
    %     [showWaitbar] (optional) a scalar logical that initializes a waitbar if nonzero
    %
    % Outputs:
    %     [costs] is an LxM matrix of minimum cost values for the minimal paths
    %     [paths] is an LxM cell array containing the shortest path arrays
    %
    % Note:
    %     If the inputs are [A,xy] or [V,E], the cost is assumed to be (and is
    %       calculated as) the point-to-point Euclidean distance
    %     If the inputs are [A,C] or [V,E3], the cost is obtained from either
    %       the C matrix or from the edge weights in the 3rd column of E3
    %
    % Usage:
    %     [costs,paths] = dijkstra(A,xy)
    %         -or-
    %     [costs,paths] = dijkstra(A,C)
    %         -or-
    %     [costs,paths] = dijkstra(V,E)
    %         -or-
    %     [costs,paths] = dijkstra(V,E3)
    %         -or-
    %     [costs,paths] = dijkstra( ... ,SID,FID)
    %         -or-
    %     [costs,paths] = dijkstra( ... ,SID,FID,true)
    %
    function [costs,paths] = Dijkstra(obj, AorV, xyCorE, SID, FID, showWaitbar)            
        narginchk(3,6);

        % Process inputs
        [n,nc] = size(AorV);
        [m,mc] = size(xyCorE);
        if (nargin < 4)
            SID = (1:n);
        elseif isempty(SID)
            SID = (1:n);
        end
        L = length(SID);
        if (nargin < 5)
            FID = (1:n);
        elseif isempty(FID)
            FID = (1:n);
        end
        M = length(FID);
        if (nargin < 6)
            showWaitbar = (n > 1000 && max(L,M) > 1);
        end

        % Error check inputs
        if (max(SID) > n || min(SID) < 1)
            eval(['help ' mfilename]);
            error('Invalid [SID] input. See help notes above.');
        end
        if (max(FID) > n || min(FID) < 1)
            eval(['help ' mfilename]);
            error('Invalid [FID] input. See help notes above.');
        end
        [E,cost] = process_inputs(AorV,xyCorE);

        % Reverse the algorithm if it will be more efficient
        isReverse = false;
        if L > M
            E = E(:,[2 1]);
            cost = cost';
            tmp = SID;
            SID = FID;
            FID = tmp;
            isReverse = true;
        end

        % Initialize output variables
        L = length(SID);
        M = length(FID);
        costs = zeros(L,M);
        paths = num2cell(NaN(L,M));

        % Create a waitbar if desired
        if showWaitbar
            hWait = waitbar(0,'Calculating Minimum Paths ... ');
        end

        % Find the minimum costs and paths using Dijkstra's Algorithm
        for k = 1:L
            % Initializations
            iTable = NaN(n,1);
            minCost = Inf(n,1);
            isSettled = false(n,1);
            path = num2cell(NaN(n,1));
            I = SID(k);
            minCost(I) = 0;
            iTable(I) = 0;
            isSettled(I) = true;
            path(I) = {I};

            % Execute Dijkstra's Algorithm for this vertex
            while any(~isSettled(FID))
                % Update the table
                jTable = iTable;
                iTable(I) = NaN;
                nodeIndex = find(E(:,1) == I);

                % Calculate the costs to the neighbor nodes and record paths
                for kk = 1:length(nodeIndex)
                    J = E(nodeIndex(kk),2);
                    if ~isSettled(J)
                        c = cost(I,J);
                        empty = isnan(jTable(J));
                        if empty || (jTable(J) > (jTable(I) + c))
                            iTable(J) = jTable(I) + c;
                            if isReverse
                                path{J} = [J path{I}];
                            else
                                path{J} = [path{I} J];
                            end
                        else
                            iTable(J) = jTable(J);
                        end
                    end
                end

                % Find values in the table
                K = find(~isnan(iTable));
                if isempty(K)
                    break
                else
                    % Settle the minimum value in the table
                    [~,N] = min(iTable(K));
                    I = K(N);
                    minCost(I) = iTable(I);
                    isSettled(I) = true;
                end
            end

            % Store costs and paths
            costs(k,:) = minCost(FID);
            paths(k,:) = path(FID);
            if showWaitbar && ~mod(k,ceil(L/100))
                waitbar(k/L,hWait);
            end
        end
        if showWaitbar
            delete(hWait);
        end

        % Reformat outputs if algorithm was reversed
        if isReverse
            costs = costs';
            paths = paths';
        end

        % Pass the path as an array if only one source/destination were given
        if L == 1 && M == 1
            paths = paths{1};
        end
        		
		
        function [E,C] = process_inputs(AorV,xyCorE)
            if (n == nc)
                if (m == n)
                    A = AorV;
                    A = A - diag(diag(A));
                    if (m == mc)
                        % Inputs = (A,cost)
                        C = xyCorE;
                        E = obj.a2e(A);
                    else
                        % Inputs = (A,xy)
                        xy = xyCorE;
                        E = obj.a2e(A);
                        D = obj.ve2d(xy,E);
                        C = sparse(E(:,1),E(:,2),D,n,n);
                    end
                else
                    eval(['help ' mfilename]);
                    error('Invalid [A,xy] or [A,C] inputs. See help notes above.');
                end
            else
                if (mc == 2)
                    % Inputs = (V,E)
                    V = AorV;
                    E = xyCorE;
                    D = obj.ve2d(V,E);
                    C = sparse(E(:,1),E(:,2),D,n,n);
                elseif (mc == 3)
                    % Inputs = (V,E3)
                    E3 = xyCorE;
                    E = E3(:,1:2);
                    C = sparse(E(:,1),E(:,2),E3(:,3),n,n);
                else
                    eval(['help ' mfilename]);
                    error('Invalid [V,E] or [V,E3] inputs. See help notes above.');
                end
            end
        end
    end
end


methods (Access = private)
    
    function edges = getAllEdges(obj, beacons, meshParams, maskTable2, distLim)
        
        %% Consider 'fineMesh'
        meshSteps = [meshParams.fineMesh(1) meshParams.fineMesh(2)];
        meshRowsN     =  meshParams.fineMesh(3);
        
        bN = length(beacons);
        edgeIndex = 1;
        edges = {};

        for i=1:bN
            beacBase = beacons{1,i};
            for j = 1:bN
                beacRef = beacons{1,j};
                % Does edge exist?
                if (i~=j) && ~CORRECTOR.fIsWallBeingCrossed2([beacBase.x beacBase.y], [beacRef.x beacRef.y], ...
                                                              meshSteps, meshRowsN, maskTable2)
                    dx = beacRef.x - beacBase.x;
                    dy = beacRef.y - beacBase.y;
                    dist = sqrt(dx^2 + dy^2);
                    % Is the edge less than the limiter?
                    if dist < distLim
                        % Does array edges contain the same edge?
                        doesContain = false;
                        if ~isempty(edges)
                            for k=1:length(edges)
                                if (i == edges{1,k}.beac2N) && (j == edges{1,k}.beac1N)
                                    doesContain = true;
                                    break;
                                end
                            end
                        end

                        % If we do not have the edge, we add it to array
                        if ~doesContain                            
                            edge.beac1N = i;
                            edge.vert1N = beacons{1,i}.vertexN;
                            edge.beac2N = j;
                            edge.vert2N = beacons{1,j}.vertexN;
                            edge.beac1X = beacBase.x;
                            edge.beac1Y = beacBase.y;
                            edge.beac2X = beacRef.x;
                            edge.beac2Y = beacRef.y;
                            edge.dist   = dist;
                            
                            edge.angle   = obj.calcEdgeAngle(dx, dy, dist);
                            edge.edgeN   = edgeIndex;
                            edge.enabled = true;
                            
                            edges{1,edgeIndex}   = edge;
                            edgeIndex = edgeIndex + 1;
                        end
                    end
                end
            end
        end
    end

    
    %% Function calculates the angle of the edge in the coordinate system 
    %  where the Y axis is directed to the South (because of the image coordinate system)
    function angle = calcEdgeAngle(~, dx, dy, dist)
        if dx ~= 0
            tempAngle = (180/pi) * asin(dy/dist);
            %           TWO=-90
            %              |
            %  angle < 0   |   angle < 0
            %   TWO=0-----ONE------TWO=0
            %  angle > 0   |   angle > 0
            %              |
            %           TWO=90
            if (dx<0) && (dy>0)
                angle = 180 - tempAngle;
            elseif (dx<0) && (dy<0)
                angle = -180 - tempAngle;
            else
                angle = tempAngle;
            end
        else
            if dy > 0
                angle = 90;
            else
                angle = -90;
            end
        end
    end
    
    
    function output = sortFoundEdges(~, edges)
        eN = length(edges);
        distArray = zeros(1, eN);
        sortedEdges = cell(1, eN);

        for i=1:eN
            distArray(i) = edges{1,i}.dist;
        end

        [~, sortedEdgesNumbs] = sort(distArray, 'descend');

        for i=1:eN
           sortedEdges{1,i} = edges{1, sortedEdgesNumbs(i)};
           sortedEdges{1,i}.edgeN = i;
        end
        output = sortedEdges;
    end

    
    function output = getNeighbourVertexes(~, edges, beaconsN)
        neighbours = cell(1, beaconsN);
        edgesN = length(edges);
        k = 1;
        neighbour = [];

        for i = 1:beaconsN
            for j = 1:edgesN
                if edges{1,j}.beac1N == i
                    neighbour(k) = edges{1,j}.beac2N;
                    k = k + 1;
                elseif edges{1,j}.beac2N == i
                    neighbour(k) = edges{1,j}.beac1N;
                    k = k + 1;
                end
            end
            
            if k>1
                neighbours{1,i} = neighbour;
            end

            k = 1;
            neighbour = [];
        end
        output = neighbours;
    end
    
    
    function result = isTriangleSideDisabled(~, sortedEdges, trVerteces)
        result = false;
        
        for eN = 1:length(sortedEdges)
            edge = sortedEdges{eN};
            if ((edge.beac1N == trVerteces(1)) || (edge.beac1N == trVerteces(2)) || (edge.beac1N == trVerteces(3))) &&...
               ((edge.beac2N == trVerteces(1)) || (edge.beac2N == trVerteces(2)) || (edge.beac2N == trVerteces(3))) &&...
                (edge.discarded == true)
           
                result = true;
                break;
            end
        end
    end
    
    
    function result = shouldEdgeBeDiscarded(~, triangles, angleTH)
        % triangles{1,tr} = [trVertex1, trVertex2, trVerteces3(i),...
        %                    side12, side13, side23,...
        %                    angleBetween12And13, angleBetween12And23, angleBetween13And23];
        result = false;        
        trN = length(triangles);
        for tr = 1:trN
            sides = triangles{1,tr}(4:6);
            [~, maxSideIndex] = max(sides);
            
            if maxSideIndex==1
                angles = triangles{1,tr}(7:9);
                if abs(angleTH) > 1e-9
                    if (angles(1) < angleTH) || (angles(2) < angleTH) %% || (angles(3) < angleTH)
                        result = true;
                    end
                else
                    result = true;
                end
            end
        end
    end
    
end
end

