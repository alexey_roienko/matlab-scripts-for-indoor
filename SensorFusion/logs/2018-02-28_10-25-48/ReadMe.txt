The device is flat on the table and initially directed to the west.
After each 5 seconds the device is turned 90 degrees around z-axis (yaw) in the following order:
-90 (west) >> 0 (north) >> 90 (east) >> 0 (north) 
None of other angles are changed during the recording.