Rectilinear motion. The device is in the hand. Portrait orientation.
Azimuth is about 150 degrees clockwise and is kept constant during motion. 
Pitch is about 45 degrees clockwise.
Roll is about 45 degrees clockwise.
The user did about 20 steps +-1 step.
Angle measurement error +-5 degrees.