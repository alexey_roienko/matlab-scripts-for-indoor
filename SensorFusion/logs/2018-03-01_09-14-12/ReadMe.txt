Rectilinear motion. The device is in the hand. Portrait orientation.
Azimuth is about 30 degrees counterclockwise and is kept constant during motion. 
Pitch is about 80 degrees clockwise.
Roll is about 0 degrees.
The user did about 20 steps +-1 step.
Angle measurement error +-5 degrees.