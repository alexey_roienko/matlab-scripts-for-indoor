The device is flat on the table and directed to the west.
After each 5 seconds the device is turned 90 degrees around y-axis (roll) in the following order:
0 >> 90 (clockwise) >> 0 (counterclockwise) >> -90 (counterclockwise) 
None of other angles are changed during the recording.