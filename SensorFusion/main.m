clc, clear, close all;

%% ================== Fusion parameters =================== %%
modeArray = {'SDK', 'Simple3', 'M/M'};
compare   = true; % for comparison of N fudion modes
if compare
    modePtr = [1 2 3];
else
    modePtr = 1;
    mode    = modeArray{modePtr};
end

% SDK fusion
fusionTime       = 0.0;  % sec.
fusionFilterCoef = 0.95;

% Madgwick/Mahony filters
filter  = 'Madgwick';
% filter = 'Mahony';
% beta   = sqrt(3/4)*pi*(0.0/180);

% Mean filter (Simple3 fusion)
windowSize = 20;

%% =============== Initialization ================= %%
logN            = 6;
fContent        = dir('logs\');
folderName      = fContent(logN+2).name;
path2Logs       = ['logs\' folderName];
angleName       = {'Pitch', 'Roll', 'Yaw'};

%% Load sensor data from logs
% temp{1} = loadjson([path2Logs '\', folderName, '_accelerometer.json']);
% temp{2} = loadjson([path2Logs '\', folderName, '_magnetic_field.json']);
% temp{3} = loadjson([path2Logs '\', folderName, '_gyroscope.json']);
% temp{4} = loadjson([path2Logs '\', folderName, '_angles.json']);
load(['Log' num2str(logN) '.mat']);
[accel, gyro, magnet, angles, time] = fExctractSensorData(temp);

%% ================= Plot Android angles ==================== %%
pl = cell(1, 3);
for i = 2:4
    figure(i-1);
    pl{i-1}(1) = plot(time, angles(:, i), 'LineWidth', 1);
    hold on;
    xlabel('Time, s');
    ylabel('Angle, deg');
    grid on;
    if max(angles(:, i)) - mean(angles(:, i)) <= 5
        set(gca, 'FontSize', 16, 'YTick', -200:1:200, 'GridAlpha', 0.5);
    elseif max(angles(:, i)) - mean(angles(:, i)) <= 20
        set(gca, 'FontSize', 16, 'YTick', -200:2:200, 'GridAlpha', 0.5);
    elseif max(angles(:, i)) - mean(angles(:, i)) <= 100
        set(gca, 'FontSize', 16, 'YTick', -200:10:200, 'GridAlpha', 0.5);
    else
        set(gca, 'FontSize', 16, 'YTick', -200:20:200, 'GridAlpha', 0.5);
    end
    
    if time(end) < 40
        set(gca,'XTick', 0:1:40);
    else
        set(gca,'XTick', 0:2:100);
    end
end

%% ================ Fusion procedure ================== %%
if compare
    for i = 1:length(modePtr) % apply each fusion mode from the list
        mode = modeArray{modePtr(i)};
        N = length(accel(:,1));
        outOrientation = zeros(N, 3);
        timeCounter    = 0;
        % Filter the sensors' data
%         filtSensors = fFiltSensorsMA(accel(:, 2:4), magnet(:, 2:4), ...
%                                      gyro(:, 2:4), 10);
%         accel(:, 2:4)  = filtSensors{1};
%         magnet(:, 2:4) = filtSensors{2};
%         gyro(:, 2:4)   = filtSensors{3};
        
        if strcmp(mode, 'SDK')
            outOrientation = fSDKFusion(accel, magnet, windowSize);
            string{1} = ['SDK fusion mode. Window size = ' num2str(windowSize)];
        elseif strcmp(mode, 'Simple3')
            outOrientation = fSimpleFusion3(accel, gyro, magnet, fusionTime, fusionFilterCoef);
            string{2} = ['Simple3 fusion mode. Fusion time = ' num2str(fusionTime)];
        elseif strcmp(mode, 'M/M')
            [outOrientation, beta] = fMadgwickMahonyFusion(accel, gyro, magnet, angles(1, 2:4), filter);
            string{3} = [filter ' fusion mode. Beta = ' num2str(beta)];
        end
        % Plot graphs of fused angles
        for j = 1:3
            figure(j);
            hold on;
            pl{j}(i+1) = plot(time, outOrientation(:, j), 'LineWidth', 2);
        end
    end
    
    for i = 1:3
        figure(i);
        title({['Device angles fusion. ', angleName{i}, '. '], ...
               [string{modePtr(1)} ' vs ' string{modePtr(2)} ' vs ' string{modePtr(3)}]});
        legend(pl{i}, 'Angle provided by Android', ...
                  ['Fused angle. ' modeArray{modePtr(1)}], ...
                  ['Fused angle. ' modeArray{modePtr(2)}], ...
                  ['Fused angle. ' modeArray{modePtr(3)}]);
    end
else
    if strcmp(mode, 'SDK')
        outOrientation = fSDKFusion(accel, magnet, windowSize);
    elseif strcmp(mode, 'Simple3')
        outOrientation = fSimpleFusion3(accel, gyro, magnet, fusionTime, fusionFilterCoef);
    elseif strcmp(mode, 'M/M')
        outOrientation = fMadgwickMahonyFusion(accel, gyro, magnet, filter);
    end
    % Plot graphs of fused angles
    for j = 1:3
        figure(j);
        hold on;
        pl(2) = plot(time, outOrientation(:, j), 'LineWidth', 2);
    end
    title(['Device angles fusion. ', angleName{i-1}, '. ' string{modePtr}]);
end