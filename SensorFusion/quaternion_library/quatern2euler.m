function euler = quatern2euler(q)
%QUATERN2EULER Converts a quaternion orientation to ZYX Euler angles
%
%   q = quatern2euler(q)
%
%   Converts a quaternion orientation to ZYX Euler angles where phi is a
%   rotation around X, theta around Y and psi around Z.
%
%   For more information see:
%   http://www.x-io.co.uk/node/8#quaternions
%
%	Date          Author          Notes
%	27/09/2011    SOH Madgwick    Initial release

    phi = atan2(2*q(3)*q(4) - 2*q(1)*q(2), 2*q(1)^2 + 2*q(4)^2 - 1);
    theta = -asin(2*q(2)*q(4) + 2*q(1)*q(3));
    psi = atan2(2*q(2)*q(3) - 2*q(1)*q(4), 2*q(1)^2 + 2*q(2)^2 - 1);

    euler = [phi theta psi];
end

