clc, clear, close all;
%% ====================== Init ===================== %%
logN = 4;
%% ==================== Load data ====================== %%
load(['angles_Log' num2str(logN) '.mat']);
load (['Full_Log' num2str(logN) '.mat']);
fullOrientation = outOrientation;
clear outOrientation;
load (['SDK_Log' num2str(logN) '.mat']);
%% =================== Set parameters =================== %%
SDKOrientation = outOrientation;
refTime   = angles(:, 1);
ptr       = [2 3 1];
angleName = {'Pitch', 'Roll', 'Yaw'};
%% ==================== Plot graphs ======================= %%
for i = 2:4
    figure();
    plot(refTime, angles(:, i), 'LineWidth', 1);
    hold on;
    plot(refTime, fullOrientation(:, ptr(i-1)), 'LineWidth', 2);
    plot(refTime, SDKOrientation(:, ptr(i-1)), 'LineWidth', 2);
    title(['Device angles fusion. ', angleName{i-1}, ...
           '. SDK fusion mode vs Full fusion mode. Fusion time - 0 sec']);
    xlabel('Time, s');
    ylabel('Angle, deg');
    grid on;
    if max(angles(:, i)) <= 5
        set(gca, 'FontSize', 16, 'YTick', -5:0.5:5, 'GridAlpha', 0.5);
    elseif max(angles(:, i)) <= 20
        set(gca, 'FontSize', 16, 'YTick', -20:2:20, 'GridAlpha', 0.5);
    elseif max(angles(:, i)) <= 100
        set(gca, 'FontSize', 16, 'YTick', -100:10:100, 'GridAlpha', 0.5);
    else
        set(gca, 'FontSize', 16, 'YTick', -200:20:200, 'GridAlpha', 0.5);
    end
    
    if refTime(end) < 40
        set(gca,'XTick', 0:1:40);
    else
        set(gca,'XTick', 0:2:100);
    end
    legend('Angle provided by Android', ...
           'Angle provided by Full fusion', ...
           'Angle provided by SDK fusion');
end