% This function is a Matlab realization of Android function 
% borrowed from the reference at http://developer.android.com/reference/android/hardware/SensorEvent.html#values
% Function calculates a rotation vector from the gyroscope angular speed values.
% Input parameters:
% gyro       = [gx; gy; gz] - vector of current Gyroscope sensor values
% timeFactor = time interval between previous and current sensor samples
%              (in sec.) divided by 2
% Output parameters:
% output     = rotation quaternion

function output = getRotationVectorFromGyro(gyro, timeFactor)

% Calculate the angular speed of the sample
omegaMagnitude = sqrt(sum(gyro.^2));

% Normalize the rotation vector if it's big enough to get the axis
if (omegaMagnitude > 1e-9)
    normValues = gyro / omegaMagnitude;
else
    normValues = [0 0 0];
end

% Integrate around this axis with the angular speed by the timestep
% in order to get a delta rotation from this sample over the timestep
% We will convert this axis-angle representation of the delta rotation
% into a quaternion before turning it into the rotation matrix.
thetaOverTwo    = omegaMagnitude * timeFactor;
sinThetaOverTwo = sin(thetaOverTwo);
cosThetaOverTwo = cos(thetaOverTwo);

output = [sinThetaOverTwo * normValues(1); ...
          sinThetaOverTwo * normValues(2); ...
          sinThetaOverTwo * normValues(3); ...
          cosThetaOverTwo];
