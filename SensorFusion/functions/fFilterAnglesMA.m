function [output, buffer] = fFilterAnglesMA(input, buffer)
% Increase the index
buffer.index = buffer.index + 1;
% Check the overflow status
if buffer.index > buffer.WS
    buffer.overflow = true;
    buffer.index = 1;
end
% Add input values to the buffer
buffer.input(buffer.index, :) = input;
% Convert angle from [-180 180] deg coordinate system into [-360 360] deg
% coordinate system
if buffer.index > 1 || buffer.overflow
    curr = buffer.index;
    prev = buffer.index-1;
    if prev == 0
        prev = buffer.WS;
    end
    
    for i = 1:3
        if buffer.input(curr, i) - buffer.input(prev, i) < -pi
            buffer.input(curr, i) = buffer.input(curr, i) + 2*pi;
        elseif buffer.input(curr, i) - buffer.input(prev, i) > pi
            buffer.input(curr, i) = buffer.input(curr, i) - 2*pi;
        end
    end
end

% Calculate mean value
if ~buffer.overflow
    buffer.output(1) = mean(buffer.input(1:buffer.index, 1));
    buffer.output(2) = mean(buffer.input(1:buffer.index, 2));
    buffer.output(3) = mean(buffer.input(1:buffer.index, 3));
else
    buffer.output(1) = mean(buffer.input(:, 1));
    buffer.output(2) = mean(buffer.input(:, 2));
    buffer.output(3) = mean(buffer.input(:, 3));
end
% Write the mean value to the output
output = buffer.output;
% Convert angle from [-360 360] deg coordinate system into [-180 180] deg
% coordinate system
for i = 1:3
    if output(i) > pi
        output(i) = output(i) - 2*pi;
    elseif output(i) < -pi
        output(i) = output(i) + 2*pi;
    end
end

end