% Function is a Matlab realization of the same Android function in Java code.
% Input parameters:
% inputOrient = [yaw, pitch, roll] - input orientation angles
% Output parameters:
% output - is a corresponding rotation matrix of size 3x3

function output = getRotationMatrixFromOrientation(inputOrient)

sinX = sin(inputOrient(2));
cosX = cos(inputOrient(2));
sinY = sin(inputOrient(3));
cosY = cos(inputOrient(3));
sinZ = sin(inputOrient(1));
cosZ = cos(inputOrient(1));

% rotation about x-axis (pitch)
xM = [1,     0,    0;...
      0,  cosX, sinX;...
      0, -sinX, cosX];

% rotation about y-axis (roll)
yM = [ cosY, 0, sinY;...
          0, 1,    0;...
      -sinY, 0, cosY];

% rotation about z-axis (azimuth)
zM = [ cosZ, sinZ, 0;...
      -sinZ, cosZ, 0;...
          0,    0, 1];

% rotation order is y, x, z (roll, pitch, azimuth)
resultMatrix = xM * yM;
output = zM * resultMatrix;

end


