% Function is a Matlab realization of the same Android function in
% SensorFusion Java code
% Input parameters:
% gyro = [gx; gy; gz] - vector of current Gyroscope sensor values
% dT   = time interval between previous and current sensor samples (sec.) !
% gyroMatrix = rotation matrix based on the gyro sensor data
% Output parameters:
% output  = [yaw, pitch, roll] - three orientation angles
% gMatrix = updated gyro-based rotation matrix

function [output, gMatrix] = calcGOrientation(gyro, dT, gyroMatrix)

% copy the new gyro values into the gyro array
% convert the raw gyro data into a rotation vector
deltaVector = getRotationVectorFromGyro(gyro, dT/2);

% convert rotation vector into rotation matrix
deltaMatrix = getRotationMatrixFromVector(deltaVector, 3);

% apply the new rotation interval on the gyroscope based rotation matrix
gMatrix = gyroMatrix * deltaMatrix;

% get the gyroscope based orientation from the rotation matrix
output = getOrientation(gMatrix);


