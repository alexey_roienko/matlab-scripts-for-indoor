% Function is a Matlab realization of the same Android function
% Input parameter:
% R - is a rotation matrix (can be whether 3x3 or 4x4 size)
% Output parameter:
% angles - calculated values between each axis of device coordinate system
% and the Earth coordinate system

function angles = getOrientation(R)

[X, Y] = size(R);

if ~((X==Y) && ((X==3) || (X==4)))
    warning('The size of R matrix must be 3x3 or 4x4!');
    angles = zeros(3,1);
    return;  
end

angles(1) = atan2(R(1,2), R(2,2));
angles(2) = asin(-R(3,2));
angles(3) = atan2(-R(3,1), R(3,3));




