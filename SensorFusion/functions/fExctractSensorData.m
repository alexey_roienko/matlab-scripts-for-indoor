function [accel, gyro, magnet, angles, time] = fExctractSensorData(data)
%% ==================== Init ======================= %%
accel  = zeros(length(data{1})-1, 4);
magnet = zeros(length(accel(:, 1)), 4);
gyro   = zeros(length(accel(:, 1)), 4);
angles = zeros(length(accel(:, 1)), 4);

%% Load Accel data to matrix "accel". It contains 4 columns and some rows
% according to timestamps
for i = 2:length(data{1}) % the cycle starts from the 2nd timestamp, 
    % because gyro data do not have timestamp = 0, but accel and magnet do
    accel(i-1, 1) = data{1}{i}.timestamp/10^6;
    accel(i-1, 2) = data{1}{i}.x;
    accel(i-1, 3) = data{1}{i}.y;
    accel(i-1, 4) = data{1}{i}.z;
end
time = accel(1:end, 1); % reference time

%% Load Magnet data to matrix "magnet". It contains 4 columns and some rows
% according to timestamps
ratio = round(length(data{2})/length(data{1}));
c = 1;
for i = 1+ratio:ratio:floor(length(data{2})/ratio)*ratio % if magnet data come 2 times more 
    % frequent, then write 3rd, 5th, 7th, sample into an array, otherwise
    % write 2nd, 3rd, 4th, ... sample
    % the cycle starts from the 2nd timestamp, 
    % because gyro data do not have timestamp = 0, but accel and magnet do
    if c > length(time)
        break;
    end
    magnet(c, 1) = time(c);
    magnet(c, 2) = data{2}{i}.x;
    magnet(c, 3) = data{2}{i}.y;
    magnet(c, 4) = data{2}{i}.z;
    c = c + 1;
end

%% Load Gyro data to matrix "gyro". It contains 4 columns and some rows
% according to timestamps
for i = 1:length(time) % gyro data timestamps are 0.0096, 0.0296, 0.0496
    % instead of 0, 0.02, 0.04, so 0.0096 is equalized to 0.02, 0.0296 - to 0.04 etc
    gyro(i, 1) = time(i);
    gyro(i, 2) = data{3}{i}.x;
    gyro(i, 3) = data{3}{i}.y;
    gyro(i, 4) = data{3}{i}.z;
end

%% Load device angles (by Android) to matrix "angles". It contains 4 columns and some rows
% according to timestamps
for i = 1:length(time)
    angles(i, 1) = time(i);
    angles(i, 2) = data{4}{i}.pitch;
    angles(i, 3) = data{4}{i}.roll;
    angles(i, 4) = data{4}{i}.azimuth;
end