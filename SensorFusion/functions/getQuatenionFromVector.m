%% Helper function to convert a rotation vector to a normalized quaternion.
%  Given a rotation vector (presumably from a ROTATION_VECTOR sensor), returns a normalized
%  quaternion in the array Q.  The quaternion is stored as [w, x, y, z]
%  @param rv the rotation vector to convert
%  @param Q an array of floats in which to store the computed quaternion

public static void getQuaternionFromVector(float[] Q, float[] rv) {
	if (rv.length >= 4) {
		Q[0] = rv[3];
	} else {
		Q[0] = 1 - rv[0] * rv[0] - rv[1] * rv[1] - rv[2] * rv[2];
		Q[0] = (Q[0] > 0) ? (float) Math.sqrt(Q[0]) : 0;
	}
	Q[1] = rv[0];
	Q[2] = rv[1];
	Q[3] = rv[2];
}