% Function is a Matlab realization of the same Android function in
% SensorFusion Java code
% Input parameters:
% grav = [gx; gy; gz] - vector of current Gravity sensor values
% magnet = [mx; my; mz] - vector of current Magnetometer sensor values
% Output parameters:
% output = [yaw, pitch, roll] - three orientation angles

function output = calcAMOrientation(grav, magnet)

[R, ~] = getRotationMatrix(grav, magnet, 3);
output = getOrientation(R);

