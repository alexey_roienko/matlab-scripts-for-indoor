% Function is a Matlab realization of the same Android function
% Input parameters:
% g_vector = [gx; gy; gz] - vector of current Gravity sensor values
% m_vector = [mx; my; mz] - vector of current Magnetometer sensor values
% sizeInd - can be equal to 3 or 4 and denotes the size of output matrixes R and I
% Output parameters:
% R - is a rotation matrix (can be whether 3x3 or 4x4 size)
% I - is an inclination matrix (can be whether 3x3 or 4x4 size); optional

function [R, I] = getRotationMatrix(gr_vector, gm_vector, sizeInd)

H(1) = gm_vector(2)*gr_vector(3) - gm_vector(3)*gr_vector(2);
H(2) = gm_vector(3)*gr_vector(1) - gm_vector(1)*gr_vector(3);
H(3) = gm_vector(1)*gr_vector(2) - gm_vector(2)*gr_vector(1);

normH = sqrt(sum(H.^2));
if (normH < 0.1)
    % device is close to free fall (or in space?), or close to
    % magnetic north pole. Typical values are > 100.
    R = zeros(3);  I = zeros(3);
    return;
end

invH = 1/normH;
H = H * invH;
invGr = 1/sqrt(sum(gr_vector.^2));

Gr = gr_vector * invGr;
M(1) = Gr(2)*H(3) - Gr(3)*H(2);
M(2) = Gr(3)*H(1) - Gr(1)*H(3);
M(3) = Gr(1)*H(2) - Gr(2)*H(1);

switch sizeInd
    case 3
        R = zeros(3);
        R(1,1:3) = H;
        R(2,1:3) = M;
        R(3,1:3) = Gr;
    case 4
        R = zeros(4);
        R(1,1:3) = H;
        R(2,1:3) = M;
        R(3,1:3) = Gr;
        R(4,4)   = 1;
    otherwise
        R = zeros(4);
end

%% Compute the inclination matrix by projecting the geomagnetic vector onto
%  the Z (gravity) and X (horizontal component of geomagnetic vector) axes
invE = 1/sqrt(sum(gm_vector.^2));
c = sum(gm_vector.*M) * invE;
s = sum(gm_vector.*gr_vector) * invE;

switch sizeInd
    case 3
        I = zeros(3);
        I(1,1) = 1;   I(2,2) = c;  I(2,3) = s;
        I(3,2) = -s;  I(3,3) = c;
    case 4
        I = zeros(4);
        I(1,1) = 1;   I(2,2) = c;  I(2,3) = s;
        I(3,2) = -s;  I(3,3) = c;
        I(4,4) = 1;
    otherwise
        I = zeros(4);
end





