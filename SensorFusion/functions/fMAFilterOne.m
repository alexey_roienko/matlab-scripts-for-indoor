function [output, buffer] = fMAFilterOne(input, buffer)
% Increase the index
buffer.index = buffer.index + 1;
% Check the overflow status
if buffer.index > buffer.WS
    buffer.overflow = true;
    buffer.index = 1;
end
% Add input values to the buffer
buffer.input(buffer.index, :) = input;
% Calculate mean value
if ~buffer.overflow
    buffer.output(1) = mean(buffer.input(1:buffer.index, 1));
    buffer.output(2) = mean(buffer.input(1:buffer.index, 2));
    buffer.output(3) = mean(buffer.input(1:buffer.index, 3));
else
    buffer.output(1) = mean(buffer.input(:, 1));
    buffer.output(2) = mean(buffer.input(:, 2));
    buffer.output(3) = mean(buffer.input(:, 3));
end
% Write the mean value to the output
output = buffer.output;

end