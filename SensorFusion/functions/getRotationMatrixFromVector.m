% Function is a Matlab realization of the same Android function that 
% converts a rotation vector to a rotation matrix.
% Input parameters:
% rotationVector  = the rotation vector to convert
% sizeInd - can be equal to 3 or 4 and denotes the size of output matrix R
% Output parameters:
% R - is a rotation matrix (can be whether 3x3 or 4x4 size)

function R = getRotationMatrixFromVector(rotationVector, sizeInd)

q1 = rotationVector(1);
q2 = rotationVector(2);
q3 = rotationVector(3);

q0 = 1 - q1^2 - q2^2 - q3^2;
if q0 > 0
    q0 = sqrt(q0);
else
    q0 = 0;
end

sq_q1 = 2 * q1^2;
sq_q2 = 2 * q2^2;
sq_q3 = 2 * q3^2;
q1_q2 = 2 * q1 * q2;
q3_q0 = 2 * q3 * q0;
q1_q3 = 2 * q1 * q3;
q2_q0 = 2 * q2 * q0;
q2_q3 = 2 * q2 * q3;
q1_q0 = 2 * q1 * q0;

switch sizeInd
    case 3
        R = zeros(3);
        R(1,1) = 1 - sq_q2 - sq_q3;
        R(1,2) = q1_q2 - q3_q0;
        R(1,3) = q1_q3 + q2_q0;

        R(2,1) = q1_q2 + q3_q0;
        R(2,2) = 1 - sq_q1 - sq_q3;
        R(2,3) = q2_q3 - q1_q0;

        R(3,1) = q1_q3 - q2_q0;
        R(3,2) = q2_q3 + q1_q0;
        R(3,3) = 1 - sq_q1 - sq_q2;
        
    case 4
        R = zeros(4);
        R(1,1) = 1 - sq_q2 - sq_q3;
        R(1,2) = q1_q2 - q3_q0;
        R(1,3) = q1_q3 + q2_q0;
        R(1,4) = 0;

        R(2,1) = q1_q2 + q3_q0;
        R(2,2) = 1 - sq_q1 - sq_q3;
        R(2,3) = q2_q3 - q1_q0;
        R(2,4) = 0;

        R(3,1) = q1_q3 - q2_q0;
        R(3,2) = q2_q3 + q1_q0;
        R(3,3) = 1 - sq_q1 - sq_q2;
        R(3,4) = 0;

        R(4,1:3) = 0;
        R(4,4) = 1;
        
    otherwise
         R = zeros(4);
end
    