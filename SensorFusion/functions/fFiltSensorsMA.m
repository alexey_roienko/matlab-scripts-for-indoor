function filtOut = fFiltSensorsMA(varargin)
%% INPUTS:
% input arrays to filter, the values in each array should be ordered into columns
% the last argument is size of the filter window
%% OUTPUTS:
% input arrays after filtering

%% ================ Init ================== %%
L       = nargin-1;
filtOut = cell(1, L);
[h, w]  = size(varargin{end});

if h > 1 && w > 1
    error('The last input argument should be a single value setting a window size!')
end

if varargin{end} < 1
    windowSize = 1;
else
    windowSize = varargin{end};
end

%% ================ Get input arrays ================ %%
for i = 1:L
    % Get the current argument
    arrIn = varargin{i};
    [h, w] = size(arrIn);
    % Initialize/reset the buffer
    buffer.WS       = windowSize;
    buffer.input    = zeros(buffer.WS, w);
    buffer.output   = zeros(1, w);
    buffer.index    = 0;
    buffer.overflow = false;
    % Filter the array
    for t = 1:h
        [filtOut{i}(t, :), buffer] = fMAFilterOne(arrIn(t, :), buffer);
    end
end

end