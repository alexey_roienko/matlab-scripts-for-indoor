% Function is a Matlab realization of the same Android function in
% SensorFusion Java code
% Input parameters:
% AMOrient = [yaw, pitch, roll] - orientation angles obtained on the basis
%            of Accelerometer and Magnetometer sensors
% GOrient  = [yaw, pitch, roll] - orientation angles obtained on the basis
%            of Gyroscope sensor
% filtCoef = feedback fusion filter coefficient (0.95 for example)
% Output parameters:
% fusedOrient = [yaw, pitch, roll] - orientation angles obtained on the
%               basis of fusion
% gyroMatrix  = updated gyro-based rotation matrix
% gyroOrient  = updated orientation angles obtained on the basis of Gyroscope

function [fusedOrient, gyroMatrix] = calcFusedOrientation(AMOrient, GOrient, filtCoef)

oneMinusCoeff = 1 - filtCoef;
            
% Fix for 179� <--> -179� transition problem:
% Check whether one of the two orientation angles (gyro or accMag) is negative while the other one is positive.
% If so, add 360� (2 * math.PI) to the negative value, perform the sensor fusion, 
% and remove the 360� from the result, if it is greater than 180�. 
% This stabilizes the output in positive-to-negative-transition cases.

%% Azimuth
if (GOrient(1) < -0.5*pi) && (AMOrient(1) > 0)
    fusedOrient(1) = filtCoef * (GOrient(1) + 2*pi) + oneMinusCoeff * AMOrient(1);
    if fusedOrient(1) > pi
        fusedOrient(1) = fusedOrient(1) - 2*pi;
    end
elseif (AMOrient(1) < -0.5*pi) && (GOrient(1) > 0)
    fusedOrient(1) = filtCoef * GOrient(1) + oneMinusCoeff * (AMOrient(1) + 2*pi);
    if fusedOrient(1) > pi
        fusedOrient(1) = fusedOrient(1) - 2*pi;
    end
else
    fusedOrient(1) = filtCoef * GOrient(1) + oneMinusCoeff * AMOrient(1);
end


%% Pitch
if (GOrient(2) < -0.5*pi) && (AMOrient(2) > 0)
    fusedOrient(2) = filtCoef * (GOrient(2) + 2*pi) + oneMinusCoeff * AMOrient(2);
    if fusedOrient(2) > pi
        fusedOrient(2) = fusedOrient(2) - 2*pi;
    end
elseif (AMOrient(2) < -0.5*pi) && (GOrient(2) > 0)
    fusedOrient(2) = filtCoef * GOrient(2) + oneMinusCoeff * (AMOrient(2) + 2*pi);
    if fusedOrient(2) > pi
        fusedOrient(2) = fusedOrient(2) - 2*pi;
    end
else 
    fusedOrient(2) = filtCoef * GOrient(2) + oneMinusCoeff * AMOrient(2);
end


%% Roll
if (GOrient(3) < -0.5*pi) && (AMOrient(3) > 0)
    fusedOrient(3) = filtCoef * (GOrient(3) + 2*pi) + oneMinusCoeff * AMOrient(3);
    if fusedOrient(3) > pi
        fusedOrient(3) = fusedOrient(3) - 2*pi;
    end
elseif (AMOrient(3) < -0.5*pi) && (GOrient(3) > 0)
    fusedOrient(3) = filtCoef * GOrient(3) + oneMinusCoeff * (AMOrient(3) + 2*pi);
    if fusedOrient(3) > pi
        fusedOrient(3) = fusedOrient(3) - 2*pi;
    end
else 
    fusedOrient(3) = filtCoef * GOrient(3) + oneMinusCoeff * AMOrient(3);
end


%% Overwrite gyro matrix and orientation with fused orientation
% to compensate gyro drift
gyroMatrix = getRotationMatrixFromOrientation(fusedOrient);

end



