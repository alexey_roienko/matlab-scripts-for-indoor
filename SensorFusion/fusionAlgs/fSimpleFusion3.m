function output = fSimpleFusion3(accel, gyro, magnet, fusionTime, fusionFilterCoef)
%% =============== Init ============== %%
if length(accel(:, 1)) ~= length(magnet(:, 1)) && ... 
        length(accel(:, 1)) ~= length(gyro(:, 1))
    error('Sensor arrays dimension mismatch');
else
    N = length(accel(:, 1));
end
output      = zeros(N, 3);
gyroMatrix  = eye(3);
timeCounter = 0;

%% ==================== Time cycle ================== %%
for t = 1:N
    % Calculate orientation angles using A and M values only
    AMOrientation = calcAMOrientation(accel(t, 2:4), magnet(t, 2:4));
    % Initialization of gyroMatrix
    if 1 == t
        [initMatrix, ~] = getRotationMatrix(accel(t, 2:4), magnet(t, 2:4), 3);
        % Matrix multiplication
        gyroMatrix = gyroMatrix * initMatrix;
    end
    
    % Calculate orientation angles based on G values only
    % Note that dT should be in SECONDS
    if 1 == t
        dT = 0;
    else
        dT = gyro(t, 1) - gyro(t-1, 1);
    end
    [GOrientation, gyroMatrix] = calcGOrientation(gyro(t, 2:4), dT, gyroMatrix);
    
    % Check time. If the FUSION_TIME seconds passes since last fusion, we should do it again
    if timeCounter >= fusionTime
        [fusedOrientation, gMatrix] = calcFusedOrientation(AMOrientation, ...
            GOrientation, fusionFilterCoef);
        gyroMatrix   = gMatrix;
        GOrientation = fusedOrientation;
        timeCounter  = 0;
    else
        timeCounter  = timeCounter + dT;
    end
    output(t, :) = GOrientation;
end

%% ========= Transform to left-hand coordinate system ============ %%
output       = output*(180/pi);
output(:, 1) = -output(:, 1);
output(:, 2) = -output(:, 2);
%% ============ Put the angles into correct order ============= %%
temp = output(:, 1);
output(:, 1) = output(:, 2);
output(:, 2) = output(:, 3);
output(:, 3) = temp;

end