function output = fSDKFusion(accel, magnet, windowSize)
%% ============ Init =============== %%
buffer.WS       = windowSize;
buffer.input    = zeros(buffer.WS, 3);
buffer.output   = zeros(1, 3);
buffer.index    = 0;
buffer.overflow = false;

if length(accel(:, 1)) ~= length(magnet(:, 1))
    error('Sensor arrays dimension mismatch');
else
    N = length(accel(:, 1));
end
output = zeros(N, 3);

%% ======================= Time cycle ========================= %%
for t = 1:N
    % Calculate orientation angles using A and M values only
    AMOrientation = calcAMOrientation(accel(t, 2:4), magnet(t, 2:4));
    [AMOrientation, buffer] = fFilterAnglesMA(AMOrientation, buffer);
    output(t, :) = AMOrientation;
end

%% ========= Transform to left-hand coordinate system ============ %%
output       = output*(180/pi);
output(:, 1) = -output(:, 1);
output(:, 2) = -output(:, 2);

%% ============ Put the angles into correct order ============= %%
temp = output(:, 1);
output(:, 1) = output(:, 2);
output(:, 2) = output(:, 3);
output(:, 3) = temp;
end