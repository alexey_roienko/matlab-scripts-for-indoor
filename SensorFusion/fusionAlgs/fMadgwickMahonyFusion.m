function [output, beta] = fMadgwickMahonyFusion(accel, gyro, magnet, angles, mode)

%% ==================== Init ===================== %%
dt         = 0.02;
quaternion = zeros(length(accel(:, 1)), 4);
output     = zeros(length(accel(:, 1)), 3);
% Check fusion mode
if strcmp(mode, 'Madgwick')
    AHRS = MadgwickAHRS('SamplePeriod', dt);
    beta = AHRS.Beta;
elseif strcmp(mode, 'Mahony')
    AHRS = MahonyAHRS('SamplePeriod', dt);
    beta = AHRS.Kp;
else
    error('Invalid fudion mode!');
end


%% ================= Time cycle ==================== %%
for t = 1:length(accel(:, 1))
    if t > 1
        AHRS.SamplePeriod = accel(t, 1) - accel(t-1, 1);
    end
    if t == 210
        disp('');
    end
    AHRS.Update(gyro(t, 2:4), accel(t, 2:4), magnet(t, 2:4));	% gyroscope units must be radians
    quaternion(t, :) = AHRS.Quaternion;
    
    % Angles convertion
    output(t, :) = quatern2euler(quaternConj(quaternion(t, :))) * (180/pi);	% use conjugate for sensor frame relative to Earth and convert to degrees.
    for i = 1:3
        output(t, i) = output(t, i) + angles(i);
        if output(t, i) > 180
            output(t, i) = output(t, i) - 360;
        elseif output(t, i) < -180
            output(t, i) = output(t, i) + 360;
        end
    end
end

end