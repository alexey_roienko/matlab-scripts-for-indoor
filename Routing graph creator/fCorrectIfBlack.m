%% Function checks whether the line between points p1 and p2 crosses 
%  a black region on the map mask. If so, function returns the corrected
%  end point. If not, returns the initial p2-point coordinates.
%  INPUT:
%  img - mask of the office
%  p1 = [x y] - coordinates of the first (start) point, in pixels
%  p2 = [x y] - coordinates of the second (end) point, in pixels
%  OUTPUT:
%  p2 = [x y] - corrected coordinates of the second (end) point, in pixels

function res = fCorrectIfBlack(img, p1, p2, varargin)

res = false;

%% Check input information
if (p1(1) < 1)
    p1(1) = 1;
elseif p1(1) > size(img, 2)
    p1(1) = size(img, 2);
end

if (p1(2) < 1)
    p1(2) = 1;
elseif p1(2) > size(img, 1)
    p1(2) = size(img, 1);
end

if (p2(1) < 1)
    p2(1) = 1;
elseif p2(1) > size(img, 2)
    p2(1) = size(img, 2);
end

if (p2(2) < 1)
    p2(2) = 1;
elseif p2(2) > size(img, 1)
    p2(2) = size(img, 1);
end
    
%% Main    
% if dx > dy
if abs(p2(1) - p1(1)) > abs(p2(2) - p1(2))
    if p2(1) - p1(1) >= 0
        incr = 1;
    else
        incr = -1;
    end

    if p2(1) ~= p1(1)
        slope = (p2(2) - p1(2)) / (p2(1) - p1(1));
    else
        slope = 0; % Special case, when denominator (p2(1) - p1(1)) is zero, i.e. points have the same x coordinate
    end

    for x = p1(1) : incr : p2(1)
        if img( ceil(slope*(x-p1(1))+p1(2)), x) == 0
            res = true;
            break;
        end
    end
    
% if dx < dy
else
	if p2(2)-p1(2) >= 0
        incr = 1;
    else
        incr = -1;
    end
    
    if p2(2) ~= p1(2)
        slope = (p2(1) - p1(1)) / (p2(2) - p1(2));
    else
        slope = 0; % Special case, when denominator (p2(2) - p1(2)) is zero, i.e. points have the same x coordinate
    end

    for y = p1(2) : incr : p2(2)
        if img(y, ceil(slope*(y-p1(2))+p1(1))) == 0
            res = true;
            break;
        end
    end
    
end


