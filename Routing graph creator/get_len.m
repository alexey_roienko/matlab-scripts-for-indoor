function len = get_len(cp_coords1, cp_coords2)
x1  = single(cp_coords1(1));
y1  = single(cp_coords1(2));
x2  = single(cp_coords2(1));
y2  = single(cp_coords2(2));
dx  = single(power((x2 - x1),2));
dy  = single(power((y2 - y1),2));
len = single(sqrt(dx + dy));
end