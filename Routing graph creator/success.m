function varargout = success(varargin)
% SUCCESS MATLAB code for success.fig
%      SUCCESS, by itself, creates a new SUCCESS or raises the existing
%      singleton*.
%
%      H = SUCCESS returns the handle to a new SUCCESS or the handle to
%      the existing singleton*.
%
%      SUCCESS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SUCCESS.M with the given input arguments.
%
%      SUCCESS('Property','Value',...) creates a new SUCCESS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before success_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to success_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help success

% Last Modified by GUIDE v2.5 25-Apr-2017 01:01:55

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @success_OpeningFcn, ...
                   'gui_OutputFcn',  @success_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before success is made visible.
function success_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to success (see VARARGIN)

% Choose default command line output for success
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes success wait for user response (see UIRESUME)
% uiwait(handles.figure1);
text = {'Data has been successfully saved in', 'folder "output" !'};
set(handles.text1, 'String', text);
pos = get(handles.figure1, 'Position');
pos(1:2) = [100 30];
set(handles.figure1, 'Position', pos);
set(handles.figure1, 'Name', 'Success');


% --- Outputs from this function are returned to the command line.
function varargout = success_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close();


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
