function manual_mode = disable_manual_mode(handles)

cla;
manual_mode = false;

% Fill logger fields
set(handles.text7, 'String', 0);
set(handles.text8, 'String', {'0' '0' '0'});
set(handles.text9, 'String', {'0' '0' '0'});

% Switch status of interface elements
set(handles.edit2, 'Enable', 'on'); % "Graph vertices step" edit
set(handles.slider2, 'Enable', 'on'); % "Graph vertices step" slider
set(findall(handles.uipanel4, '-property', 'enable'), 'enable', 'off'); % "Observer" panel
set(findall(handles.uipanel6, '-property', 'Enable'), 'Enable', 'off'); % "Graph editor" panel
set(handles.uipanel7, 'Visible', 'on'); % "Vertices" panel
set(handles.uipanel8, 'Visible', 'off'); % "Edges" panel
set(handles.uipanel10, 'Visible', 'off'); % Max edge length control panel
set(findall(handles.uibuttongroup1, '-property', 'Enable'), 'Enable', 'off'); % "What to edit?" buttongroup
set(handles.pushbutton1, 'Enable', 'on'); % "Generate vertices" button
set(handles.pushbutton1, 'String', 'Generate vertices');
set(handles.togglebutton5, 'Enable', 'on'); % "Import mode" button
set(handles.radiobutton1, 'Value', 1); % Set "Vertices" radiobutton to 1
set(handles.checkbox7, 'Enable', 'on'); % "Manual mode" checkbox

end