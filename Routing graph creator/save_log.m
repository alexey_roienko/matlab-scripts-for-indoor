function save_log(path, nodesNum, vertCount, addedVertCount, edgeCount, addedEdgeCount)

%% Write nodes number
out = fopen(path, 'w');
str = 'Mesh nodes';
fprintf(out, '%s\r\n', str);
str = 'generated:';
fprintf(out, '%s %d\r\n', str, nodesNum);

%% Write vertices data
str = 'Graph vertices';
fprintf(out, '\r\n%s\r\n', str);
str = 'total:';
fprintf(out, '%s %d\r\n', str, vertCount+addedVertCount);
str = 'generated:';
fprintf(out, '%s %d\r\n', str, vertCount);
str = 'added:';
fprintf(out, '%s %d\r\n', str, addedVertCount);

%% Write edges data
str = 'Graph edges';
fprintf(out, '\r\n%s\r\n', str);
str = 'total:';
fprintf(out, '%s %d\r\n', str, edgeCount+addedEdgeCount);
str = 'generated:';
fprintf(out, '%s %d\r\n', str, edgeCount);
str = 'added:';
fprintf(out, '%s %d\r\n', str, addedEdgeCount);
end