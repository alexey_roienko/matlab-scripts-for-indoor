function array = reduce2D(array, pos, num)

%% Init
temp    = array;
l       = length(array(:, 1));

%% Delete num elemnts, starting from pos-th one
if pos + num <= l && pos > 0 && num > 0
    for i = pos:l-num
        temp(i, :) = temp(i+num, :);
    end
end

array = temp(1:end - num, :);