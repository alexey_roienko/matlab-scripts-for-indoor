function [edgesCoords, edgesNum] = prelinker(im, vertices, radius, text)

%% Init
h = waitbar(0, text);
temp      = zeros(length(vertices(:, 1))*8, 5);
temp2     = zeros(length(vertices(:, 1))*8, 3);
connected = logical(false(length(vertices(:, 1))));
counter   = 0;
counter2  = 0;

%% Set edges
for i = 1:length(vertices(:, 1))-1
    for j = i+1:length(vertices(:, 1))
        dist   = get_len(vertices(i, :), vertices(j, :));
        isWall = fCorrectIfBlack(im, vertices(i, :), vertices(j, :));
        if dist < radius && ~connected(i, j) && ~isWall
            connected(i, j) = true;
            connected(j, i) = true;
            counter = counter + 1;
            temp(counter, :)  = [vertices(i, :) vertices(j, :) dist];
            temp2(counter, :) = [i j dist];
        end
        counter2  = counter2 + 1;
        progress = round(counter2*100 / (length(vertices(:, 1))*(length(vertices(:, 1))/2+0.5)))/100;
        waitbar(progress);
    end
    edgesCoords = temp(1:counter, :);
    edgesNum    = temp2(1:counter, :);
end

close(h);
end