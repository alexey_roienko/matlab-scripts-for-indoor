function [vertices, edges] = read_graph(path)


%% Count number of vertices and edges
% Vertices
fID = fopen(path, 'r');
if fID == -1
    msgbox('read_graph.m: File "graph_import.txt" does not exist!');
    fclose(fID);
    return;
end


%% Read the first caption
caption = fgetl(fID);
if ~strcmpi(caption, '->GRAPH')
    msgbox('read_graph.m: Wrong the first caption in "graph_import.txt"!');
    fclose(fID);
    return;
end

%% Read Verteces
maxVertN = 2000;
temp = zeros(maxVertN, 2);
vCounter = 0;

[A, numbReadN] = fscanf(fID, '%d', 2);
while numbReadN == 2    
    vCounter = vCounter + 1;
    temp(vCounter, :) = A;
    [A, numbReadN] = fscanf(fID, '%d', 2);
end
vertices = temp(1:vCounter, :);
clear temp



%% Read the second caption
caption = fgetl(fID);
if ~strcmpi(caption, '->EDGES')
    msgbox('read_graph.m: Wrong the second caption in "graph_import.txt"!');
    fclose(fID);
    return;
end

%% Read Edges
edges = (fscanf(fID, '%d', [3, Inf]))';

if isempty(vertices) || isempty(edges)
    msgbox('read_graph.m: File "graph_import.txt" is empty!');
    fclose(fID);
    return;
end

fclose(fID);
end


