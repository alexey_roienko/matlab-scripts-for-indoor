function [xNew, yNew] = magnet(x, y, mesh)

radius = 200;
xNew   = [];
yNew   = [];

minimum = 1000;
for j = 1:length(mesh(:, 1))
    dist = get_len([x y], mesh(j, :));
    if dist < radius && dist < minimum
        minimum = dist;
        xNew = mesh(j, 1);
        yNew = mesh(j, 2);
    end
end

end