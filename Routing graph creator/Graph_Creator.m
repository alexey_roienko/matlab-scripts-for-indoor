function varargout = Graph_Creator(varargin)
% Graph_Creator MATLAB code for Graph_Creator.fig
%      Graph_Creator, by itself, creates a new Graph_Creator or raises the existing
%      singleton*.
%
%      H = Graph_Creator returns the handle to a new Graph_Creator or the handle to
%      the existing singleton*.
%
%      Graph_Creator('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in Graph_Creator.M with the given input arguments.
%
%      Graph_Creator('Property','Value',...) creates a new Graph_Creator or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Graph_Creator_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Graph_Creator_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Graph_Creator

% Last Modified by GUIDE v2.5 25-May-2017 15:27:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Graph_Creator_OpeningFcn, ...
                   'gui_OutputFcn',  @Graph_Creator_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


function Graph_Creator_OpeningFcn(hObject, eventdata, handles, varargin)

% Choose default command line output for Graph_Creator
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% Init
clc; cla;
global plots;
global masks;
global im;
global roomSize;
global pSize;
global manual_mode;
global maskPars;
manual_mode = false;
set(handles.popupmenu1, 'String', '');

% Change matlab logo with IT-Jim logo
warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
jframe = get(handles.figure1,'javaframe');
jIcon = javax.swing.ImageIcon('logo.jpg');
jframe.setFigureIcon(jIcon);

% Set double row text of "Save graph" button
set(handles.pushbutton5, 'String', {'Save' 'graph'});

% Switch status of interface elements
set(findall(handles.uipanel4, '-property', 'enable'), 'enable', 'off'); % "Observer" panel
set(findall(handles.uipanel6, '-property', 'enable'), 'enable', 'off'); % "Graph editor" panel
set(handles.uipanel10, 'Visible', 'off'); % Max edge length control panel
set(handles.pushbutton1, 'Enable', 'on'); % "Generate vertices" button
set(handles.pushbutton5, 'Enable', 'off'); % "Save graph" button
set(handles.checkbox7, 'Enable', 'on'); % "Manual mode" checkbox
set(handles.checkbox7, 'Value', 0); % "Manual mode" checkbox

% Find all masks in the input folder
[masks, maskPars] = fInitMaskList('input\');

maskList = cell(size(maskPars));
for i = 1:length(maskPars)
    maskList{i} = maskPars{i}.name(1:end-4);
end
set(handles.popupmenu1, 'String', maskList);

% Define the office chosen in popup_menu
office = get(handles.popupmenu1, 'Value');

% Set global variables
roomSize = [maskPars{office}.width maskPars{office}.height];
pSize    = [maskPars{office}.pixelSize maskPars{office}.pixelSize];

% Plot mask of the office
im    = masks{office};
plots = plot_mask(im, handles.axes1);

% Get sliders' values
sliderPos1 = get(handles.slider1, 'Value');
sliderPos2 = get(handles.slider2, 'Value');
set(handles.edit1, 'String', num2str(sliderPos1*10));
set(handles.edit2, 'String', num2str(sliderPos2*10));


function varargout = Graph_Creator_OutputFcn(hObject, eventdata, handles) 

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)

pos1 = get(hObject, 'Value')*10;
pos2 = get(handles.slider2, 'Value')*10;
pos3 = get(handles.slider3, 'Value')*10;
set(handles.edit1, 'String', num2str(pos1));
if pos1 > pos2
    set(handles.slider2, 'Value', pos1/10);
    pos2 = get(handles.slider2, 'Value')*10;
    set(handles.edit2, 'String', num2str(pos1));
    if pos2 > pos3
        set(handles.slider3, 'Value', ceil(pos2/10));
        pos3 = get(handles.slider3, 'Value')*10;
        set(handles.edit3, 'String', num2str(pos3));
    end
end


function slider1_CreateFcn(hObject, eventdata, handles)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function edit1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function popupmenu1_Callback(hObject, eventdata, handles)

% Init
cla;
global plots;
global masks;
global im;
global roomSize;
global pSize;
global manual_mode;
global maskPars;

% Switch status of interface elements
set(findall(handles.uipanel4, '-property', 'enable'), 'enable', 'off'); % "Observer" panel
set(findall(handles.uipanel6, '-property', 'enable'), 'enable', 'off'); % "Graph editor" panel
set(handles.uipanel7, 'Visible', 'on'); % "Vetices" panel
set(handles.uipanel8, 'Visible', 'off'); % "Edges" panel
set(handles.uipanel10, 'Visible', 'off'); % Max edge length control panel
set(handles.pushbutton1, 'Enable', 'on'); % "Generate vertices" button
set(handles.pushbutton5, 'Enable', 'off'); % "Save graph" button
set(handles.radiobutton1, 'Value', 1); % "Vertices" radiobutton
set(handles.checkbox7, 'Enable', 'on'); % "Manual mode" checkbox
set(handles.checkbox7, 'Value', 0); % "Manual mode" checkbox

% Disable manual mode
manual_mode = disable_manual_mode(handles);

% Diable import mode
set(handles.togglebutton5, 'Value', 0); % "Import mode" button
set(handles.togglebutton5, 'BackGroundColor', [0.9 0.9 1]);
set(handles.togglebutton5, 'String', 'Import mode');

% Reset Logger fields
set(handles.text9, 'String' , '0');
set(handles.text8, 'String' , {'0' '0' '0'});
set(handles.text9, 'String' , {'0' '0' '0'});

% Define the office chosen in popup_menu
office = get(hObject, 'Value');

% Set global variables
roomSize = [maskPars{office}.width maskPars{office}.height];
pSize    = [maskPars{office}.pixelSize maskPars{office}.pixelSize];

% Plot mask of the office
im    = masks{office};
plots = plot_mask(im, handles.axes1);


function popupmenu1_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton1_Callback(hObject, eventdata, handles)

% Init
global nodesNum;
global vertCount;
global addedVertCount;
global addedVertArray;
global plots;
global im;
global roomSize;
global naviMesh;
global vertices;
global vertices_backup;
global edgesPlot;
global manual_mode;
addedVertCount = 0;
addedVertArray = [];
pos = ceil(get(handles.slider2, 'Value'));
set(handles.slider3, 'Value', pos);
set(handles.edit3, 'String', num2str(pos*10));

if ~manual_mode && get(handles.togglebutton5, 'Value') == 0
    % Clear edges
    delete(edgesPlot);
    edgesPlot = line(1, 1);
    
    % Switch status of interface elements
    set(findall(handles.uipanel1, '-property', 'enable'), 'enable', 'off'); % main panel
    set(findall(handles.uipanel5, '-property', 'enable'), 'enable', 'on'); % "Logger" panel
    
    % Get sliders' values
    mStep1 = get(handles.slider1, 'Value')/10;
    mStep2 = get(handles.slider2, 'Value')/10;
    
    % Generate navigation mesh and graph vertices
    text = 'Navigation mesh generation ...';
    naviMesh = mesh_generator(im, roomSize, mStep1, text);
    text = 'Graph vertices generation ...';
    vertices = mesh_generator(im, roomSize, mStep2, text);
    vertices_backup = vertices;
    
    % Fill logger fields
    nodesNum  = length(naviMesh(:, 1));
    vertCount = length(vertices(:, 1));
    set(handles.text7, 'String', num2str(nodesNum));
    set(handles.text8, 'String', {num2str(vertCount) num2str(vertCount) '0'});
    set(handles.text9, 'String', {'0' '0' '0'});
    
    % Plot results
    set(plots(2), 'XData', naviMesh(:, 1), 'YData', naviMesh(:, 2), 'Color', 'red', 'MarkerSize', 10);
    set(plots(3), 'XData', vertices(:, 1), 'YData', vertices(:, 2), 'Color', 'blue', 'MarkerSize', 20);
    
    % Switch on checkboxes
    set(handles.checkbox1, 'Value', 1);
    set(handles.checkbox2, 'Value', 1);
    
    % Switch status of interface elements
    set(findall(handles.uipanel2, '-property', 'enable'), 'enable', 'on'); % "Mask list" panel
    set(findall(handles.uipanel3, '-property', 'enable'), 'enable', 'on'); % "Mesh properties" panel
    set(findall(handles.uipanel7, '-property', 'enable'), 'enable', 'on'); % "Vertices" panel
    set(findall(handles.uipanel10, '-property', 'enable'), 'enable', 'on'); % Max edge length control panel
    set(findall(handles.uibuttongroup1, '-property', 'enable'), 'enable', 'on'); % "What to edit?" buttongroup
    set(handles.pushbutton2, 'Enable', 'off'); % "Reset vertices" button
    set(handles.pushbutton3, 'Enable', 'on'); % "Generate edges" button
    set(handles.togglebutton5, 'Enable', 'on'); % "Import mode" button
    set(handles.checkbox1, 'Enable', 'on'); % "Navigation mesh" checkbox
    set(handles.checkbox2, 'Enable', 'on'); % "Graph vertices" checkbox
    set(handles.checkbox7, 'Enable', 'on'); % "Manual mode" checkbox
end

if manual_mode && get(handles.togglebutton5, 'Value') == 0
    % Clear edges
    delete(edgesPlot);
    edgesPlot = line(1, 1);
    
    % Switch status of interface elements
    set(findall(handles.uipanel1, '-property', 'enable'), 'enable', 'off'); % main panel
    set(findall(handles.uipanel5, '-property', 'enable'), 'enable', 'on'); % "Logger" panel
    
    % Get slider values
    mStep1 = get(handles.slider1, 'Value')/10;
    
    % Generate navigation mesh
    text = 'Navigation mesh generation ...';
    naviMesh = mesh_generator(im, roomSize, mStep1, text);
    vertices = [];
    vertices_backup = vertices;
    
    % Fill logger fields
    nodesNum  = length(naviMesh(:, 1));
    set(handles.text7, 'String', num2str(nodesNum));
    
    % Plot results
    set(plots(2), 'XData', naviMesh(:, 1), 'YData', naviMesh(:, 2), 'Color', 'red', 'MarkerSize', 10);
    set(plots(3), 'XData', 0, 'YData', 0);
    
    % Switch on checkbox
    set(handles.checkbox1, 'Value', 1);
    
    % Switch status of interface elements
    set(handles.text3, 'Enable', 'off'); % "Graph vertices step"
    set(findall(handles.uipanel2, '-property', 'enable'), 'enable', 'on'); % "Mask list" panel
    set(findall(handles.uipanel3, '-property', 'enable'), 'enable', 'on'); % "Mesh properties" panel
    set(findall(handles.uipanel7, '-property', 'enable'), 'enable', 'on'); % "Vertices" panel
    set(findall(handles.uipanel10, '-property', 'enable'), 'enable', 'on'); % Max edge length control panel
    set(findall(handles.uibuttongroup1, '-property', 'enable'), 'enable', 'on'); % "What to edit?" buttongroup
    set(handles.pushbutton2, 'Enable', 'off'); % "Reset vertices" button
    if get(handles.togglebutton5, 'Value') == 0
        set(handles.togglebutton5, 'Enable', 'off'); % "Import mode" button
    end
    set(handles.checkbox1, 'Enable', 'on'); % "Navigation mesh" checkbox
    if ~isempty(vertices)
        set(handles.checkbox2, 'Enable', 'on'); % "Graph vertices" checkbox
    end
    set(handles.checkbox7, 'Enable', 'on'); % "Manual mode" checkbox
    set(handles.edit2, 'Enable', 'off'); % "Graph vertices step" edit
    set(handles.slider2, 'Enable', 'off'); % "Graph vertices" slider
end

if get(handles.togglebutton5, 'Value') == 1
    % Switch status of interface elements
    set(findall(handles.uipanel1, '-property', 'enable'), 'enable', 'off'); % main panel
    set(findall(handles.uipanel5, '-property', 'enable'), 'enable', 'on'); % "Logger" panel
    
    % Get slider values
    mStep1 = get(handles.slider1, 'Value')/10;
    
    % Generate navigation mesh
    text = 'Navigation mesh generation ...';
    naviMesh = mesh_generator(im, roomSize, mStep1, text);
    vertices_backup = vertices;
    
    % Fill logger fields
    nodesNum  = length(naviMesh(:, 1));
    set(handles.text7, 'String', num2str(nodesNum));
    
    % Plot results
    set(plots(2), 'XData', naviMesh(:, 1), 'YData', naviMesh(:, 2), 'Color', 'red', 'MarkerSize', 10);
    
    % Go to edges editing
    set(handles.uipanel7, 'Visible', 'off');
    set(handles.uipanel8, 'Visible', 'on');
        
    % Switch status of interface elements
    set(findall(handles.uipanel2, '-property', 'enable'), 'enable', 'on'); % "Mask list" panel
    set(findall(handles.uipanel3, '-property', 'enable'), 'enable', 'on'); % "Mesh properties" panel
    set(findall(handles.uipanel4, '-property', 'enable'), 'enable', 'on'); % "Observer" panel
    set(findall(handles.uipanel7, '-property', 'enable'), 'enable', 'on'); % "Vertices" panel
    set(findall(handles.uipanel9, '-property', 'enable'), 'enable', 'on'); % "Saver" panel
    set(findall(handles.uipanel10, '-property', 'enable'), 'enable', 'on'); % Max edge length control panel
    set(findall(handles.uibuttongroup1, '-property', 'enable'), 'enable', 'on'); % "What to edit?" buttongroup
    set(handles.pushbutton2, 'Enable', 'off'); % "Reset vertices" button
    set(handles.togglebutton3, 'Enable', 'on'); % "Add edges" button
    set(handles.togglebutton4, 'Enable', 'on'); % "Delete edges" button
    set(handles.togglebutton5, 'Enable', 'on'); % "Import mode" button
    set(handles.checkbox7, 'Enable', 'off'); % "Manual mode" checkbox
    set(handles.edit2, 'Enable', 'off'); % "Graph vertices step" edit
    set(handles.slider2, 'Enable', 'off'); % "Graph vertices" slider
    set(handles.radiobutton2, 'Value', 1); % Activate "Edges" radiobutton
end

function pushbutton2_Callback(hObject, eventdata, handles)

% Init
global vertCount
global addedVertCount;
global plots;
global vertices;
global vertices_backup;
global manual_mode;
addedVertCount = 0;

% Plot backuped vertices
vertices = vertices_backup;
if manual_mode && get(handles.togglebutton5, 'Value') == 0
    set(plots(3), 'XData', 0, 'YData', 0);
    vertCount = 0;
    set(handles.pushbutton3, 'Enable', 'off'); % "Generate edges" button
else
    set(plots(3), 'XData', vertices(:, 1), 'YData', vertices(:, 2));
    vertCount = length(vertices(:, 1));
end

% Fill Logger fields
set(handles.text8, 'String', {num2str(vertCount) num2str(vertCount) 0});

% Switch status of interface elemetns
set(hObject, 'Enable', 'off'); % "Reset vertices" button


function slider2_CreateFcn(hObject, eventdata, handles)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function edit2_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function checkbox1_Callback(hObject, eventdata, handles)

% Init
global plots;
global naviMesh;

if get(hObject, 'Value') == 1
    set(plots(2), 'XData', naviMesh(:, 1), 'YData', naviMesh(:, 2));
else
    set(plots(2), 'XData', 0, 'YData', 0);
end


function checkbox2_Callback(hObject, eventdata, handles)

% Init
global plots;
global vertices;

if get(hObject, 'Value') == 1
    set(plots(3), 'XData', vertices(:, 1), 'YData', vertices(:, 2));
else
    set(plots(3), 'XData', 0, 'YData', 0);
end

function slider2_Callback(hObject, eventdata, handles)

pos2 = get(hObject, 'Value')*10;
pos1 = get(handles.slider1, 'Value')*10;
if pos2 >= pos1
    set(handles.edit2, 'String', num2str(pos2));
else
    set(hObject, 'Value', pos1/10);
end

pos3 = get(handles.slider3, 'Value')*10;
if pos2 > pos3
    set(handles.slider3, 'Value', ceil(pos2/10));
    pos3 = get(handles.slider3, 'Value')*10;
    set(handles.edit3, 'String', num2str(pos3));
end


function togglebutton1_Callback(hObject, eventdata, handles)

% Init
global vertCount;
global addedVertCount;
global addedVertArray;
global plots;
global im;
global naviMesh;
global vertices;
global manual_mode;

% Switch status of interface elemetns
set(handles.togglebutton2, 'Enable', 'off');
set(findall(handles.uibuttongroup1, '-property', 'Enable'), 'Enable', 'off');

% Check status of the button (pushed/released)
if get(hObject, 'Value') == 0
    % Switch button color to green
    set(hObject, 'BackGroundColor', [0.3 0.9 0.3]);
    % Change text
    set(hObject, 'String', 'Add vertices');
else
    % Switch button color to yellow 
    set(hObject, 'BackGroundColor', [0.9 0.9 0.3]);
    % Change text
    set(hObject, 'String', 'Finish adding');
    % Init
    temp  = zeros(ceil(length(vertices)/2), 2);
    match = false;
    hold on;
    % While the button is pressed
    while get(hObject, 'Value') == 1
        [x, y] = ginput(1);
        % Check image border conditions
        if x <= length(im(1, :)) && y <= length(im(:, 1))
            [x, y] = magnet(x, y, naviMesh);
            if ~isempty(x) && ~isempty(y)
                % Check if this point has been chosen before
                if ~isempty(vertices)
                    for i = 1:length(vertices(:, 1))
                        if x == vertices(i, 1) && y == vertices(i, 2)
                            match = true;
                            break;
                        end
                    end
                end
                % If there is no match
                if ~match
                    addedVertCount = addedVertCount + 1;
                    % Fill Logger fields
                    set(handles.text8, 'String', {num2str(vertCount+addedVertCount) num2str(vertCount) num2str(addedVertCount)});
                    % Save the coordinates of the drawn vertex in temporary
                    % array
                    temp(addedVertCount, :) = [x y];
                    % Plot the drawn vertex
                    vertices = expand2D(vertices, temp(addedVertCount, :));
                    set(plots(3), 'XData', vertices(:, 1), 'YData', vertices(:, 2), 'Color', 'blue', 'MarkerSize', 20);
                else
                    match = false;
                end
            end
        end
    end
    
    % If at least one vertex has been added
    if addedVertCount > 0
        addedVertArray = vertices(end-addedVertCount+1:end, :);
        set(handles.pushbutton2, 'Enable', 'on'); % "Reset vertices" button
        set(handles.checkbox2, 'Enable', 'on'); % "Graph vertices" checkbox
        if manual_mode && get(handles.togglebutton5, 'Value') == 0 && addedVertCount > 1
            set(handles.pushbutton3, 'Enable', 'on'); % "Generate edges" button
        end
    end
    
    % Switch status of interface elemetns
    set(handles.togglebutton2, 'Enable', 'on');
    set(findall(handles.uibuttongroup1, '-property', 'Enable'), 'Enable', 'on');
end


function pushbutton3_Callback(hObject, eventdata, handles)

% Init
global im;
global edgeCount;
global addedEdgeCount;
global addedEdgeArray;
global pSize;
global vertices;
global edgesNum;
global edgesCoords;
global edgesPlot;
global edgesCoords_backup;
global edgesNum_backup;
global manual_mode;
addedEdgeCount = 0;
addedEdgeArray = [];

delete(edgesPlot);
edgesPlot = line(1, 1);

% Switch status of interface elements
set(findall(handles.uipanel1, '-property', 'enable'), 'enable', 'off'); % main panel
set(findall(handles.uipanel5, '-property', 'enable'), 'enable', 'on'); % "Logger" panel

% Get max edge length
radius = get(handles.slider3, 'Value')/10;
radius = radius/mean(pSize);

% Set links between current vertex and surrounding ones within the radius
if isempty(vertices)
    edgesCoords = [];
    edgesCoords_backup = edgesCoords;
    edgesNum = [];
    edgesNum_backup = edgesNum;
    
    % Fill logger fields
    edgeCount = 0;
    set(handles.text9, 'String', {'0' '0' '0'});
    
    % Plot edges
    delete(edgesPlot);
    edgesPlot = line(1, 1);
else
    text   = 'Edges generation ...';
    [edgesCoords, edgesNum] = prelinker(im, vertices, radius, text);
    edgesCoords_backup = edgesCoords;
    edgesNum_backup = edgesNum;
    
    % Fill logger fields
    edgeCount = length(edgesCoords(:, 1));
    set(handles.text9, 'String', {num2str(edgeCount) num2str(edgeCount) '0'});
    
    % Plot edges
    for i = 1:length(edgesCoords(:, 1))
        hold on;
        edgesPlot(i) = plot(edgesCoords(i, [1 3]), edgesCoords(i, [2 4]), 'b');
    end
end

% Switch on checkbox
set(handles.checkbox3, 'Value', 1);

% Switch the interface elements status
set(findall(handles.uipanel2, '-property', 'enable'), 'enable', 'on'); % "Mask list" panel
set(findall(handles.uipanel3, '-property', 'enable'), 'enable', 'on'); % "Mesh properties" panel
set(findall(handles.uipanel8, '-property', 'enable'), 'enable', 'on'); % "Edges" panel
set(findall(handles.uipanel9, '-property', 'enable'), 'enable', 'on'); % "Saver" panel
set(findall(handles.uipanel10, '-property', 'enable'), 'enable', 'on'); % Max edge length control panel
set(findall(handles.uibuttongroup1, '-property', 'enable'), 'enable', 'on'); % "What to edit?" buttongroup
set(handles.checkbox1, 'Enable', 'on'); % "Navigation mesh" checkbox
set(handles.checkbox2, 'Enable', 'on'); % "Graph vertices" checkbox
set(handles.checkbox3, 'Enable', 'on'); % "Graph edges" checkbox
set(handles.checkbox7, 'Enable', 'on'); % "Manual mode" checkbox
set(handles.pushbutton1, 'Enable', 'on'); % "Generate vertices" button
set(handles.pushbutton4, 'Enable', 'off'); % "Reset edges" button
if manual_mode
    if get(handles.togglebutton5, 'value') == 0
        set(handles.togglebutton5, 'Enable', 'off'); % "Import mode" button
    end
    set(handles.edit2, 'Enable', 'off'); % "Graph vertices step" edit
    set(handles.slider2, 'Enable', 'off'); % "Graph vertices step" slider
else
    set(handles.togglebutton5, 'Enable', 'on'); % "Import mode" button
end


function togglebutton2_Callback(hObject, eventdata, handles)

% Init
global vertCount;
global addedVertCount;
global addedVertArray;
global plots;
global im;
global vertices;
global manual_mode;

% Switch status of interface elemetns
set(handles.togglebutton1, 'Enable', 'off');
set(findall(handles.uibuttongroup1, '-property', 'Enable'), 'Enable', 'off');

% Check status of the button (pushed/released)
if get(hObject, 'Value') == 0
    % Switch button color to red
    set(hObject, 'BackGroundColor', [0.9 0.3 0.3]);
    
    % Change text
    set(hObject, 'String', 'Delete vertices');
else
    % Switch button color to yellow
    set(hObject, 'BackGroundColor', [0.9 0.9 0.3]);
    % Change text
    set(hObject, 'String', 'Finish deleting');
    
    % Init
    vertNum = 0;
    counter = 0;
    
    % While the button is pressed
    while get(hObject, 'Value') == 1 && ~isempty(vertices)
        [x, y] = ginput(1);
        % Check image border conditions
        if x <= length(im(1, :)) && y <= length(im(:, 1))
            [x, y] = magnet(x, y, vertices);
            if ~isempty(x) && ~isempty(y)
                counter = counter + 1;
                % Define the number of coordinates pair in vertices array
                for i = 1:length(vertices(:, 1))
                    if x == vertices(i, 1) && y == vertices(i, 2)
                        vertNum = i;
                        break;
                    end
                end
                % Delte this vertex from vertices array
                vertices = reduce2D(vertices, vertNum, 1);
                vertCount = vertCount - 1;
                % Fill Logger fields
                if addedVertCount > 0
                    for i = 1:length(addedVertArray(:, 1))
                        if x == addedVertArray(i, 1) && y == addedVertArray(i, 2)
                            addedVertCount = addedVertCount - 1;
                            vertCount = vertCount + 1;
                            break;
                        end
                    end
                end
                % Fill Logger fileds
                set(handles.text8, 'String', {num2str(vertCount+addedVertCount) num2str(vertCount) num2str(addedVertCount)});
                % Plot the drawn vertex
                set(plots(3), 'XData', vertices(:, 1), 'YData', vertices(:, 2));
            end
        end
    end
    % If at least one vertex has been deleted
    if counter > 0
        set(handles.pushbutton2, 'Enable', 'on'); % "Reset vertices" button
    end
    if isempty(vertices) || length(vertices(:, 1)) == 1
        set(handles.pushbutton3, 'Enable', 'off'); % "Generate edges" button
        set(handles.togglebutton3, 'Enable', 'off'); % "Add edges" button
        set(handles.togglebutton4, 'Enable', 'off'); % "Delete edges" button
    else
        if manual_mode && get(handles.togglebutton5, 'Value') == 0
            set(handles.pushbutton3, 'Enable', 'on'); % "Generate edges" button
        end
    end
    
    % Switch status of interface elemetns
    set(handles.togglebutton1, 'Enable', 'on');
    set(findall(handles.uibuttongroup1, '-property', 'Enable'), 'Enable', 'on');
end


function checkbox3_Callback(hObject, eventdata, handles)

global edgesCoords;
global edgesPlot;

if get(hObject, 'Value') == 1
    hold on;
    for i = 1:length(edgesCoords(:, 1))
        edgesPlot(i) = plot(edgesCoords(i, [1 3]), edgesCoords(i, [2 4]), 'b');
    end
else
    delete(edgesPlot);
end


function togglebutton4_Callback(hObject, eventdata, handles)

% Init
global edgeCount;
global addedEdgeCount;
global addedEdgeArray;
global im;
global vertices;
global edgesCoords;
global edgesNum;
global edgesPlot;

% Switch status of interface elemetns
set(handles.togglebutton3, 'Enable', 'off');
set(findall(handles.uibuttongroup1, '-property', 'Enable'), 'Enable', 'off');

% Delete edges
if get(hObject, 'Value') == 0
    % Switch button color to red
    set(hObject, 'BackGroundColor', [0.9 0.3 0.3]);
    % Change text
    set(hObject, 'String', 'Delete edges');
else
    % Switch button color to yellow
    set(hObject, 'BackGroundColor', [0.9 0.9 0.3]);
    % Change text
    set(hObject, 'String', 'Finish deleting');
    % Init
    count      = 1;
    x          = zeros(1, 2); 
    y          = zeros(1, 2);
    is_deleted = false;
    hold on;
    pair = plot(0, 0, '.');
    % While the button is pressed
    while get(hObject, 'Value') == 1
        % Read cursor position
        [x(count), y(count)] = ginput(1);
        % Image border consition
        if x(count) <= length(im(1, :)) && y(count) <= length(im(:, 1))
            % Move the point to the nearest mesh vertex
            [x(count), y(count)] = magnet(x(count), y(count), vertices);
            if ~isempty(x(count)) && ~isempty(y(count))
                % Outline chosen vertices
                set(pair, 'XData', x(1:count), 'YData', y(1:count), 'Color', 'red', 'Marker', '.', 'MarkerSize', 30);
                count = count + 1;
                if count == 3 && (x(count-1) ~= x(count-2) || y(count-1) ~= y(count-2))
                    count = 1;
                    for i = 1:length(edgesCoords(:, 1))
                        if (x(1) == edgesCoords(i, 1) && x(2) == edgesCoords(i, 3) && ...
                                y(1) == edgesCoords(i, 2) && y(2) == edgesCoords(i, 4)) || ...
                                (x(1) == edgesCoords(i, 3) && x(2) == edgesCoords(i, 1) && ...
                                y(1) == edgesCoords(i, 4) && y(2) == edgesCoords(i, 2))
                            edgesCoords = reduce2D(edgesCoords, i, 1);
                            edgesNum = reduce2D(edgesNum, i, 1);
                            edgeCount = edgeCount - 1;
                            is_deleted = true;
                            break;
                        end
                    end
                    % Fill Logger fields
                    if addedEdgeCount > 0
                        for i = 1:length(addedEdgeArray(:, 1))
                            if (x(1) == addedEdgeArray(i, 1) && x(2) == addedEdgeArray(i, 3) && ...
                                    y(1) == addedEdgeArray(i, 2) && y(2) == addedEdgeArray(i, 4)) || ...
                                    (x(1) == addedEdgeArray(i, 3) && x(2) == addedEdgeArray(i, 1) && ...
                                    y(1) == addedEdgeArray(i, 4) && y(2) == addedEdgeArray(i, 2)) || ...
                                    (x(1) == x(2) && y(1) == y(2))
                                addedEdgeCount = addedEdgeCount - 1;
                                edgeCount = edgeCount + 1;
                                break;
                            end
                        end
                    end
                    % Fill Logger fileds
                    set(handles.text9, 'String', {num2str(edgeCount+addedEdgeCount) num2str(edgeCount) num2str(addedEdgeCount)});
                    delete(edgesPlot);
                    for i = 1:length(edgesCoords(:, 1))
                        edgesPlot(i) = plot(edgesCoords(i, [1 3]), edgesCoords(i, [2 4]), 'b');
                    end
                end
            end
        end
    end
    delete(pair);
    if is_deleted
        set(handles.pushbutton4, 'Enable', 'on'); % button "Reset edges"
    end
    
    % Switch status of interface elemetns
    set(handles.togglebutton3, 'Enable', 'on');
    set(findall(handles.uibuttongroup1, '-property', 'Enable'), 'Enable', 'on');
end


function pushbutton4_Callback(hObject, eventdata, handles)

% Init
global edgeCount
global addedEdgeCount;
global edgesCoords;
global edgesNum;
global edgesCoords_backup;
global edgesNum_backup;
global edgesPlot;

edgesCoords = edgesCoords_backup;
edgesNum    = edgesNum_backup;
% Fill Logger fields
edgeCount = length(edgesCoords(:, 1));
addedEdgeCount = 0;
set(handles.text9, 'String', {num2str(edgeCount) num2str(edgeCount) 0});
% Plot backuped edges
delete(edgesPlot);
hold on;
for i = 1:length(edgesCoords(:, 1))
    edgesPlot(i) = plot(edgesCoords(i, [1 3]), edgesCoords(i, [2 4]), 'b');
end
set(hObject, 'Enable', 'off');


function pushbutton5_Callback(hObject, eventdata, handles)

% Init
global im;
global nodesNum;
global vertCount;
global addedVertCount;
global edgeCount;
global addedEdgeCount;
global edgesNum;
global edgesCoords;
global naviMesh;
global vertices;
edgesPlot = line(1, 1);

% Switch status of interface elements
status1 = get(handles.pushbutton4, 'Enable'); % "Reset edges" button
status2 = get(handles.slider2, 'Enable'); % "Vertices mesh step" slider
status3 = get(handles.edit2, 'Enable'); % "Vertices mesh step" edit
set(findall(handles.uipanel2, '-property', 'enable'), 'enable', 'off'); % "Mask list" panel
set(findall(handles.uipanel3, '-property', 'enable'), 'enable', 'off'); % "Mesh properties" panel
set(findall(handles.uipanel4, '-property', 'enable'), 'enable', 'off'); % "Observer" panel
set(findall(handles.uipanel5, '-property', 'enable'), 'enable', 'off'); % "Logger" panel
set(findall(handles.uipanel8, '-property', 'enable'), 'enable', 'off'); % "Edges" panel
set(findall(handles.uipanel9, '-property', 'enable'), 'enable', 'off'); % "Saver" panel
set(findall(handles.uipanel10, '-property', 'enable'), 'enable', 'off'); % Max edge length control panel
set(findall(handles.uibuttongroup1, '-property', 'enable'), 'enable', 'off'); % "What to edit?" buttongroup
set(handles.pushbutton4, 'Enable', 'off');
set(handles.checkbox7, 'Enable', 'off'); % "Manual mode" checkbox

% Save graph
path = 'output\graph.txt';
save_graph(path, vertices, edgesNum);

% Read statuses of "Saver" checkboxes
status4 = get(handles.checkbox4, 'Value'); % "Logger data" checkbox
status5 = get(handles.checkbox5, 'Value'); % "Mask with mesh" checkbox
status6 = get(handles.checkbox6, 'Value'); % "Mask with graph" checkbox

% Consider combinations of checkboxes statuses
if status4 == 1
    path = 'output\log.txt';
    save_log(path, nodesNum, vertCount, addedVertCount, edgeCount, addedEdgeCount);
end
if status5 == 1
    figure();
    set(gcf, 'Visible', 'off');
    plots(1) = imshow(im);
    hold on;
    plots(2) = plot(0, 0, '.');
    set(plots(2), 'XData', naviMesh(:, 1), 'YData', naviMesh(:, 2), 'Color', 'red', 'MarkerSize', 10);
    saveas(gcf,'output\Mask with mesh.jpg');
    close(gcf);
end
if status6 == 1
    figure();
    set(gcf, 'Visible', 'off');
    plots(1) = imshow(im);
    hold on;
    plots(2) = plot(0, 0, '.');
    set(plots(2), 'XData', vertices(:, 1), 'YData', vertices(:, 2), 'Color', 'blue', 'MarkerSize', 20);
    for i = 1:length(edgesCoords(:, 1))
        hold on;
        edgesPlot(i) = plot(edgesCoords(i, [1 3]), edgesCoords(i, [2 4]), 'b');
    end
    saveas(gcf,'output\Mask with graph.jpg');
    close(gcf);
end

% Show message of save procedure success
success;

% Switch status of interface elements
set(findall(handles.uipanel2, '-property', 'enable'), 'enable', 'on'); % "Mask list" panel
set(findall(handles.uipanel3, '-property', 'enable'), 'enable', 'on'); % "Mesh properties" panel
set(findall(handles.uipanel4, '-property', 'enable'), 'enable', 'on'); % "Observer" panel
set(findall(handles.uipanel5, '-property', 'enable'), 'enable', 'on'); % "Logger" panel
set(findall(handles.uipanel8, '-property', 'enable'), 'enable', 'on'); % "Edges" panel
set(findall(handles.uipanel9, '-property', 'enable'), 'enable', 'on'); % "Saver" panel
set(findall(handles.uipanel10, '-property', 'enable'), 'enable', 'on'); % Max edge length control panel
set(findall(handles.uibuttongroup1, '-property', 'enable'), 'enable', 'on'); % "What to edit?" buttongroup
set(handles.pushbutton4, 'Enable', status1); % "Reset edges" button
set(handles.slider2, 'Enable', status2); % "Vertices mesh step" slider
set(handles.edit2, 'Enable', status3); % "Vertices mesh step" edit
set(handles.checkbox7, 'Enable', 'on'); % "Manual mode" checkbox


function togglebutton3_Callback(hObject, eventdata, handles)

% Init
global edgeCount;
global addedEdgeCount;
global addedEdgeArray;
global im;
global vertices;
global edgesCoords;
global edgesNum;
global edgesPlot;

% Switch status of interface elemetns
set(handles.togglebutton4, 'Enable', 'off');
set(findall(handles.uibuttongroup1, '-property', 'Enable'), 'Enable', 'off');

% Delete edges
if get(hObject, 'Value') == 0
    % Switch button color to green
    set(hObject, 'BackGroundColor', [0.3 0.9 0.3]);
    % Change text
    set(hObject, 'String', 'Add edges');
else
    % Switch button color to yellow 
    set(hObject, 'BackGroundColor', [0.9 0.9 0.3]);
    % Change text
    set(hObject, 'String', 'Finish adding');
    % Init
    count       = 1;
    x           = zeros(1, 2);
    y           = zeros(1, 2);
    match       = false;
    hold on;
    pair = plot(0, 0, '.');
    while get(hObject, 'Value') == 1
        new_edge    = [];
        new_edgeNum = [];
        % Read cursor position
        [x(count), y(count)] = ginput(1);
        % Image border consition
        if x(count) <= length(im(1, :)) && y(count) <= length(im(:, 1))
            % Move the point to the nearest mesh vertex
            [x(count), y(count)] = magnet(x(count), y(count), vertices);
            if ~isempty(x(count)) && ~isempty(y(count))
                % Outline chosen vertices
                set(pair, 'XData', x(1:count), 'YData', y(1:count), 'Color', 'green', 'Marker', '.', 'MarkerSize', 30);
                count = count + 1;
                if count == 3
                    if ~isempty(edgesCoords)
                        for i = 1:length(edgesCoords(:, 1))
                            if (x(1) == edgesCoords(i, 1) && x(2) == edgesCoords(i, 3) && ...
                                    y(1) == edgesCoords(i, 2) && y(2) == edgesCoords(i, 4)) || ...
                                    (x(1) == edgesCoords(i, 3) && x(2) == edgesCoords(i, 1) && ...
                                    y(1) == edgesCoords(i, 4) && y(2) == edgesCoords(i, 2)) || ...
                                    (x(1) == x(2) && y(1) == y(2)) || ...
                                    fCorrectIfBlack(im, [x(1) y(1)], [x(2) y(2)])
                                match = true;
                                break;
                            end
                        end
                    end
                    count = 1;
                    if ~match
                        addedEdgeCount = addedEdgeCount + 1;
                        % Fill Logger fields
                        set(handles.text9, 'String', {num2str(edgeCount+addedEdgeCount) num2str(edgeCount) num2str(addedEdgeCount)});
                        new_edge    = [x(1) y(1) x(2) y(2) get_len([x(1) y(1)], [x(2) y(2)])];
                        edgesCoords = expand2D(edgesCoords, new_edge);
                        % Find the number of vertices with coordinates
                        % [x(1) y(1)] and [x(2) y(2)]
                        for i = 1:length(vertices(:, 1))
                            if x(1) == vertices(i, 1) && y(1) == vertices(i, 2)
                                num1 = i;
                            end
                            if x(2) == vertices(i, 1) && y(2) == vertices(i, 2)
                                num2 = i;
                            end
                        end
                        new_edgeNum = [num1 num2 get_len([x(1) y(1)], [x(2) y(2)])];
                        edgesNum = expand2D(edgesNum, new_edgeNum);
                    else
                        match = false;
                    end
                    delete(edgesPlot);
                    for i = 1:length(edgesCoords(:, 1))
                        edgesPlot(i) = plot(edgesCoords(i, [1 3]), edgesCoords(i, [2 4]), 'b');
                    end
                end
            end
        end
    end
    delete(pair);
    if addedEdgeCount > 0
        addedEdgeArray = edgesCoords(end-addedEdgeCount+1:end, :);
        set(handles.pushbutton4, 'Enable', 'on'); % button "Reset edges"
    end
    
    % Switch status of interface elemetns
    set(handles.togglebutton4, 'Enable', 'on');
    set(findall(handles.uibuttongroup1, '-property', 'Enable'), 'Enable', 'on');
end


function uibuttongroup1_SelectionChangedFcn(hObject, eventdata, handles)

% Init
check = get(handles.uibuttongroup1, 'SelectedObject');
tag   = get(check, 'Tag');

% Check which of radiobuttons is active
switch tag
    case 'radiobutton1'
        set(handles.uipanel7, 'Visible', 'on');
        set(handles.uipanel8, 'Visible', 'off');
        set(handles.uipanel10, 'Visible', 'off');
    case 'radiobutton2'
        set(handles.uipanel7, 'Visible', 'off');
        set(handles.uipanel8, 'Visible', 'on');
        if get(handles.togglebutton5, 'Value') == 0
            set(handles.uipanel10, 'Visible', 'on');
        end
end


function togglebutton5_Callback(hObject, eventdata, handles)

% Init
global plots;
global edgesPlot;
global im;
global naviMesh;
global nodesNum;
global vertices;
global vertices_backup;
global vertCount;
global edgesNum;
global edgesCoords;
global edgesCoords_backup;
global edgeCount;
global manual_mode;

naviMesh    = [];
vertices    = [];
edgesCoords = [];
edgesNum    = [];
edgesPlot   = line(1, 1);
nodesNum    = 0;

status = get(hObject, 'Value');
if status == 0
    % Switch button color to grey 
    set(hObject, 'BackGroundColor', [0.9 0.9 1]);
    
    % Change text
    set(hObject, 'String', 'Import mode');
    
    % Disable manual mode
    manual_mode = disable_manual_mode(handles);
    set(handles.checkbox7, 'Value', 0); 
    
    % Reset plots
    plots(1) = imshow(im);
    hold on;
    plots(2) = plot(1, 1, '.');
    plots(3) = plot(1, 1, '.');
    hold off;
else
    % Switch button color to yellow 
    set(hObject, 'BackGroundColor', [0.9 0.9 0.3]);
    
    % Change text
    set(hObject, 'String', 'Normal mode');
    
    % Enable manual mode
    manual_mode = enable_manual_mode(handles);
    set(handles.checkbox7, 'Enable', 'off'); % "Manual mode" checkbox
    
    % Read graph
    [vertices, edgesNum] = read_graph('input\graph_import.txt');
    if isempty(vertices)
        msg = gcf;
        figure(handles.figure1);
        
        % Disable manual mode
        manual_mode = disable_manual_mode(handles);
        
        % Reset plots
        plots(1) = imshow(im);
        hold on;
        plots(2) = plot(1, 1, '.');
        plots(3) = plot(1, 1, '.');
        hold off;
        
        % Switch button color to grey 
        set(hObject, 'BackGroundColor', [0.9 0.9 1]);
        
        % Change text
        set(hObject, 'String', 'Import mode');

        % Release "Import mode" button
        set(hObject, 'Value', 0);
        figure(msg);
        return;
    end
    
    % Convert edgesNum into edgesCoords
    edgesCoords = zeros(5, length(edgesNum(:, 1)));
    for i = 1:length(edgesNum(:, 1))
        num1 = edgesNum(i, 1);
        num2 = edgesNum(i, 2);
        edgesCoords(:, i) = [vertices(num1, 1); vertices(num2, 1); ...
                             vertices(num1, 2); vertices(num2, 2); ...
                             edgesNum(i, 3)];
    end
    
    % Make copies
    vertices_backup = vertices;
    edgesCoords_backup = edgesCoords;
    
    % Plot results
    plots(1) = imshow(im);
    hold on;
    plots(2)  = plot(1, 1, '.');    
    % Depict Vertices
    plots(3)  = scatter(vertices(:, 1), vertices(:, 2), 15, 'blue', 'filled');    
    % Depict Edges
    edgesPlot = line(edgesCoords(1:2, :), edgesCoords(3:4, :), 'Color', 'blue');

    hold off;
    % Switch status of interface elements
    set(findall(handles.uipanel4, '-property', 'Enable'), 'Enable', 'on');
    set(handles.checkbox1, 'Enable', 'off');
    % Fill logger fields
    vertCount = length(vertices(:, 1));
    edgeCount = length(edgesNum(:, 1));
    
    set(handles.text7, 'String', 0);
    set(handles.text8, 'String', {num2str(vertCount) num2str(vertCount) '0'});
    set(handles.text9, 'String', {num2str(edgeCount) num2str(edgeCount) '0'});
    set(handles.checkbox7, 'Value', 1);
end


function checkbox7_Callback(hObject, eventdata, handles)

% Init
global plots;
global im;
global vertices;
global vertCount;
global edgesCoords;
global edgesNum;
global addedEdgeCount;
global addedEdgeArray;
global manual_mode;

vertices    = [];
vertCount   = 0;
edgesCoords = [];
edgesNum    = [];
addedEdgeArray = [];
addedEdgeCount = 0;

status = get(handles.checkbox7, 'Value');

if status == 0
    manual_mode = disable_manual_mode(handles);
    
    % Reset plots
    plots(1) = imshow(im);
    hold on;
    plots(2) = plot(0, 0, '.');
    plots(3) = plot(0, 0, '.');
    hold off;
else
    % Switch status of interface elements
    set(handles.togglebutton5, 'Enable', 'off'); % "Import mode" button
    manual_mode = enable_manual_mode(handles);
    
    % Reset plots
    plots(1) = imshow(im);
    hold on;
    plots(2) = plot(0, 0, '.');
    plots(3) = plot(0, 0, '.');
    hold off;
end


function edit3_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function slider3_Callback(hObject, eventdata, handles)
global manual_mode;

pos2 = get(handles.slider2, 'Value')*10;
pos3 = get(hObject, 'Value')*10;
if ~manual_mode
    if pos3 >= pos2
        set(handles.edit3, 'String', num2str(pos3));
    else
        set(hObject, 'Value', ceil(pos2/10));
    end
else
    set(handles.edit3, 'String', num2str(pos3));
end


function slider3_CreateFcn(hObject, eventdata, handles)

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
