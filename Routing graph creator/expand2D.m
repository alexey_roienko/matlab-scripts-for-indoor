function array1 = expand2D(array1, array2)

%% Init 
if isempty(array1)
    w1 = 0;
else
    w1 = length(array1(1, :));
end
w2 = length(array2(1, :));

%% Expand
if w1 == w2 || w1 == 0
    temp = [array1; array2];
    array1 = temp;
end
end