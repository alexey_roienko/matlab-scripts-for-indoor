%% Script for setting the mesh grid for location and navigation purposes 
%  using the mask image. Mask image represents balck and white image where
%  black pixels correspond to the positions prohibited for user location
%  and vice versa.

function mesh = mesh_generator(im, roomSize, mStep, text)

%% Init
h = waitbar(0, text);
realL   = roomSize(1);
realW   = roomSize(2);
pImageL = length(im(1,:));
pImageH = length(im(:,1));
scaleX  = realL/pImageL; % meters per 1 pixel
scaleY  = realW/pImageH; % meters per 1 pixel
radius  = 15;
pStepX  = round(mStep/scaleX);
pStepY  = round(mStep/scaleY);

%% Checkpoints visualization
mesh     = zeros(length(1:pStepX:pImageL)*length(1:pStepY:pImageH), 2);
counter  = 0;
counter2 = 0;

for x = 1:pStepX:pImageL
    for y = 1:pStepY:pImageH
        skip = false;
        %% Check the boundary conditions
        if (x - radius <= 0) || (y - radius <= 0) || (pImageL - x < radius) || (pImageH - y < radius)
            skip = true;
        else            
            %% Define the coordinates of neighbor points
            k_arr = x - radius : x+radius;
            q_arr = y - radius : y+radius;

            %% Check the neighbor points for belonging to the mask (black color)
            for k = k_arr
                for q = q_arr
                    if im(q, k) == 0
                        skip = true;
                    end
                end
            end
        end
        
        if ~skip
            counter = counter + 1;
            mesh(counter, :) = [x y];
        end
        
        counter2 = counter2 + 1;
        progress = round(counter2*100/length(mesh(:, 1)))/100;
        waitbar(progress);
    end
end

%% meshP_xy size correction
for i = 1:length(mesh(:, 1))
    if sum(mesh(i, :)) == 0;
        mesh = mesh(1:i-1, :);
        break;
    end
end

close(h);
end