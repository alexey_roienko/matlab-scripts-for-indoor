function plots = plot_mask(im, handles)

axes(handles);
% Diplay the plots
plots(1) = imshow(im);
hold on;
plots(2) = plot(0, 0, '.');
plots(3) = plot(0, 0, '.');

xlim([1 length(im(1, :, 1))]);
ylim([1 length(im(:, 1, 1))]);

end