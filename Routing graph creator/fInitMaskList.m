%% Function defines nubmber of masks in the input folder and gets their
% parameters
%% Author:
%  Yevhen Chervoniak, eugenecher94@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  25/05/17

function [masks, maskPars] = fInitMaskList(path)

count = 0;
f_content = dir(path);
tempName  = cell(1, length(f_content)-2);
masks     = cell(1, length(f_content)-2);

for i = 3:length(f_content)
    extension = f_content(i).name(end-2:end);
    if strcmp(extension, 'bmp') || strcmp(extension, 'png')
        count = count + 1;
        tempName{count} = f_content(i).name;
        % Image data type conversion and processing
        masks{count} = imread([path tempName{count}]);
        if isinteger(masks{count})
            if max(masks{count}(:)) >= 10
                masks{count} = im2bw(masks{count}, 0.95);
            else
                masks{count} = im2bw(masks{count}, 0);
            end
        end
        masks{count} = bwmorph(masks{count}, 'dilate', 2);
        masks{count} = bwmorph(masks{count}, 'erode', 2);
    end
end
maskPars = cell(count, 1);
for j = 1:length(maskPars)
    tempPars = loadjson([path, tempName{j}(1:end-4), '.json']);
    if ~isempty(tempPars{1})
        maskPars{j} = tempPars{1};
        maskPars{j}.name = tempName{j};
    end
end
end