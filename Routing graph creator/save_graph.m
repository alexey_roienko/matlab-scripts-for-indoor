function save_graph(path, vertices, edgesNum)

%% First part
out = fopen(path, 'w');
if out == -1
    mkdir output;
    out = fopen(path, 'w');
end
str = '->GRAPH';
fprintf(out, '%s\r\n', str);

for i = 1:length(vertices(:, 1))
    fprintf(out, '%d %d\r\n', vertices(i, 1), vertices(i, 2));
end

%% Second part
str = '->EDGES';
fprintf(out, '%s\r\n', str);

for i = 1:length(edgesNum(:, 1))
    fprintf(out, '%d %d ', edgesNum(i, 1), edgesNum(i, 2));
    fprintf(out, '%.2f\r\n', edgesNum(i, 3));
end
fclose(out);