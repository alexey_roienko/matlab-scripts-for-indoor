clc, clear
close all;


%% ===================== PATHES SETTINGS ========================= %
currentDir = pwd;

cd('..\Indoor classes');
classesDir = pwd;
addpath(classesDir, 'Common', 'Trilat Navigator');
cd(currentDir)

cd('..\Indoor maps\Jilin University');
buildingMapDir = pwd;
addpath(buildingMapDir);
cd(currentDir)


%% ========================= SETTINGS ============================ %
% Building map floor
bFloor      = 'floor 3 final';
floorMapDir = [buildingMapDir '\' bFloor];

% Date of the log-files
logsDateTime = {'2018-10-10_14-40-44', ...
                '2018-10-10_14-41-09', ...
                '2018-10-10_14-41-54', ...
                '2018-10-10_14-42-31'};
logsN = length(logsDateTime);

% Read 'config.ini'
[INI, mapParams, meshParams, beaconsParams, beaconsID, mapImage, mapMask] = ...
    READER.fReadAllSettingsBLE(floorMapDir, 'config.ini');
beaconsN = length(beaconsParams);

maxRawRSSIs = zeros(logsN+1, beaconsN);
maxFiltRSSIs = zeros(logsN+1, beaconsN);


%% MAIN cycle
for lg = 1:logsN
    logsDir = [pwd '\logs\' logsDateTime{lg}];
    [bleData, bleDataAbsTs] = READER.read([logsDir '\input_ble_packets.txt'], INI, beaconsID);
    packetsNumber = length(bleData(:,1));
    rawBleData = bleData;
    rawBleData(rawBleData == 0) = -Inf;

    %% ===================== DEBUG ======================= % 
    if INI.BLE_debug.showInitialRSSIMap
         TRILATPLOTTER.fShowRSSIMap(bleData, 'Raw RSSI');
    end
    %% =================== END DEBUG ===================== % 



    %% ======================= RSSI KALMAN FILTERING =========================%
    if INI.BLE_filters.RSSIKalmanFilter
        fprintf('Appling KALMAN RSSI filter...\n');
        % covariance matrix
        P = [INI.BLE.RssiKalmanPCoef 0; 0 INI.BLE.RssiKalmanPCoef];
        % model error covariance
        Q = [INI.BLE.RssiKalmanQCoef 0; 0 INI.BLE.RssiKalmanQCoef];
        % measurement error covariance
        R = INI.BLE.RssiKalmanRCoef;
        objKalman = KalmanFilterRSSI ('P_init', P,  'Q', Q,  'R', R,...
                                      'beacNumb', beaconsN,...
                                      'timeout1', INI.BLE.timeout1,...
                                      'timeout2', INI.BLE.timeout2);

        KalmanFilteredData = zeros(packetsNumber, beaconsN+1);
        kalmanIndex = 1;
        for pN=1:packetsNumber
            if pN==1
                dt = 0;
            else
                dt = bleData(pN,1) - bleData(pN-1,1);
            end
            % normal situation, timestamp is correct
            KalmanFilteredData(kalmanIndex, 1) = bleData(pN,1);
            KalmanFilteredData(kalmanIndex, 2:end) = objKalman.FilterMany(dt, bleData(pN,2:end));
            kalmanIndex = kalmanIndex + 1;
        end         
    else 
        KalmanFilteredData = bleData;
    end


    %% ======================= DEBUG ======================= % 
    if INI.BLE_debug.showKalmanFilteredRSSIMap
        TRILATPLOTTER.fShowRSSIMap(KalmanFilteredData, 'Kalman Filtered RSSI');
    end

    if INI.BLE_debug.plotMapWithBeacons
        mapFig = TRILATPLOTTER.PlotMapWithBeacons(mapImage, mapParams, logsDateTime, mapParams.pixelSize, beaconsParams);
    end

    if INI.BLE_debug.showRSSIasFunc
    %      PLOTTER.fShowRSSITimeScale(KalmanFilteredData, 4, 'Kalman-filtered RSSI');
         TRILATPLOTTER.fShowRSSIComparison(bleData, KalmanFilteredData, 4, 'Raw and Kalman-filtered RSSI');
    end
    %% ===================== END DEBUG ===================== %

    KalmanFilteredData(KalmanFilteredData == 0) = -Inf;

    maxRawRSSIs(lg, :) = max(rawBleData(:,2:beaconsN+1));
    maxFiltRSSIs(lg, :) = max(KalmanFilteredData(:,2:beaconsN+1));

end

%% ======================= DEBUG ======================= % 
if INI.Proximity_debug.loadProxyGraphFromFile
    fprintf('Loading Proximity GRAPH...\n');
    initGraph  = READER.read([pwd '\logs\proxyGraph.dat']);
    objGraph   = GraphCreator();
    proxyGraph = objGraph.signBeaconsCoords(initGraph, beaconsParams, true);        

    shouldDelete = false;
    PROXYPLOTTER.PlotAllTrajectories(proxyGraph, mapParams.pixelSize, 'red', shouldDelete, 4, 'Proximity graph');
end
%% ===================== END DEBUG ===================== % 



%%
maxRawRSSIs(end, :) = max(maxRawRSSIs(1:end-1, :));
maxFiltRSSIs(end, :) = max(maxFiltRSSIs(1:end-1, :));
finalMaxRSSIs = max(maxRawRSSIs(end, :), maxFiltRSSIs(end, :));

