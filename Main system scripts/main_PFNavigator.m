

%% Read mesh for walls
if ~(INI.correctors.mapCorrection || INI.correctors.meshCorrection ...
      || INI.correctors.wallsCorrection)
    meshFilesPath = [floorMapDir '\mesh files'];
    % Fine mesh
    meshSizeInd   = round(meshParams.fineMesh(1)*10);
    maskTable2    = load([meshFilesPath '\masktable0' sprintf('%d', meshSizeInd) '.out']);
end

%% Get PF input data
if INI.PF_debug.useDataFromUAlog
    fprintf('Reading INPUT DATA from UA log...\n');
    inputPFdata = READER.read([pwd '\logs\output_pf_delta_z.txt'], INI);
else
    fprintf('Preparing INPUT DATA...\n');
    tempData = PARTICLE_FILTER.fPrepareInputData(...
           bleUserCoords, bleDataAbsTs, sensorUserCoords, accelDataAbsTs);
    inputPFdata = tempData(INI.PF_debug.startBLEindex:end, :);
    clear tempData
end


%% ========================= DEBUG ======================= % 
% Creation of Test Files Headers 
if INI.PF_testfiles.writeTestFiles
    fidI1 = fopen('test files/tPFbeaconsCoords.txt', 'w');        
    fidI2 = fopen('test files/tPFaccelXYIncrement.txt', 'w');
    fidPF = fopen('test files/tPFparticlesCoords.txt', 'w');
    fidO1 = fopen('test files/tPFuserCoords.txt', 'w');

    fprintf(fidI1, 'Iteration [X Y]\n');
    fprintf(fidI1, '1\t');
    fprintf(fidI1, '\t[%.3f %.3f]\n', userCoords(1,2:end));

    fprintf(fidI2, 'Iteration [dX dY]\n');
    fprintf(fidI2, '1\n');        

    fprintf(fidPF, 'Iteration [X1 Y1]\t\t [X2 Y2]\t\t [X3 Y3]\t\t [X4 Y4]\t\t [X5 Y5]\t\t [X6 Y6]\t\t [X7 Y7]\n');
    fprintf(fidPF, '1\t');

    fprintf(fidO1, 'Iteration [X Y]\n');
    fprintf(fidO1, '1\n');      

    PFilter = 1;
end

% Download pseudorandom particle coordinates for testing
if INI.PF_debug.testMode
    testFilesPath = [classesDir '\PFtestModeFiles\simrand_reference_data'];
    %posVariations1 = dlmread([testFilesPath '\firstStageCoords30particles.txt'], '\t');
    posVariations1 = dlmread([testFilesPath '\simrandpi.dat'], ' ');
end
%% ======================= END DEBUG ======================= % 



%% ========================= SEEDING ======================== %
%particlesCPP       = READER.read([pwd '\logs\output_pf_particles_cpp.txt'], INI);
fprintf('Running SEEDING...\n');
corrParticleCoords = zeros(INI.PF.particlesNo, 2);
for pN = 1:INI.PF.particlesNo
    
    if INI.PF_debug.testMode
        %% ========================= DEBUG ======================= % 
        % PF is run in TEST MODE with substitution of predefined values
        %corrParticleCoords(pN,:) = abs(inputPFdata(1,4:5) + posVariations1(pN, :));
        corrParticleCoords(pN,:) = inputPFdata(1,4:5) + posVariations1(pN, :) * INI.PF.initDelta;
        
        if INI.PF_testfiles.writeTestFiles
            fprintf(fidPF, '\t[%.3f %.3f]', corrParticlesCoords(pN,:));
            if pN == INI.PF.particlesNo
                fprintf(fidPF, '\n');
            end
        end
        %% ======================= END DEBUG ======================= %         
    else
        % real operation with random numbers
        corrParticleCoords(pN,:) = inputPFdata(1,4:5) + ...
            (rand(1,2) * 2 * INI.PF.initDelta - INI.PF.initDelta);
    end
end


%% Correct particles coordinates
fprintf('Correcting particles AFTER SEEDING...\n');
alivedParticlesCoords = zeros(INI.PF.particlesNo, 2);
for pN = 1:INI.PF.particlesNo
    if INI.correctors.mapCorrection
        alivedParticlesCoords(pN,:) = CORRECTOR.fMapCorrector(corrParticleCoords(pN,:),...
            meshParams, maskTable2, 'fineMesh');
    elseif INI.correctors.meshCorrection
        alivedParticlesCoords(pN,:) = CORRECTOR.fMeshCorrector(corrParticleCoords(pN,:),...
            meshParams, maskTable2, 'fineMesh');
    else
        alivedParticlesCoords(pN,:) = corrParticleCoords(pN,:);
    end
end


%% ======================= DEBUG ======================= %
%% Show map on the screen
% imshow(imread(INI_PF.paths.mapImage));
% hold on;

% Show situation after SEEDING
if INI.PF_debug.showOnTheMap
    mapFigRef = TRILATPLOTTER.PlotMapWithBeacons(mapImage);
    PFPLOTTER.fPlotOnMapPF(inputPFdata(1,4:5), inputPFdata(1,4:5), mapParams.pixelSize,...
                         alivedParticlesCoords);
end
%% ======================= END of DEBUG ======================= %


PFuserCoordsNo = size(inputPFdata(:,1), 1);
PFuserCoords   = zeros(PFuserCoordsNo, 3);

PFuserCoords(:,1)    = inputPFdata(:,1);
PFuserCoords(1, 2:3) = inputPFdata(1, 4:5);

showTime = 0;
accumIncrement = [0 0]; 
prevParticlesCoords = alivedParticlesCoords;

fprintf('Running MAIN loop...\n');
for t = 2:size(inputPFdata, 1)    
    % If deltaX and deltaY from Sensor Navigator are equal to 0, then
    % just copy previous position, PF is not run.
    if (abs(inputPFdata(t,2)) < 1e-10) && (abs(inputPFdata(t,3)) < 1e-10)
        PFuserCoords(t, 2:3) = PFuserCoords(t-1, 2:3);            
%             str = sprintf('Deltas=(0; 0),  BLE Nav.=(%.4f; %.4f);', inputPFdata(n,4:5));
%             disp(str);
%             str = sprintf('PF coords=(%.4f; %.4f);', PFUserCoords(n, 2:3));
%             disp(str);
            
    else
        % Otherwise, if deltaX and deltaY from Sensor Navigator ~= 0, then
        % run evaluation of Particle Filter

%             str = sprintf('Deltas=(%.4f; %.4f),  BLE Nav.=(%.4f; %.4f);', ...
%                    inputPFdata(n,2:3), inputPFdata(n,4:5));
%             disp(str);
        [PFuserCoords(t, 2:3), alivedParticlesCoords, newParticlesCoords] = ...
              PARTICLE_FILTER.fGetCoordinates( prevParticlesCoords, ...
                  inputPFdata(t,4:5), inputPFdata(t,2:3), INI, meshParams, maskTable2);
    end
        
        
    %% ======================= DEBUG ======================= %
    if INI.PF_testfiles.writeTestFiles
       PFilter = PFilter + 1;

       fprintf(fidI1, '%d\t', PFilter);
       fprintf(fidI1, '\t[%.3f %.3f]\n', alivedParticlesCoords(bleTime, 2:end));

       fprintf(fidI2, '%d\t', PFilter);
       fprintf(fidI2, '\t[%.3f %.3f]\n', accumIncrement);

       fprintf(fidPF, '%d\t', PFilter);
       for p=1:INI.PF.particleNO
           fprintf(fidPF, '\t[%.3f %.3f]', alivedParticlesCoords(p,:));
       end
       fprintf(fidPF, '\n');

       fprintf(fidO1, '%d\t', PFilter);
       fprintf(fidO1, '\t[%.3f %.3f]\n', PFuserCoords);
    end
    %% ===================== END DEBUG ===================== %
        
        
    %% Map correction
    if INI.correctors.mapCorrection            
        temp = CORRECTOR.fMapCorrector(PFuserCoords(t, 2:3), meshParams, maskTable2, 'fineMesh');
        PFuserCoords(t, 2:3) = temp;
    end


    %% Mesh correction
    if INI.correctors.meshCorrection
        temp = CORRECTOR.fMeshCorrector(PFuserCoords(t, 2:3), meshParams, maskTable2, 'fineMesh');
        PFuserCoords(t, 2:3) = temp;
    end
        
%         str = sprintf('PF coords=(%.4f; %.4f);', PFUserCoords(n, 2:3));
%         disp(str);
%         pause();
        

    %% ======================= DEBUG ======================= %
    if INI.PF_debug.showOnTheMap
        if (abs(inputPFdata(t,2)) < 1e-10) && (abs(inputPFdata(t,3)) < 1e-10)
            PFPLOTTER.fPlotOnMapPF(PFuserCoords(t, 2:3), inputPFdata(t,4:5), mapParams.pixelSize,...
                prevParticlesCoords);
        else
            PFPLOTTER.fPlotOnMapPF(PFuserCoords(t, 2:3), inputPFdata(t,4:5), mapParams.pixelSize,...
                prevParticlesCoords, alivedParticlesCoords, newParticlesCoords);
        end
    end
    prevParticlesCoords = alivedParticlesCoords;
    %% ======================= END of DEBUG ======================= %
end


%% ======================= DEBUG ======================= %
% Close files with test data
if INI.PF_testfiles.writeTestFiles
    fclose(fidI1);
    fclose(fidI2);
    fclose(fidPF);
    fclose(fidO1);
end

% Compare CPP or UA results with Matlab results
if INI.PF_debug.comparePositionCPP
    cppData    = READER.read([logsDir '\output_position_pf.txt'], INI);
    mapPlotter = MAPPLOTTER(mapImage, mapParams.pixelSize, 'Comparison Matlab vs. CPP results (PN)');
    mapPlotter.mPlotCompareMLvsCPP(PFuserCoords, cppData, INI.PF_debug.recordVideoMLvsCPP);
end
if INI.PF_debug.showPositionCPP
    cppData    = READER.read([logsDir '\output_position_pf.txt'], INI);
    mapPlotter = MAPPLOTTER(mapImage, mapParams.pixelSize, 'CPP results (PN)');
    mapPlotter.mPlotCPPresults(cppData, INI.PF_debug.recordVideoMLvsCPP);
end
if INI.PF_debug.comparePositionUA
    appData    = READER.read([logsDir '\output_position_ua-' logDateTime '.txt'], INI);
    mapPlotter = MAPPLOTTER(mapImage, mapParams.pixelSize, 'Comparison Matlab vs. UA results (PN)');
    mapPlotter.mPlotCompareMLvsUA(PFuserCoords, appData, 0, 0);
end
%% ===================== END DEBUG ===================== %









