%% Script uses more advanced STEP detector for tracking user position,
%  mainly developed by Felix Sirenko with some minor corrections from 
%  Alexey Roienko and Alexey Grechnev.
%  Detector throws various state FLAGS which provide Sensor Navigator
%  information what to do with the current acceleration data.


%% ============= READING DATA from log-files ============== %
[accelLocalData, accelDataAbsTs] = READER.read([logsDir '\input_accel.txt'], INI);
[anglesData, anglesDataAbsTs]    = READER.read([logsDir '\input_angles.txt'], INI);
L1              = length(accelLocalData);
L2              = length(anglesData);
timestamps      = accelLocalData(:,1);
objConvertor    = CONVERTOR();
if INI.Sensor_debug.comparePositionUAOnline
    [UAData, uaDataAbsTs] = READER.read([logsDir '\output_position_ua-' logDateTime '.txt'], INI);
end


%% Transform the accel values to uniform view depending on the platform
if strcmpi(INI.general.logsSource, 'SensorLogger') ...
   && strcmpi(INI.general.deviceOS, 'Android')    
    accelLocalData(:,2) = accelLocalData(:,2) / 9.81;
    accelLocalData(:,3) = accelLocalData(:,3) / 9.81;
    accelLocalData(:,4) = accelLocalData(:,4) / 9.81;

elseif strcmpi(INI.general.logsSource, 'SensorLogger') ...
       && strcmpi(INI.general.deviceOS, 'iOS')    
    accelLocalData(:,2) = accelLocalData(:,2) * (-1);
    accelLocalData(:,3) = accelLocalData(:,3) * (-1);
    accelLocalData(:,4) = accelLocalData(:,4) * (-1);
end


%% Angles filtering but for Android only
anglesFiltData = anglesData;
if (INI.Sensor.anglesFiltering) && strcmpi(INI.general.deviceOS, 'Android')
    disp('Filtering raw angles...');
    % X axis
    anglesFiltData(:,2) = fASWFiltering2(anglesData(:,2), INI.Sensor.awfWS);
    % Y axis
    anglesFiltData(:,3) = fASWFiltering2(anglesData(:,3), INI.Sensor.awfWS);
    % Z axis
    anglesFiltData(:,4) = fASWFiltering2(anglesData(:,4), INI.Sensor.awfWS);
end


%% Map Angle Correction
disp('Correcting YAW angle according to MAP ANGLE...');
anglesCorrData = anglesFiltData;
for n=1:L1
    anglesCorrData(n,4) = anglesFiltData(n,4);% - mapParams.mapAngle;
    if anglesCorrData(n,4) > 180
        anglesCorrData(n,4) = -360 + anglesCorrData(n,4);
    elseif anglesCorrData(n,4) < -180
        anglesCorrData(n,4) = 360 + anglesCorrData(n,4);
    end    
end



%% ======================= DEBUG ======================= % 
% Writing the filtered Angles to the file for testing 
if (INI.Sensor_testfiles.writeFilteredAngles) && strcmpi(INI.general.deviceOS, 'Android')
    fid = fopen('test files/tSNfilteredAngles.txt', 'w');
    fprintf(fid, 'Timestamp  Pitch\t\tRoll\t\tYaw\n');
    for n=1:L1
        fprintf(fid, '%.5f\t %.5f\t %.5f\t %.5f\n', anglesCorrData(n,:));
    end
    fclose(fid);
end

% Writing the input data for Sensor Navigator for Android
if (INI.Sensor_testfiles.writeInputAndroidNavData) && strcmpi(INI.general.deviceOS, 'Android')
    % Local acceleration data
    fid = fopen('test files/tSNinputAccData.txt', 'w');
    fprintf(fid, 'Timestamp  accX\t\t accY\t\t accZ\n');
    for n=1:L1
        fprintf(fid, '%.5f\t %.5f\t %.5f\t %.5f\n', accelLocalData(n,:));
    end
    fclose(fid);
    
    % Filtered angles
    fid = fopen('test files/tSNinputAnglesData.txt', 'w');
    fprintf(fid, 'Timestamp  Pitch\t\tRoll\t\tYaw\n');
    for n=1:L1
        fprintf(fid, '%.5f\t %.5f\t %.5f\t %.5f\n', anglesCorrData(n,:));
    end
    fclose(fid);
end

% Show additional information
figN = 1;
if INI.Sensor_debug.showLocalAccel
    figure(figN);
    plot(1:L1, accelLocalData(:,2), 1:L1, accelLocalData(:,3), 1:L1, accelLocalData(:,4));
    legend('X', 'Y', 'Z');
    title('Local accelerations');
    grid on
    figN = figN + 1;
end

if INI.Sensor_debug.showInitialFilteredAngles
    figure(figN);
    plot(1:L2, anglesData(:,2), 1:L2, anglesData(:,3), 1:L2, anglesData(:,4),...
         1:L2, anglesFiltData(:,2), 1:L2, anglesFiltData(:,3), 1:L2, anglesFiltData(:,4), ...
         1:L2, anglesCorrData(:,4));
    legend('pitch', 'roll', 'yaw',...
           'pitch filtered', 'roll filtered', 'yaw filtered',...
           'yaw corrected');
    title('Initial and Filtered angles (local)');
    grid on
    figN = figN + 1;
end
%% ===================== END DEBUG ===================== % 



disp('Converting from LOCAL to GLOBAL...');
accelGlobalData      = zeros(size(accelLocalData));
accelGlobalData(:,1) = accelLocalData(:,1);
for n = 1:L1
    objConvertor.angles = [anglesCorrData(n,2) anglesCorrData(n,3) anglesCorrData(n,4)];
    % Convert from Euler angles to quaternion
    objConvertor.quaternion = objConvertor.fGetQuaterionFromAngles();
    % Converting acceleration from local CS to global CS
    accelGlobalData(n, 2:4) = objConvertor.fFromLocalToGlobal(...
        [accelLocalData(n,2) accelLocalData(n,3) accelLocalData(n,4)]);
    % Transformation from RIGHT-HAND TO LEFT-HAND coordinate system
 %   accGlobal(2) = -accGlobal(2);
end



%% ======================= DEBUG ======================= % 
if INI.Sensor_debug.showGlobalAccel
    figure(figN);
    plot(1:L1, accelGlobalData(:,2), 1:L1, accelGlobalData(:,3), 1:L1, accelGlobalData(:,4));
    legend('X', 'Y', 'Z');
    title('Accelerations global');
    grid on
    figN = figN + 1;
end
%% ===================== END DEBUG ===================== % 



%% Calculate Butterworth filter coefficients
if INI.Sensor.useButterworthFilter
    % Calculate filter coefficients
    disp('Calculating Butterworth filter coefficients...');    
    [b, a] = butter(INI.Sensor.filterOrder,...
                   (2*INI.Sensor.fCutoff) / (1/INI.Sensor.samplePeriod), 'low');
    
    % Apply the Butterworth filter
    accelGlobalFiltData      = zeros(size(accelGlobalData));
    accelGlobalFiltData(:,1) = accelGlobalData(:,1);    
    accelGlobalFiltData(:,2) = filter(b, a, accelGlobalData(:,2));
    accelGlobalFiltData(:,3) = filter(b, a, accelGlobalData(:,3));
    accelGlobalFiltData(:,4) = filter(b, a, accelGlobalData(:,4) - 1);
    accelGlobalFiltData(:,4) = accelGlobalFiltData(:,4) + 1;    
else
    accelGlobalFiltData = accelGlobalData;
end


%% Calculate the module of the Global Acceleration
accelGlobalAbsFiltData      = zeros(length(accelGlobalFiltData(:,1)), 2);
accelGlobalAbsFiltData(:,1) = accelGlobalFiltData(:,1);
for n=1:L1
    accelGlobalAbsFiltData(n,2) = sqrt(accelGlobalFiltData(n,2)^2 + ...
                                       accelGlobalFiltData(n,3)^2 + ...
                                       accelGlobalFiltData(n,4)^2);
end


%% ======================= DEBUG ======================= % 
if INI.Sensor_debug.showGlobalFilteredAccel
    figure(figN);
    plot(1:L1, accelGlobalFiltData(:,2), ...
        1:L1, accelGlobalFiltData(:,3), ...
        1:L1, accelGlobalFiltData(:,4),...
        1:L1, accelGlobalAbsFiltData(:,2));
    legend('filtered(a_x)', 'filtered(a_y)', 'filtered(a_z)', 'filtered(abs(a3))');
    title('Filtered Global Accelerations');
    grid on
    figN = figN + 1;
end

if INI.Sensor_testfiles.writeStepDetectorFlags
    fID_T1 = fopen([pwd, '\test files\stepDetectorInput.txt'], 'w');
    fprintf(fID_T1, 'timestamp\t accAbsGlobFilt\n');
    for n=1:L1
        fprintf(fID_T1, '%.4f\t\t %6.4f\n', ...
            accelGlobalAbsFiltData(n,1), accelGlobalAbsFiltData(n,2));
    end
    fclose(fID_T1);

    fID_T1 = fopen([pwd, '\test files\stepDetectorFlags.txt'], 'w');
    fprintf(fID_T1, 'timestamp\t accAbsGlobFilt\t Flag\n');
    fprintf(fID_T1, '%.4f\t %6.4f\t %16s\n', ...
        accelGlobalAbsFiltData(1,1), accelGlobalAbsFiltData(1,2), 'STEP_NONE');
end
%% ==================== END DEBUG ======================= % 



%accelGlobalAbsFiltDataT = dlmread('logs/step_test/in.txt');

%% STEP DETECTOR Initializer
stepDetector = STEP_DETECTOR_Felix('accGlobAbsFirst', accelGlobalAbsFiltData(1,2), ...
                                   'INIStruct', INI);


%% ======================= DEBUG ======================= % 
if INI.Sensor_debug.comparePositionUAOnline || INI.Sensor_debug.showTrajectory
    mapPlotter = MAPPLOTTER(mapImage, mapParams.pixelSize, 'Estimated trajectory (SN)');
    if INI.Sensor_debug.comparePositionUAOnline
        tIndex = 2;
        while accelGlobalFiltData(tIndex,1) < INI.Sensor.TdifUAandAcc
            tIndex = tIndex + 1;
        end
        UAshowIndex = 2;
        tShow = accelGlobalFiltData(tIndex-1, 1) + INI.Sensor.timeStep;
    else
        tShow = 1;
    end
else
    tShow = 1;
end
%% ===================== END DEBUG ===================== % 
                               
                               
L1         = length(accelGlobalAbsFiltData(:,1));
steps      = zeros(L1, 3);
steps(:,1) = accelGlobalAbsFiltData(:,1);
steps(:,3) = accelGlobalAbsFiltData(:,2);
steps(1,2) = -1;
deltaT     = zeros(L1, 2);
deltaT(:,1)= accelGlobalAbsFiltData(:,1);
deltaXY    = zeros(L1, 3);
deltaXY(:,1) = accelGlobalAbsFiltData(:,1);
addX       = 0;
addY       = 0;
hasNewStep = false;
pos        = zeros(L1, 3);
pos(1,:)   = [0 INI.Sensor.initialX INI.Sensor.initialY];
pos(:,1)   = accelGlobalAbsFiltData(:,1);

sensorUserCoords      = zeros(L1, 3);
sensorUserCoords(1,:) = [0 INI.Sensor.initialX INI.Sensor.initialY];
ang = anglesCorrData;
if anglesCorrData(1,4) > 0
    ang(1,4) = degtorad(anglesCorrData(1,4) - 180);
else
    ang(1,4) = degtorad(anglesCorrData(1,4) + 180);
end 

for n=2:L1
    %% Activity Detection
    [flag, output] = stepDetector.getSteps(accelGlobalAbsFiltData(n,1), ...
                                           accelGlobalAbsFiltData(n,2));
    
    % Coordinate axis transformation to axis of the map, and to radians
    if anglesCorrData(n,4) > 0
        ang(n,4) = degtorad(anglesCorrData(n,4) - 180);
    else
        ang(n,4) = degtorad(anglesCorrData(n,4) + 180);
    end    
    deltaT(n,2) = accelGlobalAbsFiltData(n,1) - accelGlobalAbsFiltData(n-1,1);
    
    if strcmpi(flag, 'STEP_RESET_START')
        steps(n,2) = -0.5;
        addX = 0;
        addY = 0;
        stepTimeIndex = n;
        hasNewStep = false;
        
    elseif strcmpi(flag, 'STEP_END_START')
        steps(n,2) = 1;
        dist = INI.Sensor.userAverageVelocity * deltaT(n,2);
        hasNewStep = true;
        
    elseif strcmpi(flag, 'STEP_RESET')
        steps(n,2) = 0;
        addX = 0;
        addY = 0;
        hasNewStep = false;
        
    elseif strcmpi(flag, 'STEP_START')
        steps(n,2) = 2;
        addX = 0;
        addY = 0;
        stepTimeIndex = n;
        hasNewStep = false;
        
    elseif strcmpi(flag, 'STEP_NONE')
        steps(n,2) = -1;
        addX = 0;
        addY = 0;
        hasNewStep = false;
        
    elseif strcmpi(flag, 'STEP_IN')
        steps(n,2) = 0.5;
        dist = INI.Sensor.userAverageVelocity * deltaT(n,2);
        
        deltaXY(n,2) = dist * sin(ang(n,4));
        deltaXY(n,3) = dist * cos(ang(n,4));
        addX = addX + deltaXY(n,2);
        addY = addY + deltaXY(n,3);
        hasNewStep = false;
    end

    %% ======================= DEBUG ======================= % 
    if INI.Sensor_testfiles.writeStepDetectorFlags
        fprintf(fID_T1, '%.4f\t\t %6.4f\t %16s\n', ...
            accelGlobalAbsFiltData(n,1), accelGlobalAbsFiltData(n,2), flag);
    end
    %% ===================== END DEBUG ===================== % 
    
    if hasNewStep
        pos(n,2)   = sensorUserCoords(n-1,2) + addX;
        pos(n,3)   = sensorUserCoords(n-1,3) + addY;
        addX = 0;
        addY = 0;
    else
        pos(n,2:3) = pos(n-1,2:3);
    end
    sensorUserCoords(n,:) = pos(n,:);    
    
    
    %% ============== MapCorrector =============== %%
    if INI.correctors.mapCorrection
        temp = CORRECTOR.fMapCorrector(sensorUserCoords(n,2:3), meshParams, maskTable2, 'fineMesh');
        sensorUserCoords(n,2:3) = temp;
    end
   
    
    %% ============== MeshCorrector =============== %%    
    if INI.correctors.meshCorrection
        if ~INI.correctors.mapCorrection
            temp1 = CORRECTOR.fMapCorrector(sensorUserCoords(n,2:3), meshParams, maskTable2, 'fineMesh');
        else
            temp1 = sensorUserCoords(n, 2:3);
        end
        temp2 = CORRECTOR.fMeshCorrector(temp1, meshParams, maskTable2, 'fineMesh');
        sensorUserCoords(n,2:3) = temp2;
    end    
    
    
    %% ============== WallsCorrector =============== %%    
    if INI.correctors.wallsCorrection
        if ~INI.correctors.mapCorrection
            temp1 = CORRECTOR.fMapCorrector(sensorUserCoords(n,2:3), meshParams, maskTable2, 'fineMesh');
        else
            temp1 = sensorUserCoords(n, 2:3);
        end
        temp2 = CORRECTOR.fWallCorrector2(sensorUserCoords(n-1, 2:3), temp1, meshParams, maskTable2);
        sensorUserCoords(n,2:3) = temp2;
    end    
    
    
    %% ======================= DEBUG ======================= % 
    if INI.Sensor_debug.showTrajectory
        if ~((sensorUserCoords(n-1,2) == sensorUserCoords(n,2)) && ...
            (sensorUserCoords(n-1,3) == sensorUserCoords(n-1,3)))
        
             mapPlotter.mPlotLine(sensorUserCoords(n-1,2:3) / mapParams.pixelSize, ...
                                  sensorUserCoords(n,2:3) / mapParams.pixelSize, ...
                                  'blue', 'blue', 5, 'ML trajectory');
             axis image
        end
    end
    
    % If it is time to show the position, then ...
    if (tShow - accelGlobalFiltData(n,1) < 0)
        % If it is necessary to check the UA data, then ...
        if INI.Sensor_debug.comparePositionUAOnline
            if UAshowIndex <= length(UAData(:,1))
                mapPlotter.mPlotLine(UAData(UAshowIndex-1, 2:3) / mapParams.pixelSize, ...
                                     UAData(UAshowIndex,   2:3) / mapParams.pixelSize, ...
                                     'red', 'red', 8, 'UA data');                                 
                mapPlotter.mPlotLine(sensorUserCoords(n-1, 2:3) / mapParams.pixelSize, ...
                                     sensorUserCoords(n,   2:3) / mapParams.pixelSize, ...
                                     'blue', 'blue', 5, 'MatLab data');
%                 pause
            end
            UAshowIndex = UAshowIndex + 1;
        end

        % This is done to draw a line on the plot
        tShow = tShow + INI.Sensor.timeStep;
    end
    %% ======================= END DEBUG ======================= % 
end

% Count number of steps
stepsCounter = 0;
for i = 1:length(steps)
   if steps(i,2) == 1
       stepsCounter = stepsCounter + 1;
   end
end
fprintf('The number of STEPS in log = %d\n', stepsCounter);


%% ======================= DEBUG ======================= % 
if INI.Sensor_testfiles.writeStepDetectorFlags
    fclose(fID_T1);
end

if INI.Sensor_debug.showStepDetectorDetails
    PLOTTER.fPlotStepDetector2(steps, accelGlobalFiltData, ones(size(accelGlobalFiltData)));
%     tempArray = zeros(size(steps,1),4);
%     PLOTTER.fPlotStepDetector2(steps, tempArray, ones(size(accelGlobalFiltData)));
end

% Writing the output data for Sensor Navigator for Android
if (INI.Sensor_testfiles.writeInputAndroidNavData) && ...
    strcmpi(INI.general.deviceOS, 'Android')
    
    % Local acceleration data
    fid = fopen('test files/tSNoutputData.txt', 'w');
    fprintf(fid, 'Timestamp  X\t\t Y\n');
    for n=1:L1
        fprintf(fid, '%.5f\t %.5f\t %.5f\n', sensorUserCoords(n,:));
    end
    fclose(fid);
end
%% ======================= END DEBUG ======================= % 


%% ========================= DEBUG ========================= %
if INI.Sensor_debug.comparePositionCPP        
    cppData = READER.read([logsDir '\output_position_sensor.txt'], INI);
    mapPlotter = MAPPLOTTER(mapImage, mapParams.pixelSize, 'Comparison Matlab vs. CPP results (SN)');
    mapPlotter.mPlotCompareMLvsCPP(sensorUserCoords, cppData, INI.Sensor_debug.recordVideoMLvsCPP);
end

if INI.Sensor_debug.comparePositionUA
    if ~INI.Sensor_debug.comparePositionUAOnline
        [UAData, uaDataAbsTs] = READER.read([logsDir '\output_position_ua-' logDateTime '.txt'], INI);
    end
    mapPlotter = MAPPLOTTER(mapImage, mapParams.pixelSize, 'Comparison Matlab vs. UA results (SN)');
    mapPlotter.mPlotCompareMLvsUA(sensorUserCoords, UAData, accelDataAbsTs, uaDataAbsTs);
end
%% ======================= END DEBUG ======================= %





