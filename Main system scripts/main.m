clc, clear
close all;


%% ===================== PATHES SETTINGS ========================= %
currentDir = pwd;

cd('..\Indoor classes');
classesDir = pwd;
addpath(classesDir, 'Common', 'PFtestModeFiles');
cd(currentDir)

cd('..\Indoor maps\Jilin University');
buildingMapDir = pwd;
addpath(buildingMapDir);
cd(currentDir)


%% ========================= SETTINGS ============================ %
% Building map floor
bFloor      = 'floor 3 final';
floorMapDir = [buildingMapDir '\' bFloor];

% Possible variants: BLE (Trilat, Proxy), Sensor, PF (Trilat, Proxy)
systemMode = 'BLE';
BLEMode    = 'PROXY';

% Date of the log-files
logDateTime = '2018-10-10_14-42-31';
logsDir = [pwd '\logs\' logDateTime];


% Read 'config.ini'
[INI, mapParams, meshParams, beaconsParams, beaconsID, mapImage, mapMask] = ...
    READER.fReadAllSettingsBLE(floorMapDir, 'config.ini');
beaconsN = length(beaconsParams);

% Load maskTable1 file, greater mesh for Mesh Corrector
if INI.correctors.mapCorrection || INI.correctors.meshCorrection ...
      || INI.correctors.wallsCorrection
    meshFilesPath = [floorMapDir '\mesh files'];
    % Coarse mesh
    delta = abs(10*round(meshParams.coarseMesh(1)*10) - meshParams.coarseMesh(1)*100);
    if delta > 0
        % if mesh step looks like 0.53 or 0.37
        meshSizeInd   = round(meshParams.coarseMesh(1)*100);
    else
        % if mesh step looks like 0.3 or 0.5
        meshSizeInd   = round(meshParams.coarseMesh(1)*10);
    end
    maskTable1    = load([meshFilesPath '\masktable0' sprintf('%d', meshSizeInd) '.out']);
    % Fine mesh
    delta = abs(10*round(meshParams.fineMesh(1)*10) - meshParams.fineMesh(1)*100);
    if delta > 0
        % if mesh step looks like 0.13 or 0.17
        meshSizeInd   = round(meshParams.fineMesh(1)*100);
    else
        % if mesh step looks like 0.1 or 0.3
        meshSizeInd   = round(meshParams.fineMesh(1)*10);
    end
    maskTable2    = load([meshFilesPath '\masktable0' sprintf('%d', meshSizeInd) '.out']);
end


%% ========================= MAIN PART =========================== %
fprintf('Processing logs from <%s>...\n\n', logDateTime);
fprintf('System MODE - <%s> with <%s> BLE mode\n', systemMode, BLEMode);

if strcmpi(systemMode, 'BLE')
    %% BLE Navigator section
    
    addpath([classesDir '\Trilat Navigator']);
    
    if strcmpi(BLEMode, 'trilat')
        fprintf('\nRunning TRILAT navigator:\n');
        main_TrilatNavigator;
    elseif strcmpi(BLEMode, 'proxy')        
        addpath([classesDir '\Proxy Navigator']);
        fprintf('\nRunning PROXIMITY navigator:\n');
        main_ProxyNavigator;
    end

    
elseif strcmpi(systemMode, 'Sensor')
    %% Sensor Navigator section 
    addpath([classesDir '\Sensor Navigator']);
    
    % With Felix's Step Detector
    fprintf('\nRunning SENSOR navigator:\n');
    main_SensorNavigator;

    
elseif strcmpi(systemMode, 'PN')    
    %% Particle Filter Navigator section
    addpath([classesDir '\Trilat Navigator'], [classesDir '\Sensor Navigator'],...
            [classesDir '\PF Navigator']);
    
    if strcmpi(BLEMode, 'trilat')
        fprintf('\nRunning TRILAT navigator:\n');
        main_TrilatNavigator;
    elseif strcmpi(BLEMode, 'proxy')
        fprintf('\nRunning PROXIMITY navigator:\n');
        addpath([classesDir '\Proxy Navigator']);
        main_ProxyNavigator;
    end
    
    % SN should start from the third BLE estimated point
    % The 3rd point is due to the latency in SDK code
    fprintf('\nRunning SENSOR navigator:\n');
    INI.Sensor.initialX = bleUserCoords(INI.PF_debug.startSNindex, 2);
    INI.Sensor.initialY = bleUserCoords(INI.PF_debug.startSNindex, 3);
    main_SensorNavigator;
    
    fprintf('\nRunning PARTICLE navigator:\n');
    main_PFNavigator;
    
else
    error('Unknown Working Mode!');
end

fprintf('\n');

