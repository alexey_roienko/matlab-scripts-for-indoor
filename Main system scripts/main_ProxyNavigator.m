

%% ============= READING DATA from log-files ============== %
[bleData, bleDataAbsTs] = READER.read([logsDir '\input_ble_packets.txt'], INI, beaconsID);
packetsNumber = length(bleData(:,1));

% Check existance of a maskTable2
if ~exist('maskTable2', 'var')
    meshFilesPath = [floorMapDir '\meshFiles'];
    % Fine mesh
    meshSizeInd   = round(meshParams.fineMesh(1)*10);
    maskTable2    = load([meshFilesPath '\masktable0' sprintf('%d', meshSizeInd) '.out']);
end


%% ===================== DEBUG ======================= % 
if INI.BLE_debug.showInitialRSSIMap
     TRILATPLOTTER.fShowRSSIMap(bleData, 'Raw RSSI');
end
%% =================== END DEBUG ===================== % 


%% ======================= RSSI KALMAN FILTERING =========================%
if INI.BLE_filters.RSSIKalmanFilter
    fprintf('Appling KALMAN RSSI filter...\n');
    % covariance matrix
    P = [INI.BLE.RssiKalmanPCoef 0; 0 INI.BLE.RssiKalmanPCoef];
    % model error covariance
    Q = [INI.BLE.RssiKalmanQCoef 0; 0 INI.BLE.RssiKalmanQCoef];
    % measurement error covariance
    R = INI.BLE.RssiKalmanRCoef;
    objKalman = KalmanFilterRSSI ('P_init', P,  'Q', Q,  'R', R,...
                                  'beacNumb', beaconsN,...
                                  'timeout1', INI.BLE.timeout1,...
                                  'timeout2', INI.BLE.timeout2);
    
    KalmanFilteredData = zeros(packetsNumber, beaconsN+1);
    kalmanIndex = 1;
    for pN=1:packetsNumber
        if pN==1
            dt = 0;
        else
            dt = bleData(pN,1) - bleData(pN-1,1);
        end
        % normal situation, timestamp is correct
        KalmanFilteredData(kalmanIndex, 1) = bleData(pN,1);
        KalmanFilteredData(kalmanIndex, 2:end) = objKalman.FilterMany(dt, bleData(pN,2:end));
        kalmanIndex = kalmanIndex + 1;
    end         
else 
    KalmanFilteredData = bleData;
end



%% ======================= DEBUG ======================= % 
if INI.BLE_debug.showKalmanFilteredRSSIMap
    TRILATPLOTTER.fShowRSSIMap(KalmanFilteredData, 'Kalman Filtered RSSI');
end

if INI.BLE_debug.plotMapWithBeacons
    mapFig = TRILATPLOTTER.PlotMapWithBeacons(mapImage, mapParams, logDateTime, mapParams.pixelSize, beaconsParams);
end

if INI.BLE_debug.showRSSIasFunc
%      PLOTTER.fShowRSSITimeScale(KalmanFilteredData, 4, 'Kalman-filtered RSSI');
     TRILATPLOTTER.fShowRSSIComparison(bleData, KalmanFilteredData, 4, 'Raw and Kalman-filtered RSSI');
end

    % ========= Creation of Test Files Headers ========= % 
if INI.BLE_testfiles.write_Positions
    fID_T2 = fopen('test files\tBLEpositions.txt', 'w');
    fprintf(fID_T2, 'Timestamp\tX\tY\n');
end
%% ===================== END DEBUG ===================== % 



%% ======================= ProxyGraph Creation ========================= %%
if INI.Proximity_debug.loadProxyGraphFromFile
    fprintf('Loading Proximity GRAPH...\n');
    initGraph  = READER.read([pwd '\logs\proxyGraph.dat']);
    objGraph   = GraphCreator();
    proxyGraph = objGraph.signBeaconsCoords(initGraph, beaconsParams, true);        
else
    fprintf('Creating Proximity GRAPH...\n');
    objGraph   = GraphCreator();
    maxEdgeLength = 20;
    CRIT_ANGLE = 20;
    proxyGraph = objGraph.getGraph(beaconsParams, meshParams, maskTable2, INI.Proximity.maxEdgeLength, ...
                                   INI.Proximity.critAngle);    
end



%% ======================= DEBUG ======================= % 
if INI.Proximity_debug.plotBeaconsGraph
    if ~INI.BLE_debug.plotMapWithBeacons
        mapFig = TRILATPLOTTER.PlotMapWithBeacons(mapImage, mapParams, logDateTime, mapParams.pixelSize, beaconsParams);
    end
    shouldDelete = false;
    PROXYPLOTTER.PlotAllTrajectories(proxyGraph, mapParams.pixelSize, 'red', shouldDelete, 4, 'Proximity graph');
end
%% ===================== END DEBUG ===================== % 



%% ======================= Reformatting data for Dijkstra algorithm ======================= %%
V_Dijkstra = zeros(length(beaconsParams),3);
for i = 1:length(beaconsParams)
    V_Dijkstra(i,:) = [beaconsParams{1,i}.x beaconsParams{1,i}.y beaconsParams{1,i}.z];
end
E3_Dijkstra = zeros(length(proxyGraph),3);
k = 1;
for i = 1:length(proxyGraph)
    E3_Dijkstra(k,:) = [proxyGraph{1,i}.beac1N  proxyGraph{1,i}.beac2N ...
        1/sqrt((proxyGraph{1,i}.beac1X - proxyGraph{1,i}.beac2X)^2 + (proxyGraph{1,i}.beac1Y - proxyGraph{1,i}.beac2Y)^2)];
    E3_Dijkstra(k+1,:) = [proxyGraph{1,i}.beac2N  proxyGraph{1,i}.beac1N  E3_Dijkstra(i,3)];
    k = k + 2;
end


%% ======================= MAIN CYCLE ======================= %%
KalmanFilteredData(KalmanFilteredData == 0) = -inf;
KalmanFilteredData(1,1) = 0;
mode = 'RSSI';                         % Modes: 'dist', 'RSSI' or 'relRSSI'
bleUserCoords = zeros(length(KalmanFilteredData(:,1)), 3);
posIndex = 1;
dt = 0;

fprintf('Running MAIN loop...\n');
for t = 1:length(KalmanFilteredData(:,1))
    
    % This part of code is necessary because UA asks SDK for current
    % coordinates one in a 'INI.BLE.beaconsCycle'; if 'dt' is greater than 
    % 'INI.BLE.beaconsCycle + 0.5*INI.BLE.beaconsCycle' then the
    % coordinates are just doubled in 'position'-matrix
    if t~=1
        dt = KalmanFilteredData(t,1) - KalmanFilteredData(t-1,1) ;
    end
    if dt > INI.BLE.beaconsCycle + 0.5*INI.BLE.beaconsCycle
        bleUserCoords(posIndex,1)   = KalmanFilteredData(t-1,1) + INI.BLE.beaconsCycle;
        bleUserCoords(posIndex,2:3) = bleUserCoords(posIndex-1,2:3);
        posIndex = posIndex + 1;        
    end
    
    bleUserCoords(posIndex,1) = KalmanFilteredData(t,1);
    
    %% ======================= Reformatting for positioning ======================= %%
    beaconsWithRSSI = cell(1, length(beaconsParams)-1); 
    for bn = 1:length(beaconsParams)
        beacon = beaconsParams{1,bn};
        beacon.RSSI = KalmanFilteredData(t,bn + 1);
        if beacon.RSSI ~= -inf
            beacon.relRSSI = (beacon.txpower / beacon.RSSI)^beacon.damp;
        else
            beacon.relRSSI = -inf;
        end
        beacon.dist = Lateration.fRssi2Dist({beacon});
        beaconsWithRSSI{bn} = beacon;
    end
    
    
    %% ======================= Checking the number of active beacons and edge candidates ================== %%
    [oneBeaconFlag, beaconN] = Proximity.isThereOneActiveBeacon(beaconsWithRSSI);
%     if oneBeaconFlag
%         % if there is the only active beacon, then let's find the number of
%         % its adjacent edges...
%         oneEdgeCandidateFlag = Proximity.isThereOneEdgeCandidate(beaconN, graphG);
%     end
    
    % if there is the only active beacon and several adjacent edges, then
    % beacon position is the user position, because there is an ambiguity in user positions
    if oneBeaconFlag %&& ~oneEdgeCandidateFlag
        RSSIvalues = zeros(length(beaconsWithRSSI),1);
        for b=1:length(beaconsWithRSSI)
            RSSIvalues(b) = beaconsWithRSSI{1,b}.RSSI;
        end
        [val, pos] = max(RSSIvalues);
        bleUserCoords(posIndex,2:3) = [beaconsWithRSSI{1,pos}.x  beaconsWithRSSI{1,pos}.y];
    else
        %% ======================= Searching the current active edge ======================= %%
        selectedEdge = Proximity.findActiveEdge(beaconsWithRSSI, proxyGraph, mode);
        if INI.Proximity_debug.plotBeaconsGraph
            PROXYPLOTTER.PlotSelectedCut(selectedEdge, beaconsWithRSSI, mapParams.pixelSize);
        end
        
        %% ======================= Searching the position-candidates ====================== %%
        [trueCrossings, allCrossings] = Proximity.getCandidatesPositions(beaconsWithRSSI, ...
            {[selectedEdge.beac1N selectedEdge.beac2N selectedEdge.dist]});
        if INI.Proximity_debug.plotBeaconsGraph
            PROXYPLOTTER.PlotCirclesAndCrossings(beaconsWithRSSI, allCrossings, trueCrossings, mapParams.pixelSize);
        end

        %% ======================= Averaging the candidates and getting the final position ================ %%
        bleUserCoords(posIndex,2:3) = Proximity.getFinalPosition(beaconsWithRSSI, trueCrossings, ...
            [selectedEdge.beac1N selectedEdge.beac2N]);
    end
    
    
    %% ======================= DEBUG ======================= %
    % Debug when DistanceLimiter2 is OFF
    %str = sprintf('dt = %.4f, posIndex = %d, x = %.4f, y = %.4f', KalmanFilteredData(t,1), posIndex, bleUserCoords(t,2:3));
    %disp(str);
    %% ===================== END DEBUG ===================== %
    
    
    %% ======================= Usage of the distance limiter (OPTIONAL) ======================= %%
    if (INI.BLE_filters.distanceLimiter == true) && (sum(isnan(bleUserCoords(t,2:3))) == 0)
        if t == 1
            tPrev = KalmanFilteredData(t,1);
            positionPrev = bleUserCoords(posIndex,2:3);
            if exist('selectedEdge', 'var')
                selectedEdgePrev = selectedEdge;
            end
        else
            if ~exist('selectedEdgePrev', 'var')
                selectedEdgePrev = selectedEdge;
            end
            
            if selectedEdgePrev.dist ~= selectedEdge.dist
                pathGraph = Proximity.findBeaconsToJoin(objGraph, V_Dijkstra, E3_Dijkstra, ...
                    selectedEdgePrev, selectedEdge);
            else
                pathGraph = NaN;
            end

            [bleUserCoords(posIndex,2:3), selectedEdgePrev] = Proximity.distanceLimiter2(tPrev,...
                 KalmanFilteredData(t,1), positionPrev, selectedEdgePrev,...
                 bleUserCoords(posIndex,2:3),  selectedEdge, pathGraph,...
                 beaconsWithRSSI, INI.BLE.limiterCoef, INI.BLE_debug.distanceLimiterInfo);

            if isnan(selectedEdgePrev.dist)
                for i = 1:length(proxyGraph)
                    if (selectedEdgePrev.beac1N == proxyGraph{1,i}.beac1N) && (selectedEdgePrev.beac2N == proxyGraph{1,i}.beac2N)
                       selectedEdgePrev.angle = proxyGraph{1,i}.angle;
                       selectedEdgePrev.dist = proxyGraph{1,i}.dist;
                       break;
                    end
                end
            end
            tPrev = KalmanFilteredData(t,1);
            positionPrev = bleUserCoords(posIndex,2:3);
        end
    end
	
%     PROXYPLOTTER.PlotPositionsAndNumbers(position(t,2:3), mapParams.pixelSize, t);
    
    
    %% ======================= DEBUG ======================= %
    % Debug when DistanceLimiter2 is ON
%     str = sprintf('dt = %.4f, x = %.4f, y = %.4f', KalmanFilteredData(t,1), position(t,2:3));
%     disp(str);
    % ==================== Write Test Files  ===================== % 
    if INI.BLE_testfiles.write_Positions
        fprintf(fID_T2, '%6.3f  %6.3f  %6.3f \n', bleUserCoords(posIndex,1:3));            
    end
    % ================= End writing to Test Files  ================= %     
    %% ===================== END DEBUG ===================== %
    
    posIndex = posIndex + 1;
end


%% ======================= DEBUG ======================= %
if INI.BLE_debug.comparePositionCPP
    [CPPData, cppDataTs]  = READER.read([logsDir '\output_position_ble.txt'], INI);    
    mapPlotter = MAPPLOTTER(mapImage, mapParams.pixelSize, 'Comparison Matlab vs. CPP results (BLE)');
    mapPlotter.mPlotCompareMLvsCPP(bleUserCoords, CPPData, INI.BLE_debug.recordVideoMLvsCPP);
elseif INI.BLE_debug.comparePositionUA
    [UAData, uaDataTs]  = READER.read([logsDir '\output_position_ua-' logDateTime '.txt'], INI);
    mapPlotter = MAPPLOTTER(mapImage, mapParams.pixelSize, 'Comparison Matlab vs. UA results (BLE)');
    mapPlotter.mPlotCompareMLvsUA(bleUserCoords, UAData, bleDataAbsTs, uaDataTs);
end
%% ===================== END DEBUG ===================== %





