in.dat  : Your input file

out.dat : timestamp outpos(mine) outpos(yours)

step_log.dat : Detailed log inside stepfilter :

dt = Timestamp difference
vecD0 = Step vector since the last position
D = its norm
DMAX = Maximal allowed step (for this dt)
vecD = Step vector renormalized
