

%% ============= READING DATA from log-files ============== %
[bleData, bleDataAbsTs] = READER.read([logsDir '\input_ble_packets.txt'], INI, beaconsID);
packetsNumber = length(bleData(:,1));

% Load log-file from UA for testing purposes
if INI.BLE_debug.compareTriplets
    cppTriplets = READER.read([logsDir '\output_triplets_ble.txt'], INI, beaconsID);
    cppTripletsindex = 1;
end


%% ======================= DEBUG ======================= % 
%% ============= Creation of Test Files Headers ============== % 
if INI.BLE_testfiles.write_SelectedBeacons
    fID_T1 = fopen('test files\tBLEselectedBeacons.txt', 'w');
    % Can be more than 3 beacons; then header should be rewritten
    fprintf(fID_T1, 'Timestamp\tBeacon1\tBeacon2\tBeacon3\n');
end
if INI.BLE_testfiles.write_Positions
    fID_T2 = fopen('test files\tBLEpositions.txt', 'w');
    fprintf(fID_T2, 'Timestamp\tX\tY\n');
end
if INI.BLE_testfiles.write_SelectedBeaconsRSSI
    fID_T3 = fopen('test files\tBLEselectedBeaconsRSSI.txt', 'w');
    % Can be more than 3 beacons; then header should be rewritten
    fprintf(fID_T3, 'Timestamp\tBeacon1\tBeacon2\tBeacon3\n');
end
if INI.BLE_testfiles.write_CheckTriplets
    fID_T4 = fopen('test files\tBLEtripletsRSSImod.txt', 'w');
    fprintf(fID_T4, 'Timestamp\t');
    for bN = 1:length(bleData(1,2:end))
        fprintf(fID_T4, 'Beacon%d\t\t', bN);
    end
    fprintf(fID_T4, '\n');
    
    fID_T5 = fopen('test files\tBLEtripletsCombs.txt', 'w');
    fprintf(fID_T5, 'Timestamp\t');
    for bN = 1:INI.BLE.lateratBeacNO
        fprintf(fID_T5, 'Beacon%d\t', bN);
    end
    fprintf(fID_T5, '\n');
end
if INI.BLE_testfiles.write_DistanceLimiter
    fID_T7 = fopen('test files\tBLEdistLimiter.txt', 'w');
    fprintf(fID_T7, 'Timestamp    Input coords    Output coords\n');
end
%% ================ End of Test Files Headers ================ % 
if INI.BLE_debug.showRSSIMap
     PLOTTER.fShowInitialRSSIMap(bleData, 'Raw RSSI');
end
if INI.BLE_debug.showRSSIasFunc
     PLOTTER.fShowRSSITimeScale(bleData, 4, 'Raw RSSI');
end
%% ===================== END DEBUG ===================== % 


%% ======================= RSSI KALMAN FILTRATION =========================%
if INI.BLE_filters.RSSIKalmanFilter
    % covariance matrix
    P = [INI.BLE.RssiKalmanPCoef 0; 0 INI.BLE.RssiKalmanPCoef];
    % model error covariance
    Q = [INI.BLE.RssiKalmanQCoef 0; 0 INI.BLE.RssiKalmanQCoef];
    % measurement error covariance
    R = INI.BLE.RssiKalmanRCoef;
    objKalman = KalmanFilterRSSI ('P_init', P,  'Q', Q,  'R', R,...
                                  'beacNumb', beaconsN,...
                                  'timeout1', INI.BLE.timeout1,...
                                  'timeout2', INI.BLE.timeout2);
    
    KalmanFilteredData = zeros(packetsNumber, beaconsN + 1);    
    kalmanIndex = 1;
    for pN=1:packetsNumber
        if pN==1
            dt = 0;
        else
            dt = bleData(pN,1)-bleData(pN-1,1);
        end
        % normal situation, timestamp is correct
        KalmanFilteredData(kalmanIndex, 1) = bleData(pN,1);
        KalmanFilteredData(kalmanIndex, 2:end) = objKalman.FilterMany(dt, bleData(pN,2:end));
        kalmanIndex = kalmanIndex + 1;
    end
else
    KalmanFilteredData = bleData;
end


%% ======================= DEBUG ======================= % 
if INI.BLE_debug.showKalmanFilteredRSSIMap
    PLOTTER.fShowRSSIMap(KalmanFilteredData, 'Kalman Filtered RSSI');
end
if INI.BLE_debug.plotMapWithBeacons
    logFileName = ['input_ble_packets-' logDateTime '.txt'];
    mapFig = PLOTTER.PlotMap(mapImage, mapParams, logFileName,...
                             mapParams.pixelSize, beaconsParams);
end
if INI.BLE_debug.showRSSIasFunc
%      PLOTTER.fShowRSSITimeScale(KalmanFilteredData, 4, 'Kalman-filtered RSSI');
     PLOTTER.fShowRSSIComparison(bleData, KalmanFilteredData, 4, 'Raw and Kalman-filtered RSSI');
end
%% ==================== Write Test Files  ===================== % 
if INI.BLE_testfiles.write_KalmanFilteredRSSI
    fID_T6 = fopen('test files\tBLEkalmanFilteredRSSI.txt', 'w');
    fprintf(fID_T6, 'Timestamp\t');
    for bN = 1:length(KalmanFilteredData(1,2:end))
        fprintf(fID_T6, 'Beacon%d\t', bN);
    end
    fprintf(fID_T6, '\n');

    for t = 1:length(KalmanFilteredData(:,1))
        fprintf(fID_T6, '%.3f\t\t', KalmanFilteredData(t,1));
        fprintf(fID_T6, '%.3f\t',   KalmanFilteredData(t,2:end));
        fprintf(fID_T6, '\n');
    end
    fclose(fID_T6);
end
%% ================= End writing to Test Files  ================= % 
%% ===================== END DEBUG ===================== % 


%% Initialization of KalmanXY Filter
if INI.BLE_filters.XYKalmanFilter
    % How sure I am in initial position
    P = [INI.BLE.XYKalmanPCoef 0; 0 INI.BLE.XYKalmanPCoef];
    % What mistake can I make when predicting a new one position
    Q = [INI.BLE.XYKalmanQCoef 0; 0 INI.BLE.XYKalmanQCoef];
    % Distribute weights between the model and the measurements
    R = eye(3) * INI.BLE.XYKalmanRCoef;
    objKalmanXY = KalmanFilterXY('P_init', P, 'Q', Q, 'R', R);
end


%% Initialization of Distance Limiter class
if INI.BLE_filters.distanceLimiter
    objDistLimiter = DistanceLimiter2('limiterCoef', INI.BLE.limiterCoef);
end


%% Initialization of KalmanXY Filter%% Main cycle
bleUserCoords   = zeros(length(KalmanFilteredData(:,1)), 3);
SelectedBeacons = struct('beacNO', [], 'RSSI', []);
addedTSindex = 1;

for t = 1:length(KalmanFilteredData(:,1))
     bleUserCoords(t,1) = KalmanFilteredData(t,1);
%      if t==57
%          disp('');
%      end
     %% ======================= DEBUG ======================= % 
     if INI.BLE_debug.compareTriplets
         str = sprintf('Timestamp number : %d', t);
         disp(str);
     end
     %% ===================== END DEBUG ===================== %
     
     %% Choice of Beacon Triplet
     if INI.BLE_filters.tripletsFilter
         if INI.BLE_testfiles.write_CheckTriplets
             [SelectedBeacons(t), isSuccessfully] = ...
                 LATERATION.fGetTripletsAdvanced( ...
                     KalmanFilteredData(t, 2:end), INI, ...
                     INI.BLE_debug.tripletsFunction, beaconsParams, ...
                     beaconsID, KalmanFilteredData(t, 1), fID_T4, fID_T5);
         else
             [SelectedBeacons(t), isSuccessfully] = ...
                 LATERATION.fGetTripletsAdvanced( ...
                      KalmanFilteredData(t, 2:end), INI, ...
                      INI.BLE_debug.tripletsFunction, beaconsParams);
         end
         
     else
        [SelectedBeacons(t), isSuccessfully] = ...
            LATERATION.fGetTripletStandard( KalmanFilteredData(t, 2:end), ...
                                            INI.BLE.lateratBeacNO, false);
     end

     
     %% ======================= DEBUG ======================= % 
     if INI.BLE_debug.compareTriplets
         if exist('addedTS','var') && (addedTSindex <= length(addedTS)) ...
                                    && (addedTS(addedTSindex) == t)
             str = sprintf('Selected beacons by MatLab : %d  %d  %d, %3.2f %3.2f %3.2f', ...
                 SelectedBeacons(t).beacNO(1:3), SelectedBeacons(t).RSSI(1:3));
             disp(str);
             str = sprintf('Selected beacons by  SDK   : passed timestamp');
             disp(str);
         else
             str = sprintf('Selected beacons by MatLab : %d  %d  %d, %3.2f %3.2f %3.2f',...
                 SelectedBeacons(t).beacNO(1:3), SelectedBeacons(t).RSSI(1:3));
             disp(str);
             str = sprintf('Selected beacons by  SDK   : %d  %d  %d, %3.2f %3.2f %3.2f', ...
                 cppTriplets(cppTripletsindex,2:4), cppTriplets(cppTripletsindex,5:7));
             disp(str);
             cppTripletsindex = cppTripletsindex + 1;
         end
     end
     %% ==================== Write Test Files  ===================== % 
     if INI.BLE_testfiles.write_SelectedBeacons
         fprintf(fID_T1, '%.3f\t\t', KalmanFilteredData(t,1));
         for bN = 1:INI.BLE.lateratBeacNO
             majMin = beaconsID{SelectedBeacons(t).beacNO(bN)};
             fprintf(fID_T1, '%s\t', majMin(6:end));
         end
         fprintf(fID_T1, '\n');
     end
     %% ================= End writing to Test Files  ================= %     
     %% ===================== END DEBUG ===================== %
     
     
     if isSuccessfully
        %% If beacons have been succesfully chosen, then ...
        beaconsLaterat = cell(1,INI.BLE.lateratBeacN);
        for j=1:INI.BLE.lateratBeacN
            beacon = beaconsParams{1,SelectedBeacons(t).beacNO(j)};
            beacon.RSSI = SelectedBeacons(t).RSSI(j);
            beaconsLaterat{j} = beacon; 
        end     

        distancesLaterat = LATERATION.fRssi2Dist(beaconsLaterat);     

        %% Choose the positioning algorithm
        if strcmpi(INI.BLE.posMethod, 'Trilat')
            bleUserCoords(t,2:3) = LATERATION.fTrilateration(...
                beaconsLaterat, distancesLaterat, INI.general.userHeight);
            
        elseif strcmpi(INI.BLE.posMethod, 'Multilat')
            bleUserCoords(t,2:3) = LATERATION.fMultilateration(...
                beaconsLaterat, distancesLaterat, INI.general.userHeight);               
        else
            error('Error in Lateration method');
        end

        
        %% ======================= DEBUG ======================= %
        if INI.BLE_debug.printMLPosBeforeCorr
            str0 = sprintf('======================= POSITION ======================');
            str1 = sprintf('MatLab Before Correction        : %6.3f  %6.3f  %6.3f ',...
                            bleUserCoords(t,:));
            disp(str0);
            disp(str1);                            
        end
        %% ==================== Write Test Files  ===================== % 
        if INI.BLE_testfiles.write_SelectedBeaconsRSSI
            fprintf(fID_T3, '%.3f\t\t', bleUserCoords(t,1));
            for bN = 1:INI.BLE.lateratBeacNO                
                fprintf(fID_T3, '%6.3f  ', beaconsLaterat{bN}.RSSI);
            end
            fprintf(fID_T3, '\n');
        end
        %% ================= End writing to Test Files  ================= %     
        %% ===================== END DEBUG ===================== %
        
        
        %% Apply Map Correction
        if INI.correctors.mapCorrection || INI.correctors.meshCorrection
            temp = CORRECTOR.fMapCorrector(bleUserCoords(t,2:3), meshParams, maskTable1, 'coarseMesh');
            bleUserCoords(t,2:3) = temp;
        end
        
        
        %% Apply XY Kalman Filter
        if INI.BLE_filters.XYKalmanFilter
            if exist('addedTS','var') && (addedTSindex <= length(addedTS))...
                                      && (addedTS(addedTSindex) == t)
                bleUserCoords(t,2:3) = bleUserCoords(t-1,2:3);
                addedTSindex = addedTSindex + 1;
            else
                bleUserCoords(t,2:3) = objKalmanXY.FilterByKalman(bleUserCoords(t,2:3),...
                    [beaconsLaterat{1,1}.x beaconsLaterat{1,1}.y],...
                    [beaconsLaterat{1,2}.x beaconsLaterat{1,2}.y],...
                    [beaconsLaterat{1,3}.x beaconsLaterat{1,3}.y]);

                %% Apply Map Correction once again after filter application
                if INI.correctors.mapCorrection || INI.correctors.meshCorrection
                    temp = CORRECTOR.fMapCorrector(bleUserCoords(t,2:3), meshParams, maskTable1, 'coarseMesh');
                    bleUserCoords(t,2:3) = temp;
                end 
            end
        end
        
        
        %% ======================= DEBUG ======================= %
        %% ==================== Write Test Files  ===================== % 
        if INI.BLE_testfiles.write_DistanceLimiter
            fprintf(fID_T7, '%.3f\t\t', bleUserCoords(t,1));
            fprintf(fID_T7, '%.4f  %.4f\t\t', bleUserCoords(t,2:3));
        end
        %% ================= End writing to Test Files  ================= %     
        %% ===================== END DEBUG ===================== %
        
        %% Apply Distance Limiter
        if INI.BLE_filters.distanceLimiter
            temp = objDistLimiter.applyLimiter(bleUserCoords(t,1), bleUserCoords(t,2:3)', ...
                INI.BLE_debug.distanceLimiterInfo);
            if INI.BLE_filters.XYKalmanFilter
                objKalmanXY.setX(temp);
            end
            bleUserCoords(t,2:3) = temp;
        end
        
        %% ======================= DEBUG ======================= %
        %% ==================== Write Test Files  ===================== % 
        if INI.BLE_testfiles.write_DistanceLimiter
            fprintf(fID_T7, '%.4f  %.4f\n', bleUserCoords(t,2:3));
        end
        %% ================= End writing to Test Files  ================= %     
        %% ===================== END DEBUG ===================== %
        
        
        %% Apply Mesh Correction
        if INI.correctors.meshCorrection || INI.correctors.wallsCorrection
            temp = CORRECTOR.fMeshCorrector(bleUserCoords(t,2:3), meshParams, maskTable1, 'coarseMesh');
            bleUserCoords(t,2:3) = temp;
        end
        
        
        %% ======================= DEBUG ======================= %
        if INI.BLE_debug.plotBeaconsOnMap
            PLOTTER.fPlotBeaconCirclesRSSI(mapFig, bleUserCoords(t,2:3), beaconsLaterat, ...
                    distancesLaterat, mapParams.pixelSize, mapImage);
        end
        %% ==================== Write Test Files  ===================== % 
        if INI.BLE_testfiles.write_Positions
            fprintf(fID_T2, '%6.3f  %6.3f  %6.3f \n', ...
                    KalmanFilteredData(t,1), bleUserCoords(t,2:3));            
        end
        %% ================= End writing to Test Files  ================= %     
        %% ===================== END DEBUG ===================== %
    end
end


%% ======================= DEBUG ======================= %
%% ==================== Write Test Files  ===================== % 
if INI.BLE_testfiles.write_SelectedBeacons    
    fclose(fID_T1);
end
if INI.BLE_testfiles.write_Positions
    fclose(fID_T2);
end
if INI.BLE_testfiles.write_SelectedBeaconsRSSI
    fclose(fID_T3);
end
if INI.BLE_testfiles.write_CheckTriplets
    fclose(fID_T4);
    fclose(fID_T5);
end
if INI.BLE_testfiles.write_DistanceLimiter
    fclose(fID_T7);
end
%% ================= End writing to Test Files  ================= %  
if INI.BLE_debug.comparePositionCPP        
    [CPPData, cppDataTs]  = READER.read([logsDir '\output_position_ble.txt'], INI);    
    mapPlotter = MAPPLOTTER(mapImage, mapParams.pixelSize, 'Comparison Matlab vs. CPP results (BLE)');
    mapPlotter.mPlotCompareMLvsCPP(bleUserCoords, CPPData, INI.BLE_debug.recordVideoMLvsCPP);
end
if INI.BLE_debug.comparePositionUA
    [UAData, uaDataTs]  = READER.read([logsDir '\output_position_ua-' logDateTime '.txt'], INI);
    mapPlotter = MAPPLOTTER(mapImage, mapParams.pixelSize, 'Comparison Matlab vs. UA results (BLE)');
    mapPlotter.mPlotCompareMLvsUA(bleUserCoords, UAData, bleDataAbsTs, uaDataTs);
end
%% ===================== END DEBUG ===================== %




