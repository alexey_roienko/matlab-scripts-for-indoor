clc, clear
close all;


%% ===================== PATHES SETTINGS ========================= %
currentDir = pwd;
floorMapDir = [currentDir '\map\'];
addpath([currentDir '\classes\']);


%% ========================= SETTINGS ============================ %
% Date of the log-files
logDateTime = '2018-01-11_16-49-32';
logsDir = [currentDir '\logs\'];


switchTH = 3;
useFeedbackFilter = true;
% filterCoef = 0.9;
filterCoef = 0.83;
% weights  = [7 5 3 1];
weights  = [7 2 1];

% Read 'config.ini'
[INI, mapParams, meshParams, beaconsParams, beaconsID, mapImage, mapMask] = ...
    READER.fReadAllSettingsBLE(floorMapDir, 'config.ini');
beaconsN = length(beaconsParams);


%bleData = READER.read([logsDir logDateTime '_ble_logger.json'], INI, beaconsID);
bleData = READER.read([logsDir 'input_ble_packets-' logDateTime '.txt'], INI, beaconsID);
packetsNumber = length(bleData(:,1));

% Read 'config.ini'
beaconsN  = length(beaconsParams);
% mapsBeacN = [9 2];
mapsBeacN = [5 4];


%% ========================= MAIN PART =========================== %
fprintf('Processing logs from <%s>...\n\n', logDateTime);
%fprintf('System MODE - <%s> with <%s> BLE mode\n', systemMode, BLEMode);

initialMlf = zeros(packetsNumber, 2);
filteredMlf = zeros(packetsNumber, 2);
mapSwitchCounter = 0;

for ts = 1:packetsNumber
    initialMlf(ts, :) = calcMLfunc(bleData(ts, 2:beaconsN+1), weights, mapsBeacN);
    
    % feedback filter for mlf values
    if useFeedbackFilter 
        if (ts == 1)
            filteredMlf(ts,:) = initialMlf(ts, :);
        else
            filteredMlf(ts,:) = filteredMlf(ts-1,:) * filterCoef + (1 - filterCoef) * initialMlf(ts, :);
        end
    else
        filteredMlf(ts,:) = initialMlf(ts, :);
    end

    [maxVal, calcMap] = max(filteredMlf(ts, :));
    
    if ts == 1
        currentMap = calcMap;        
        switch calcMap
            case 1
                fprintf('ts = %.2f, current map is THE 3rd FLOOR\n', bleData(ts,1));                
            case 2
                fprintf('ts = %.2f, current map is THE 4th FLOOR\n', bleData(ts,1));
            otherwise
                disp('Unknown Floor!');
        end        
    else
        if (calcMap ~= prevMap)
            mapSwitchCounter = 0;
            if (calcMap ~= currentMap)
                mapSwitchCounter = mapSwitchCounter + 1;
            end
        else
            if (calcMap ~= currentMap)
                mapSwitchCounter = mapSwitchCounter + 1;
                if (mapSwitchCounter == switchTH)
                    mapSwitchCounter = 0;
                    fprintf('ts = %.2f, MAP IS SWITCHED\n', bleData(ts,1));
                    currentMap = calcMap;
                end
            else
                switch calcMap
                    case 1
                        fprintf('ts = %.2f, current map is THE 3rd FLOOR\n', bleData(ts,1));                
                    case 2
                        fprintf('ts = %.2f, current map is THE 4th FLOOR\n', bleData(ts,1));
                    otherwise
                        disp('Unknown Floor!');
                end        
            end            
        end
    end
    prevMap = calcMap;
end

fprintf('\n');

figure(1); 
hold on;   grid on;
plot(bleData(:, 1), initialMlf(:, 1), 'LineWidth', 0.5);
plot(bleData(:, 1), initialMlf(:, 2), 'LineWidth', 0.5);
plot(bleData(:, 1), filteredMlf(:, 1), 'LineWidth', 2, 'Color', [0.49 0.18 0.56]);
plot(bleData(:, 1), filteredMlf(:, 2), 'LineWidth', 2, 'Color', [0.93 0.69 0.13]);
legend('Initial MLF (3rd)', 'Initial MLF (4th)',...
       'Filtered MLF (3rd)', 'Filtered MLF (4th)');
xlim([0 bleData(end, 1)]);
xlabel('Time, sec.');
ylabel('MLF values');











