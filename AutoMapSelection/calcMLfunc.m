function output = calcMLfunc(inputBLEdata, initWeights, mapsBeacN)

%% Removing zero-values RSSIs
nonZeroBLE = inputBLEdata;
subsConst  = -300;
nonZeroBLE(nonZeroBLE == 0) = subsConst;

%% Sorting current RSSI values
[sortedPacks, indeces] = sort(nonZeroBLE, 'descend');

%% Defining weights vector taking into account possible duplicates
beaconsN     = length(inputBLEdata);
weights      = zeros(1, beaconsN);
weights(1)   = initWeights(1);
weightIndex  = 1;
doublesCounter = 1;
doublesFlag  = false;
for b = 2:beaconsN
    if sortedPacks(b) == subsConst
        weights(b) = 0;
    else
        if sortedPacks(b) == sortedPacks(b-1)
            weights(b)  = weights(b-1);
            if doublesFlag
                doublesFlag = true;
                doublesCounter = doublesCounter + 1;
            end
        else
            if doublesFlag
                weights(b-1:-1:b-doublesCounter) = weights(b-1)/doublesCounter;
                doublesCounter = 1;
                doublesFlag    = false;
            end
            weightIndex = weightIndex + 1;
            weights(b)  = initWeights(weightIndex);
        end
    end
    
    if weightIndex+1 > length(initWeights)
        break;
    end
end


%% Calculating each Floor Maximum Likelihood Functions (mlf)
mapBeacons    = zeros(length(mapsBeacN), max(mapsBeacN));
mapBeacons(1, 1:mapsBeacN(1)) = 1:mapsBeacN(1);
mapBeacons(2, 1:mapsBeacN(2)) = mapsBeacN(1)+1 : mapsBeacN(1)+mapsBeacN(2);
mlf = zeros(1, length(mapsBeacN));

for m = 1:length(mapsBeacN)
    for b = 1:mapsBeacN(m)
        % if current beacon has non-zero input value, then start algorithm
        if inputBLEdata(mapBeacons(m, b)) ~= 0
            for i = 1:length(indeces)
                if indeces(i) == mapBeacons(m, b)
                    mlf(m) = mlf(m) + weights(i);
                    break;
                end
            end
        end
    end
end

% nonZeroMlf   = mlf(mlf~= 0);
% mlfAvgGeom   = (prod(nonZeroMlf))^(1/length(nonZeroMlf));
% 
% output       = mlf / mlfAvgGeom;
output       = mlf;

end





