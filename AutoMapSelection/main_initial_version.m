clc, clear
close all;


%% ===================== PATHES SETTINGS ========================= %
currentDir = pwd;
floorMapDir = [currentDir '\map\'];
addpath([currentDir '\classes\']);


%% ========================= SETTINGS ============================ %
% Date of the log-files
logDateTime = '2018-05-30_16-43-39';
% logDateTime = '2018-05-30_16-44-36';
% logDateTime = '2018-05-30_16-45-13';
% logDateTime = '2018-05-30_16-46-08';
logsDir = [currentDir '\logs\'];

switchTH = 3;
useFeedbackFilter = true;
% filterCoef = 0.9;
filterCoef = 0.83;
% weights  = [7 5 3 1];
weights  = [7 2 1];

mapsBeacN = [9 2];

% Read 'config.ini'
[INI, mapParams, meshParams, beaconsParams, beaconsID, mapImage, mapMask] = ...
    READER.fReadAllSettingsBLE(floorMapDir, 'config.ini');
beaconsN = length(beaconsParams);


bleData = READER.read([logsDir logDateTime '_ble_logger.json'], INI, beaconsID);
packetsNumber = length(bleData(:,1));

% Read 'config.ini'
beaconsN = length(beaconsParams);


%% ========================= MAIN PART =========================== %
fprintf('Processing logs from <%s>...\n\n', logDateTime);
%fprintf('System MODE - <%s> with <%s> BLE mode\n', systemMode, BLEMode);

initialMlf = zeros(packetsNumber, 2);
filteredMlf = zeros(packetsNumber, 2);
mapSwitchCounter = 0;

for ts = 1:packetsNumber
    initialMlf(ts, :) = calcMLfunc(bleData(ts, 2:12), weights, mapsBeacN);
    
    % feedback filter for mlf values
    if useFeedbackFilter 
        if (ts == 1)
            filteredMlf(ts,:) = initialMlf(ts, :);
        else
            filteredMlf(ts,:) = filteredMlf(ts-1,:) * filterCoef + (1 - filterCoef) * initialMlf(ts, :);
        end
    else
        filteredMlf(ts,:) = initialMlf(ts, :);
    end
    
    
    if filteredMlf(ts, 2) > filteredMlf(ts, 1)
        %fprintf('ts = %.2f, current map is CORRIDOR\n', bleData(ts,1));
        if ts == 1
            currentMap = 2;   % or string-name of the map
            prevMap    = 2;
            fprintf('ts = %.2f, current map is CORRIDOR\n', bleData(ts,1));
        else
            if (currentMap ~= 2)
                mapSwitchCounter = mapSwitchCounter + 1;
                if (mapSwitchCounter == switchTH)
                    mapSwitchCounter = 0;
                    fprintf('ts = %.2f, current map is CORRIDOR, MAP IS SWITCHED\n', bleData(ts,1));
                    currentMap = 2;   % or string-name of the map
                else
                    fprintf('ts = %.2f, MAPSWITCHCOUNTER is INCREASED\n', bleData(ts,1));
                end
            else
                if filteredMlf(ts-1, 2) < filteredMlf(ts-1, 1)
                    mapSwitchCounter = 0;
                end
                fprintf('ts = %.2f, current map is CORRIDOR\n', bleData(ts,1));
            end
        end
        
    elseif filteredMlf(ts, 1) > filteredMlf(ts, 2)
        %fprintf('ts = %.2f, current map is OFFICE\n', bleData(ts,1));
        if ts == 1
            currentMap = 1;   % or string-name of the map
            prevMap    = 1;
            fprintf('ts = %.2f, current map is OFFICE\n', bleData(ts,1));
        else
            if (currentMap ~= 1)
                mapSwitchCounter = mapSwitchCounter + 1;
                if (mapSwitchCounter == switchTH)
                    mapSwitchCounter = 0;
                    fprintf('ts = %.2f, current map is OFFICE, MAP IS SWITCHED\n', bleData(ts,1));
                    currentMap = 1;   % or string-name of the map
                else
                    fprintf('ts = %.2f, MAPSWITCHCOUNTER is INCREASED\n', bleData(ts,1));
                end
            else
                if filteredMlf(ts-1, 2) > filteredMlf(ts-1, 1)
                    mapSwitchCounter = 0;
                end
                fprintf('ts = %.2f, current map is OFFICE\n', bleData(ts,1));
            end
        end
        
    else
        fprintf('ts = %.2f, ML functions are EQUAL\n', bleData(ts,1));
        if ts == 1
            currentMap = 1;   % or string-name of the map
            fprintf('ts = %.2f, current map is OFFICE\n', bleData(ts,1));
        else
            mapSwitchCounter = mapSwitchCounter + 1;
            if (currentMap == 1)
                if (mapSwitchCounter == switchTH)
                    mapSwitchCounter = 0;
                    fprintf('ts = %.2f, current map is CORRIDOR, MAP IS SWITCHED\n', bleData(ts,1));
                    currentMap = 2;   % or string-name of the map
                end
            elseif (currentMap == 2)
                if (mapSwitchCounter == switchTH)
                    mapSwitchCounter = 0;
                    fprintf('ts = %.2f, current map is OFFICE, MAP IS SWITCHED\n', bleData(ts,1));
                    currentMap = 1;   % or string-name of the map
                end
            end
        end
    end
end

fprintf('\n');

figure(1); 
hold on;   grid on;
plot(bleData(:, 1), initialMlf(:, 1), 'LineWidth', 0.5);
plot(bleData(:, 1), initialMlf(:, 2), 'LineWidth', 0.5);
plot(bleData(:, 1), filteredMlf(:, 1), 'LineWidth', 2, 'Color', [0.49 0.18 0.56]);
plot(bleData(:, 1), filteredMlf(:, 2), 'LineWidth', 2, 'Color', [0.93 0.69 0.13]);
legend('Initial MLF (office)', 'Initial MLF (corridor)',...
       'Filtered MLF (office)', 'Filtered MLF (corridor)');
xlim([0 bleData(end, 1)]);
xlabel('Time, sec.');
ylabel('MLF values');











