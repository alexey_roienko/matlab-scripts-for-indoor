These log files were recorded for testing the RSSI values from beacons on different maps.

True amount of steps in each log:
2018-05-30_16-43-39_ble_logger - from corridor to office, to Lina's window,
2018-05-30_16-44-36_ble_logger - from corridor to office, to Lina's window,
2018-05-30_16-45-13_ble_logger - from office, Lina's window, to corridor
2018-05-30_16-46-08_ble_logger - from office, Lina's window, to corridor

2 beacons were in corridor, and 9 - in Kaa office.

Corresponding videos are available via link - https://drive.google.com/open?id=1K1au-kofWG8rXJTi-zfx_QWI82sih5GL