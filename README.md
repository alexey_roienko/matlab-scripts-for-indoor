# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repo is devoted to MatLab realization of different parts of Indoor Navigation system.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* There are a number of branches. Each branch is devoted to one of the system functional blocks or fully modeled the system.
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner - IT-Jim Company, it-jim.com 
* admin - Alexey Roienko, e-mail: a.roienko@it-jim.com