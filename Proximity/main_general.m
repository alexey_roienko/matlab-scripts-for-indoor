clc, clear
close all;

addpath('COMMON classes', 'COMMON classes\jsonlab', 'logs', 'map', 'BLE Navigator', 'NewClasses');

%% ========================= SETTINGS ============================ %
% Date of the log-files
logDateTime = '2018-01-11_14-04-36';

% Read 'config.ini'
[INI, mapParams, beaconsParams, beaconsID, mapImage, mapMask] = ...
    READER.fReadAllSettingsBLE('config.ini');
% Rewrite parameter values, which correspond to meshes, in INI entity
READER.fCorrectMeshData(INI);

% Load maskTable1 file, greater mesh for Mesh Corrector
if INI.correctors.mapCorrection || INI.correctors.meshCorrection ...
      || INI.correctors.wallsCorrection
    maskTable1 = load([pwd INI.general.pathMasktable1]);
    maskTable2 = load([pwd INI.general.pathMasktable2]);
end


%% ========================= MAIN PART =========================== %
main_BLE;