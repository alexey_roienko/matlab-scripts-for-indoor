
[bleData, bleDataAbsTs] = READER.read([pwd '\logs\input_ble_packets-' logDateTime '.txt'],...
                                       INI, beaconsID);
packetsNumber = length(bleData(:,1));


%% ===================== DEBUG ======================= % 
if INI.BLE_debug.showInitialRSSIMap
     PLOTTER.fShowRSSIMap(bleData, 'Raw RSSI');
end
%% =================== END DEBUG ===================== % 


% predictPackN = ceil(bleData(end,1)/INI.BLE.beaconsCycle) + 1;

%% ======================= RSSI KALMAN FILTRATION =========================%
if INI.BLE_filters.RSSIKalmanFilter
    P = [100 0; 0 100];        % covariance matrix         
    Q = [0.001 0; 0 0.001];    % model error covariance [0.002 0; 0 0.002]
    R = 5;                     % measurement error covariance 0.11
    objKalman = KalmanFilterRSSI ('P_init', P,  'Q', Q,  'R', R,...
                                  'beacNumb', INI.BLE.beaconsN,...
                                  'timeout1', INI.BLE.timeout1,...
                                  'timeout2', INI.BLE.timeout2);
    numbBeac = size(bleData, 2) - 1;
%     KalmanFilteredData = zeros(predictPackN, numbBeac + 1);
    KalmanFilteredData = zeros(packetsNumber, numbBeac + 1);
    addedTSindex = 1;
    kalmanIndex = 1;
    for pN=1:packetsNumber
        if pN==1
            dt = 0;
        else
            dt = bleData(pN,1) - bleData(pN-1,1);
        end
        
%         if dt > INI.BLE.beaconsCycle + (0.5*INI.BLE.beaconsCycle)
%             % abnormal situation, some timestamps are missing by receiver
%             realTS = round(dt / INI.BLE.beaconsCycle) - 1;
%             rssiValues = KalmanFilteredData(kalmanIndex-1, 2:end);
%             for i = 1:realTS
%                 % set proper timestamp
%                 KalmanFilteredData(kalmanIndex, 1) = bleData(pN-1,1) + i*INI.BLE.beaconsCycle;
%                 addedTS(addedTSindex) = kalmanIndex;
%                 addedTSindex = addedTSindex + 1;
%                 % double the last RSSI values
%                 KalmanFilteredData(kalmanIndex, 2:end) = rssiValues;
%                 kalmanIndex = kalmanIndex + 1;
%             end
%         end

        % normal situation, timestamp is correct
        KalmanFilteredData(kalmanIndex, 1) = bleData(pN,1);
        KalmanFilteredData(kalmanIndex, 2:end) = objKalman.FilterMany(dt, bleData(pN,2:end));
        kalmanIndex = kalmanIndex + 1;
    end         
else 
    KalmanFilteredData = bleData;
end


%% ======================= DEBUG ======================= % 
if INI.BLE_debug.showKalmanFilteredRSSIMap
    PLOTTER.fShowRSSIMap(KalmanFilteredData, 'Kalman Filtered RSSI');
end
if INI.BLE_debug.plotMapWithBeacons
    logFileName = ['input_ble_packets-' logDateTime '.txt'];
    mapFig = PLOTTER.PlotMap(mapImage, mapParams, logFileName,...
                             mapParams.pixelSize, beaconsParams);
end
% ============= Creation of Test Files Headers ============== % 
if INI.BLE_testfiles.write_Positions
    fID_T2 = fopen('test files\tBLEpositions.txt', 'w');
    fprintf(fID_T2, 'Timestamp\tX\tY\n');
end
%% ===================== END DEBUG ===================== % 


%% ===================== Calculating a map graph ======================= %%
objGraphG = GraphCreatorG();
maxEdgeLength = 20;
GRIT_ANGLE = 20;
graphG = objGraphG.getGraph(beaconsParams, INI.general.mesh2Rows, maskTable2, ...
    [INI.general.mesh2StepX INI.general.mesh2StepY], maxEdgeLength, GRIT_ANGLE);
shouldDelete = false;
if INI.BLE_debug.plotBeaconsGraph
    if ~INI.BLE_debug.plotMapWithBeacons
        logFileName = ['input_ble_packets-' logDateTime '.txt'];
        mapFig = PLOTTER.PlotMap(mapImage, mapParams, logFileName,...
                                 mapParams.pixelSize, beaconsParams);
    end
    NewPlotterAlex.PlotAllTrajectories(graphG, mapParams.pixelSize, 'red', shouldDelete, 4, 'Graph');
end


%% ======================= Reformatting data for Dijkstra algorithm ======================= %%
V_Dijkstra = zeros(length(beaconsParams),3);
for i = 1:length(beaconsParams)
    V_Dijkstra(i,:) = [beaconsParams{1,i}.x beaconsParams{1,i}.y beaconsParams{1,i}.z];
end
E3_Dijkstra = zeros(length(graphG),3);
k = 1;
for i = 1:length(graphG)
    E3_Dijkstra(k,:) = [graphG{1,i}.beac1N  graphG{1,i}.beac2N  ...
        1/sqrt((graphG{1,i}.beac1X - graphG{1,i}.beac2X)^2 + (graphG{1,i}.beac1Y - graphG{1,i}.beac2Y)^2)];
    E3_Dijkstra(k+1,:) = [graphG{1,i}.beac2N  graphG{1,i}.beac1N  E3_Dijkstra(i,3)];
    k = k + 2;
end


%% ======================= MAIN CYCLE ======================= %%
KalmanFilteredData(KalmanFilteredData == 0) = -inf;
KalmanFilteredData(1,1) = 0;
mode = 'RSSI';                         % Modes: 'dist', 'RSSI' or 'relRSSI'
position = zeros(length(KalmanFilteredData(:,1)), 3);

for t = 1:length(KalmanFilteredData(:,1))
    position(t,1) = KalmanFilteredData(t,1);
    
    %% ======================= Reformatting for positioning ======================= %%
    beaconsWithRSSI = cell(1, length(beaconsParams)-1); 
    for bn = 1:length(beaconsParams)
        beacon = beaconsParams{1,bn};
        beacon.RSSI = KalmanFilteredData(t,bn + 1);
        if beacon.RSSI ~= -inf
            beacon.relRSSI = (beacon.txpower / beacon.RSSI)^beacon.damp;
        else
            beacon.relRSSI = -inf;
        end
        beacon.dist = LATERATION.fRssi2Dist({beacon});
        beaconsWithRSSI{bn} = beacon;
    end
    
    
    %% ======================= Checking the number of active beacons and edge candidates ================== %%
    [oneBeaconFlag, beaconN] = ProximityAlex.isThereOneActiveBeacon(beaconsWithRSSI);
%     if oneBeaconFlag
%         % if there is the only active beacon, then let's find the number of
%         % its adjacent edges...
%         oneEdgeCandidateFlag = ProximityAlex.isThereOneEdgeCandidate(beaconN, graphG);
%     end
    
    % if there is the only active beacon and several adjacent edges, then
    % beacon position is the user position, because there is an ambiguity in user positions
    if oneBeaconFlag %&& ~oneEdgeCandidateFlag
        RSSIvalues = zeros(length(beaconsWithRSSI),1);
        for b=1:length(beaconsWithRSSI)
            RSSIvalues(b) = beaconsWithRSSI{1,b}.RSSI;
        end
        [val, pos] = max(RSSIvalues);
        position(t,2:3) = [beaconsWithRSSI{1,pos}.x  beaconsWithRSSI{1,pos}.y];
    else
        %% ======================= Searching the current active edge ======================= %%
        selectedEdge = ProximityAlex.findActiveEdge(beaconsWithRSSI, graphG, mode);
        NewPlotterAlex.PlotSelectedCut(selectedEdge, beaconsWithRSSI, mapParams.pixelSize);

        %% ======================= Searching the position-candidates ====================== %%
        [trueCrossings, allCrossings] = ProximityAlex.getCandidatesPositions(beaconsWithRSSI, ...
            {[selectedEdge.beac1N selectedEdge.beac2N selectedEdge.dist]});
        NewPlotterAlex.PlotCirclesAndCrossings(beaconsWithRSSI, allCrossings, trueCrossings, mapParams.pixelSize);

        %% ======================= Averaging the candidates and getting the final position ================ %%
        position(t,2:3) = ProximityAlex.getFinalPosition(beaconsWithRSSI, trueCrossings, ...
            [selectedEdge.beac1N selectedEdge.beac2N]);
    end
    
    
    %% ======================= DEBUG ======================= %
    % Debug when DistanceLimiter2 is OFF
    str = sprintf('dt = %.4f, x = %.4f, y = %.4f', KalmanFilteredData(t,1), position(t,2:3));
    disp(str);
    %% ===================== END DEBUG ===================== %
    
    
    %% ======================= Usage of the distance limiter (OPTIONAL) ======================= %%
    if (INI.BLE_filters.distanceLimiter2 == true) && (sum(isnan(position(t,2:3))) == 0)
        if t == 1
            tPrev = KalmanFilteredData(t,1);
            positionPrev = position(t,2:3);
            if exist('selectedEdge', 'var')
                selectedEdgePrev = selectedEdge;
            end
        else
            if ~exist('selectedEdgePrev', 'var')
                selectedEdgePrev = selectedEdge;
            end
            
            if selectedEdgePrev.dist ~= selectedEdge.dist
                pathGraph = ProximityAlex.findBeaconsToJoin(objGraphG, V_Dijkstra, E3_Dijkstra, ...
                    selectedEdgePrev, selectedEdge);
            else
                pathGraph = NaN;
            end

            [position(t,2:3), selectedEdgePrev] = ProximityAlex.distanceLimiter2(tPrev,...
                                                                        KalmanFilteredData(t,1),...
                                                                        positionPrev,...
                                                                        selectedEdgePrev,...
                                                                        position(t,2:3),...
                                                                        selectedEdge,...
                                                                        pathGraph,...
                                                                        beaconsWithRSSI,...
                                                                        INI.BLE.distanceLimiter2Coef);

            if isnan(selectedEdgePrev.dist)
                for i = 1:length(graphG)
                    if (selectedEdgePrev.beac1N == graphG{1,i}.beac1N) && (selectedEdgePrev.beac2N == graphG{1,i}.beac2N)
                       selectedEdgePrev.angle = graphG{1,i}.angle;
                       selectedEdgePrev.dist = graphG{1,i}.dist;
                       break;
                    end
                end
            end
            tPrev = KalmanFilteredData(t,1);
            positionPrev = position(t,2:3);
        end
    end
	
    NewPlotterAlex.PlotPositionsAndNumbers(position(t,2:3), mapParams.pixelSize, t);
    
    
    %% ======================= DEBUG ======================= %
    % Debug when DistanceLimiter2 is ON
%     str = sprintf('dt = %.4f, x = %.4f, y = %.4f', KalmanFilteredData(t,1), position(t,2:3));
%     disp(str);
    % ==================== Write Test Files  ===================== % 
    if INI.BLE_testfiles.write_Positions
        fprintf(fID_T2, '%6.3f  %6.3f  %6.3f \n', position(t,1:3));            
    end
    % ================= End writing to Test Files  ================= %     
    %% ===================== END DEBUG ===================== %
end





