%% Description:
% This is the test case created for checking the guess of Grechnev about
% the problems that might happen with the developed by us algorithm.
% Three test cases will be considered:
% Case 1: Default case, our initial algorithm (let`s call it "j=i+1" case)
% Case 2: Modified default case, our algorithm but with j=1 (let`s call it "j=1" case)
% Case 3: Alexey Grechnev algorithm (let`s call it "Grechnev" case)

clc, clear, close all;
addpath('classes', 'map files');

%% Beacons generation
% The only parameters of the beacons we need are the coordinates of each beacon [x y z].
% We expect to generate them automatically.

% Settings
beacN = 10;
pixelSize = 0.0075;
meshSteps = [0.1 0.1];
rowsN = 54;

% Load map and maskTable
map = imread('map.bmp');
maskTable = load('masktable.out');

% assist
mapSizePix = size(map);
mapSizeMet = mapSizePix * pixelSize;

% beacons generation
beacons = cell(1,beacN);
flag = false;
for b = 1:beacN
    while flag == false
        x = mapSizeMet(2) * rand(1,1);
        y = mapSizeMet(1) * rand(1,1);
        z = 3;
        
        nX = round(x / meshSteps(1));
        nY = round(y / meshSteps(2));
        
        index = int32(nX*rowsN + nY);
        
        value = int32(maskTable(index+1));
        if value == index
            flag = true;
%         else
%             disp('no');
        end
    end
    beac.x = x;
    beac.y = y;
    beac.z = z;
    beacons{1,b} = beac;
    flag = false;
end

% load 'testBeacons.mat'


%% Graph Creation Case 1: Felix, "j=i+1"
figure('units','normalized','outerposition',[0 0 0.5 1])
imshow(map);
hold on;

objGraphF = GraphCreatorF();
graphF = objGraphF.getGraph(beacons, rowsN, maskTable, meshSteps);
NewPlotterAlex.PlotAllTrajectories(graphF, pixelSize, 'red', 0, 4, 'Felix graph version');

% Depict beacons on the map
for b=1:beacN
    scatter(beacons{1,b}.x/pixelSize, beacons{1,b}.y/pixelSize, 'filled', 'MarkerEdgeColor','k', 'MarkerFaceColor','b');
    text(beacons{1,b}.x/pixelSize, (beacons{1,b}.y+0.14)/pixelSize, num2str(b), 'Color', 'black', 'FontSize', 12);
end
hold off;


%% CASE 3: "Grechnev"
figure('units','normalized','outerposition',[0.5 0 1 1])
imshow(map);
hold on;

objGraphG = GraphCreatorG();

maxEdgeLength = 20;
GRIT_ANGLE = 0;

graphG = objGraphG.getGraph(beacons, rowsN, maskTable, meshSteps, maxEdgeLength, GRIT_ANGLE);
NewPlotterAlex.PlotAllTrajectories(graphG, pixelSize, 'red', 0, 4, 'Alexey graph version');

% % Depict beacons on the map
for b=1:beacN
    scatter(beacons{1,b}.x/pixelSize, beacons{1,b}.y/pixelSize, 'filled', 'MarkerEdgeColor','k', 'MarkerFaceColor','b');
    text(beacons{1,b}.x/pixelSize, (beacons{1,b}.y+0.14)/pixelSize, num2str(b), 'Color', 'black', 'FontSize', 12);
end
hold off;






