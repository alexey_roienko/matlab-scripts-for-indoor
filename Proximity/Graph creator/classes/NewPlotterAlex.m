classdef NewPlotterAlex
    methods (Access = public, Static)
        
        function PlotAllTrajectories(traject, pix, color, shouldDelete, size, titleStr)
            trajectLength = length(traject);
            plotArray = zeros(trajectLength,1);
            textArray = zeros(trajectLength,1);
            for i = 1 : trajectLength
                plotArray(i) = plot([traject{1,i}.beac1X traject{1,i}.beac2X]./pix,...
                    [traject{1,i}.beac1Y traject{1,i}.beac2Y]./pix, 'Color', color, 'LineWidth',size);
                if traject{1,i}.beac1X < traject{1,i}.beac2X
                    x = traject{1,i}.beac1X + (traject{1,i}.beac2X - traject{1,i}.beac1X) / 2;
                else
                    x = traject{1,i}.beac2X + (traject{1,i}.beac1X - traject{1,i}.beac2X) / 2;
                end
                if traject{1,i}.beac1Y < traject{1,i}.beac2Y
                    y = traject{1,i}.beac1Y + (traject{1,i}.beac2Y - traject{1,i}.beac1Y) / 2;
                else
                    y = traject{1,i}.beac2Y + (traject{1,i}.beac1Y - traject{1,i}.beac2Y) / 2;
                end
                textArray(i) = text(x/pix-50, y/pix-20, ['Line ' num2str(traject{1,i}.edgeN) ...
                    '(' num2str(traject{1,i}.beac1N) '  ' num2str(traject{1,i}.beac2N) ')'], ...
                    'Color' , 'blue', 'FontSize', 8);                
%                 pause(0.1);
            end
%             pause();
            if shouldDelete
                delete(plotArray);
                delete(textArray);
            end
            
            title(titleStr);
        end
    end
end

