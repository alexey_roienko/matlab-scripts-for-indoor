classdef GraphCreator < handle
%GRAPHCREATOR Summary of this class goes here
%   Detailed explanation goes here
    
methods (Access = public)
    %% Object Constructor
    function obj = GraphCreator()
    end


    %% Method gets the vertices and the edges of the graph,
    %  who describes the available trajectories for a user
    function correctEdges = getGraphFelixVer(obj, beacons, mesh2Rows, maskTable2, meshSteps)

        bN = length(beacons);
        edges = {};
        edgesN = 1;
        eN = 0;
        k = 1;

        for i = 1:bN
            beacBase = beacons{1,i};
            for j = (i+1):bN
                if j~=i
                    beacRef = beacons{1,j};
                    if ~CORRECTOR.fIsWallBeingCrossed2([beacBase.x beacBase.y], [beacRef.x beacRef.y], ...
                            meshSteps, mesh2Rows, maskTable2)
                        edge.beac1N = i;
                        edge.beac1X = beacBase.x;
                        edge.beac1Y = beacBase.y;
                        edge.beac2N = j;
                        edge.beac2X = beacRef.x;
                        edge.beac2Y = beacRef.y;
                        dx = beacRef.x - beacBase.x;
                        dy = beacRef.y - beacBase.y;
                        if ~(dx==0 && dy==0)
                            edge.dist = sqrt(dx^2 + dy^2);
                            edge.line = k;
                            edge.angle = obj.calcEdgeAngle(dx, dy, edge.dist);                        

                            k = k + 1;
                            edges{1,edgesN} = edge;
                            edgesN = edgesN + 1;
                        else
                            warning('dx=0 and dy=0!');
                        end
                    end
                end
            end
            
            if ~isempty(edges)
                corrEdgesFound = obj.findCorrectEdges2(edges);
                L = length(corrEdgesFound);
                correctEdges(eN+1 : eN+L) = corrEdgesFound;

                % postprocessing
                eN = eN + L;
                edges = {};
            end
            edgesN = 1;
        end
    end


    function finalEdges = getGraphGrechnevVer(obj, beac, mesh2Rows, maskTable2, meshSteps, lim, pix)
        %% STAGE 1: SEARCHING FOR ALL POSSIBLE EDGES WITHOUT REPETITIONS,
        %% BELOW THE LIMIT
        edges = obj.GetAllEdges(beac, mesh2Rows, maskTable2, meshSteps, lim);
        NewPlotterAlex.PlotAllTrajectories(edges, pix, 'red', 1, 1, '');

        %% STAGE 2: SORTING BY THE EDGE LENGTH
        edges = obj.SortFoundEdges(edges);
        NewPlotterAlex.PlotAllTrajectories(edges, pix, 'green', 0, 1, '');


        %% STAGE 3: FINDING NEIGHBOURS BY VERTEX
        % In the array "neighbours" in every cell you see the numbers
        % of vertexes that are in contact with the base vertex
        neighbours = obj.GetNeighbourVertexes(edges, length(beac));

        % DebugInfo
        for i = 1 : length(neighbours)
            clearvarmessage = ['Vertex: ', num2str(i), '   ', 'Neighbours: '];
            for j = 1 : length(neighbours{1,i})
                clearvarmessage = strcat(clearvarmessage, num2str(neighbours{1,i}(j)), '-');
            end
            disp(clearvarmessage);
            clearvars message;
        end
        % EndDebug


        %% STAGE 4: BUILDING TRIANGLES
        lenE = length(edges);
        triangleNO = 1;
        k = 1;
        triangles = {};

        % Forming triangles
        for edgeNO = 1 : lenE
            triangVertOne = edges{1,edgeNO}.One;
            triangVertTwo = edges{1,edgeNO}.Two;
            arOne = neighbours{1, triangVertOne};
            arTwo = neighbours{1, triangVertTwo};
            triangVertThree = intersect(arOne,arTwo);
            if ~isempty(triangVertThree)
                for i = 1 : length(triangVertThree)
                    isAlreadyInTriangles = false;
                    if ~isempty(triangles)
                        for j = 1 : length(triangles)
                            iterOuter = sort([triangVertOne triangVertTwo triangVertThree(i)]);
                            iterInner = sort(triangles{1,j}(1:3));
                            if iterInner(1) == iterOuter(1) && iterInner(2) == iterOuter(2) && iterInner(3) == iterOuter(3)
                                isAlreadyInTriangles = true;
                            end
                        end
                    end
                    if ~isAlreadyInTriangles
                        % Side ij(see the description)
                        sideOneTwo = sqrt((beac{1,triangVertOne}.x - beac{1,triangVertTwo}.x)^2 + (beac{1,triangVertOne}.y - beac{1,triangVertTwo}.y)^2);
                        % Side ik
                        sideOneThree = sqrt((beac{1,triangVertOne}.x - beac{1,triangVertThree(i)}.x)^2 + (beac{1,triangVertOne}.y - beac{1,triangVertThree(i)}.y)^2);
                        % Side jk
                        sideTwoThree = sqrt((beac{1,triangVertTwo}.x - beac{1,triangVertThree(i)}.x)^2 + (beac{1,triangVertTwo}.y - beac{1,triangVertThree(i)}.y)^2);
                        % First contiguous to sideOneTwo angle
                        angleBetweenOneTwoAndOneThree = 180/pi * acos((sideOneTwo^2 + sideOneThree^2 - sideTwoThree^2) / (2 * sideOneTwo * sideOneThree));
                        % Second contiguos to sideOneTwo angle
                        angleBetweenOneTwoAndTwoThree = 180/pi * acos((sideOneTwo^2 + sideTwoThree^2 - sideOneThree^2) / (2 * sideOneTwo * sideTwoThree));
                        % Only opposite to sideOneTwo angle
                        angleBetweenOneThreeAndTwoThree = 180/pi * acos((sideOneThree^2 + sideTwoThree^2 - sideOneTwo^2) / (2 * sideOneThree * sideTwoThree));

                        triangles{1,k} = [triangVertOne,...
                            triangVertTwo,...
                            triangVertThree(i),...
                            sideOneTwo,...
                            sideOneThree,...
                            sideTwoThree,...
                            angleBetweenOneTwoAndOneThree,...
                            angleBetweenOneTwoAndTwoThree,...
                            angleBetweenOneThreeAndTwoThree];
                        k = k + 1;
                    end
                end
            end
        end

        % Discarding the Edges
        for tr = 1 : length(triangles)
            % Finding longest side
            % side 1 is formed by vertexes One and Two
            % side 2 is formed by vertexes One and Three
            % side 3 is formed by vertexes Two and Three
            [~,NOOfEdgeToBeExcluded] = sort(triangles{1,tr}(4:6), 'descend');
            if NOOfEdgeToBeExcluded(1) == 1
                edgeToBeExcluded = [triangles{1,1}(1) triangles{1,1}(2)];
            elseif NOOfEdgeToBeExcluded(1) == 2
                edgeToBeExcluded = [triangles{1,1}(1) triangles{1,1}(3)];
            else
                edgeToBeExcluded = [triangles{1,1}(2) triangles{1,1}(3)];
            end

            % Checking Critical angle 12 deg
            % Angle near i is triangles{1,1}(7), or angleBetweenOneTwoAndOneThree
            % Angle near j is triangles{1,1}(8), or angleBetweenOneTwoAndTwoThree
            if triangles{1,tr}(7) < 12 || triangles{1,tr}(8) < 12
                shouldDiscard = true;
            else
                shouldDiscard = false;
            end
            % Disabling edges if they fulfill the condition of
            % discarding
            if shouldDiscard == true
                for i = 1 : length(edges)
                    if (edges{1,i}.One == edgeToBeExcluded(1) && ...
                            edges{1,i}.Two == edgeToBeExcluded(2)) || ...
                            (edges{1,i}.One == edgeToBeExcluded(1) && ...
                            edges{1,i}.Two == edgeToBeExcluded(1))

                        edges{1,i}.enabled = false;
                    end
                end
            end
        end

        % Formatting the output
        finalEdges = {};
        k = 1;
        for i = 1 : lenE
            if edges{1,i}.enabled
                finalEdges{1,k} = edges{1,i};
                k = k + 1;
            end                
        end
    end
end


methods (Access = private)
    
    %% Function calculates the angle of the edge in the coordinate system 
    %  where the Y axis is directed to the South (because of the image coordinate system)
    function angle = calcEdgeAngle(~, dx, dy, dist)
        if dx ~= 0
            tempAngle = (180/pi) * asin(dy/dist);
            %           TWO=-90
            %              |
            %  angle < 0   |   angle < 0
            %   TWO=0-----ONE------TWO=0
            %  angle > 0   |   angle > 0
            %              |
            %           TWO=90
            if (dx<0) && (dy>0)
                angle = 180 - tempAngle;
            elseif (dx<0) && (dy<0)
                angle = -180 - tempAngle;
            else
                angle = tempAngle;
            end
        elseif dy > 0
            angle = 90;
        else
            angle = -90;
        end
    end
    
    
    function output = findCorrectEdges1(obj, edges)
        j = 1;
        k = 1;
        % Splitting into right left and center beacons refering to base beacon
        rightPosLinks = {};
        leftPosLinks = {};
        eN = length(edges);

        for i = 1:eN
            if edges{i}.beac1X <= edges{i}.beac2X
                rightPosLinks{j} = edges{i};
                j = j + 1;
            else
                leftPosLinks{k} = edges{i};
                k = k + 1;
            end
        end
        tempLength = 0;
        if ~isempty(rightPosLinks)
            temp =  obj.FindUniq(rightPosLinks);
            output = temp;
            tempLength = length(temp);
        end
        if ~isempty(leftPosLinks)
            temp = obj.FindUniq(leftPosLinks);
            if tempLength ~= 0
                output(1, tempLength + 1 : tempLength + length(temp)) = temp;
            else
                output = temp;
            end
        end
    end
    
    function output = findCorrectEdges2(~, edges)
        eN = length(edges);
        
        for i=1:eN
            if edges{i}.dist ~= -1
                a = edges{i}.angle;
                for j=(i+1):eN
                    if j~=i
                        if edges{j}.dist ~= -1
                            b = edges{j}.angle;
                            if (a>0) && (b<0)
                                b = b + 360;
                            elseif (a<0) && (b>0)
                                b = b - 360;
                            end

                            if abs(a-b)<30
                                if edges{i}.dist > edges{j}.dist
                                    edges{i}.dist = -1;
                                    break;
                                else
                                    edges{j}.dist = -1;
                                end
                            end
                        end
                    end
                end
            end            
        end
        
        k = 1;
        for i=1:eN
            if edges{i}.dist ~= -1
                temp{1,k} = edges{i};
                k = k + 1;
            end
        end
        
        output = temp;
    end

    
    function uniqLinks = FindUniq (~,links)
        linksSize = length(links);
        % selector: Selection angle = 30 degrees
        for i = 1 : linksSize
            if links{1,i}.dist ~= -1
                for j = i + 1 : linksSize
                    if links{1,j}.dist ~= -1
                        if abs(links{1,i}.angle - links{1,j}.angle) < 15
                            if links{1,j}.dist < links{1,i}.dist
                                links{1,i}.dist = -1;
                            else
                                links{1,j}.dist = -1;
                            end
                        end
                    end
                end
            end
        end

        % Output array formation
        k = 1;
        for i = 1 : linksSize
            if links{1,i}.dist ~= -1
                uniqLinks{1,k} = links{1,i};
                k = k + 1;
            end
        end
    end


    function edges = GetAllEdges(~, beac, mesh2Rows, maskTable2, meshSteps, lim)
        bn = length(beac);
        m = 1; % Iterator for the selected edges;
        edges={};

        for i = 1 : bn
            beacBase = beac{1,i};
            for j = 1 : bn
                beacRef = beac{1,j};
                % Does edge exist?
                if i ~= j && ~CORRECTOR.fIsWallBeingCrossed2([beacBase.x beacBase.y], [beacRef.x beacRef.y], meshSteps, mesh2Rows, maskTable2)
                    edge.dist = sqrt((beacBase.x - beacRef.x)^2 + (beacBase.y - beacRef.y)^2);

                    % Is the edge less than the limiter?
                    if edge.dist < lim
                        edge.One = i;
                        edge.Two = j;

                        % Does array edges contain the same edge?
                        doesContain = false;
                        if ~isempty(edges)
                            for k = 1 : length(edges)
                                if edge.One == edges{1,k}.Two  && edge.Two == edges{1,k}.One
                                    doesContain = true;
                                    break;
                                end
                            end
                        end

                        % If we do not have the edge, we add it to array
                        if doesContain == false
                            edge.OneX = beacBase.x;
                            edge.OneY = beacBase.y;
                            edge.TwoX = beacRef.x;
                            edge.TwoY = beacRef.y;
                            dx = edge.TwoX - edge.OneX;
                            dy = edge.TwoY - edge.OneY;
                            if dx ~= 0
                                edge.angle = (180 / pi)*asin((beacRef.y - beacBase.y) / edge.dist);
                                if (dx < 0)&&(dy > 0)
                                    edge.angle =  180 - edge.angle;
                                elseif (dx < 0)&&(dy < 0)
                                    edge.angle = -180 - edge.angle;
                                end
                            else
                                if dy > 0
                                    edge.angle =  90;
                                else
                                    edge.angle = -90;
                                end
                            end
                            edge.line = m;
                            edge.enabled = true;
                            edges{1,m} = edge;
                            m = m + 1;
                        end
                    end
                end
            end
        end
    end

    function edges = SortFoundEdges(~, edges )
        lenE = length(edges);
        dist_ar = zeros(1, lenE);
        sorted_edges = cell(1, lenE);

        for i = 1 : length(edges)
            dist_ar(i) = edges{1,i}.dist;
        end

        [~, dN] = sort(dist_ar, 'descend');

        for i = 1 : length(edges)
           sorted_edges{1,i} = edges{1,dN(i)};
           sorted_edges{1,i}.line = i;
        end
        edges = sorted_edges;
    end

    function neighbours = GetNeighbourVertexes(~, edges, bn )
        neighbours = cell(1, bn);
        lenE = length(edges);
        k = 1;

        for i = 1 : bn
            for j = 1 : lenE
                if edges{1,j}.One == i
                    neighbour(k) = edges{1,j}.Two;
                    k = k + 1;
                end
                if edges{1,j}.Two == i
                    neighbour(k) = edges{1,j}.One;
                    k = k + 1;
                end
            end
            if k>1
                neighbours{1,i} = neighbour;
            end

            k = 1;
            neighbour = [];                
        end
    end        
end
end

