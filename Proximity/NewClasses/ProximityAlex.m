classdef ProximityAlex
    methods(Access = public, Static)
        
        %% Function answers the question is the number of active beacons 
        %  at the current moment equal to 1 or not?
        function [answer, varargout] = isThereOneActiveBeacon(beacons)
            answer = true;            
            varargout{1} = -1;
            N = length(beacons);            
            counter = 0;
            for bN = 1:N
                if abs(beacons{1,bN}.RSSI) ~= Inf
                    counter = counter + 1;
                    beaconN = bN;
                    if counter > 1
                        answer = false;
                        break;
                    end
                end
            end
            if counter==0
                error("There are no active beacons!");
            elseif counter == 1
                varargout{1} = beaconN;
            end
        end
        
        
        %% Function answers the question is there a one edge candidate
        %  at the current moment or not?
        function answer = isThereOneEdgeCandidate(beaconN, graph)
            answer = true;
            k = 0;
            for i = 1:length(graph)
                if graph{1,i}.beac1N == beaconN || graph{1,i}.beac2N == beaconN
                    k = k + 1;
                    if k > 1
                        answer = false;
                        break;
                    end
                end
            end
        end
        
        
        %% Function determines the edge of the graph where the User is standing on
        function outputEdge = findActiveEdge(beacons, graph, flag)
            
            % 0 STEP: Reformatting input for sorting
            for i = 1:length(beacons)
                selectedData(i,:) = [beacons{1,i}.RSSI, beacons{1,i}.relRSSI, beacons{1,i}.dist];               
            end
            
            % 1 STEP: Sorting beacons accroding to RSSI or relRSSI or dist
            if strcmpi(flag, 'RSSI')
                [~, beacNumbSorted] = sort(selectedData(:,1), 'descend');
            elseif strcmpi(flag, 'relRSSI')
                [~, beacNumbSorted] = sort(selectedData(:,2), 'descend');
            elseif strcmpi(flag, 'dist')                
                [~, beacNumbSorted] = sort(selectedData(:,3), 'ascend');
            else
                error('Error in "findActiveEdge()" in "ProximityAlex.m"');
            end            
            
            % 2 STEP: Finding the beacon with the maximum RSSI
            maxBeacNO = beacNumbSorted(1);

            
            % 3 STEP: Searching for the edge-candidates (max-RSSI vertex + second-max RSSI vertex
            k = 1;
            for i = 1:length(graph)
                if graph{1,i}.beac1N == maxBeacNO || graph{1,i}.beac2N == maxBeacNO
                    edgeCandidates{1,k} = graph{1,i};
                    k = k + 1;
                end
            end
            
            
            % 4 STEP: Finding the "true" edge from the edge-candidates
            edgeCandidatesLength = length(edgeCandidates);
            res = find(beacNumbSorted == edgeCandidates{1,1}.beac1N, 1) + ...
                  find(beacNumbSorted == edgeCandidates{1,1}.beac2N, 1);
            outputEdge = edgeCandidates{1,1};
            if edgeCandidatesLength > 1
                for i = 2:edgeCandidatesLength
                    if find(beacNumbSorted == edgeCandidates{1,i}.beac1N, 1) + ...
                       find(beacNumbSorted == edgeCandidates{1,i}.beac2N, 1) < res
                        outputEdge = edgeCandidates{1,i};
                        res = find(beacNumbSorted == edgeCandidates{1,i}.beac1N, 1) + ...
                              find(beacNumbSorted == edgeCandidates{1,i}.beac2N, 1);
                    end
                end
            else
                outputEdge = edgeCandidates{1,1};
            end
        end
        
        
        
        %% Function determines all possible positions of User as a crossings
        %  of RSSI circles and the lines (cuts)            
        function [output, testing] = getCandidatesPositions(beacons, cuts)            
            %% STEP 0: Initialization
            output = [];
            beacLength = length(beacons);
            cutLength = length(cuts);
            k = 1;
            m = 1;
            
            %% STEP 1 Getting crossings from all beacons and selected cut
            % iterate over readings
            for i = 1 : beacLength
                if beacons{1,i}.RSSI ~= 0
                    % iterate over cuts
                    for j = 1 : cutLength
                        [positions, test] = ProximityAlex.findCrossings(beacons{1,i}.dist, [beacons{i}.x beacons{i}.y],...
                            [beacons{1, cuts{1,j}(1)}.x beacons{1, cuts{1,j}(1)}.y],...
                            [beacons{1, cuts{1,j}(2)}.x beacons{1, cuts{1,j}(2)}.y]);
                        
                        
                        %% STEP 2 Formatting the output array
                        if size(positions,1) == 2
                            output{k}(:) =  [positions(1,1) positions(1,2) i];
                            output{k+1}(:) = [positions(2,1) positions(2,2) i];
                            k = k + 2;
                        elseif size(positions,1) == 1
                            output{k}(:) =  [positions(1,1) positions(1,2) i];
                            k = k + 1;
                        end
                        %%%%%%%%%%%%%%%% testing %%%%%%%%%%%%%%%%%%%%%
                        testing{m} =  [test(1,1) test(1,2)];
                        testing{m+1} = [test(2,1) test(2,2)];
                        m = m + 2;
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    end
                end
            end
        end
        
        
        function pos = getFinalPosition(beacons, trueCrossings, poses)
            %% STEP 1: Checking number of position-candidates
            %% If there are more than one candidate then following the algorithm
            trueCrosLength = length(trueCrossings);
            if trueCrosLength > 1
                
                
                %% STEP 2: Finding the positions obtained from the beacons, which are the vertexes of the edge
                p1.x = NaN;
                p1.y = NaN;
                p2.x = NaN;
                p2.y = NaN;
                
                for i = 1 : trueCrosLength
                    if trueCrossings{1,i}(3) == poses(1)
                        p1.x = trueCrossings{1,i}(1);
                        p1.y = trueCrossings{1,i}(2);
                    elseif trueCrossings{1,i}(3) == poses(2)
                        p2.x = trueCrossings{1,i}(1);
                        p2.y = trueCrossings{1,i}(2);
                    end
                end
                
                %% STEP 3: Finding the averaged coordinates on the edge
                if ~isnan(p1.x) && ~isnan(p2.x)
                avCoordsSelLine = [(p1.x + p2.x) / 2 ...
                    (p1.y + p2.y) / 2];
                else
                    sumX = 0;
                    sumY = 0;
                    for i = 1:trueCrosLength
                        sumX = sumX + trueCrossings{1,i}(1);
                        sumY = sumY + trueCrossings{1,i}(2);
                    end
                    avCoordsSelLine = [sumX/trueCrosLength  sumY/trueCrosLength];
                end
               
                
                %% STEP 4: Weighting and reducing
                w = zeros(1, trueCrosLength);
                
                for i = 1 : trueCrosLength
                    w(i) = 1 / ((beacons{1,trueCrossings{1,i}(3)}.x - avCoordsSelLine(1))^2 + ...
                                (beacons{1,trueCrossings{1,i}(3)}.y - avCoordsSelLine(2))^2)^0.5;
                end
                w_norm = w/sum(w);
                
                
                %% STEP 5: Evaluating the final position
                sumX = 0;
                sumY = 0;
                for i = 1 : trueCrosLength
                    sumX = sumX + w_norm(i) * trueCrossings{1,i}(1);
                    sumY = sumY + w_norm(i) * trueCrossings{1,i}(2);
                end
                pos = [sumX sumY];
            
                
            %% STEP 1: Checking number of position-candidates
            %% If there is only one candidate then he is the chosen
            elseif trueCrosLength > 0
                pos = [trueCrossings{1,1}(1) trueCrossings{1,1}(2)];

                
            %% STEP 1: Checking number of position-candidates
            %% If there are no candidates, then nothing to talk about                
            else
                pos = NaN;
                disp('Warning! NO POSITIONS FOUND!');
            end
        end
        
        
        function pathGraph = findBeaconsToJoin(o, V, E, prevEdge, currEdge)
            %% STEP 0: Reformatting
            prevBeacons = [prevEdge.beac1N prevEdge.beac2N];
            currBeacons = [currEdge.beac1N currEdge.beac2N];
            
            %% STEP 1: Looking for common beacon
            commonBeacon = intersect(prevBeacons,currBeacons);
            
            %% STEP 2: Branch with the common beacon
            if ~isempty(commonBeacon)
                if prevEdge.beac1N == commonBeacon
                    SID = prevEdge.beac2N;
                else
                    SID = prevEdge.beac1N;
                end
                if currEdge.beac1N == commonBeacon
                    FID = currEdge.beac2N;
                else
                    FID = currEdge.beac1N;
                end
                pathGraph = [SID commonBeacon FID];
                %OBSOLETE CODE (DO NOT DELETE)
%                [~, pathGraph] = o.Dijkstra(V, E,SID,FID,false);
                
%                if ~ismember(comBeac, pathGraph)
%                    pathGraph(3) = pathGraph(2);
%                    pathGraph(2) = comBeac;
%                end                    
            
            %% STEP 3: No common beacon found. Looking for the trajectory 
            %% using the Dijkstra algorithm
            else
                [~, temp{1,1}] =  o.Dijkstra(V, E, prevEdge.beac1N, currEdge.beac1N, false);
                [~, temp{1,2}] =  o.Dijkstra(V, E, prevEdge.beac2N, currEdge.beac1N, false);
                [~, temp{1,3}] =  o.Dijkstra(V, E, prevEdge.beac1N, currEdge.beac2N, false);
                [~, temp{1,4}] =  o.Dijkstra(V, E, prevEdge.beac2N, currEdge.beac2N, false);
            
                
                %% STEP 4: Finding the longest trajectory    
                pathGraph = temp{1,1};
                for i = 2:4
                    if length(temp{1,i}) > length(pathGraph)
                        pathGraph = temp{1,i};
                    end
                end
            end
        end
        
        
        function [pos, edge] = distanceLimiter2(tPrev, tCurr, prevPos, prevEdge, currPos, currEdge, path, beac, limCoef)
            %% This method limits the displacement of the User according to
            %% the time between the adjucent readings from beacons
            % tPrev - previous time moment
            % tCurr - current time moment
            % prevPos - previous position, m;
            % prevCut - number of previous cut;
            % currPos - current position, m;
            % currCut - number of current but;
            % beacs - beacon parameters;
            % performance - the performance, according to which we
            % determine the limiting magnitude of displacment time vs displ;
            
            %% STEP 1: Determine the maximum distance that can be covered by
            %% the User per time 'dt'
            dt = tCurr - tPrev;
            distLim = limCoef * dt;
            
            %% STEP 2: If the User is on the same line against he/she was
            %% at previous step            
            % Same line            
            if currEdge.dist == prevEdge.dist                
                
                %% STEP 3: If the evaluated distance is less than the distance limiter
                % Displacement is less than distance limit
                if sqrt((currPos(1,1) - prevPos(1,1))^2 + (currPos(1,2) - prevPos(1,2))^2) <= distLim
                    disp('Same line. NO LIMITER');
                    pos = currPos;                    
                
                %% STEP 4: If the evaluated distance is greater than the distance limiter
                % Displacement is greater than distance limit    
                else
                    disp('Same line. DISTANCE LIMITER');
                    dist = ProximityAlex.findCrossings(distLim,...
                        prevPos,...
                        [beac{1,currEdge.beac1N}.x beac{1,currEdge.beac1N}.y],...
                        [beac{1,currEdge.beac2N}.x beac{1,currEdge.beac2N}.y]);
                    if size(dist,1) > 1
                        distOne = sqrt((currPos(1,1) - dist(1,1))^2 + (currPos(1,2) -  dist(1,2))^2);
                        distTwo = sqrt((currPos(1,1) - dist(2,1))^2 + (currPos(1,2) -  dist(2,2))^2);
                        if distOne > distTwo
                            pos = dist(2,:);
                        else
                            pos = dist(1,:);
                        end
                    else
                        pos = dist(1,:);
                    end
                end
                edge = currEdge;
                
                
            %% STEP 5: If the User is on different line against he/she was
            %% at previous step. Calculate the covered distance.  
            % Different lines    
            else
                % Path length evaluation
                dist_ar(1) = sqrt((beac{1,path(2)}.x - prevPos(1,1))^2 + (beac{1,path(2)}.y - prevPos(1,2))^2);
                k = 2;
                while k <= size(path,2) - 2
                    dist_ar(k) = sqrt((beac{1,path(k+1)}.x - beac{1,path(k)}.x)^2 + (beac{1,path(k+1)}.y - beac{1,path(k)}.y)^2);
                    k = k + 1;
                end
                dist_ar(k) = sqrt((currPos(1,1) - beac{1,path(length(path)-1)}.x)^2 + (currPos(1,2) - beac{1,path(length(path)-1)}.y)^2);
 
                %% STEP 6: If the evaluated distance is less than the distance limiter
                % Displacement is less than distance limit
                if distLim > sum(dist_ar)
                    disp('Different lines. NO LIMITER');                    
                    pos = currPos;
                    edge = currEdge;             
                    
                    
                %% STEP 7: If the evaluated distance is greater than the distance limiter, but the line reamains the same  
                % Displacement is greater than distance limit 
                else
                   % Displacement is greater than distance limit, but line
                    % is the same
                    if distLim < dist_ar(1)
                        disp('Different lines. DISTANCE LIMITER. RESULT - SAME LINE');                        
                        dist = ProximityAlex.findCrossings(distLim,...
                            prevPos,...
                            [beac{1,path(1)}.x beac{1,path(1)}.y],...
                            [beac{1,path(2)}.x beac{1,path(2)}.y]);
                        if size(dist,1) > 1
                            distOne = sqrt((beac{1,path(2)}.x - dist(1,1))^2 + (beac{1,path(2)}.y -  dist(1,2))^2);
                            distTwo = sqrt((beac{1,path(2)}.x - dist(2,1))^2 + (beac{1,path(2)}.y -  dist(2,2))^2);
                            if distOne > distTwo
                                pos = dist(2,:);
                            else
                                pos = dist(1,:);
                            end
                        else
                            pos = dist(1,:);
                        end
                        edge = prevEdge;
                       
                    
                    %% STEP 8: If the evaluated distance is greater than the distance limiter, but the line is different
                    % Displacement is greater than distance limit, but lines are different                          
                    else
                        disp('Different lines. DISTANCE LIMITER. RESULT - SAME LINE');
                        distAccum = dist_ar(1);
                        for k = 2 : size(dist_ar,2)
                            if distAccum + dist_ar(k) < distLim
                                distAccum = distAccum + dist_ar(k);
                            else
                                distLim = distLim - distAccum;
                                beac1 = beac{1,path(k)};
                                beac2 = beac{1,path(k+1)};
                                break;
                            end
                            k = k + 1;
                        end
                        dist = ProximityAlex.findCrossings(distLim,...
                            [beac1.x beac1.y],...
                            [beac1.x beac1.y],...
                            [beac2.x beac2.y]);
                        if size(dist,1) > 1
                            distOne = sqrt((currPos(1,1) - dist(1,1))^2 + (currPos(1,2) -  dist(1,2))^2);
                            distTwo = sqrt((currPos(1,1) - dist(2,1))^2 + (currPos(1,2) -  dist(2,2))^2);
                            if distOne > distTwo
                                pos = dist(2,:);
                            else
                                pos = dist(1,:);
                            end
                        else
                            pos = dist(1,:);
                        end
                        % PostProcessing
                        if path(k) < path(k+1)
                            edge.beac1N = path(k);
                            edge.beac1X = beac1.x;
                            edge.beac1Y = beac1.y;
                            edge.beac2N = path(k+1);
                            edge.beac2X = beac2.x;
                            edge.beac2Y = beac2.y;
                        else
                            edge.beac1N = path(k+1);
                            edge.beac1X = beac2.x;
                            edge.beac1Y = beac2.y;
                            edge.beac2N = path(k);
                            edge.beac2X = beac1.x;
                            edge.beac2Y = beac1.y;
                        end
                        edge.dist = sqrt((beac1.x - beac2.x)^2 + (beac1.y - beac2.y)^2);
                        edge.angle = NaN;
                        edge.edgeN = NaN;
                   end
                end
            end
        end
    end
    
    methods(Access = private, Static)
        function [coords, testing] = findCrossings(dist, beacSign, beacA, beacB)
            %% INPUT PARAMS
            % beacSign - the coordinates of beacon which RSSI is considered
            % beacA - beginning of line AB formed by two beacons A and B
            % beacB - end of line AB
            % dist - distance from the beacSign (determined from the RSSI)
            %% OUTPUT PARAMS
            % coords - found crossings
            coords = [];

            
            %% STEP 1: Check beacon positions
            % RULE: Beacon A must be the left one. If Beacon A is right one
            % in reference to beacon B, then they are sweped.
            if beacA(1,1) > beacB(1,1)
                temp = beacA;
                beacA = beacB;
                beacB = temp;
            end
            
            
            %% STEP 2: Calculating the coefficients of the quadratic equation
            % Tilt angle of AB line
            k = (beacB(1,2) - beacA(1,2)) / (beacB(1,1) - beacA(1,1));
            
            % coefficients of quadratic equation a, b, c
            a = 1 + k^2;
            b = -2 * (beacSign(1,1) - k * (beacA(1,2) - beacSign(1,2) - k * beacA(1,1)));
            c = (beacA(1,2) - beacSign(1,2) - k * beacA(1,1))^2 + beacSign(1,1)^2 - dist^2;
            
            
            %% STEP 3: Solving the standard quadratic equation 
            % Standard solution of quadratic equation
            D = b^2 - 4 * a * c;
            if D >= 0
                x1 = (-b + sqrt(D)) / (2 * a);
                x2 = (-b - sqrt(D)) / (2 * a);
                
                y1 = k * (x1 - beacA(1,1)) + beacA(1,2);
                y2 = k * (x2 - beacA(1,1)) + beacA(1,2);
                
                m = 1;
                
                
                %% STEP 4: Checking the found roots for their "correctmess"
                % finding valid crossings from available
                if ProximityAlex.checkFoundRoots([x1 y1], beacA, beacB)
                    coords(m,:) = [x1 y1];
                    m = m + 1;
                end
                if ProximityAlex.checkFoundRoots([x2 y2], beacA, beacB)
                    coords(m,:) = [x2 y2];
                end
                
                
                %% STEP 3: Writing default values in case no roots
                %% have been found
                %%%%%%%%% TESTTING %%%%%%%%%
                testing = [x1 y1; x2 y2];
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            else
                testing = [0.01 0.01; 0.01 0.01];
            end
        end
        
        
        function result = checkFoundRoots (coords, A, B)
            if coords(1,1) > A(1,1) && coords(1,1) < B(1,1)
                result = true;
            else
                result = false;
            end
        end
    end
end

