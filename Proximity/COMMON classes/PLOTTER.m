classdef PLOTTER < handle
    %PLOTTER Summary of this class goes here
    %   Detailed explanation goes here      
    methods (Access = public, Static)
        
        function fShowRSSIMap(RSSI, name)            
            % Plotting the data
            figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9])
            hold on;
            timestamps = RSSI(:,1)';  
            hold on;
            
            for i=1:length(timestamps)
                plot([timestamps(i) timestamps(i)], [0 14], 'k:');
            end
            
            % Depicting points for packets available
            sizeRSSI = size(RSSI);
            for beac = 2 : sizeRSSI(2) % iterate over beacons (starts from 2 because the first column in time)
                for read = 1 : sizeRSSI(1) % iterate over readings of a beacon 
                    if RSSI(read, beac) ~=0
                        scatter(RSSI(read,1) , beac - 1, 'filled', 'MarkerEdgeColor',[0 .5 .5], 'MarkerFaceColor',[0 .7 .7]);
                        text(RSSI(read,1), beac - 1 + 0.3, num2str(roundn(RSSI(read,beac), -1)) , 'Color', 'black', 'FontSize', 8) ;
                    end                     
                end
            end
            
            title(['\color{magenta}\fontsize{16}' name]);
            xlabel('Time, sec.');
            ylabel('Beacons number in json-file)');
                       
            ax = gca;
            ax.XTick = ceil(min(timestamps)) - 1 : ceil(max(timestamps));
            ax.YTick = 0:sizeRSSI(2);
            xlim([(ceil(min(timestamps)) - 1) ceil(max(timestamps))]);
            ylim([0 sizeRSSI(2)]);
            ax.YGrid = 'on';
            ax.GridLineStyle = '--';
            ax.GridAlpha = 0.5;
            
            T = get(gca,'tightinset');
            set(gca,'position',[T(1) T(2) 1-T(1)-T(3) 1-T(2)-T(4)]);            
            
            box on;     
%             pause();
%             close;
        end
        
        function fShowRSSITimeScale (RSSI, name)
            figure('units','normalized','outerposition',[0.05 0.05 0.95 0.95])
            hold on;

            for i=2 : length(RSSI(1,:))
                plot(RSSI(:,1), RSSI(:,i), 'LineWidth' , 3, 'DisplayName' , ['BEAC' int2str(i-1)] );
            end
            legend('Location','northeast');
            title(['\color{magenta}\fontsize{16}' name]);
            T = get(gca,'tightinset');
            set(gca,'position',[T(1) T(2) 1-T(1)-T(3) 1-T(2)-T(4)]);
            xlabel('Time, sec.');
            ylabel('Beacons number in json-file)');
            ax = gca;
            ax.XTick = 0:ceil(max(RSSI(:,1)));
            ylim([-100 -60]);
            xlim([0 ceil(max(RSSI(:,1)))]);
            ax.YGrid = 'on';
            ax.GridLineStyle = '--';
            ax.GridAlpha = 0.5;
            box on;

            pause();
            close;
        end
        
        function mapFigRef = PlotMap(varargin)
            mapFigRef = figure('units', 'normalized', 'outerposition', [0.05 0.05 0.95 0.95]);
%             set(gca,'XTick',[]); 
%             set(gca,'YTick',[]);
            imshow(varargin{1,1});
            hold on;
             title(varargin{1,3}, 'FontSize',16);            
             xlabel(['Processing ' varargin{1,3} ' ...'], 'FontSize',16 );
             pix = varargin{1,4};
             beac = varargin{1,5};
           
             for i = 1: length(beac)
                 plot(beac{1,i}.x/pix, beac{1,i}.y/pix, 'LineStyle', 'none',...
                     'Marker', 'o', 'MarkerFaceColor', 'yellow', 'markersize', 10,...
                 'DisplayName', strcat('B', i));
             text(beac{1,i}.x / pix - 30, beac{1,i}.y / pix, ['B' int2str(i)], 'FontSize', 14);
             end
        end
        
        function PlotIMUReadings(parameter, name, yLabel, leg, yLims)
            figure('units','normalized','outerposition',[0.05 0.05 .95 .95]);
            fSize = 14;            
            
            subplot(3,1,1)
            plot(parameter(:,1), parameter(:,2));
            legend(leg{1});
            title(name);
            xlabel('Time, s');
            ylabel(yLabel);
            ylim(yLims);
            set(gca,'fontsize',fSize);
            grid on;
            grid minor;

            subplot(3,1,2)
            plot(parameter(:,1), parameter(:,3));
            legend(leg{2});
            title(name);
            xlabel('Time, s');
            ylabel(yLabel);
            ylim(yLims);
            set(gca,'fontsize',fSize);
            grid on
            grid minor

            subplot(3,1,3)
            plot(parameter(:,1), parameter(:,4));
            legend(leg{3});
            title(name);
            xlabel('Time, s');
            ylabel(yLabel);
            ylim(yLims);
            set(gca,'fontsize',fSize);
            grid on
            grid minor
            pause;
            close;
        end
        
        function fPlotStationary(ar, threshold)
             figure('units', 'normalized', 'outerposition', [0.05 0.05 .95 .95])
             plot(ar(:,1), ar(:,2), 'r', 'DisplayName', 'a_x');
             hold on;
             plot(ar(:,1), ar(:,3), 'g', 'DisplayName', 'a_y');             
             plot(ar(:,1), ar(:,4), 'b', 'DisplayName', 'a_z' );             
             plot(ar(:,1), ar(:,5), 'LineWidth', 2, 'DisplayName', 'a_abs');             
             plot(ar(:,1), ar(:,6), 'k', 'LineWidth', 2 ,'DisplayName', 'stationary');
             plot(ar(:,1), threshold*ones(size(ar(:,1))), 'c', 'LineWidth', 1.5 ,'DisplayName', 'threshold');
             title('Stationary Periods' );
             xlabel('Time (s)' );
             ylabel('a m/s^2' );
             grid on;
             grid minor;
             legend('show');             
             pause;
             close;
        end
        
        
        function fPlotBeaconCirclesRSSI(figRef, userCoords, BeacParam, distToUser, pix, mapImage) 
            figure(figRef);
            hold on;
            
            %% Plot Selected Beacons
            XY = zeros(length(BeacParam),2);
            
            for i = 1:length(BeacParam)
                XY(i,1) = BeacParam{1,i}.x;
                XY(i,2) = BeacParam{1,i}.y;
            end
            
            selBeac = plot(XY(:,1)/pix, XY(:,2)/pix,...
                           'LineStyle', 'none', 'Marker', 'o', ...
                           'MarkerFaceColor', 'r', 'markersize', 30);
           
            %% Plot RSSI of Selected Beacons 
            captions = zeros(length(BeacParam),1);
            
            for i = 1:length(BeacParam)
                captions(i,1) = BeacParam{1,i}.RSSI;
            end
        
            texts = text(XY(:,1)/pix-40, XY(:,2)/pix, int2str(captions(:,1)), 'FontSize', 14);
                
            circle = zeros(length(distToUser), 1);
            for i = 1:length(distToUser)
                ang=0:0.01:2*pi; 
               	xp=distToUser(i) * cos(ang);
               	yp=distToUser(i) * sin(ang);
               	circle(i) = plot((XY(i,1)+xp)/pix, (XY(i,2)+yp)/pix);
            end
            
            coords = plot(userCoords(1)/pix, userCoords(2)/pix, 'LineStyle', 'none',...
                          'Marker', 'o', 'MarkerFaceColor', 'b', 'markersize', 20);
            xlim([1 length(mapImage(1, :, 1))]);
            ylim([1 length(mapImage(:, 1, 1))]);
            
            pause(0.1);
            
            delete(selBeac);
            delete(circle);
%             delete(coords);
            delete(texts);
        end
        
        
        % varargin{1} = particlesAlived
        % varargin{2} = particlesAllNew
        function fPlotOnMapPF(PFuserCoords, bleCoords, pixelSize, ...
                              particlesPrev, varargin)
                          
            metrics    = plot(PFuserCoords(:,1)/pixelSize, PFuserCoords(:,2)/pixelSize,...
                              'LineStyle', 'none', 'Marker', 'o', ...
                              'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'r',...
                              'MarkerSize', 11, 'DisplayName', 'User position');
            
            beacon     = plot(bleCoords(1)/pixelSize, bleCoords(2)/pixelSize,...
                             'LineStyle', 'none', 'Marker', 'o', ...
                             'MarkerFaceColor', 'c', 'MarkerEdgeColor', 'k', ...
                             'MarkerSize', 8, 'DisplayName', 'BLE position');
            
            switch length(varargin)
                case 1
                    particlesAlived = varargin{1}(:,:);
                    alivP   = plot(particlesAlived(:,1)/pixelSize, particlesAlived(:,2)/pixelSize,...
                                  'LineStyle', 'none', 'Marker', 'o', ...
                                  'MarkerFaceColor', 'g', 'MarkerEdgeColor', 'k', ...
                                  'MarkerSize', 5, 'DisplayName', 'New particles');
                case 2
                    particlesAlived = varargin{1}(:,:);
                    particlesAllNew = varargin{2}(:,:);
                    allNewP = plot(particlesAllNew(:,1)/pixelSize, particlesAllNew(:,2)/pixelSize,...
                                  'LineStyle', 'none', 'Marker', 'o', ...
                                  'MarkerFaceColor', 'b', 'MarkerEdgeColor', 'k', ...
                                  'MarkerSize', 7, 'DisplayName', 'All new particles');
                    alivP   = plot(particlesAlived(:,1)/pixelSize, particlesAlived(:,2)/pixelSize,...
                                  'LineStyle', 'none', 'Marker', 'o', ...
                                  'MarkerFaceColor', 'g', 'MarkerEdgeColor', 'g', ...
                                  'MarkerSize', 4, 'DisplayName', 'Alived particles');
            end
            
            prevP   = plot(particlesPrev(:,1)/pixelSize, particlesPrev(:,2)/pixelSize,...
                          'LineStyle', 'none', 'Marker', 'o', ...
                          'MarkerFaceColor', 'm', 'MarkerEdgeColor', 'k', ...
                          'MarkerSize', 5, 'DisplayName', 'Old particles');
            
%             caption = text(200, 1800, ['t = ',num2str(round(time, 2)), 'c'], ...
%                            'Color', 'red', 'FontSize', 10);
            axis image
            
            switch length(varargin)
                case 1
                    legend([alivP prevP metrics beacon], 'Location', 'SouthWest');
                case 2
                    legend([allNewP alivP prevP metrics beacon], 'Location', 'SouthWest');
                otherwise
                    legend([prevP metrics beacon], 'Location', 'SouthWest');
            end
            
            pause();
            
            switch length(varargin)
                case 1
                    delete(alivP);
                case 2
                    delete(alivP);
                    delete(allNewP);
            end
            
            delete(prevP);
            delete(beacon);
            delete(metrics);
%             delete(caption);
        end
        
        
        function fPlotSimpleApproach (mapFigRef, coord, coord_prev)
            figure(mapFigRef);
            line([coord_prev(1) coord(1)], [coord_prev(2) coord(2)], ...
                'LineStyle', '-', 'Marker', 'o', 'MarkerFaceColor', 'red',...
                'MarkerSize', 5, 'DisplayName', 'Current coordinates');
            pause(0.01);
        end
        
        
        function fPlotStepDetector2(steps, accelGlob, isStationary)
            figure('units', 'normalized', 'outerposition', [0.05 0.05 .95 .95])
            title('Detailed Figure of Step Detector Operation');
            grid on, grid minor
            set(gca,'fontsize',12);
            hold on;
            
            % Count number of steps
            stepsCounter = 0;
            for i = 1:length(steps)
               if  steps(i,2) == 1
                   stepsCounter = stepsCounter + 1;
               end
            end
            
            plot(steps(:,1), steps(:,3), 'g', 'LineWidth', 2, 'DisplayName', 'abs(accel)');
            plot(steps(:,1), steps(:,2), 'r', 'LineWidth', 3, 'DisplayName', 'flags');
            plot(accelGlob(:,1), accelGlob(:,2), 'c', 'DisplayName', 'a_x(t)');
            plot(accelGlob(:,1), accelGlob(:,3), 'm', 'DisplayName', 'a_y(t)');
            plot(accelGlob(:,1), accelGlob(:,4), 'b', 'DisplayName', 'a_z(t)');
            plot(isStationary(:,1), .5*isStationary(:,2) + 1.3, 'k',...
                            'LineWidth', 3, 'DisplayName', 'isStationary');
            xlim([-5.5 steps(end,1)+3]);
            
            leftShift = -5;
            text(leftShift, 2,   'STEP START',       'Color','r');
            text(leftShift, 1,   'STEP END START',   'Color','r');
            text(leftShift, .5,  'STEP IN',          'Color','r');
            text(leftShift, 0,   'STEP RESET',       'Color','r');
            text(leftShift, -.5, 'STEP RESET START', 'Color','r');
            text(leftShift, -1,  'STEP NONE',        'Color','r');
            
            text(steps(length(steps), 1), 1.3, 'MOVING',   'Color', 'k');
            text(steps(length(steps), 1), 1.8, 'STANDING', 'Color', 'k');
            
            xLim = get(gca, 'xlim');            
            yLim = get(gca, 'ylim');            
            text((xLim(1)+xLim(2))/2.5, yLim(1)+0.2, ['STEPS = ', int2str(stepsCounter)],...
                 'Color', 'b', 'FontSize', 16);
            
            xlabel('Time, sec.');
            ylabel('Acceleration, grav.units');
                        
            legend('Location', 'NorthEastOutside');
%             pause, close;
        end
        
    end
end
