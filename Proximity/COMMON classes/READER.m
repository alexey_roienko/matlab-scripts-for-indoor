classdef READER < handle        
    methods (Access = public, Static)
        
        function [INI, mapParams, beaconsParams, beaconsIdentifiers, mapImage, mapMask] = ...
                                          fReadAllSettingsBLE(fileSettings)

            INI = READER.fReadINI(fileSettings);
            
            mapParams = READER.fReadJson([pwd, INI.pathes.pathMapSettings], 'map');
            
            beaconsParams = READER.fReadJson([pwd, INI.pathes.pathMapSettings], 'beaconsParams');
            
            if strcmpi(INI.general.deviceOS, 'Android') && strcmpi(INI.general.logsSource, 'BLELogger')
                beaconsIdentifiers = READER.fReadJson([pwd, INI.pathes.pathMapSettings], 'MAC');
            
            elseif strcmpi(INI.general.deviceOS, 'Android') && strcmpi(INI.general.logsSource, 'UserAppLogger')
                beaconsIdentifiers = READER.fReadJson([pwd, INI.pathes.pathMapSettings], 'MajorMinor');
            
            elseif strcmpi(INI.general.deviceOS, 'iOS')
                beaconsIdentifiers = READER.fReadJson([pwd, INI.pathes.pathMapSettings], 'MajorMinor');
            
            else
                error('Wrong OS');
            end
            
            mapImage = READER.fReadImage(INI.pathes.pathMapImage);
            
            mapMask = READER.fReadImage(INI.pathes.pathMaskImage);
            INI.BLE.beaconsN = length(beaconsIdentifiers);
        end          
        
        
        function [INI, mapParams, mapImage, mapMask] = fReadAllSettingsSensor(fileSettings)
            %% Read INI-file 
            INI = READER.fReadINI(fileSettings);
            %% Read map parameters from map JSON-file
            mapParams = READER.fReadJson([pwd, INI.pathes.pathMapSettings], 'map');
            %% Read MAP image
            mapImage = READER.fReadImage(INI.pathes.pathMapImage);
            %% Read MASK image
            mapMask = READER.fReadImage(INI.pathes.pathMaskImage);
        end
        

        %% Method rewrites meshes parameters in INI entity. There are two files
        %  for each mesh: 'mesh0x.in' and 'masktable0x.out'. The former
        %  file contains info about mesh steps, number of columns and rows
        %  in mesh, and the coordinates of initial mesh point.
        %  Method looks for mesh files and read parameters from them.
        %  Afterwards, it corrects the proper mesh values in INI entity.        
        %% INPUT:
        % ini         - INI entity, read from 'config.ini'; passed by reference;
        % varargin{1} - path to the mesh files; if it is absent, by default
        %               '\map'-folder is used
        function fCorrectMeshData(ini, varargin)
            
            if nargin == 1
                path = [pwd, '\map'];
            else
                path = [pwd, varargin(1)];
            end
            
            dirContent = dir(path);
            numberOfFiles = length(dirContent);
            j = 1;
            
            %maskTablesAr = strings(1,2);
            for i = 3:numberOfFiles
                str = dirContent(i).name;
                if (length(str)>8) && (strcmpi(str(1:9), 'masktable'))
                    maskTablesAr(j,:) = str;
                    j = j + 1;
                end
            end
            
            %% SmallerMesh
            fID = fopen([path '/mesh', maskTablesAr(1,10:11), '.in' ]);
            line = fgetl(fID);
            delimiter = ' ';
            dataForINIString           = strsplit(line,delimiter);
            ini.general.mesh2StepX     = str2double(dataForINIString(3));
            ini.general.mesh2StepY     = str2double(dataForINIString(4));
            ini.general.mesh2Rows      = str2double(dataForINIString(2));
            ini.general.pathMasktable2 = ['/map/', maskTablesAr(1,:)];
            fclose(fID);
            
            %% BiggerMesh                            
            fID = fopen([path '/mesh', maskTablesAr(2,10:11), '.in' ]);
            line = fgetl(fID);
            delimiter = ' ';
            dataForINIString           = strsplit(line,delimiter);
            ini.general.mesh1StepX     = str2double(dataForINIString(3));
            ini.general.mesh1StepY     = str2double(dataForINIString(4));
            ini.general.mesh1Rows      = str2double(dataForINIString(2));
            ini.general.pathMasktable1 = ['/map/' maskTablesAr(2,:)];
            fclose(fID);
        end
        
        
        function [output, varargout] = read(file, ini, varargin)
            [~, name, ext] = fileparts(file);
            
            if contains(name , 'accelerometer') && strcmpi('.json' , ext)
                if nargout == 2
                    [output, varargout{1}] = READER.fReadAccelJSON(file, ini.general.deviceOS);
                else
                    output = READER.fReadAccelJSON(file, ini.general.deviceOS);
                end
            
            elseif contains(name , 'input_accel') && strcmpi('.txt' , ext)
                if nargout == 2
                    [output, varargout{1}] = READER.fReadAccelTXT(file, ini.general.deviceOS);
                else
                    output = READER.fReadAccelTXT(file, ini.general.deviceOS);
                end
                
            elseif contains(name , 'magnetic_field') && strcmpi('.json' , ext)
                if nargout == 2
                    [output, varargout{1}] = READER.fReadCustomJSON(file);
                else
                    output = READER.fReadCustomJSON(file);
                end
                
            elseif contains(name , 'gyroscope') && strcmpi('.json' , ext)
                if nargout == 2
                    [output, varargout{1}] = READER.fReadCustomJSON(file);
                else
                    output = READER.fReadCustomJSON(file);
                end
                                        
            elseif contains(name , 'angles') && strcmpi('.json' , ext)
                if nargout == 2
                    [output, varargout{1}] = READER.fReadAnglesJSON(file, ini.general.deviceOS);
                else
                    output = READER.fReadAnglesJSON(file, ini.general.deviceOS);
                end
            
            elseif contains(name , 'input_angles') && strcmpi('.txt' , ext)
                if nargout == 2
                    [output, varargout{1}] = READER.fReadAnglesTXT(file, ini.general.deviceOS);
                else
                    output = READER.fReadAnglesTXT(file, ini.general.deviceOS);
                end
            
            elseif contains(name , 'output_triplets_ble') && strcmpi('.txt' , ext)
                if nargin > 2
                    if nargout == 2
                        [output, varargout{1}] = READER.fReadTripletsTXT(file, varargin{1});
                    else
                        output = READER.fReadTripletsTXT(file, varargin{1});
                    end
                else
                    error('Error reading selected triplets. Please, add ID array (Major-Minor or Mac');
                end
                
            elseif contains(name, 'output_position_ua') && strcmpi('.txt' , ext)
                if nargout == 2
                    [output, varargout{1}] = READER.fReadPositionUATXT(file);
                else
                    output = READER.fReadPositionUATXT(file);
                end
                
            elseif contains(name, 'output_position_sensor_cpp') && strcmpi('.txt' , ext)
                if nargout == 2
                    [output, varargout{1}] = READER.fReadPositionCPPTXT(file);
                else
                    output = READER.fReadPositionCPPTXT(file);
                end
            
            elseif contains(name, 'output_pf_delta_z_cpp') && strcmpi('.txt' , ext)
                if nargout == 2
                    [output, varargout{1}] = READER.fReadTestLogsCPPTXT(file);
                else
                    output = READER.fReadTestLogsCPPTXT(file);
                end
                
            elseif contains(name, 'output_position_ble') && strcmpi('.txt' , ext)
                if nargout == 2
                    [output, varargout{1}] = READER.fReadPositionCPPTXT(file);
                else
                    output = READER.fReadPositionCPPTXT(file);
                end
                
            elseif contains(name, 'bluetooth') && strcmpi('.json' , ext)
                output = READER.fReadLogJSON(ini, file, varargin{1});
            
            elseif contains(name, 'input_ble_packets') && strcmpi('.txt' , ext) 
                if nargout == 2
                    [output, varargout{1}] = READER.fReadLogTXT(file, varargin{1});
                else
                    output = READER.fReadLogTXT(file, varargin{1});
                end
                
            elseif contains(name, 'output_position_pf_cpp') && strcmpi('.txt' , ext)
                if nargout == 2
                    [output, varargout{1}] = READER.fReadPositionCPPTXT(file);
                else
                    output = READER.fReadPositionCPPTXT(file);
                end
                
            elseif contains(name, 'output_pf_particles_cpp') && strcmpi('.txt', ext)
                if nargout == 2
                    [output, varargout{1}] = READER.fReadParticlesCPPTXT(file, ini.PF.particlesNo);
                else
                    output = READER.fReadParticlesCPPTXT(file, ini.PF.particlesNo);
                end
                
            else
                error ('Read function cannot read the specified file. Please check your code.');
            end
        end
        
        
        function output = fReadINI(file)
            output = INI('File',file).read();        
        end        
    end
    
    
    %% PRIVATE methods
    methods (Access = private, Static)
        
        function [output, varargout] = fReadAccelJSON(name, os)
            accel = loadjson(name);
            
            output = zeros(length (accel), 4);
            if strcmpi(os, 'Android')
                for i = 1: length(accel)
                    output(i,1) = accel{1,i}.timestamp / 10.^6;                    
                    output(i,2) = accel{1,i}.x / 9.81;
                    output(i,3) = accel{1,i}.y / 9.81;
                    output(i,4) = accel{1,i}.z / 9.81;    
                end  
            elseif strcmpi(os, 'iOS')
                for i = 1: length(accel)
                    output(i,1) = accel{1,i}.timestamp / 10.^6;
                    output(i,2) = accel{1,i}.x * (-1);
                    output(i,3) = accel{1,i}.y * (-1);
                    output(i,4) = accel{1,i}.z * (-1);    
                end  
            else
                error('Wrong OS. Check settings');
            end
            
            if nargout == 2
                varargout{1} = accel{1,1}.timestamp;
            end
        end
        
        
        function [output, varargout] = fReadCustomJSON(name)
            
            data = loadjson(name);            
            output = zeros(length(data), 4);            
            for i = 1:length(data)
                output(i,1) = data{1,i}.timestamp / 10.^6;  % to sec.
                output(i,2) = data{1,i}.x;
                output(i,3) = data{1,i}.y;
                output(i,4) = data{1,i}.z;
            end            
            if nargout == 2
                varargout{1} = data{1,1}.timestamp;
            end
        end
           
        
        function [output, varargout] = fReadAccelTXT(name, os)
            delimiter = ' ';
            startRow = 1;
            endRow = inf;            

            formatSpec = '%f%f%f%f%[^\n\r]';

            %% Open the text file.
            fileID = fopen(name,'r');

            %% Read columns of data according to format string.
            dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines', startRow(1)-1, 'ReturnOnError', false);
            for block=2:length(startRow)
                frewind(fileID);
                dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines', startRow(block)-1, 'ReturnOnError', false);
                for col=1:length(dataArray)
                    dataArray{col} = [dataArray{col};dataArrayBlock{col}];
                end
            end
            fclose(fileID);

            %% Allocate imported array to column variable names
            timestamp = dataArray{:, 1};
            x = dataArray{:, 2};
            y = dataArray{:, 3};
            z = dataArray{:, 4};
            
            L = length(timestamp);
            output = zeros(L, 4);
            if strcmpi(os, 'Android')
                output(:,1) = (timestamp - timestamp(1))/10.^3;
                output(:,2) = x;
                output(:,3) = y;
                output(:,4) = z;
            elseif strcmpi(os, 'iOS')
                output(:,1) = (timestamp - timestamp(1))/10.^3;
                output(:,2) = x;
                output(:,3) = y;
                output(:,4) = z;
            else
                error('Wrong OS. Check settings');
            end
            
            if nargout == 2
                varargout{1} = dataArray{1,1}(1,1);
            end
        end
        
        function output = fReadAnglesJSON(name, os)
            angles = loadjson(name);
            output = zeros(length (angles), 4);
            if strcmp(os, 'Android')
                for i = 1: length(angles)
                    output(i,1) = angles{1,i}.timestamp / 10.^6;
                    output(i,2) = angles{1,i}.pitch;
                    output(i,3) = angles{1,i}.roll;
                    output(i,4) = angles{1,i}.azimuth;    
                end     
            elseif strcmp(os, 'iOS')
                for i = 1: length(angles)
                    output(i,1) = angles{1,i}.timestamp / 10.^6;
                    output(i,2) = angles{1,i}.pitch;
                    output(i,3) = angles{1,i}.roll;
                    output(i,4) = angles{1,i}.yaw;    
                end     
            else
                error('Wrong OS. Check settings');
            end
        end
        
        
        function [output, varargout] = fReadAnglesTXT(filename, os)
            delimiter = ' ';
            startRow = 1;
            endRow = inf;            

            formatSpec = '%f%f%f%f%[^\n\r]';

            %% Open the text file.
            fileID = fopen(filename,'r');

            %% Read columns of data according to format string.
            dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines', startRow(1)-1, 'ReturnOnError', false);
            for block=2:length(startRow)
                frewind(fileID);
                dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines', startRow(block)-1, 'ReturnOnError', false);
                for col=1:length(dataArray)
                    dataArray{col} = [dataArray{col};dataArrayBlock{col}];
                end
            end
            fclose(fileID);
            %% Allocate imported array to column variable names
            timestamp = dataArray{:, 1};
            yaw       = dataArray{:, 2};
            pitch     = dataArray{:, 3};
            roll      = dataArray{:, 4};
            
            L = length(timestamp);
            output = zeros(L, 4);
            if strcmpi(os, 'Android')
                output(:,1) = (timestamp - timestamp(1))/10.^3;
                output(:,2) = pitch;
                output(:,3) = roll;
                output(:,4) = yaw;
            elseif strcmpi(os, 'iOS')
                output(:,1) = (timestamp - timestamp(1))/10.^3;
                output(:,2) = pitch;
                output(:,3) = roll;
                output(:,4) = yaw;
            else
                error('Wrong OS. Check settings');
            end
            
            if nargout == 2
                varargout{1} = dataArray{1,1}(1,1);
            end
        end
        
        
        function [output, varargout] = fReadTripletsTXT(filename, identifiers)
            delimiter = ' ';
            startRow = 1;
            endRow = inf;            

            formatSpec = '%13f%37s%6s%s%10f%[^\n\r]';

            %% Open the text file.
            fileID = fopen(filename,'r');

            %% Read columns of data according to format string.
            dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1, ...
                'Delimiter', delimiter, 'EmptyValue' ,NaN,...
                'HeaderLines', startRow(1)-1, 'ReturnOnError', false);
            for block=2:length(startRow)
                frewind(fileID);
                dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1,...
                    'Delimiter', delimiter, 'EmptyValue' ,NaN,...
                    'HeaderLines', startRow(block)-1, 'ReturnOnError', false);
                for col=1:length(dataArray)
                    dataArray{col} = [dataArray{col}; dataArrayBlock{col}];
                end
            end
            fclose(fileID);
            %% Allocate imported array to column variable names
            timestamp = dataArray{:, 1};
            major     = dataArray{:, 3};
            minor     = dataArray{:, 4};
            RSSI_val  = dataArray{:, 5};
            
            col = 2;
            row = 1;
            output = zeros(length(timestamp)/3, 7);
            
            for i = 1:3:length(timestamp)
                for j = 0:2
                    index = 1;
                    while ~strcmpi(strcat(major(i+j), minor(i+j)), identifiers(index))
                        index = index + 1;
                    end
                    output(row, col) = index;
                    output(row, col+3) = RSSI_val(i+j);
                    col = col + 1;
                end                
                output(row, 1) = (timestamp(i) - timestamp(1)) / 10^3;
                col = 2;
                row = row + 1;
            end
            
            if nargout == 2
                varargout{1} = dataArray{1,1}(1,1);
            end
        end
        
        function [output, varargout] = fReadTestLogsCPPTXT(filename)
            startRow = 1;
            endRow = inf;

            formatSpec = '%13f%f%f%f%f%[^\n\r]';

            fileID = fopen(filename,'r');

            dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1,...
                'Delimiter', '', 'WhiteSpace', '', 'HeaderLines', startRow(1)-1,...
                'ReturnOnError', false, 'EndOfLine', '\r\n');
            
            for block=2:length(startRow)
                frewind(fileID);
                dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1,...
                    'Delimiter', '', 'WhiteSpace', '', 'HeaderLines', startRow(block)-1,...
                    'ReturnOnError', false, 'EndOfLine', '\r\n');
                for col=1:length(dataArray)
                    dataArray{col} = [dataArray{col};dataArrayBlock{col}];
                end
            end

            fclose(fileID);
            output(:,1) = (dataArray{:,1} - dataArray{1,1}(1))  / 10^3;
            output(:,2) =  dataArray{:,2};
            output(:,3) =  dataArray{:,3};
            output(:,4) =  dataArray{:,4};
            output(:,5) =  dataArray{:,5};
            
            if nargout == 2
                varargout{1} = dataArray{1,1}(1);
            end
        end
        
        function [output, varargout] = fReadPositionUATXT(filename)
            startRow = 1;
            endRow = inf;

            formatSpec = '%13f%f%f%[^\n\r]';

            fileID = fopen(filename,'r');

            dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1,...
                'Delimiter', '', 'WhiteSpace', '', 'HeaderLines', startRow(1)-1,...
                'ReturnOnError', false, 'EndOfLine', '\r\n');
            
            for block=2:length(startRow)
                frewind(fileID);
                dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1,...
                    'Delimiter', '', 'WhiteSpace', '', 'HeaderLines', startRow(block)-1,...
                    'ReturnOnError', false, 'EndOfLine', '\r\n');
                for col=1:length(dataArray)
                    dataArray{col} = [dataArray{col};dataArrayBlock{col}];
                end
            end

            fclose(fileID);
            output(:,1) = (dataArray{:,1} - dataArray{1,1}(1))  / 10^3;
            output(:,2) =  dataArray{:,2};
            output(:,3) =  dataArray{:,3};
            
            if nargout == 2
                varargout{1} = dataArray{1,1}(1);
            end
        end
        
        
        function [output, varargout] = fReadPositionCPPTXT(filename)
            startRow = 1;
            endRow = inf;

            formatSpec = '%13f%f%f%f%[^\n\r]';

            fileID = fopen(filename,'r');

            dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1,...
                'Delimiter', '', 'WhiteSpace', '', 'HeaderLines', startRow(1)-1,...
                'ReturnOnError', false, 'EndOfLine', '\r\n');
            
            for block=2:length(startRow)
                frewind(fileID);
                dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1,...
                    'Delimiter', '', 'WhiteSpace', '', 'HeaderLines', startRow(block)-1,...
                    'ReturnOnError', false, 'EndOfLine', '\r\n');
                for col=1:length(dataArray)
                    dataArray{col} = [dataArray{col};dataArrayBlock{col}];
                end
            end

            fclose(fileID);
            output(:,1) = (dataArray{:,1} - dataArray{1,1}(1))/ 10^3;
            output(:,2) =  dataArray{:,2};
            output(:,3) =  dataArray{:,3};
            
            if nargout == 2
                varargout{1} = dataArray{1,1}(1);
            end
        end
        
        
        function [particlesCpp, varargout] = fReadParticlesCPPTXT(filename, particlesNo)
            delimiter = ' ';
            startRow = 1;
            endRow = inf;

            formatSpec = '%f%f%[^\n\r]';

            fileID = fopen(filename,'r');

            dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1, ...
                'Delimiter', delimiter, 'MultipleDelimsAsOne', true, ...
                'EmptyValue', NaN, 'HeaderLines', startRow(1)-1, 'ReturnOnError', false);
            for block=2:length(startRow)
                frewind(fileID);
                dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1,...
                    'Delimiter', delimiter, 'MultipleDelimsAsOne', true, ...
                    'EmptyValue', NaN, 'HeaderLines', startRow(block)-1, 'ReturnOnError', false);
                for col=1:length(dataArray)
                    dataArray{col} = [dataArray{col}; dataArrayBlock{col}];
                end
            end

            fclose(fileID);
            
            dataArray = cellfun(@(x) num2cell(x), dataArray, 'UniformOutput', false);
            temp = [dataArray{1:end-1}];
            
            particlesCpp = struct([]);
            timestampIndex = 1;
            cellIndex = 1;
            while cellIndex <= size(temp,1)
                particlesCpp(timestampIndex).timestamp = temp{cellIndex,1};
                pArray = zeros(particlesNo,2);
                for p=1:particlesNo
                    pArray(p,1) = temp{cellIndex+p, 1};
                    pArray(p,2) = temp{cellIndex+p, 2};
                end
                particlesCpp(timestampIndex).pCoords = pArray;
                timestampIndex = timestampIndex + 1;
                cellIndex = cellIndex + particlesNo + 1;
            end
            
            if nargout == 2
                varargout{1} = dataArray{1,1}(1);
            end
        end
        
        
        function output = fReadLogJSON(INI, dataFromJSON, identifiers)
            beaconsN = length(dataFromJSON);
            indecesN = length(identifiers);
            indeces = zeros(1, indecesN);
            %% Finding beacons order in the log
            if strcmpi(INI.settingsBeacons.deviceOS , 'Android')
                for bN = 1:beaconsN
                    currentMAC = dataFromJSON{bN}{1}.MACaddress;
                    for n=1:indecesN
                        if strcmpi(currentMAC, identifiers{n})
                            indeces(n) = bN;
                            break;
                        end
                    end
                end
            elseif strcmpi(INI.settingsBeacons.deviceOS , 'iOS' )
                for bN = 1:beaconsN
                    currentMajorMinor = strcat(int2str(dataFromJSON{bN}{1}.major) , int2str(dataFromJSON{bN}{1}.minor) );
                    for n=1:indecesN
                        if strcmpi(currentMajorMinor, identifiers{n})
                            indeces(n) = bN;
                            break;
                        end
                    end
                end
            else
                error('Wrong OS');
            end
            %% Reading RSSI and timestamp values
            rssi = cell(1, indecesN);
            ts   = cell(1, indecesN);
            for bN = 1:indecesN
                if indeces(bN)~=0
                    bNpacks  = length(dataFromJSON{indeces(bN)});
                    rssi{bN} = zeros(bNpacks, 1);
                    ts{bN}   = zeros(bNpacks, 1);
                    for pN = 1:bNpacks
                        if strcmpi(INI.settingsBeacons.deviceOS , 'Android')
                            rssi{bN}(pN) = dataFromJSON{indeces(bN)}{pN}.RSSI;         
                            ts{bN}(pN)   = dataFromJSON{indeces(bN)}{pN}.TimeStamp;   
                        elseif strcmpi(INI.settingsBeacons.deviceOS , 'iOS')
                            rssi{bN}(pN) = dataFromJSON{indeces(bN)}{pN}.rssi;         
                            ts{bN}(pN)   = dataFromJSON{indeces(bN)}{pN}.timestamp;                               
                        end
                    end
                end
            end
            %%%%%%%%%%% Diagnostics methods
             if INI.debug.debugMissingBeaconWarning
                for bN=1:INI.settingsBeacons.beaconsN
                    if indeces(bN)==0
                        disp(['Warning! Beacon #' num2str(bN) ' is absent in the log!']);
                    end
                end
             end  
            %%%%%%%%%%% Converting to an appropriate format         
            j = 1;
            k = 1;
            arrUniqueTimes = [];
            %% Iterate to find the unique timestamps from all beacons
            for i=1:length(ts)              % Iterate through beacons
                for j=1:length(ts{1,i})      % Iterate through beacon readings
                    if strcmpi(INI.settingsBeacons.deviceOS, 'Android')
                        value = (ceil(ts{1,i}(j,1)/10))/100;  % get time in seconds
                    else
                        value = ceil(ts{1,i}(j,1) * 10) / 10; % get time in seconds
                    end
                    if ~any(abs(value - arrUniqueTimes) < 0.03)     % If time is not in the array
                        arrUniqueTimes(end+1,1) = value;            % add it to an array
                    end
                    j=j+1;
                end
                k=1;        
            end
            arrUniqueTimes = sort(arrUniqueTimes);                  % Sort times
            clear value;
            %% Iterate to fill in the table
            output = zeros(length(arrUniqueTimes), length(ts) + 1);
            output(:,1) = arrUniqueTimes;
            iter = 1;
            
            for i=1 : length(ts)              % Iterate through beacons
                for j=1: length(ts{1,i})      % Iterate through beacon readings
                    if strcmp(INI.settingsBeacons.deviceOS , 'Android')
                        value.time = ceil(ts{1,i}(j,1)/10)/100;  % get time in seconds
                    else
                        value.time = ceil(ts{1,i}(j,1) * 10) / 10; % get time in seconds
                    end
                    value.rssi = rssi{1,i}(j,1);               % get RSSI
                    value.beacNO = i;                          % get Beacon number
                    
                    for k = iter : length (arrUniqueTimes)     % iterate through the output array
                        if abs(value.time - arrUniqueTimes(k)) < 0.03 % If time of registration matches the time in output array
                            output(k, value.beacNO + 1) = value.rssi; % Fill in the RSSI in the corresponding cell (cell corresponds to beacon number)
                            iter = k;   % As time is sorted, so we can be sure that the next timestamp takes the position after current
                            break;
                        end
                    end
                end
                iter = 1;   % Reset the iteration number when switching to a new beacon data
            end 
        end
        
        
        function [output, varargout] = fReadLogTXT(filename, identifiers)
            delimiter = ' ';
            startRow = 1;
            endRow = inf;
            
            formatSpec = '%f%s%f%f%f%[^\n\r]';

            fileID = fopen(filename,'r');
            dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1,...
                'Delimiter', delimiter, 'HeaderLines', startRow(1)-1,...
                'ReturnOnError', false, 'EndOfLine', '\r\n');
            
            for block=2:length(startRow)
                frewind(fileID);
                dataArrayBlock = textscan(fileID, formatSpec,...
                    endRow(block)-startRow(block)+1, 'Delimiter', delimiter,...
                    'HeaderLines', startRow(block)-1, 'ReturnOnError', false,...
                    'EndOfLine', '\r\n');
                dataArray{1} = [dataArray{1}; dataArrayBlock{1}];
            end
            fclose(fileID);
            
            MajorMinorRSSI      = zeros(length(dataArray{1,1}), 4);
            MajorMinorRSSI(:,1) = (dataArray{1,1}(:,1) - dataArray{1,1}(1,1))/1000; % Time
            MajorMinorRSSI(:,2) = dataArray{1,3}(:,1); % Major
            MajorMinorRSSI(:,3) = dataArray{1,4}(:,1); % Minor
            MajorMinorRSSI(:,4) = dataArray{1,5}(:,1); % RSSI
            
            % Initialization
            idLength = size(identifiers);
            iterator = 1;
            output(iterator, 1:idLength(2)+1) = 0;            
            
            for read = 1:size(MajorMinorRSSI,1)
                if (MajorMinorRSSI(read,1) > output(iterator,1)) && ...
                   (MajorMinorRSSI(read,1) - output(iterator,1) > 0.1)
               
                    iterator = iterator + 1;                    
                    output(iterator, 1) = MajorMinorRSSI(read,1);
                    % Fill the output array row with zeros
                    output(iterator, 2:idLength(2)+1) = 0;
                end
                
                id = strcat(num2str(MajorMinorRSSI(read,2)) , num2str(MajorMinorRSSI(read,3)) );                
                
                for bN = 1:idLength(2)
                    if strcmpi(id, identifiers(1,bN))
                        output(iterator, bN + 1) = MajorMinorRSSI(read, 4);
                        break;
                    end
                end
            end
            
            if nargout == 2
                varargout{1} = dataArray{1,1}(1,1);
            end
        end
               
        function im = fReadImage(imName)
            if (~isnan([pwd , imName]))
                im  = imread([pwd , imName]);
            end
        end
        
        
        function output = fReadJson(path, type)
            persistent runN

            f_content = dir(path);
            f_index   = 3;

            % Looking for JSON file in the folder
            for f=f_index:length(f_content)
                extension = f_content(f).name;
                if strcmp(extension(end-4:end), '.json')
                    if isempty(runN)
                        disp(['Processing ' f_content(f).name ' ...']);
                        runN = 0;
                    end
                    data = loadjson([path f_content(f).name]);
                    beacons_data = data{1}.beacons;
                    beaconsN = length(beacons_data);
                    break;
                elseif f == length(f_content)
                    error(['There is no map JSON-file in a folder ' [pwd path]]);
                end
            end

            % Choose the necessary information
            switch type
                case 'beaconsN'
                    output = beaconsN;

                case 'MAC'
                    beacOrderedMACs = cell(1, beaconsN);
                    for bN=1:beaconsN
                        beacOrderedMACs{bN} = beacons_data{bN}.macAddress;
                    end
                    output = beacOrderedMACs;
                case 'MajorMinor'
                    MajorMinors = cell(1,beaconsN);
                    for bN=1:beaconsN
                        MajorMinors{bN} = strcat(int2str(beacons_data{bN}.major), int2str(beacons_data{bN}.minor));
                    end
                    output = MajorMinors;

                case 'beaconsParams'
                    coords = cell(1, beaconsN);
                    for bN=1:beaconsN
                        coords{bN}.x = beacons_data{bN}.x;
                        coords{bN}.y = beacons_data{bN}.y;
                        coords{bN}.z = beacons_data{bN}.z;
                        coords{bN}.damp    = beacons_data{bN}.damp;
                        coords{bN}.txpower = beacons_data{bN}.txpower;
                    end
                    output = coords;

                case 'map'        
                    mapParams.title     = data{1}.description;
                    mapParams.width     = data{1}.width;
                    mapParams.height    = data{1}.height;
                    mapParams.pixelSize = data{1}.pixelSize;
                    output = mapParams;
            end
        end
    end
end