%% Script for testing Corrections

%% Test 1: Function calcEdgeAngle(edge);
clc, close, clear;
addpath('NewClasses');
% input
x = 100;
y = 200;
r = 50;

th = 0:pi/60:2*pi;
xunit = r * cos(th) + x;
yunit = r * sin(th) + y;
h = plot(xunit, yunit);
xlim([x-r-30 x+r+30]);
ylim([y-r-30 y+r+30]);
hold on;


% test
th = 0:pi/6:2*pi;
dx = r * cos(th);
dx(4) = 0;
dx(10) = 0;
dy = r * sin(th);
dy(1) = 0;
dy(13) = 0;
dist = sqrt(dx.^2+dy.^2);

lDist = length(dist);
edges = cell(1, lDist);
oG = GraphTraj_m();

for i = 1 : lDist
    edge.dx = dx(i);
    edge.dy = dy(i);
    edge.dist = dist(i);
    edge.angle = oG.calcEdgeAngle(edge);
    plot([x, x + edge.dx], [y, y + edge.dy], 'Color', 'r');
    text(x + edge.dx, y + edge.dy, num2str(edge.angle), 'Color', 'r', 'FontSize', 14);
    edges{1,i} = edge;
end
hold off;
pause();


%% Test 2: Function: FindCorrectEdges
% input
x = 100;
y = 200;
r = 50;
beacNO = 10;


% test
th = 0:pi/beacNO:2*pi;
dx = r * cos(th) + 10 * randn(1, length(th));
dy = r * sin(th) + 10 * randn(1, length(th));
x_ar = x + dx;
y_ar = y + dy;
dist = sqrt(dx.^2+dy.^2);

lDist = length(dist);
edges = cell(1, lDist);
oG = GraphTraj_m();
scatter(x, y, 'MarkerEdgeColor','r',...
              'MarkerFaceColor','r',...
              'LineWidth',5)
hold on;

for i = 1 : lDist
    edge.x = x_ar(i);
    edge.y = y_ar(i);
    edge.dx = dx(i);
    edge.dy = dy(i);
    edge.dist = dist(i);
    edge.angle = oG.calcEdgeAngle(edge);
    plot([x, x + edge.dx], [y, y + edge.dy],...
        'Marker', 'o',...
        'MarkerSize', 10,...
        'Color', 'b',...
        'MarkerFaceColor','b',...
        'LineWidth',1.5);
    edges{1,i} = edge;
end
pause(3);
trueEdges = oG.FindCorrectEdges(edges);
for i = 1 : length(trueEdges)
    for j = 1 : lDist
        if trueEdges{1,i}.x == edges{1,j}.x && trueEdges{1,i}.y == edges{1,j}.y
            plot([x, x + trueEdges{1,i}.dx], [y, y + trueEdges{1,i}.dy],...
                'Marker', 'o',...
                'MarkerSize', 7,...
                'Color', 'r',...
                'MarkerFaceColor','r',...
                'LineWidth',0.5);
        end
    end
end

