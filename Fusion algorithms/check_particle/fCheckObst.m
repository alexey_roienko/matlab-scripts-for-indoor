function isWall = fCheckObst(image, xy1, xy2)

%% Init
x1 = xy1(1);  y1 = xy1(2);
x2 = xy2(1);  y2 = xy2(2);

if x1==x2
    b1 = 1;
else
    b1 = x2 - x1;
end

if y1 == y2
    b2 = 1;
else
    b2 = y2 - y1;
end

ln_condn1 = 0.55/abs(b2);
ln_condn2 = 0.55/abs(b1);
isWall   = false; % obstacle presence

if x1 <= x2 % define incremental or decremental cycle of x
    xarray = x1:1:x2;
else
    xarray = x1:-1:x2;
end

if y1 <= y2 % define incremental or decremental cycle of y
    yarray = y1:1:y2;
else
    yarray = y1:-1:y2;
end

%% Main part
for x = xarray % main cycle
    for y = yarray
        a1 = x - x1;
        a2 = y - y1;
        diff = abs(a1/b1 - a2/b2); % line equation
        
        if x1 == x2 || y1 == y2
            diff = 0;
        end
        
        % if the point lies near the line
        if diff < ln_condn1 || diff < ln_condn2 || diff == 0
            if image(y, x) == 0 % if the pixel is black
                isWall = true;
                return;
            end
        end
    end
end



