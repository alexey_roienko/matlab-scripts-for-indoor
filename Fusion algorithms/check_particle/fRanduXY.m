function rnd = fRanduXY(rad, num, std)

%% Init
rnd  = zeros(num, 2);
i = 0;

%% Body
while i < num
    temp = [random('normal', 0, std) random('normal', 0, std)];
    temp = temp*rad;
    dist = sqrt(temp(1)^2 + temp(2)^2);
    if dist <= rad
        i = i+1;
        rnd(i, :) = temp;
    end
end