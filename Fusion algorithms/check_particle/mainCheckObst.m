clear, clc;
close all

%% Init
x   = 1100;
y   = 600;
num = 100;    % number of points
k   = 0;

cloud = zeros(num, 2);
mask  = imread('input\map mask.bmp');
rad   = length(mask(1, :, 1))/11;     % radius of circle (meter length in pixels in this case)
std   = 0.4; % standard deviation

%% Body
imshow(mask);
hold on;
% array of x- and y-increments added to the current point coordiantes (x, y)
rnd = fRanduXY(rad, num, std); 
for i = 1:num
    pnt = round([x y] + rnd(i, :)); % coordinates of the cloud point
    if ~fCheckObst(mask, [x y], pnt) % check if the points are separated by obstacle
        k = k + 1;
        cloud(k, :) = pnt; % write a point into array
    end
end
cloud = cloud(1:k, 1:2);
plot(cloud(:, 1), cloud(:, 2), 'r.', 'MarkerSize', 10);
plot(x, y, '.', 'MarkerSize', 20);