%% Function implements tracking system based on BLE beacons log file
%% INPUT:
% "settings" - structure, contains all necessary info for system operation
% "logFile"  - JSON log-file, recorded by BLELogger IT-Jim application
%% OUTPUT:
% matrix, each row has three numbers [x, y, t]: X-coordinate, Y-coordinate,
% T moment of time (from log-file), when these coordinates were estimated.
%% Authors:
% Alexey Roienko,    a.roienko@it-jim.com
% Corrections:
% Yevhen Chervoniak, eugenecher94@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  23/05/17

function [output, varargout] = fTracker(settings, logFile)

beaconsParams = fReadBeaconsJson('/map_files/', 'beaconsParams'); % used in getMostPowerfulBeacons
beaconsMACs   = fReadBeaconsJson('/map_files/', 'MAC');
mapParams     = fReadBeaconsJson('/map_files/', 'map');
%mapParams.width = 10.5;
maskTable     = load('masktable.out');
maskImage     = imread('mask.png');


%% Data extration from the log file
data = loadjson(logFile);
[raw_rssi, time, indices] = fGetData(data, beaconsMACs); % used in getMostPowerfulBeacons


%% Displaying warning if one or more beacons are absent in the log
if strcmpi(settings.debugInfo, 'yes')
    for bN=1:settings.beaconsN
        if indices(bN)==0
            disp(['Warning! Beacon #' num2str(bN) ' is absent in the log!']);
        end
    end
end


%% Exctract all the timestamps
timestamps = fExtractTS(time, indices);    

% Find unique timestamps
unTimestamps = fFindUniqueTS(timestamps, settings.deltaTS);
first_ts     = unTimestamps(1);

% Packets timestamps corrections
correctTime = fCorrectTS(time, unTimestamps, settings.deltaTS);


%% Finding available packets from all beacons
beac_packets = zeros(length(unTimestamps), settings.beaconsN);
for bN=1:settings.beaconsN
    if indices(bN)~=0
        for pN=1:length(correctTime{bN})
            index = find(unTimestamps == correctTime{bN}(pN));
            beac_packets(index, bN) = 1;
        end
    end
end

%% INITIAL POINT - TRACKING ITS POSITION
% Init array of structures for each beacon buffer
cBuffer = struct([]);
for bN=1:settings.beaconsN
    cBuffer(bN).input  = zeros(settings.WS, 1);
    cBuffer(bN).index  = 1;
    cBuffer(bN).reset  = settings.timeout;
    cBuffer(bN).output = 0;
    cBuffer(bN).WSflag = 0;
end

current_time = first_ts/1000; % used in getMostPowerfulBeacons
counters = ones(1, settings.beaconsN); % used in getMostPowerfulBeacons
ts = 1; % used in getMostPowerfulBeacons
step = settings.start_period; % used in getMostPowerfulBeacons
mode = 0; % used in getMostPowerfulBeacons

% Block of code
getMostPowerfulBeacons;

if tb < 3
    if strcmp(settings.debugInfo, 'yes')
        warning(['timestamp = ' num2str(unTimestamps(ts)) ' has less than 3 beacons packets!']);        
    end
else
    %% If number of beacons found is less than "beacNumbPos", it is
    %  necessary to change the size of the "trilat_beacons" cell array
    if tb ~= settings.beacNumbPos
        temp = cell(1, tb);
        for b=1:tb
            temp{b} = trilat_beacons{b}; % taken from getMostPowerfulBeacons
        end
        trilat_beacons = temp;
        clear temp;
    end
    dist_array = fRssi2Dist(trilat_beacons);

    %% !!! POSITIONING algorithm point !!! %%
    if strcmpi(settings.pos_method, 'trilat')
        %% Trilateration 1 - over three beacons with the largest RSSIs
        userCoords = fTrilateration(trilat_beacons, dist_array, settings.userHeight);
    elseif strcmpi(settings.pos_method, 'trilat_T2')
        if tb == 3
            userCoords = fTrilateration(trilat_beacons, dist_array, settings.userHeight);
        else
            userCoords = fTrilaterationT2(trilat_beacons, dist_array, settings.combSize, settings.userHeight);
        end
    elseif strcmpi(settings.pos_method, 'multilat')
        userCoords = fMultilateration(trilat_beacons, dist_array, settings.userHeight);
    end

    % Depict estimated point on the map
    if userCoords(1) > mapParams.width-0.05
        userCoords(1) = mapParams.width-0.05;
    elseif userCoords(1) < 0.05
        userCoords(1) = 0.05;
    end
    if userCoords(2) > mapParams.height-0.05
        userCoords(2) = mapParams.height-0.05;
    elseif userCoords(2) < 0.05
        userCoords(2) = 0.05;
    end

    %% Write RAW results, i.e. without any corrections
    if strcmpi(settings.saveDataFile, 'yes')
        rawCoords = [userCoords(1) userCoords(2)];   % rawCoords
    end
    
    %% Correction using mesh and "masktable.out" file
    if strcmpi(settings.meshCorrector, 'yes')
        userCoords = fMeshCorrector(userCoords, settings.meshSteps, mapParams.height, maskTable, settings.debugInfo);
        if strcmpi(settings.saveDataFile, 'yes')
            meshCoords = [userCoords(1) userCoords(2)];   % meshCorrection
        end
    end
    
    %% Correction using mask and maximal distance from previous point
    if strcmpi(settings.maxDistCorrector, 'yes') && strcmp(settings.saveDataFile, 'yes')    
        distCoords = [userCoords(1) userCoords(2)];   % distCorrection
    end
    
    output           = [userCoords(1) userCoords(2) (first_ts/1000+settings.start_period)];    
    prevUserCoordsPx = round(userCoords / mapParams.pixelSize);
end




%% NEXT POINTS - TRACKING THEIR POSITIONS ONCE IN A "time_step"
pointsCounter = 1;
current_time = first_ts/1000 + settings.start_period + 0.001; % used in getMostPowerfulBeacons
step = settings.time_step; % used in getMostPowerfulBeacons
mode = 1; % used in getMostPowerfulBeacons
while (ts <= length(unTimestamps))
    % Block of code
    getMostPowerfulBeacons;
    
    if tb < 3
        if strcmp(settings.debugInfo, 'yes')
            warning(['Time period from ' num2str(current_time) ' sec. to ' ...                
              num2str(current_time+settings.time_step) ' sec. has less than 3 beacons packets!']);
        end
        current_time = current_time + settings.time_step;
        pointsCounter = pointsCounter + 1;
        continue;
    end

    %% If number of beacons found is less than "beacNumbPos", it is
    %  necessary to change the size of the "trilat_beacons" cell array
    if tb ~= settings.beacNumbPos
        cellN = 0;
        for b=1:length(trilat_beacons)
            if ~isempty(trilat_beacons{b})
                cellN = cellN + 1;
            end
        end
        temp = cell(1, cellN);
        for b=1:cellN
            temp{b} = trilat_beacons{b};
        end        
        trilat_beacons = temp;
        clear temp;
    end

    %% POSITIONING
    dist_array = fRssi2Dist(trilat_beacons);
    if strcmpi(settings.pos_method, 'trilat')
        % Trilateration 1 - over three beacons with the largest RSSIs
        userCoords = fTrilateration(trilat_beacons, dist_array, settings.userHeight);
    elseif strcmpi(settings.pos_method, 'trilat_T2')
        if tb == 3
            userCoords = fTrilateration(trilat_beacons, dist_array, settings.userHeight);
        else
            userCoords = fTrilaterationT2(trilat_beacons, dist_array, settings.combSize, settings.userHeight);
        end
    elseif strcmpi(settings.pos_method, 'multilat')
        userCoords = fMultilateration(trilat_beacons, dist_array, settings.userHeight);
    end


    %% Depict estimated point on the map
    if userCoords(1) > mapParams.width-0.05
        userCoords(1) = mapParams.width-0.05;
    elseif userCoords(1) < 0.05
        userCoords(1) = 0.05;
    end
    if userCoords(2) > mapParams.height-0.05
        userCoords(2) = mapParams.height-0.05;
    elseif userCoords(2) < 0.05
        userCoords(2) = 0.05;
    end
    
    %% Write RAW results, i.e. without any corrections
    if strcmpi(settings.saveDataFile, 'yes')
        rawCoords = [rawCoords; userCoords(1) userCoords(2)];   % rawCoords
    end
    
    
    %% Correction using mesh and "masktable.out" file
    if strcmpi(settings.meshCorrector, 'yes')
        userCoords = fMeshCorrector(userCoords, settings.meshSteps, mapParams.height, maskTable, settings.debugInfo);
        if strcmpi(settings.saveDataFile, 'yes')
            meshCoords = [meshCoords; userCoords(1) userCoords(2)];   % meshCorrection
        end
    end

    %% Correction using mask and maximal distance from previous point
    if strcmpi(settings.maxDistCorrector, 'yes')
        % Transforming to pixels from meters
        userCoordsBefore = round(userCoords / mapParams.pixelSize);
        % Doing correction
        maxDistancePx = settings.maxDistance/mapParams.pixelSize;
        userCoordsAfter = fMaxDistCorrector(prevUserCoordsPx, userCoordsBefore, maskImage,...
                                            maxDistancePx , settings.debugInfo);
        % Coming back to meters
        userCoords = userCoordsAfter * mapParams.pixelSize;
        
        if strcmpi(settings.saveDataFile, 'yes')
            distCoords = [distCoords; userCoords(1) userCoords(2)];   % distCorrection
        end
    end
        
    output = [output; userCoords(1) userCoords(2) (current_time+settings.time_step)];
    prevUserCoordsPx = round(userCoords / mapParams.pixelSize);

    current_time = current_time + settings.time_step;    
end


%% Outputing DEBUG results
if strcmpi(settings.saveDataFile, 'yes')
    serviceInfo{1} = rawCoords;
    if strcmpi(settings.meshCorrector, 'yes')
        serviceInfo{2} = meshCoords;
    end
    if strcmpi(settings.maxDistCorrector, 'yes')
        serviceInfo{3} = distCoords;
    end
    pxSize = mapParams.pixelSize;
    
    matFilename = logFile(1:end-5);    
    save(['results\debug_' , matFilename, '.mat'], 'serviceInfo', 'output', 'pxSize');
end

end






