%% Function for extraction of all packets timestamps 

function output = extract_ts(time, indeces)

beaconsN = length(time);
for bN=1:beaconsN
    if indeces(bN)~=0
        bNpacks = length(time{bN});
        for pN=1:bNpacks
            k = time{bN}(pN);
            if k==0
                k=1;
            end
            temp(k) = k;
        end
    end
end

k = 1;
timestamps = zeros(length(find(temp > 0)), 1);
for i=1:length(temp)
    if temp(i) > 0
        timestamps(k) = temp(i);
        k = k+1;
    end
end
%timestamps = timestamps/1000; 

output = timestamps;