%% Implementation of Alpha-Beta filter (another name g-h filter)
% data     - input sample of data to be processed (vector)
% a_value  - value of alpha parameter, possible values are from 0 to 1
% b_value  - value of beta parameter, possible values are from 0 to 1
% Ts       - time interval between two data samples
% rFilt    - output filtered signal, its length is equal to the length of
%    "data", i.e. the function filters the whole signal at once.
% Last review - 13/03/17 by Alexey Roienko, a.roienko@it-jim.com

function rFilt = ab_filter(data, a_value, b_value, Ts)

if nargin ~= 4
    error('Function needs four arguments!')
elseif a_value > 1 || a_value < 0
    error('Wrong value of Alpha parameter!')
elseif b_value > 1 || b_value < 0
    error('Wrong value of Beta parameter!')
else
    %% Init part
    L      = length(data);
    rFilt  = zeros(size(data));    

    %% Main part
    for l=1:L
        [rFilt(l), ~] = alpha_beta_filter(data(l), a_value, b_value, Ts, 'false');
        if l==L
            [~, ~] = alpha_beta_filter(data(l), a_value, b_value, Ts, 'true');
        end
    end    
end


%% Subfunction, run for the current data sample
function [rFiltered, vEst] = alpha_beta_filter(rMeasured, a, b, Ts, cleared)

persistent rPred vPred
if isempty(rPred)
    rPred = rMeasured;
end
if isempty(vPred)
    vPred = 0;
end

if strcmp(cleared, 'true')
    rPred = [];
    vPred = 0;
    rFiltered = 0;
    vEst = 0;
else
    rFiltered = rPred + a*(rMeasured - rPred);
    vEst  = vPred + (b/Ts)*(rFiltered - rPred);

    rPred = rFiltered + vEst*Ts;
    vPred = vEst;
end




