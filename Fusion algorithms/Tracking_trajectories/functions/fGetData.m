%% Function extracts the RSSI values and timestamps from the input data matrix
%  which is had got from downloading a json log-file.

function [rssi, ts, indeces] = get_data(data, beacMAClist)

beaconsN = length(data);
indecesN = length(beacMAClist);
indeces = zeros(1, indecesN);

%% Finding beacons order in the log
for bN = 1:beaconsN
    currentMAC = data{bN}{1}.MACaddress;
    for n=1:indecesN
        if strcmp(currentMAC, beacMAClist{n})
            indeces(n) = bN;
            break;
        end
    end
end

%% Reading RSSI and timestamp values
rssi = cell(1, indecesN);
ts   = cell(1, indecesN);
for bN = 1:indecesN
    if indeces(bN)~=0
        bNpacks  = length(data{indeces(bN)});
        rssi{bN} = zeros(bNpacks, 1);
        ts{bN}   = zeros(bNpacks, 1);
        for pN = 1:bNpacks
            rssi{bN}(pN) = data{indeces(bN)}{pN}.RSSI;         
            ts{bN}(pN)   = data{indeces(bN)}{pN}.TimeStamp;         
        end
    end
end
