% Implementation of Moving Average filter
% data     - input sample of data to be processed (vector)
% win_size - filter window size
% rFilt    - output filtered signal, its length is equal to the length of
%    "data", i.e. the function filters the whole signal at once.
% Last review - 09/03/17 by Alexey Roienko, a.roienko@it-jim.com

function rFilt = ma_filter(data, win_size)

if nargin ~= 2
    error('Function needs two arguments!')
elseif win_size < 0
    error('Wrong value of Window Size!')
else
    %% Init part
    L     = length(data);
    rFilt = zeros(size(data));

    %% Main part
    for l=1:L
        if l==1
            rFilt(l) = data(l);
        elseif l<win_size
            rFilt(l) = mean(data(1:l));
        else
            rFilt(l) = mean(data(l-win_size+1:l));
        end
    end
end