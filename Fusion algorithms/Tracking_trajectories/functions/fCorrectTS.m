%% Function for correction those packet timestamps which are not equal but 
%  very closed to unique timestamp values.
%  Function rewrites the initial timestamp values with the closest unique
%  ones.

function output = correct_ts(initTS, uniqueTS, deltaTS)

beaconsN = length(initTS);
output   = cell(1, beaconsN);
for bN=1:beaconsN
    if ~isempty(initTS{bN})
        bNpacks = length(initTS{bN});
        output{bN} = zeros(bNpacks, 1);
        for pN=1:bNpacks
            currentTS = initTS{bN}(pN);
            output{bN}(pN) = findClosedValue(currentTS, uniqueTS, deltaTS);
        end
    end
end



function answer = findClosedValue(curTS, unTS, deltaTS)

tsN = length(unTS);
for i=1:tsN
    if (curTS > unTS(i)-deltaTS) && (curTS < unTS(i)+deltaTS)
        answer = unTS(i);
        break;
    end
end





