%% Function provides estimation of distances to the selected beacons 
%  using beacon parameters Damp and TXpower

function output = rssi2dist(beacons)

beaconsN = length(beacons);
output   = zeros(1, beaconsN);

for bN = 1:beaconsN
    output(bN) = 10^(0.1*(beacons{bN}.txpower - beacons{bN}.RSSI) / beacons{bN}.damp);
end