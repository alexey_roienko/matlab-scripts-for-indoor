%% Function provides coordinates correction using "masktable.out" file
%  which is based on mesh representation of navigation area.
%% INPUT:
%  rawXY - vector, 1x2, raw values of estimated coordinates, in meters, (X,Y)
%  stepsXY - vector, 1x2, values of mesh steps along two axes, in meters (X, Y)
%  mapHeight - height or width (Y axis direction) of an area covered by navigation system
%  maskTable - vector, 1xN, contains indecies for transformation of mesh
%    vertices according to the map mask
%  debug - if is equal to 'yes', service information will be printed in
%  Command Window
%% OUTPUT:
%  correctedXY - vector, 1x2, corrected values of estimated coordinates, in meters, (X,Y)
%% Author:
%  Yevhen Chervoniak
%  Corrected:
%  Alexey Roienko, a.roienko@it-jim.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  17/05/17

function correctedXY = fMaxDistCorrector(prevPoint, curPoint, maskFile, maxDist, debug)

if strcmp(debug, 'yes')
    fprintf('Before correction: X=%.2f, Y=%.2f\n', prevPoint);
end


%% Init
x1 = prevPoint(1);  y1 = prevPoint(2);
x2 = curPoint(1);   y2 = curPoint(2);

if x1==x2
    xConstFlag = 1;
else
    xConstFlag = 0;
end
if y1==y2
    yConstFlag = 1;
else
    yConstFlag = 0;
end


%% If there is a general case
if (xConstFlag == 0) && (yConstFlag == 0)    
    %% Calculating the line "k" and "b" coefficients
    kCoef = (y2 - y1)/(x2 - x1);
    bCoef = y1 - x1*kCoef;
    if x1 <= x2
        xArray = x1:x2;
    else
        xArray = x1:-1:x2;
    end
    
    %% Main part
    for i=2:length(xArray)
        currX = xArray(i);
        currY = round(kCoef*currX + bCoef);

        % Check the intersection with the black areas
        if maskFile(currY, currX)==0
            % black mask pixel        
            correctedXY(1) = xArray(i-1);
            correctedXY(2) = kCoef*xArray(i-1) + bCoef;
            return;
        else
            % Check for exceeding maximal distance
            currD = ((x1-currX)^2 + (y1-currY)^2)^0.5;
            if currD > maxDist
                correctedXY(1) = xArray(i-1);
                correctedXY(2) = kCoef*xArray(i-1) + bCoef;
                if strcmp(debug, 'yes')
                    fprintf('After correction: X=%.2f, Y=%.2f\n', correctedXY);
                end
                return;
            end
        end    
    end
    
    
%% If Y coordinates are the same for both points
elseif (yConstFlag == 1)
    if x1 <= x2
        xArray = x1:x2;
    else
        xArray = x1:-1:x2;
    end
    
    %% Main part
    for i=2:length(xArray)
        currX = xArray(i);
        currY = y1;

        % Check the intersection with the black areas
        if maskFile(currY, currX)==0
            % black mask pixel        
            correctedXY(1) = xArray(i-1);
            correctedXY(2) = currY;
            return;
        else
            % Check for exceeding maximal distance
            currD = abs(x1 - currX);
            if currD > maxDist
                correctedXY(1) = xArray(i-1);
                correctedXY(2) = currY;
                if strcmp(debug, 'yes')
                    fprintf('After correction: X=%.2f, Y=%.2f\n', correctedXY);
                end
                return;
            end
        end    
    end
    
    
%% If X coordinates are the same for both points
elseif (xConstFlag == 1)
    if y1 <= y2
        yArray = y1:y2;
    else
        yArray = y1:-1:y2;
    end
    
    %% Main part
    for i=2:length(yArray)
        currX = x1;
        currY = yArray(i);

        % Check the intersection with the black areas
        if maskFile(currY, currX)==0
            % black mask pixel        
            correctedXY(1) = currX;
            correctedXY(2) = yArray(i-1);
            return;
        else
            % Check for exceeding maximal distance
            currD = abs(y1 - currY);
            if currD > maxDist
                correctedXY(1) = currX;
                correctedXY(2) = yArray(i-1);
                if strcmp(debug, 'yes')
                    fprintf('After correction: X=%.2f, Y=%.2f\n', correctedXY);
                end
                return;
            end
        end    
    end
end
    
correctedXY = curPoint;

if strcmp(debug, 'yes')
    fprintf('After correction: X=%.2f, Y=%.2f\n', correctedXY);
end

end

