
% Trilateration algorithm T2
% Applied for the number of beacons greater than 3
% Idea: Step 1. Get all possible combinations in the group of 3 from the beacons available
% Step 2. Estimate receiver coordinates using the parameters of each group of beacons
% Step 3. Average receiver coordinate estimates over all obtained values
% from the step 2.
% Authors: Eugene Gorovoy, Alexey Roienko

% usage: estRXCoords = trilateration_T2(beacons, DistMatrix, combSize, userH) 
% beacons = [[x1; y1] [x2; y2] ...] - i.e. beacon coordinates
% DistMatrix = Distance Matrix = [s1 s2 s3 s4 ..] estimated distance matrix 
%   from the receiver to the corresponding beacon
% combSize = (integer) - size of each combination of the beacons, i.e.
%   number of variants to be considered is equal to number of combinations
%   of size "combSize" from "beaconsN"
% userH = (float) - distance from the device to the floor
% estRXCoords = Estimated Receiver Coordinates - vector-column, i.e. [x;y]

% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY!

function estRXCoords = fTrilaterationT2(beacons, DistMatrix, combSize, userH)

%combSize = 3;
distN    = length(DistMatrix);
beaconsN = length(beacons);

if (distN ~= beaconsN)
    error('Number of reference points and distances are different');
else
    %% Step 1
    beacon_combs = nchoosek(1:beaconsN, combSize);
    [Ncomb, ~] = size(beacon_combs);
    
    %% Step 2
    xy_trilComb = zeros(Ncomb,2);     
    temp_beacons = cell(1, combSize);
    for nc=1:Ncomb
        for cS=1:combSize
            temp_beacons{cS} = beacons{beacon_combs(nc,cS)};
        end
        if combSize == 3
            xy_trilComb(nc,:) = ...
                fTrilateration(temp_beacons, DistMatrix(beacon_combs(nc,:)), userH);
        elseif combSize > 3
            temp = ...
                fMultilateration(temp_beacons, DistMatrix(beacon_combs(nc,:)), userH);
            xy_trilComb(nc,:) = temp(1:2);
        else
            error('Combination size should be greater than 2');
        end
    end
        
    %% Step 3
    if Ncomb == 1
        estRXCoords = xy_trilComb;
    else
        estRXCoords = mean(xy_trilComb);
    end
end

