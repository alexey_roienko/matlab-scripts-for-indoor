%% Function provides coordinates correction using "masktable.out" file
%  which is based on mesh representation of navigation area.
%% INPUT:
%  rawXY     - vector, 1x2, raw values of estimated coordinates, in meters, (X,Y)
%  stepsXY   - vector, 1x2, values of mesh steps along two axes, in meters (X, Y)
%  mapHeight - height (Y axis direction) of an area covered by navigation system
%  maskTable - vector, 1xN, contains indecies for transformation of mesh
%    vertices according to the map mask
%  debug     - if is equal to 'yes', service information will be printed in Command Window
%% OUTPUT:
%  correctedXY - vector, 1x2, corrected values of estimated coordinates, in meters, (X,Y)
%% Author:
%  Eugene Chervonyak
%  Corrected:
%  Alexey Roienko, a.roienko@it-jim.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  17/05/17

function correctedXY = fMeshCorrector(rawXY, stepsXY, mapHeight, maskTable, debug)

% Quantize raw X and Y coordinates with respect to the mesh step sizes
if strcmp(debug, 'yes')
    fprintf('Before correction: X=%.2f, Y=%.2f\n', rawXY);
end
qX = round(rawXY(1) / stepsXY(1));
qY = round(rawXY(2) / stepsXY(2));

% Find number of raws in mesh grid
Ny = round(mapHeight / stepsXY(2)) + 1;


% Then index in 1D maskTable is ...
index = qX*Ny + qY;

%% If mesh vertex should not be corrected...
if maskTable(index+1) == index
    correctedXY(1) = qX * stepsXY(1);
    correctedXY(2) = qY * stepsXY(2);    
else
    %% If the correction is necessary ...
    qX = floor(maskTable(index+1) / Ny);
    qY = maskTable(index+1) - qX*Ny;

    correctedXY(1)  = qX * stepsXY(1);
    correctedXY(2)  = qY * stepsXY(2);    
end

if strcmp(debug, 'yes')
    fprintf('After correction: X=%.2f, Y=%.2f\n\n', correctedXY);
end

end


