
% Multilateration algorithm
% paper "An algebraic solution to the multilateration problem"
% Author: Norrdine, Abdelmoumen  (norrdine@hotmail.de)
% https://www.researchgate.net/publication/275027725_An_Algebraic_Solution_to_the_Multilateration_Problem
% usage: [N1 N2] = multiilateration(RP,DM, W) 
% RP = Reference Points Matrix = [[x1; y1; z1] [x2; y2; z2] ...]
% DM = Distance Matrix = [s1 s2 s3 s4 ..] distance matrix.
% W : Weights Matrix (Statistics).
% N : calculated solution
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY!!

function [N1, N2] = fMultilateration(beacons, DistMatrix, userH)

beaconsN = length(beacons);
distN = length(DistMatrix);

if (distN ~= beaconsN)
    error('Number of reference points and distances are different');
end

A = zeros(beaconsN, 4);
b = zeros(beaconsN, 1);
for bN=1:beaconsN
    x = beacons{bN}.x; 
    y = beacons{bN}.y;
    z = 0;%beacons{bN}.z - userH;
    
    A(bN, :) = [1 -2*x  -2*y  -2*z]; 
    b(bN, 1) = DistMatrix(bN)^2-x^2-y^2-z^2;
end

if beaconsN == 3
    %warning('OFF', MsgID);
    Xp = A\b;  % Gaussian elimination
    
    % or Xp=pinv(A)*b; 
    % the matrix  inv(A'*A)*A' or inv(A'*C*A)*A'*C or pinv(A)
    % depends only on the reference points
    % it could be computed only once
    
    xp = Xp(2:4, :);
    Z = null(A,'r');
    z = Z(2:4, :);
    if rank(A)==3
        %Polynom coeff.
        a2 = z(1)^2 + z(2)^2 + z(3)^2 ;
        a1 = 2*(z(1)*xp(1) + z(2)*xp(2) + z(3)*xp(3))-Z(1);
        a0 = xp(1)^2 +  xp(2)^2+  xp(3)^2-Xp(1);
        p = [a2 a1 a0];
        t = roots(p);

        %Solutions
        N1 = Xp + t(1)*Z;
        N2 = Xp + t(2)*Z;
    end
    
elseif beaconsN > 3
    %Particular solution

    % if W~=diag(ones(1,size(W)))
    %      C = W'*W;
    %      Xpdw =inv(A'*C*A)*A'*C*b; % Solution with Weights Matrix
    % else
    
    % Solution without Weights Matrix
    Xpdw = pinv(A)*b; 
    % end
 
    % the matrix  inv(A'*A)*A' or inv(A'*C*A)*A'*C or pinv(A)
    % depends only on the reference points
    % it could be computed only once
    N1 = Xpdw;
    N2 = N1;
end

N1 = real(N1(2:4, 1));
N2 = real(N2(2:4, 1));


