%% Simple trilateration algorithm, version 2
% Applied for signals from three beacons only
%% Authors:
% Alexey Roienko,    a.roienko@it-jim.com
% Yevhen Chervoniak, eugenecher94@gmail.com

% usage: estRXCoords = trilateration(RefPoints, DistMatrix) 
% beacons - cell array of structures for each beacons
% having coordinates as well as beacon parameters.
% DistMatrix = Distance Matrix = [s1 s2 s3] = distance matrix from the receiver
% to the corresponding beacon.
% estRXCoords = Estimated Receiver Coordinates - calculated solution

% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY!

function estRXCoords = fTrilateration(beacons, DistMatrix, userH)

distN = length(DistMatrix);
beaconsN = length(beacons);

if (distN ~= beaconsN)
    error('Number of reference points and distances are different!');
    
elseif (distN ~= 3)||(beaconsN ~= 3)
    error('Number of reference points and/or distances differs from 3!');
else
    RefPoints = zeros(3,beaconsN);
    for bN=1:beaconsN
        RefPoints(1,bN) = beacons{bN}.x;
        RefPoints(2,bN) = beacons{bN}.y;
        RefPoints(3,bN) = beacons{bN}.z - userH;
    end
    
    %% Step 1 - Calculate A, B, C, Xs and Ys coefficients for trilateration formulas
    A = RefPoints(1,1)^2 + RefPoints(2,1)^2 - DistMatrix(1)^2;
    B = RefPoints(1,2)^2 + RefPoints(2,2)^2 - DistMatrix(2)^2;
    C = RefPoints(1,3)^2 + RefPoints(2,3)^2 - DistMatrix(3)^2;

    Xs_array(1) = RefPoints(1,1) - RefPoints(1,3);
    Xs_array(2) = RefPoints(1,2) - RefPoints(1,1);
    Xs_array(3) = RefPoints(1,3) - RefPoints(1,2);

    Ys_array(1) = RefPoints(2,1) - RefPoints(2,3);
    Ys_array(2) = RefPoints(2,2) - RefPoints(2,1);
    Ys_array(3) = RefPoints(2,3) - RefPoints(2,2);
    
    %% Step 2 - Calculate Receiver X ...
    num(1)   = A*Ys_array(3) + B*Ys_array(1) + C*Ys_array(2);
    denom(1) = 2*(RefPoints(1,1)*Ys_array(3) + ...
                  RefPoints(1,2)*Ys_array(1) + ...
                  RefPoints(1,3)*Ys_array(2));
    estRXCoords(1) = num(1)/denom(1);

    %% ... and Y coordinates
    num(2)   = A*Xs_array(3) + B*Xs_array(1) + C*Xs_array(2);
    denom(2) = 2*(RefPoints(2,1)*Xs_array(3) + ...
                  RefPoints(2,2)*Xs_array(1) + ...
                  RefPoints(2,3)*Xs_array(2));
    estRXCoords(2) = num(2)/denom(2);     
end





