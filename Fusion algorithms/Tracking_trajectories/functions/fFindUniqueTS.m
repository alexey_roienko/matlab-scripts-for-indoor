%% Function for extraction of all packets timestamps 

function output = find_unique_ts(initTimestamps, deltaTS)

tsN = length(initTimestamps);
temp = zeros(tsN, 1);

currentUniqueTS = initTimestamps(1);
counter = 1;
temp(counter) = currentUniqueTS;

i = 2;
while i < tsN+1
    if (initTimestamps(i)-currentUniqueTS > deltaTS)
        counter = counter + 1;
        currentUniqueTS = initTimestamps(i); 
        temp(counter) = currentUniqueTS;
    end
    i = i+1;
end

output = temp(1:counter);