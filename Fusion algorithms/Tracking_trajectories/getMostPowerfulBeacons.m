%% Script detects necessary number of the most powerful beacons
% and takes their RSSI's
%% Authors:
% Alexey Roienko,    a.roienko@it-jim.com
% Corrections:
% Yevhen Chervoniak, eugenecher94@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  20/05/17

while (ts<=length(unTimestamps)) && ...
          ((unTimestamps(ts)/1000) < (current_time+step))
    % Extracting beacons' RSSI values for the current timestamp
    if strcmp(settings.debugInfo, 'yes')
        disp(['current timestamp = ' num2str(unTimestamps(ts))]);        
    end
    for bN=1:settings.beaconsN
        if (indices(bN)~=0) && (beac_packets(ts, bN) == 1)
            % Add RSSI to cBuffer
            cBuffer(bN).input(cBuffer(bN).index) = raw_rssi{bN}(counters(bN));
            % Increase counter
            counters(bN) = counters(bN) + 1;
            if mode == 1
                % Recover Reset time
                cBuffer(bN).reset  = settings.timeout;
            end
            % Calculate current output - RSSI FILTERING here!
            if cBuffer(bN).WSflag==0
                % means the flag is not set and cBuffer was not rotated
                cBuffer(bN).output = mean(cBuffer(bN).input(1:cBuffer(bN).index));
            else
                % means the flag is set and cBuffer was rotated
                cBuffer(bN).output = mean(cBuffer(bN).input);
            end
            % Increase cBuffer index                
            cBuffer(bN).index = cBuffer(bN).index + 1;
            % Set flag and rotate cBuffer index if it is greater than WS
            if cBuffer(bN).index > settings.WS
                cBuffer(bN).index = 1;
                cBuffer(bN).WSflag = 1;
            end
            
        elseif (indices(bN)~=0) && (beac_packets(ts, bN) == 0) && ...
                ((cBuffer(bN).index ~= 1)||(cBuffer(bN).WSflag ~= 0))
            % if there is no packet from a beacon, we should take this
            % into account for checking the timeout
            % delta between current and previous timestamps
            deltaT = (unTimestamps(ts)-unTimestamps(ts-1))/1000;
            cBuffer(bN).reset = cBuffer(bN).reset - deltaT;
            % if timeout, then reset the corresponding beacon's cBuffer
            if cBuffer(bN).reset<=0
                cBuffer(bN).input  = zeros(settings.WS, 1);
                cBuffer(bN).index  = 1;
                cBuffer(bN).reset  = settings.timeout;
                cBuffer(bN).output = 0;
                cBuffer(bN).WSflag = 0;
            end
        end
    end
    ts = ts + 1;
end


frame_buffer = zeros(1, settings.beaconsN);
for bN=1:settings.beaconsN
    if (indices(bN)~=0)
        frame_buffer(bN) = cBuffer(bN).output;
    end
end
% Select beacons with the greatest RSSIs
[sort_rssi, order] = sort(frame_buffer, 'descend');
trilat_beacons = cell(1, settings.beacNumbPos);
tb = 1;
str = '';
for b=1:settings.beaconsN
    if sort_rssi(b)~=0
        trilat_beacons{tb} = beaconsParams{order(b)};
        trilat_beacons{tb}.RSSI = sort_rssi(b);
        tb = tb + 1;
        str = [str num2str(order(b)) ' '];
    end
    if tb > settings.beacNumbPos
        break;
    end
end
tb = tb - 1;

if strcmp(settings.debugInfo, 'yes')
    disp(['Selected beacons are ' str]);
end



