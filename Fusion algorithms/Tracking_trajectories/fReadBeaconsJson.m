%% Extraction of beacons data from map json file

function output = fReadBeaconsJson(path, type)

persistent runN

% Initialization
f_content = dir([pwd path]);
f_index   = 3;


% Looking for JSON file in the folder
for f=f_index:length(f_content)
    extension = f_content(f).name;
    if strcmp(extension(end-4:end), '.json')
        if isempty(runN)
            disp(['Processing ' f_content(f).name ' ...']);
            runN = 0;
        end
        data = loadjson([path f_content(f).name]);
        beacons_data = data{1}.beacons;
        beaconsN = length(beacons_data);
        break;
    elseif f == length(f_content)
        error(['There is no map JSON-file in a folder ' [pwd path]]);
    end
end


% Choose the necessary information
switch type
    case 'beaconsN'
        output = beaconsN;
    
    case 'MAC'
        beacOrderedMACs = cell(1, beaconsN);
        for bN=1:beaconsN
            beacOrderedMACs{bN} = beacons_data{bN}.macAddress;
        end
        output = beacOrderedMACs;
        
    case 'beaconsParams'
        coords = cell(1, beaconsN);
        for bN=1:beaconsN
            coords{bN}.x = beacons_data{bN}.x;
            coords{bN}.y = beacons_data{bN}.y;
            coords{bN}.z = beacons_data{bN}.z;
            coords{bN}.damp    = beacons_data{bN}.damp;
            coords{bN}.txpower = beacons_data{bN}.txpower;
        end
        output = coords;
        
    case 'map'        
        mapParams.title     = data{1}.description;
        mapParams.width     = data{1}.width;
        mapParams.height    = data{1}.height;
        mapParams.pixelSize = data{1}.pixelSize;
        output = mapParams;
end






