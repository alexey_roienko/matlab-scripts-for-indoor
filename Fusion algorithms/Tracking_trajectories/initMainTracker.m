%% Initialization file for mainTracker.m script
%% Author:
%  Alexey Roienko, a.roienko@it-jim.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  23/05/17

settings.userHeight   = 1.5;       % meters, the height above the ground of the terminal
settings.timeout      = 1.5;       % msec., time when lost packets are recovered
settings.deltaTS      = 10;        % msec., defines possible difference between closed timestamps
settings.time_step    = 1;         % sec., time period between calculation of two neighbour positions
settings.start_period = 2;         % sec., time period for calculating the first position point; 
                                   %    it is started counting from the first received packet
settings.RSSI_filtering = 'on';    % flag for turning on/off RSSI filtering
if strcmp(settings.RSSI_filtering, 'on')
    settings.WS = 3;               % if RSSI filtering is ON, select size of filter sliding window
end
settings.meshSteps    = [0.3 0.3]; % meters, widths of mesh steps applied for correcting coordinates values

%% Finding beacons number form map JSON-file
settings.beaconsN = fReadBeaconsJson('/map_files/', 'beaconsN');


%% Positioning methods parameters
settings.pos_method  = 'trilat';     % possible values are 'multilat', 'trilat', 'trilat_T2'
settings.beacNumbPos = settings.beaconsN; 
   % parameter "settings.beacNumbPos" selects the number of beacons used 
   % while calculating the user coordinates:
   % all available - for Multilateration; 3 best - for Trilateration;
   % 4 and more - for Trilateration_T2.
if strcmp(settings.pos_method, 'trilat_T2')
    settings.combSize = 4;
elseif strcmp(settings.pos_method, 'trilat')
    settings.beacNumbPos = 3;
elseif settings.beacNumbPos<3
    error('Positioning can not be done with less than three beacons!');
end


%% Needed for investigation the accuracy of estimating the initial coordinates only
% start_coords = [680 491];   % Trajectory #1
% start_coords = [1080 735];  % Trajectory #2
% start_coords = [670 650];   % Trajectory #3
% start_coords = [1090 765];  % Trajectory #4
% start_coords = [1050 70];   % Trajectory #5

settings.imageSize = size(mapImage);

% Turn on MeshCorrector stage
settings.meshCorrector    = 'yes';
% Turn on MaxDistanceCorrector stage
settings.maxDistCorrector = 'no';
% Max meters that person can walk per one time interval
settings.maxDistance      = 2;

% Connect each new position point in the Map figure with the previous one
visualization.lines       = 'yes';
% Show order number near each position point in the Map figure
visualization.numbers     = 'no';

% Show DEBUG info in Command Window
settings.debugInfo        = 'no';
% Collect Intermediate results and write them to MAT-file in RESULTS dir
settings.saveDataFile     = 'yes';


