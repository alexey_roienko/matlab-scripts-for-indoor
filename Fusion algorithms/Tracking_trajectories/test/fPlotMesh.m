%% Function plots navigation mesh nodes over the map
%% Author:
%  Yevhen Chervoniak, eugenecher94@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  23/05/17

function fPlotMesh(stepX, stepY, maskHeight, pixelSize)

%% Init
load('masktable.out');
hold on;
ny = round(maskHeight*pixelSize/stepY) + 1;

%% Body
for i = 0:length(masktable) - 1
    cx = floor(i/ny);
    cy = i - cx*ny;
    x  = round(cx*stepX/pixelSize);
    y  = round(cy*stepY/pixelSize);
    if cx == 18 && cy == 3
        l = 1;
    end
    if masktable(i+1) == i
        plot(x, y, 'b.', 'Markersize', 10);
    else
        plot(x, y, 'r.', 'Markersize', 10);
    end
end