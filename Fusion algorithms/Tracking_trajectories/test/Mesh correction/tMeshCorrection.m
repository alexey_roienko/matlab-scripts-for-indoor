%% Script provides testing of mesh correction of coordinates
% There are two modes available: automatic and manual
% In automatic mode the log-files are loaded from
% "Navigator_root\beacon_logs\"
%% Author:
%  Yevhen Chervoniak, eugenecher94@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  23/05/17

clc, clear, close all;

%% Init
mode = 'manual';

initialDir = pwd;
cd ../..
projectDir = pwd;
addpath([projectDir '/functions/'], [projectDir '/test/'], [projectDir '/map_files/']);

figN = 1;
f_start = 3;
f_content = dir([projectDir '/beacon_logs/']);


%% Body
% Load map file and its parameteres
mapImage  = imread('map.png');
mapParams = fReadBeaconsJson('/map_files/', 'map');
%mapParams.width = 6;
initMainTracker;

if strcmp(mode, 'auto')
    % Initialization of main TRACKER parameters
    settings.maxDistCorrector = 'no';
    settings.meshCorrector    = 'yes';
    
    % Cycle over all log-files available in the "datafiles" subfolder
    for f=f_start:length(f_content)
        
        % Depict map
        figure(figN);
        imshow(mapImage);
        title([mapParams.title ', file #' num2str(f-f_start+1)]);
        axis image;
        hold on;
        
        % Running TRACKING for the log file
        [rawCoords, meshCorrection, distCorrection, output] = fTracker(settings, f_content(f).name);
        % Plot trajectories
        plotData = {rawCoords/mapParams.pixelSize, meshCorrection/mapParams.pixelSize};
        plot(plotData{1}(:, 1), plotData{1}(:, 2), 'b*-');
        for p = 1:length(plotData{1}(:, 1))
            if p==1
                text(plotData{1}(p, 1)-20, plotData{1}(p, 2)+20, 'Start', 'Color', 'blue');
            else
                text(plotData{1}(p, 1)-20, plotData{1}(p, 2)+20, num2str(p), 'Color', 'blue');
            end
        end
        plot(plotData{2}(:, 1), plotData{2}(:, 2), 'ro-');
        for p = 2:length(plotData{2}(:, 1))
            text(plotData{2}(p, 1)-20, plotData{2}(p, 2)+20, num2str(p), 'Color', 'red');
        end
        legend('Raw data', 'Mesh correction');
        fPlotMesh(settings.meshSteps(1), settings.meshSteps(1), settings.imageSize(1), mapParams.pixelSize);
        figN = figN + 1;
    end
    
elseif strcmp(mode, 'manual')
    %% Init
    output           = [];
    outputPx         = [];
    meshCorrection   = [];
    meshCorrectionPx = [];
    imshow(mapImage);
    title([mapParams.title ', manual mode']);
    axis image;
    hold on;
    fPlotMesh(settings.meshSteps(1), settings.meshSteps(1), settings.imageSize(1), mapParams.pixelSize);
    p(1) = line(0, 0, 'LineStyle', '--');
    p(2) = line(0, 0);
    
    %% Body
    maskTable = load('masktable.out');
    while(1)
        % Read the coordinates
        [x, y]   = ginput(1);
        % Boundary condition
        if x > mapParams.width/mapParams.pixelSize-10
            x = mapParams.width/mapParams.pixelSize-10;
        elseif x < 10
            x = 10;
        end
        if y > mapParams.height/mapParams.pixelSize-10
            y = mapParams.height/mapParams.pixelSize-10;
        elseif y < 10
            y = 10;
        end
        % Raw data
        output   = [output; [round(x)*mapParams.pixelSize round(y)*mapParams.pixelSize]];
        outputPx = [outputPx; [round(x) round(y)]];
        % Make a correction
        xy = fMeshCorrector(output(end, :), settings.meshSteps, mapParams.height, maskTable, settings.debugInfo);
        % Corrected data
        meshCorrection   = [meshCorrection; [xy(1) xy(2)]];
        meshCorrectionPx = [meshCorrectionPx; [round(xy(1)/mapParams.pixelSize) round(xy(2)/mapParams.pixelSize)]];
        % Visualize the results
        set(p(1), 'XData', outputPx(:, 1), 'YData', outputPx(:, 2), 'Color', 'blue', 'Marker', 'o');
        set(p(2), 'XData', meshCorrectionPx(:, 1), 'YData', meshCorrectionPx(:, 2), 'Color', 'red', 'Marker', '*');
    end
end

cd(initialDir);


