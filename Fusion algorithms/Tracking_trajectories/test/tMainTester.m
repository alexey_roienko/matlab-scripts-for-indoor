clc, clear, close all;

load('input.mat');
addpath([pwd '/map_files/']);

mapImage  = imread('map_beacons_v2.jpg');
imshow(mapImage);
hold on;

save1 = save1/pxSize;
save2 = save2/pxSize;
output = output/pxSize;
plotData = {save1, save2, output};

plot(plotData{1}(:, 1), plotData{1}(:, 2), 'b*-');
for p = 1:length(plotData{1}(:, 1))
    if p==1
        text(plotData{1}(p, 1)-20, plotData{1}(p, 2)+20, 'Start', 'Color', 'blue');
    else
        text(plotData{1}(p, 1)-20, plotData{1}(p, 2)+20, num2str(p), 'Color', 'blue');
    end
end

plot(plotData{2}(:, 1), plotData{2}(:, 2), 'ro-');
for p = 2:length(plotData{2}(:, 1))
    text(plotData{2}(p, 1)-20, plotData{2}(p, 2)+20, num2str(p), 'Color', 'red');
end

plot(plotData{3}(:, 1), plotData{3}(:, 2), 'ks-');
for p = 2:length(plotData{3}(:, 1))
    text(plotData{3}(p, 1)-20, plotData{3}(p, 2)+20, num2str(p), 'Color', 'black');
end
legend('Raw data', 'Maximal distance correction', 'Maximal distance correction+Mesh correction');
% legend('Raw data', 'Mesh correction', 'Mesh correction+Maximal distance correction');
fPlotMesh(0.3, 0.3, 994, pxSize);