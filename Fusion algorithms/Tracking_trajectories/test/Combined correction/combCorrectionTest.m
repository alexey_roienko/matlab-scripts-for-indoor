%% Script provides testing of max distance correction of coordinates
% There are two modes available: automatic and manual
% In automatic mode the log-files are loaded from
% "Navigator_root\beacon_logs\"
%% Author:
%  Yevhen Chervoniak, eugenecher94@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  23/05/17

clc, clear, close all;

%% Init
mode = 'manual';

addpath([pwd '/functions/'], [pwd '/beacon_logs/'], [pwd '/map_files/']);
figN = 1;
f_start = 3;
f_content = dir([pwd '/beacon_logs/']);

%% Body
% Load map file and its parameteres
mapImage  = imread('map_beacons_v2.jpg');
mapParams = fReadBeaconsJson('/map_files/', 'map');
mapParams.width = 10.5;
initMainTracker;

if strcmp(mode, 'auto')
    % Initialization of main TRACKER parameters
    settings.maxDistCorrector = 'yes';
    settings.meshCorrector    = 'yes';
    
    % Cycle over all log-files available in the "datafiles" subfolder
    for f=f_start:length(f_content)
        
        % Depict map
        figure(figN);
        imshow(mapImage);
        title([mapParams.title ', file #' num2str(f-f_start+1)]);
        axis image;
        hold on;
        
        % Running TRACKING for the log file
        [rawCoords, meshCorrection, distCorrectionPrev, output] = fTracker(settings, f_content(f).name);
        
        % Plot trajectories
        if strcmp(settings.correctionOrder, 'direct')
            plotData = {rawCoords/mapParams.pixelSize, distCorrectionPrev/mapParams.pixelSize, meshCorrection/mapParams.pixelSize};
        elseif strcmp(settings.correctionOrder, 'inverse')
            plotData = {rawCoords/mapParams.pixelSize, meshCorrection/mapParams.pixelSize, distCorrectionPrev/mapParams.pixelSize};
        end
        plot(plotData{1}(:, 1), plotData{1}(:, 2), 'b*-');
        for p = 1:length(plotData{1}(:, 1))
            if p==1
                text(plotData{1}(p, 1)-20, plotData{1}(p, 2)+20, 'Start', 'Color', 'blue');
            else
                text(plotData{1}(p, 1)-20, plotData{1}(p, 2)+20, num2str(p), 'Color', 'blue');
            end
        end
        
        plot(plotData{2}(:, 1), plotData{2}(:, 2), 'ro-');
        for p = 2:length(plotData{2}(:, 1))
            text(plotData{2}(p, 1)-20, plotData{2}(p, 2)+20, num2str(p), 'Color', 'red');
        end
        
        plot(plotData{3}(:, 1), plotData{3}(:, 2), 'ks-');
        for p = 2:length(plotData{3}(:, 1))
            text(plotData{3}(p, 1)-20, plotData{3}(p, 2)+20, num2str(p), 'Color', 'black');
        end
        
        if strcmp(settings.correctionOrder, 'direct')
            legend('Raw data', 'Stage 1. Maximal distance correction', 'Stage 2. Maximal distance correction+Mesh correction', 'Location','northoutside');
        elseif strcmp(settings.correctionOrder, 'inverse')
            legend('Raw data', 'Stage 1. Mesh correction', 'Stage 2. Mesh correction+Maximal distance correction', 'Location','northoutside');
        end
        fPlotMesh(settings.meshSteps(1), settings.meshSteps(1), settings.imageSize(1), mapParams.pixelSize);
        figN = figN + 1;
    end
elseif strcmp(mode, 'manual')
    %% Init
    output           = [];
    outputPx         = [];
    meshCorrection   = [];
    meshCorrectionPx = [];
    distCorrectionPrev   = [];
    distCorrectionPrevPx = [];
    maxDistancePx = 200;
    count         = 0;
    maskImage     = imread('mask.png');
    imshow(mapImage);
    title([mapParams.title ', manual mode']);
    axis image;
    hold on;
    fPlotMesh(settings.meshSteps(1), settings.meshSteps(1), settings.imageSize(1), mapParams.pixelSize);
    p(1) = line(0, 0, 'LineStyle', '-.');
    p(2) = line(0, 0, 'LineStyle', '--');
    p(3) = line(0, 0);
    p(4) = line(0, 0);
    
    %% Body
    maskTable = load('masktable.out');
    while(1)
        count = count + 1;
        % Read the coordinates
        [x, y]   = ginput(1);
        % Boundary condition
        if x > mapParams.width/mapParams.pixelSize-10
            x = mapParams.width/mapParams.pixelSize-10;
        elseif x < 10
            x = 10;
        end
        if y > mapParams.height/mapParams.pixelSize-10
            y = mapParams.height/mapParams.pixelSize-10;
        elseif y < 10
            y = 10;
        end
        % Raw data
        output(count, :)   = [round(x)*mapParams.pixelSize round(y)*mapParams.pixelSize];
        outputPx(count, :) = [round(x) round(y)];
        % Corrected data
        distCorrectionPrev(1, :)   = output(1, :);
        distCorrectionPrevPx(1, :) = outputPx(1, :);
        meshCorrection(1, :)   = output(1, :);
        meshCorrectionPx(1, :) = outputPx(1, :);
        % Plot raw data
        set(p(1), 'XData', outputPx(:, 1), 'YData', outputPx(:, 2), 'Color', 'blue', 'Marker', 'o');
        % Define the order of correction stages
        if strcmp(settings.correctionOrder, 'inverse')
            xy = fMeshCorrector(output(end, :), settings.meshSteps, mapParams.height, maskTable, settings.debugInfo);
            outputPx(count, :) = output(count, :)/mapParams.pixelSize;
            meshCorrection(count, :)   = [xy(1) xy(2)];
            meshCorrectionPx(count, :) = [round(xy(1)/mapParams.pixelSize) round(xy(2)/mapParams.pixelSize)];
            % Plot the corrected data (mesh correction)
            set(p(2), 'XData', meshCorrectionPx(:, 1), 'YData', meshCorrectionPx(:, 2), 'Color', 'red', 'Marker', '*');
        end
        if count > 1
            % Plot a circle
            angle = 0:pi/180:2*pi;
            circle(:, 1) = maxDistancePx*cos(angle) + distCorrectionPrevPx(count-1, 1);
            circle(:, 2) = maxDistancePx*sin(angle) + distCorrectionPrevPx(count-1, 2);
            set(p(4), 'XData', circle(:, 1), 'YData', circle(:, 2), 'Color', 'black');
            % Make a correction
            distCorrectionPrevPx(count, :) = fMaxDistCorrector(distCorrectionPrevPx(count-1, :), meshCorrectionPx(count, :), maskImage,...
                maxDistancePx , settings.debugInfo);
            % Corrected data
            distCorrection(count, :)   = distCorrectionPrevPx(count, :)*mapParams.pixelSize;
            % Plot the corrected data (max dist correction)
            set(p(3), 'XData', distCorrectionPrevPx(:, 1), 'YData', distCorrectionPrevPx(:, 2), 'Color', 'black', 'Marker', 's');
        end
        if strcmp(settings.correctionOrder, 'direct')
            xy = fMeshCorrector(output(end, :), settings.meshSteps, mapParams.height, maskTable, settings.debugInfo);
            outputPx(count, :) = output(count, :)/mapParams.pixelSize;
            meshCorrection(count, :)   = [xy(1) xy(2)];
            meshCorrectionPx(count, :) = [round(xy(1)/mapParams.pixelSize) round(xy(2)/mapParams.pixelSize)];
            % Plot the corrected data (mesh correction)
            set(p(2), 'XData', meshCorrectionPx(:, 1), 'YData', meshCorrectionPx(:, 2), 'Color', 'red', 'Marker', '*');
        end
    end
end