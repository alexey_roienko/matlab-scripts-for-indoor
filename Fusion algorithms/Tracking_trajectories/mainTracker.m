%% Script simulates operation of Indoor Navigation System based on BLE beacons signals
%% Authors:
%  Alexey Roienko,    a.roienko@it-jim.com
%  Yevhen Chervoniak, eugenecher94@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  23/05/17

clc, close all;
clear;

%% Initialization
addpath([pwd '/functions/'], [pwd '/beacon_logs/'], [pwd '/map_files/']);
figN = 1;
f_start = 3;
f_content = dir([pwd '/beacon_logs/']);


%% Load map file and its parameteres
mapImage  = imread('map.png');
mapParams = fReadBeaconsJson('/map_files/', 'map');
% mapParams.width = 10.5;


%% Initialization of main TRACKER parameters
initMainTracker;


%% Cycle over all log-files available in the "datafiles" subfolder
for f=f_start:length(f_content)

    %% Depict map
    figure(figN);
    imshow(mapImage); 
    title([mapParams.title ', file #' num2str(f-f_start+1)]);
    axis image
    hold on;    
    disp(['Processing ' f_content(f).name ' ...']);    
    
    
    %% Running TRACKING for the log file
    output = fTracker(settings, f_content(f).name);
    
    
    %% Displaying points on the map for testing purposes    
    for p=1:length(output)
        tr.x = output(p,1)/mapParams.pixelSize;
        tr.y = output(p,2)/mapParams.pixelSize;
        
        if tr.x > settings.imageSize(2)
            tr.x = settings.imageSize(2);
        elseif tr.x == 0
            tr.x = 1;
        end
        if tr.y > settings.imageSize(1)
            tr.y = settings.imageSize(1);
        elseif tr.y == 0
            tr.y = 1;
        end
        
%         plot(tr.x, tr.y, 'b*');
%         plot(660, 650, 'rd', 'MarkerSize', 10, 'LineWidth', 2);
%         legend('Estimated', 'Initial', 'Location', 'SouthWest');
        if p==1
            plot(tr.x, tr.y, 'rd');
            if strcmpi(visualization.numbers, 'yes')
                text(tr.x-20, tr.y+20, 'Start', 'Color', 'black') ;
            end
            if strcmpi(visualization.lines, 'yes')
                prev_point = tr;
            end
        else        
            plot(tr.x, tr.y, 'b*');
            if strcmpi(visualization.numbers, 'yes')
                text(tr.x-20, tr.y+20, num2str(p), 'Color', 'black') ;
            end
            if strcmpi(visualization.lines, 'yes')
                line([prev_point.x tr.x], [prev_point.y tr.y]);
                prev_point = tr;            
            end
        end
    end
    figN = figN + 1;
    
end
    











