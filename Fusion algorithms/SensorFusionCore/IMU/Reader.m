classdef Reader < handle
%% Author:
%  Feliks Sirenko, sirenkofelix@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  21/05/17
%% Class definition
%PLOTTER Class who contains all functions responsible for plotting the results
    
    %% Public properties %%
    properties (Access = public)
        accelPath;      % Path with acceleration raw data accel.json
        gyroPath;       % Path with gyroscope raw data gyro.json
        loggerPath      % Path with logger raw data angles.json
        accel;          % Acceleration raw readings
        gyro;           % Angular speed raw readings
        anglesLogger;   % Angles raw readings (Logger)
        isPrintable;    % Indicator who shows, should this reading be plotted

    end
    
    methods (Access = public) 
        %% Object Initializer
         function obj = Reader(varargin)
                     % accelPath = '.../*.json'
                     % gyroPath = '.../*.json'
                     % isPrintable = 1 or 0, if 1 - then print the graph,
                     % else don not print
                for i = 1:2:nargin
                    if  strcmp(varargin{i}, 'accelPath')
                        obj.accelPath = varargin{i+1};
                    elseif  strcmp(varargin{i}, 'gyroPath')
                        obj.gyroPath = varargin{i+1};
                    elseif  strcmp(varargin{i}, 'loggerPath')
                        obj.loggerPath = varargin{i+1};
                    elseif  strcmp(varargin{i}, 'isPrintable')
                        obj.isPrintable = varargin{i+1};                        
                    else
                        error('Invalid argument');
                    end
                end;
         end;
    end;
    
    %% Public methods %%
    methods (Access = public)
        function fReadAccel( obj )
        %% fREADACCEL - reads raw accel
        % INPUTS:
        % obj.accelPath - object property, who must be set prior to the call of the function
        % OUTPUTS:
        % obj.accel - object property, who can be obtained ONLY after
        % method have been called
        % REQUIREMENTS:
        % JSON library;
        % fStandartizeAccel();            
            content = fileread(obj.accelPath);
            obj.accel = obj.fStandartizeAccel (JSON.parse(content));
        end
        
        %% Read gyroscope %%
        function fReadGyro (obj)
        %% fREADGYRO - reads raw angular speed
        % INPUTS:
        % obj.gyroPath - object property, who must be set prior to the call of the function
        % OUTPUTS:
        % obj.gyro - object property, who can be obtained ONLY after
        % method have been called
        % REQUIREMENTS:
        % JSON library;
        % fStandartizeGyro();             
            content = fileread(obj.gyroPath);
            obj.gyro = obj.fStandartizeGyro(JSON.parse(content));
        end
        %% Read gyroscope %%
        function fReadAngles (obj)
        %% fREADANGLES - reads raw angular speed
        % INPUTS:
        % obj.loggerPath - object property, who must be set prior to the call of the function
        % OUTPUTS:
        % obj.angles - object property, who can be obtained ONLY after
        % method have been called
        % REQUIREMENTS:
        % JSON library;
        % fStandartizeLogger();             
            content = fileread(obj.loggerPath);
            obj.anglesLogger = obj.fStandartizeLogger(JSON.parse(content));
        end
        
    end
    
    %% Private methods   
    methods (Access = private)
        
        %% Convert acceleration %%
        function recDataAveraged = fStandartizeAccel( obj, arrayOfVectors )
        %% fSTANDARTIZEACCEL - converts raw data to an approptiate format
        % INPUTS:
        % arrayOfVectors - array of structures, each of which represents a single measurement arrayOfVectors[struct[timestamp x y z]]
        % obj.isPrintable - object property, who must be set prior to the call of the function
        % OUTPUTS:
        % recDataAveraged - structure with array inside, each element of which represents a single measurement struct[timestamp x y z]
        recDataAveraged = repmat(struct('timestamp', 0, 'x', 0, 'y', 0, 'z', 0), numel(arrayOfVectors), 1 );
            for i = 1: numel(arrayOfVectors)
                recDataAveraged(i).timestamp = arrayOfVectors{1,i}.timestamp / 10.^6;
                recDataAveraged(i).x = arrayOfVectors{1,i}.x;
                recDataAveraged(i).y = arrayOfVectors{1,i}.y;
                recDataAveraged(i).z = arrayOfVectors{1,i}.z;    
            end
            if obj.isPrintable == 1
                obj.fPlotAcceleration(recDataAveraged)
            end
        end       
        function recDataAveraged = fStandartizeGyro( obj, arrayOfVectors )
        %% fSTANDARTIZEACCEL - converts raw data to an approptiate format
        % INPUTS:
        % arrayOfVectors - array of structures, each of which represents a single measurement arrayOfVectors[struct[timestamp x y z]]
        % obj.isPrintable - object property, who must be set prior to the call of the function
        % OUTPUTS:
        % recDataAveraged - structure with array inside, each element of which represents a single measurement struct[timestamp x y z]
        recDataAveraged = repmat(struct('timestamp', 0, 'x', 0, 'y', 0, 'z', 0), numel(arrayOfVectors), 1 );
            for i = 1: numel(arrayOfVectors)
                recDataAveraged(i).timestamp = arrayOfVectors{1,i}.timestamp / 10.^6;
                recDataAveraged(i).x = arrayOfVectors{1,i}.x;
                recDataAveraged(i).y = arrayOfVectors{1,i}.y;
                recDataAveraged(i).z = arrayOfVectors{1,i}.z;    
            end
            if obj.isPrintable == 1
                obj.funcPlotGyro(recDataAveraged)
            end
        end
        function recDataAveraged = fStandartizeLogger( obj, arrayOfVectors )
        %% fSTANDARTIZELOGGER - converts raw data to an approptiate format
        % INPUTS:
        % arrayOfVectors - array of structures, each of which represents a single measurement arrayOfVectors[struct[timestamp pitch roll yaw]]
        % obj.isPrintable - object property, who must be set prior to the call of the function
        % OUTPUTS:
        % recDataAveraged - structure with array inside, each element of which represents a single measurement struct[timestamp x y z]

            recDataAveraged = repmat(struct('timestamp', 0, 'x', 0, 'y', 0, 'z', 0), numel(arrayOfVectors), 1 );
            for i = 1: numel(arrayOfVectors)
                recDataAveraged(i).timestamp = arrayOfVectors{1,i}.timestamp / 10.^6;
                recDataAveraged(i).x = arrayOfVectors{1,i}.pitch; 
                recDataAveraged(i).y = arrayOfVectors{1,i}.roll; 
                recDataAveraged(i).z = arrayOfVectors{1,i}.azimuth;
            end
            if obj.isPrintable == 1
                obj.fPlotAnglesLogger(recDataAveraged)
            end
        end
        
        function fPlotAcceleration(~, accel)
        %fPLOTACCELERATION Plots the raw recoreded acceleration
        % INPUTS:
        % accel - struct with raw acceleration in format accel[timestamp x
        % y z] in gravity units
            figure('units','normalized','outerposition',[0 0 1 1])

            subplot(3,1,1)
            plot([accel.timestamp], [accel.x] * 9.8);
            legend('ax');
            title('Raw Acceleration');
            xlabel('Time, s');
            ylabel('a, m/s^2');
            set(gca,'fontsize',16);
            grid on
            grid minor

            subplot(3,1,2)
            plot([accel.timestamp], [accel.y] * 9.8);
            legend('ay');
            title('Raw Acceleration');
            xlabel('Time, s');
            ylabel('a, m/s^2');
            set(gca,'fontsize',16);
            grid on
            grid minor

            subplot(3,1,3)
            plot([accel.timestamp], [accel.z] * 9.8);
            legend('az');
            title('Raw Acceleration');
            xlabel('Time, s');
            ylabel('a, m/s^2');
            set(gca,'fontsize',16);
            grid on
            grid minor
            pause;
            close 
        end      
        function fPlotGyro(~, gyro)
        %fPLOTGYRO Plots the raw recoreded angular speed
        % INPUTS:
        % gyro - struct with raw angular speeds in format gyro[timestamp x
        % y z] in s^-1
            figure('units','normalized','outerposition',[0 0 1 1])

            subplot(3,1,1)
            plot([gyro.timestamp], [gyro.x]);
            legend('\omega x');
            title('Angular speed');
            xlabel('Time, s');
            ylabel('\omega, 1/s');
            set(gca,'fontsize',16);
            grid on
            grid minor

            subplot(3,1,2)
            plot([gyro.timestamp], [gyro.y]);
            legend('\omega y');
            title('Angular speed');
            xlabel('Time, s');
            ylabel('\omega, 1/s');
            set(gca,'fontsize',16);
            grid on
            grid minor

            subplot(3,1,3)
            plot([gyro.timestamp], [gyro.z]);
            legend('\omega z');
            title('Angular speed');
            xlabel('Time, s');
            ylabel('\omega, 1/s');
            set(gca,'fontsize',16);
            grid on
            grid minor
            pause;
            close;
        end
        function fPlotAnglesLogger(~, angles)
        %fPLOTANGLESLOGGER Plots the raw recoreded angles
        % INPUTS:
        % angles - struct with raw angles detected by the logger in format
        % gyro[timestamp x y z] in grades
            figure('units','normalized','outerposition',[0 0 1 1])

            subplot(3,1,1)
            plot([angles.timestamp], [angles.x]);
            legend('Pitch');
            title('Angle');
            xlabel('Time, s');
            ylabel('Pitch, deg');
            set(gca,'fontsize',16);
            grid on
            grid minor

            subplot(3,1,2)
            plot([angles.timestamp], [angles.y]);
            legend('Roll');
            title('Angle');
            xlabel('Time, s');
            ylabel('Roll, deg');
            set(gca,'fontsize',16);
            grid on
            grid minor

            subplot(3,1,3)
            plot([angles.timestamp], [angles.z]);
            legend('\omega z');
            title('Angle');
            xlabel('Time, s');
            ylabel('Yaw, deg');
            set(gca,'fontsize',16);
            grid on
            grid minor
            pause;
            close;
        end
    end
end

