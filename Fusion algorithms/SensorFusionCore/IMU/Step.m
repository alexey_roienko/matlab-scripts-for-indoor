classdef Step
%% Author:
%  Feliks Sirenko, sirenkofelix@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  21/05/17
%% Class definition
%STEP Class implements a step detection algorithm

    %% Public properties %%
    properties (Access = public)
        samplePeriod; % Sample period
        cutoffHigh;   % Cutoff frequency for highpass Butterworth filter
        cutoffLow;    % Cutoff frequency for lowpass Butterworth filter
        stationaryIndicator;    % Boundary of stationary 
    end
    
    %% Public methods %%
    methods (Access = public)    
        %% Object initializer
         function obj = Step(varargin)
                for i = 1:2:nargin
                    if  strcmp(varargin{i}, 'samplePeriod')
                        obj.samplePeriod = varargin{i+1};
                    elseif  strcmp(varargin{i}, 'cutoffHigh')
                        obj.cutoffHigh = varargin{i+1};
                    elseif  strcmp(varargin{i}, 'cutoffLow')
                        obj.cutoffLow = varargin{i+1};
                     elseif  strcmp(varargin{i}, 'stationaryIndicator')
                        obj.stationaryIndicator = varargin{i+1};
                    else
                        error('Invalid argument');
                    end
                end;
         end;
    end;
    
    %% Public methods %%
    methods (Access = public)
        function stat = fDetectSteps (obj, acc)
        %% fDETECTSTEPS Core function responsible for the step detection
        % INPUTS:
        % obj.samplePeriod - object property, who must be set prior to the call of the function.
        % obj.cutoffHigh - object property, who must be set prior to the call of the function.
        % obj.cutoffLow - object property, who must be set prior to the call of the function.
        % obj.stationaryIndicator - object property, who must be set prior to the call of the function.
        % OUTPUTS:
        % stat - binaty array of states (0 - motion, 1 - stationary)
        % REQUIREMENTS:
        % obj.funcButterworthFilter_High(accel);
        % obj.funcButterWorthFilter_Low(acc_norm);
        % obj.fPlotRawAndStationary (t, ax, ay, az, anorm, stationary)
        acc_norm = ([acc.x].^2 + [acc.y].^2 + [acc.z].^2).^0.5;                      % Compute accelerometer magnitude

            delta_f = 1 / (length(acc_norm) * obj.samplePeriod);
            f = 0: delta_f: (1/obj.samplePeriod - delta_f);
            
%            obj.fPlotAccelInFrequencyDomain(f, acc_norm, 'Acceleration before filtration');
          
%            acc_norm = obj.fButterworthFilter_High(acc_norm);
            acc_norm = obj.fButterWorthFilter_Low(acc_norm);
%            obj.fPlotAccelInFrequencyDomain(f, acc_norm, 'Acceleration after BUT HP and LP');
            acc_norm(acc_norm < 0) = 0;
            
            stat = acc_norm < obj.stationaryIndicator;                            % Threshold detection    1 - point is stationary, 0 - point is not stationary
%            obj.fPlotRawAndStationary( [acc.timestamp], [acc.x], [acc.y], [acc.z], [acc_norm.'], [stat'] );
        end
    end
    
    %% Private methods %%
    methods (Access = private)
        function param = fButterworthFilter_High(obj, param)
        %% fBUTTERWORTHFILTER_HIGH Highpass Butterworth filter
        % INPUTS:
        % param - acceleartion that must be filtered
        % obj.samplePeriod - object property, who must be set prior to the call of the function.
        % obj.cutoffHigh - object property, who must be set prior to the call of the function.
        % OUTPUTS:
        % param - filtered acceleartion
            [b, a] = butter(8, (2 * obj.cutoffHigh)/(1/obj.samplePeriod), 'high' ); 
            param = filtfilt(b, a, param);                                      
        end
        
        function param = fButterWorthFilter_Low(obj, param)
        %% fBUTTERWORTHFILTER_LOW Highpass Butterworth filter
        % INPUTS:
        % param - acceleartion that must be filtered
        % obj.samplePeriod - object property, who must be set prior to the call of the function.
        % obj.cutoffLow - object property, who must be set prior to the call of the function.
        % OUTPUTS:
        % param - filtered acceleartion
           [b, a] = butter(8, (2 * obj.cutoffLow)/(1/obj.samplePeriod), 'low' ); 
            param = filtfilt(b, a, param); 
        end
        
        function fPlotAccelInFrequencyDomain(~,freq, accel, Name)
            % fPLOTACCELINFREQUENCYDOMAIN Plots the magnitude of
            % an acceleration in frequency domain
            % INPUTS:
            % accel - magnitude of acceleration
            figure('units','normalized','outerposition',[0 0 1 1])
            plot (freq, abs(fft(accel)) / length(accel), 'r', 'LineWidth' , 2);
            xlabel('Frequency (Hz)' );
            ylabel('Magnitude (g)' );
            title(Name);
            legend('Accel');
            grid on;
            grid minor;
            pause;
            close;            
        end
        function fPlotRawAndStationary( ~, time, accX, accY, accZ, accNorm, isStationary )
        %% fPLOTRAWANDSTATIONARY Plots data raw sensor data and stationary periods
        % INPUTS:
        % time - time;
        % accX - acceleration along X axis in local CS
        % accY - acceleration along Y axis in local CS
        % accZ - acceleration along Z axis in local CS
        % accNorm - normalized acceleration
        % isStationary - binary indication of the stationarity
                 figure('units','normalized','outerposition',[0 0 1 1])
                 plot(time, accX, 'r' );
                 hold on;
                 plot(time, accY, 'g' );
                 hold on;
                 plot(time, accZ, 'b' );
                 hold on;
                 plot(time, accNorm, 'LineWidth' , 2  );
                 hold on;
                 plot(time, isStationary, 'k' , 'LineWidth' , 2);
                 title('Accelerometer' );
                 xlabel('Time (s)' );
                 ylabel('Acceleration (g)' );
                 legend('X' , 'Y' , 'Z' , 'Filtered' , 'Stationary' );
                 grid on;
                 grid minor;
                 hold off;
                 pause;
                 close;
        end
    end
end

