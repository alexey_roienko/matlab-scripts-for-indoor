classdef ZUPT
%% Author:
%  Feliks Sirenko, sirenkofelix@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  21/05/17
%% Class definition
% ZUPT is a class who implements a zero velocity update algorithm  
    properties (Access = public)
        accel;  % filtered acceleration in global CS
        stat;   % stationary moments of time
        samplePeriod;   % sample period
    end
    methods (Access = public)
        function obj = ZUPT(varargin)
            % Object initializer
            for i = 1:2:nargin
                if  strcmp(varargin{i}, 'accel'), obj.accel = varargin{i+1};
                elseif strcmp(varargin{i}, 'stat'), obj.stat = varargin{i+1}; 
                elseif strcmp(varargin{i}, 'samplePeriod'), obj.samplePeriod = varargin{i+1}; 
                else
                    error('Invalid argument');
                end
            end
        end
    end
    
    methods (Access = public)
        function vel = fGetVelocities (obj)
            %% fGETVELOCITIES ZUPT update
            % INPUT
            % obj.accel - object property, who must be set prior to the call of the function.
            % obj.stat - object property, who must be set prior to the call of the function.
            % obj.samplePeriod - - object property, who must be set prior to the call of the function.
            % OUTPUT
            % vel - velocity in global CS (vel = [velx vely velz])
            
            vel = zeros(size(obj.accel));
            veltemp = zeros(size(vel));
            
            for t = 2:length(obj.accel)
                vel(t,:) = vel(t-1,:) + obj.accel(t,:) * obj.samplePeriod;
                veltemp(t,:) = vel(t,:);
            end
            
            for t = 2:length(vel)
                vel(t,:) = vel(t-1,:) + obj.accel(t,:) * obj.samplePeriod;               
                if obj.stat(t) == 1 
                    vel(t,:) = [0 0 0];
                end
            end
%{
            for t = 2 : length(vel) 
                if abs(vel(t,1)) > 3
                    vel(t,1) = 0;
                    vel(t,2) = 0;     % apply ZUPT update when the device is stationary
                end
                if abs(vel(t,2)) > 3 
                    vel(t,1) = 0; 
                    vel(t,2) = 0;% apply ZUPT update when the device is stationary                
                end
                if abs(vel(t,3)) > 3 
                    vel(t,3) = 0;     % apply ZUPT update when the device is stationary
                end
            end
%}
            %{
            for t = 2 : length(vel)
                if (abs(veltemp(t,1) / veltemp(t,2)) > 3.5 )
                    vel(t,2) = 0.1 * vel(t,2);
                end
                if (abs(veltemp(t,2) / veltemp(t,1)) > 3.5 )
                    vel(t,1) = 0.1 * vel(t,1);
                end 
            end
            %}
        end
    end    
end

