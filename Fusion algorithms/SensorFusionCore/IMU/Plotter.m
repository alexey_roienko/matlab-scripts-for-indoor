classdef Plotter
%% Author:
%  Feliks Sirenko, sirenkofelix@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  21/05/17
%% Class definition
%PLOTTER Class who contains all functions responsible for plotting the results
    methods (Access = public)
        %% Object Initializer
        function obj = Plotter()
        end
    end
    
    methods (Access = public)
        function fPlotAccelGlobalAndLocal(~, time, accLocal, accGlobal)
        %% fPLOTACCELGLOBALANDLOCAL Plots raw accelerations in local CS and treated acceleration in global CS 
        % INPUTS:
        % time[] - moments of time in seconds
        % accLocal[x y z] - accelerations in local CS in gravity units
        % accGlobal[x y z] - acceleration in global CS in gravity units
            figure('units','normalized','outerposition',[0 0 1 1])
            hold on; 
            subplot(3,1,1)
            Pitch_init = plot(time, accLocal(:,1) * 9.8);
            set(Pitch_init,'LineWidth',1);
            hold on;
            Pitch_filt = plot(time, accGlobal(:,1) * 9.8);
            set(Pitch_filt,'LineWidth',2);
            legend('ax Local CS', 'ax Global CS');
            title('Acceleration along pitch axis');
            xlabel('Time, s');
            ylabel('ax, m/s^2');
            set(gca,'fontsize',16);
            grid on
            grid minor

            subplot(3,1,2)
            Pitch_init = plot(time, accLocal(:,2) * 9.8);
            set(Pitch_init,'LineWidth',1);
            hold on;
            Pitch_filt = plot(time, accGlobal(:,2) * 9.8);
            set(Pitch_filt,'LineWidth',2);
            legend('ay Local CS', 'ay Global CS');
            title('Acceleration along roll axis');
            xlabel('Time, s');
            ylabel('ay, m/s^2');
            set(gca,'fontsize',16);
            grid on
            grid minor

            subplot(3,1,3)
            Pitch_init = plot(time, accLocal(:,3) * 9.8);
            set(Pitch_init,'LineWidth',1);
            hold on;
            Pitch_filt = plot(time, accGlobal(:,3) * 9.8);
            set(Pitch_filt,'LineWidth',2);
            legend('az Local CS', 'az Global CS');
            title('Acceleration along yaw axis');
            xlabel('Time, s');
            ylabel('az, m/s^2');
            set(gca,'fontsize',16);
            grid on
            grid minor
            hold off;
            pause;
            close;
        end
        
        function PlotAngles(~, time, Madgwick, logger)
            figure('units','normalized','outerposition',[0 0 1 1])
            subplot(3,1,1)
            Pitch_Madgwick = plot(time, Madgwick(:,1), 'markersize',5,'DisplayName','Pitch Madgwick');
            set(Pitch_Madgwick,'LineWidth', 2);
            hold on;
            Pitch_Logger = plot(time, logger(:,1), 'markersize',5,'DisplayName','Pitch Logger');
            set(Pitch_Logger,'LineWidth', 2);
            legend([Pitch_Madgwick Pitch_Logger]);
            title ('Pitch')
            xlabel('Time, s');
            ylabel('Pitch, deg');
            set(gca,'fontsize',16);
            grid on
            grid minor
            hold off;
            
            subplot(3,1,2)
            Roll_Madgwick = plot(time, Madgwick(:,2), 'markersize',5,'DisplayName','Roll Madgwick');
            set(Roll_Madgwick,'LineWidth', 2);
            hold on;
            Roll_Logger = plot(time, logger(:,2), 'markersize',5,'DisplayName','Roll Logger');
            set(Roll_Logger,'LineWidth', 2);
            legend([Roll_Madgwick Roll_Logger]);
            title ('Roll')
            xlabel('Time, s');
            ylabel('Roll, deg');
            set(gca,'fontsize',16);
            grid on
            grid minor
            hold off;

            subplot(3,1,3)
            Yaw_Madgwick = plot(time, Madgwick(:,3), 'markersize',5,'DisplayName','Yaw Madgwick');
            set(Yaw_Madgwick,'LineWidth',2);
            hold on;
            Yaw_Logger = plot(time, logger(:,3), 'markersize',5,'DisplayName','Yaw Logger');
            set(Yaw_Logger,'LineWidth',2);
            legend([Yaw_Madgwick Yaw_Logger]);
            title ('Yaw')
            xlabel('Time, s');
            ylabel('Yaw, deg');
            set(gca,'fontsize',16);
            grid on
            grid minor
            hold off;
            pause;
            close;
        end
        
        
        function fPlotGlobalVelocity(~, time, velocity)
        %% fPLOTGLOBALCELOCITY Plots ZUPT updated velocitiesin global CS 
        % INPUTS:
        % time[] - moments of time in seconds
        % velocity[x y z] - velocities in global CS in m/s
            figure('units','normalized','outerposition',[0 0 1 1])
            hold on; 
            VelocX = plot(time, velocity(:,1));
            set(VelocX,'LineWidth',2);
            hold on;
            VelocY = plot(time, velocity(:,2));
            set(VelocY,'LineWidth',2); 
            hold on;
            VelocZ = plot(time, velocity(:,3));
            set(VelocZ,'LineWidth',2);
            legend('Vx Global CS', 'Vy Global CS', 'Vz Global CS');
            title('Velocities');
            xlabel('Time, s');
            ylabel('Vx, m/s');
            set(gca,'fontsize',16);
            grid on;
            grid minor;
            hold off;
            pause;
            close
        end
        
        function fPlotRoomMeters(~)
            %% [OBSOLETE] fPLOTROOMMETERS Plots office in meters 
            scale = 0.2684;
            figure('units','normalized','outerposition',[0 0 1 1])
            set(gca,'XTick',[]); 
            set(gca,'YTick',[]);
            hold on;
            %% ROOM
            limitX = [0.0 41.0  41.0  0.0  0.0];
            linitY = [0.0  0.0 -27.5 -27.5 0.0];
            line1X = [ 0.0  2.0  2.0 12.5  12.5];
            line1Y = [-8.0 -8.0 -8.5 -8.5 -27.5];
            line2X = [ 12.5 12.5 26.5 25.5  25.5 ];
            line2Y = [ -4.0 -3.5 -3.5 -3.5 -27.5 ];
            line3X = [30.0 30.0 ];
            line3Y = [ 0.0 -1.0 ];
            line4X = [29.0 30.0 30.0 30.0 41.0 39.0  33.5 30.0];
            line4Y = [-3.5 -3.5 -3.0 -3.5 -3.5 -3.5  -7.5 -3.5];
            line5X = [ 33.5  33.5];
            line5Y = [-27.5 -12.0];
            line(limitX*scale,linitY*scale, 'LineWidth', 2, 'Color', [0 0 0] );
            line(line1X*scale,line1Y*scale, 'LineWidth', 2, 'Color', [0 0 0] );
            line(line2X*scale,line2Y*scale, 'LineWidth', 2, 'Color', [0 0 0] );
            line(line3X*scale,line3Y*scale, 'LineWidth', 2, 'Color', [0 0 0] );
            line(line4X*scale,line4Y*scale, 'LineWidth', 2, 'Color', [0 0 0] );
            line(line5X*scale,line5Y*scale, 'LineWidth', 2, 'Color', [0 0 0] );
            %% FURNITURE
            % 1
            [pegX, pegY] = fCircle(14,-9.5 ,0.75);
            line(pegX*scale,pegY*scale, 'LineWidth', 0.5, 'Color', [0 0 0] );
            %Table 1
            Table1 = [12.6 -16.5 4 5];
               rectangle('Position',Table1*scale,'Curvature',0)
               axis equal
            %Table 2
            Table2 = [12.6 -21.7 1 5];
               rectangle('Position',Table2*scale,'Curvature',0)
               axis equal
            %Table 3
            Table3 = [12.6 -27 5 5];
               rectangle('Position',Table3*scale,'Curvature',0)
               axis equal
            %Table 4
            Table4 = [20.4 -27 5 5];
               rectangle('Position',Table4*scale,'Curvature',0)
               axis equal
            %Table 5
            Table5 = [21.3 -8.6 4 5];
               rectangle('Position',Table5*scale,'Curvature',0)
               axis equal
            %Sofa
            Sofa = [25.5 -12.6 3 9];
               rectangle('Position',Sofa*scale,'Curvature',0)
               axis equal  
            %Table 6
            Table6 = [25.5 -23.3 3 1.5];
               rectangle('Position',Table6*scale,'Curvature',0)
               axis equal
            %Fridge
            Fridge = [25.5 -27.5 2 2];
               rectangle('Position',Fridge*scale,'Curvature',0)
               axis equal
            %KitchenTable
            KitchenTable = [27.8 -27.5 5.5 2];
               rectangle('Position',KitchenTable*scale,'Curvature',0)
               axis equal
            %KitchenChair
            [ChairX, ChairY] = fCircle(32.5,-24.5,0.75);
            line(ChairX*scale , ChairY*scale , 'LineWidth', 0.5, 'Color', [0 0 0] );
            %Chair1
            Chair1 = [33.8 -19.5 1.5 1.5];
               rectangle('Position',Chair1*scale,'Curvature',0)
               axis equal
            %Chair2
            Chair2 = [39.3 -19.5 1.5 1.5];
               rectangle('Position',Chair2*scale,'Curvature',0)
               axis equal
            %RoundTable
            RoundTable = [35.4 -22.5 3.8 7.5];
               rectangle('Position',RoundTable*scale,'Curvature',[0.9 0.6])
               axis equal;
        end  
        
        function fPlotRoomPixels(~, image, ass)
            %% fPLOTROOMPIXELS Plots office in pixels
            % INPUTS:
            % image - binary mask of the office
            figure('units','normalized','outerposition',[0 0 1 1])

            set(gca,'XTick',[]); 
            set(gca,'YTick',[]);
            imshow(image);
            title(ass);
            axis image
            hold on;
        end 
        
        function fCurrentParticles(~,high, low, beac, av, ideal)
        %% fCURRENTPARTICLES Plots current position of particles, metrics, and beacon readings
        % INPUTS:
        % high[x y] - coordinates of points with high weights
        % low[x y] - coordinates of points with low weights
        % beac[x,y] - coordinates of beacons
        % av[x y] - metrics of points with high weights
            pl_low = plot(high(:,1), high(:,2), 'LineStyle', 'none', 'Marker', 'o', 'MarkerFaceColor', 'green', 'markersize',5,'DisplayName','High probability particles');
            pl_high = plot(low(:,1), low(:,2), 'LineStyle', 'none', 'Marker', 'o', 'MarkerFaceColor' , 'black', 'markersize',2.5 , 'DisplayName','Low probability particles');
            pl_beac = plot(beac(1,1), beac(1,2), 'LineStyle', 'none', 'Marker', 'o', 'MarkerFaceColor' , 'blue' , 'markersize', 12.5, 'DisplayName','Beacon readings');  
            pl_av = plot(av(1,1), av(1,2), 'LineStyle', 'none', 'Marker', 'o', 'MarkerFaceColor' , 'red' , 'markersize', 12.5, 'DisplayName','Metrics');
            p1_ideal = plot(ideal(1,1), ideal(1,2), 'LineStyle', '-', 'Marker', 'o', 'MarkerFaceColor' , 'magenta' , 'markersize', 5, 'DisplayName','GroundTruth');

            legend([pl_low pl_high pl_beac pl_av], 'Location','south');

            pause(0.005);
            delete(pl_low);
            delete(pl_high);
            delete(pl_beac);
%            delete(pl_av);
        end 
        
        function fBeconPlot (~, beac, ideal)
             pl_beac = plot(beac(1,1), beac(1,2), 'LineStyle', 'none', 'Marker', 'o', 'MarkerFaceColor' , 'blue' , 'markersize', 6, 'DisplayName','Beacon readings');  
             p1_ideal = plot(ideal(1,1), ideal(1,2), 'LineStyle', '-', 'Marker', 'o', 'MarkerFaceColor' , 'magenta' , 'markersize', 5, 'DisplayName','GroundTruth');
             leg = legend([pl_beac p1_ideal], 'Location','southwest');
             set(leg,'FontSize',16);
 
             pause(0.01); 
        end      

        function fPlotSimpleApproach (~, coord, coord_prev)
            plot([coord_prev(1,1); coord(1,1)], [coord_prev(1,2) coord(1,2)], 'LineStyle', '-', 'Marker', 'o', 'MarkerFaceColor', 'red', 'markersize',5,'DisplayName','Current coordinates');
            pause(0.01);
        end
    end
end

        function [xunit, yunit] = fCircle(x,y,r)
        %% fCIRCLE calculates all points of the circle
        % INPUT:
        % x - x coordinate of the center
        % y - y coordinate of the center
        % r - radius of the circle
        % OUTPUT:
        % xunit - x coordinate of the circle
        % yunit - y coordinate of the circle       
            th = 0:pi/50:2*pi;
            xunit = r * cos(th) + x;
            yunit = r * sin(th) + y;
        end



