classdef Checker
%% Author:
%  Feliks Sirenko, sirenkofelix@gmail.com
% Eugene Chervonyak
% Alexey Roienko, a.roienko@it-jim.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  21/05/17
%% Class definition
%CHECKER The class contains methods who check the correctness of a trajectory 
% Is it within the map limitations?
% Does the trajectory crosses the wall?
% If the trajectory is somewhere in the restricted zone, then fMeshCorrection method brings the trajectory to normal state
%% Public properties
    
    methods (Static, Access = public)
        function [xy2] = fCorrectIfIsNotOnMap(image, xy2)
            %% fISONMAP - the function checks, if the current point is on the map
            % And if it is not, then it fixes this by shifting it to within the map
            % Example : xy2 = [20 -2] => Correction => xy2 = [20 1]
            % INPUT: image - map of the builing (floor, office etc.)
            %        xy2 - array of current point coordinates [x(i) y(i)]
            % OUTPUT:
            %        xy2 - correctedCoordinates of current point
            if xy2(1) < 1
                xy2(1) = 1;
            end
            if xy2(2) < 1
                xy2(2) = 1;
            end
            [y_length, x_length] = size(image);
            if xy2(1) > x_length
                xy2(1) = x_length;
            end
            if xy2(2) > y_length
                xy2(2) = y_length;
            end
        end
        function correctedXY = fMeshCorrector(rawXY, stepsXY, mapHeight, maskTable)
        %% Function provides coordinates correction using "masktable.out" file
        %  which is based on mesh representation of navigation area.
        %% INPUT:
        %  rawXY     - vector, 1x2, raw values of estimated coordinates, in meters, (X,Y)
        %  stepsXY   - vector, 1x2, values of mesh steps along two axes, in meters (X, Y)
        %  mapHeight - height (Y axis direction) of an area covered by navigation system
        %  maskTable - vector, 1xN, contains indecies for transformation of
        %  mesh vertices according to the map mask
        %% OUTPUT:
        %  correctedXY - vector, 1x2, corrected values of estimated coordinates, in meters, (X,Y)
            qX = round(rawXY(1) / stepsXY(1));
            qY = round(rawXY(2) / stepsXY(2));

            % Find number of raws in mesh grid
            Ny = round(mapHeight / stepsXY(2)) + 1;

            % Then index in 1D maskTable is ...
            index = qX*Ny + qY;

            %% If mesh vertex should not be corrected...
            if maskTable(index+1) == index
                correctedXY(1) = qX * stepsXY(1);
                correctedXY(2) = qY * stepsXY(2);    
            else
                %% If the correction is necessary ...
                qX = floor(maskTable(index+1) / Ny);
                qY = maskTable(index+1) - qX*Ny;

                correctedXY(1)  = qX * stepsXY(1);
                correctedXY(2)  = qY * stepsXY(2);    
            end
        end
        function is_wall = fCheck_obst(image, xy1, xy2)
    %% The function is not commented, because it will be changed

    %% Init
    x1 = xy1(1);
    y1 = xy1(2);
    if x1==0
        x1=1;
    end
    if y1==0
        y1=1;
    end 
    x2 = xy2(1);
    y2 = xy2(2);
    if x2==0
        x2=1;
    end   
    if y2==0
        y2=1;
    end  
    if x1 == x2
        b1 = 1;
    else
        b1 = x2 - x1;
    end
    if y1 == y2
        b2 = 1;
    else
        b2 = y2 - y1;
    end
    ln_condn1 = 0.55/abs(b2);
    ln_condn2 = 0.55/abs(b1);
    is_wall   = false; % obstacle presence

    if x1 <= x2 % define incremental or decremental cycle of x
        xarray = x1:1:x2;
    else
        xarray = x1:-1:x2;
    end

    if y1 <= y2 % define incremental or decremental cycle of y
        yarray = y1:1:y2;
    else
        yarray = y1:-1:y2;
    end

    %% Main part
    for x = xarray % main cycle
        for y = yarray
            a1 = x - x1;
            a2 = y - y1;
            diff = abs(a1/b1 - a2/b2); % line equation
            if x1 == x2 || y1 == y2
                diff = 0;
            end
            if diff < ln_condn1 || diff < ln_condn2 || diff == 0 % if the point lies near the line
                if image(y, x) == 0 % if the pixel is black
                    is_wall = true;
                    return;
                end
            end
        end
    end
end
    end
end