classdef MadgwickAHRS < handle
%% Author:
%  Feliks Sirenko, sirenkofelix@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  21/05/17
%% Class definition
%MadgwickAHRS Implements Madgwick filter
%% Public properties
    properties (Access = public)
        SamplePeriod = 1/256;       % sample period
        Quaternion = [1 0 0 0];     % output quaternion describing the Earth relative to the sensor
        Beta = 1;               	% algorithm gain
    end
    %% Public methods
    methods (Access = public)
        %% Object Initializer
        function obj = MadgwickAHRS(varargin)
            for i = 1:2:nargin
                if  strcmp(varargin{i}, 'SamplePeriod'), obj.SamplePeriod = varargin{i+1};
                elseif  strcmp(varargin{i}, 'Quaternion'), obj.Quaternion = varargin{i+1};
                elseif  strcmp(varargin{i}, 'Beta'), obj.Beta = varargin{i+1};
                else error('Invalid argument');
                end
            end;
        end
    end
    methods (Access = public)
        function obj = fUpdateIMU(obj, Gyroscope, Accelerometer)
            %% fUPDATEIMU - function who evaluates array orientation quaternions in local or global CS (depends on )
            % INPUTS:
            % Gyroscope = [gx gy gz] - raw gyroscope readings
            % Accelerometer = [ax ay az] - raw accelerometer readings
            % OUTPUTS:            
            % obj.Quaternion - object property, who must be accessed only after the function work is over
            % REQUIREMENTS:
            % funcQuaternProd (quaternion1, quaterion2);
           
            q = obj.Quaternion; % short name local variable for readability

            % Normalise accelerometer measurement
            if(norm(Accelerometer) == 0), return; end	% handle NaN
            Accelerometer = Accelerometer / norm(Accelerometer);	% normalise magnitude

            % Gradient decent algorithm corrective step
            F = [2*(q(2)*q(4) - q(1)*q(3)) - Accelerometer(1)
                2*(q(1)*q(2) + q(3)*q(4)) - Accelerometer(2)
                2*(0.5 - q(2)^2 - q(3)^2) - Accelerometer(3)];
            J = [-2*q(3),	2*q(4),    -2*q(1),	2*q(2)
                2*q(2),     2*q(1),     2*q(4),	2*q(3)
                0,         -4*q(2),    -4*q(3),	0    ];
            step = (J'*F);
            step = step / norm(step);	% normalise step magnitude

            % Compute rate of change of quaternion
            qDot = 0.5 * fQuaternProd(q, [0 Gyroscope(1) Gyroscope(2) Gyroscope(3)]) - obj.Beta * step';

            % Integrate to yield quaternion
            q = q + qDot * obj.SamplePeriod;
            obj.Quaternion = q / norm(q); % normalise quaternion
        end
    end
end

%%
function ab = fQuaternProd(a, b)
%% QUATERNPROD Calculated the production of 2 quaternions
%   a - quaternion a = [0 0 0 0];
%   b - quaterion b = [0 0 0 0];
 ab(:,1) = a(:,1).*b(:,1) -a(:,2).*b(:,2) -a(:,3).*b(:,3) - a(:,4).*b(:,4);
 ab(:,2) = a(:,1).*b(:,2)+a(:,2).*b(:,1)+a(:,3).*b(:,4) - a(:,4).*b(: ,3);
 ab(:,3) = a(:,1).*b(:,3) - a(:,2).*b(:,4)+a(:,3).*b(:,1)+a(:,4).*b(:,2);
 ab(:,4) = a(:,1).*b(:,4)+a(:,2).*b(:,3) - a(:,3).*b(:,2)+a(:,4).*b(:,1);
end