function angleCorrected = fRoughFilterDegMod(angle)
%%ROUGHFILTER Filters that prevents the quaternion instability ("leaps") within the region near 0, 90, 180, 270, 360 degrees
%   INPUT:
%   angle = [pitch, roll, yaw] - array of raw angles
%   OUTPUT:
%   angleCorrected = filtered array of angles
    leapX = 0;
    leapY = 0;
    leapZ = 0;
    
    angleCorrected(1,1) = angle(1,1);
    angleCorrected(1,1) = angle(1,1);
    angleCorrected(1,1) = angle(1,1);

        for i = 2 : length(angle)
                
            if  angle(i,1) - angle(i-1,1) > 10 || angle(i,1) - angle(i-1,1) < - 10               % leap or dip
                leapX = angle(i,1) - angleCorrected(i - 1,1);
            end          
            angleCorrected(i,1) = angle(i,1) - leapX;

            if  angle(i , 2) - angle(i-1 , 2) > 10 || angle(i , 2) - angle(i-1 , 2) < - 10               % leap or dip
                leapY = angle(i , 2) - angleCorrected(i - 1 , 2);
            end          
            angleCorrected(i , 2) = angle(i , 2) - leapY;
                
            if  angle(i , 3) - angle(i-1 , 3) > 10 || angle(i , 3) - angle(i-1 , 3) < - 10               % leap or dip
                leapZ = angle(i , 3) - angleCorrected(i - 1 , 3);
            end          
            angleCorrected(i , 3) = angle(i , 3) - leapZ;
        end
end