classdef ParticleFilter
%% Author:
%  Feliks Sirenko, sirenkofelix@gmail.com
%% Owner:
%  IT-Jim Company
%% Last update:
%  21/05/17
%% Class definition
%ParticleFilter Implements different functions used for particle filtration
  
    methods(Access = public)
        %% Object Initializer
         function obj = ParticleFilter(varargin)
         end
    end
    
    methods(Access = public)
        function [xy, xy_prev] = fCheckPixels(~, xy , xy_prev , pix, map )
        %% fCHECKPIXELS Checks the correctness of the position in current and previous moment of time in pixels 
          % to be sure that they are not outside the map 
        % INPUTS:
        % xy[x y] - current value ( in pixels),
        % xy_prev[x y] - previous iteration (in pixels),
        % pix - pixel size
        % OUTPUTS:
        % xy[x y] - corrected position in current moment of time (in pixels),
        % xy_prev[x y] -  corrected position in previous moment of time (in pixels)
        % all x - must be positive, y - must be negative 
            if xy_prev(1) < 0
                xy_prev(1) = pix;
            end
            if xy_prev(2) > 0
                xy_prev(2) = - pix;
            end
            
            if xy(1) < 0
                xy(1) = pix;
            end
            if xy(2) > 0
                xy(2) = - pix;
            end
            if xy_prev(1) / pix > size(map, 2) 
                xy_prev(1) = size(map, 2) * pix
            end
            if -xy_prev(2) / pix > size(map, 1)
                xy_prev(2) = -size(map, 1) * pix;
            end
            if xy(1) / pix > size(map, 2) 
                xy(1) = size(map, 2) * pix
            end
            if -xy(2) / pix > size(map, 1)
                xy(2) = -size(map, 1) * pix;
            end
 
        end
        
        function w_norm = fGetWeights(~, xy_prev, xy, pix, cov, z, office_map, method, av)
        %% fGETWEIGHTS Calculates the weights of particles
        % INPUTS:
        % xy[x y] - current value ( in pixels),
        % xy_prev[x y] - previous iteration (in pixels),
        % pix - pixel size
        % cov = [cov(x) cov(y)] - covariance matrix of the model (IMU)
        % z = [zx, zy] - measurements in current moment of time (beacon readings)
        % office_map - map of the office (binary array)    
        % OUTPUTS:
        % w_norm[] - weights of all particles 
        % REQUIREMENTS:
        % fcheck_obst(map, xy[x -y), xy_prev[x, -y]);       
            if ~fCheck_obst(office_map , round([xy_prev(1) -xy_prev(2)]/pix) , round([xy(1,1) -xy(1,2)]/pix) )
                if strcmp(method,'IMU_BEAC')
                    w(1, :) = (1 ./ sqrt(2 .* pi .* cov(1,:))) .* exp(-(z(1,:) - xy(1,:)).^2 ./ (2 .* cov(1,:)));
                elseif strcmp(method,'IMU')
                    w(1, :) = (1 ./ sqrt(2 .* pi .* cov(1,:))) .* exp(-(av(1,:) - xy(1,:)).^2 ./ (2 .* cov(1,:)));
                end
            else
                w (1,:) = [0 0];
            end
            w_norm = w(1, 1) .* w(1, 2);
        end
     
        function [coordNew, weightNew] = fResample (~, coord, weight, numElem)
        %% fRESAMPLES Resamples the particles who survived in the current iteration to form a new generation
         % of the same size from a new one iteration. Resampling is made
         % according to the Resampling wheel
        % INPUTS:
        % coord[x,y] - coordinates of all particles ( in pixels),
        % weight[] - weights of all particles,
        % numElem - number of particles
        % OUTPUTS:
        % coord[x,y] - coordinates of particles after resampling wheel ( in pixels),
        % NOTE The particles with the highest weight get in this array,
        % some of the particles with the highest weight may get in more
        % than once
        % weightNew[] - weights of the survieved particles from the array weight[]          
            coordNew = zeros(numel(weight),2);
            weightNew = zeros(numel(weight),1);
            k = 1;
            
            index = randi([1 numElem],1,1);
            betta = 0;
            for i=1:numel(weight)
                betta = betta + 2 * max(weight)*rand(1,1);
                while betta > weight(index)
            		betta = betta - weight(index);
                    if (index > numElem - 1)
                        index = 1;
                    end
            		index = index + 1;
                end
                coordNew(k,:) = coord(index,:);
                weightNew(k) = weight(index);
                k = k + 1;
            end
        end
    end     
end

