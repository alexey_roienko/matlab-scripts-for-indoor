%% ========== Setting ========== %%
clear all;
close all;
clc;
addpath('IMU');
addpath('Res');
addpath('Map');
%% ============== Pathes  ============== %%
%
% 2017-05-22_11-32-22 IT-GYM
    ac_1 = strcat(pwd,'\Res\2017-05-22_11-32-22_iOS\2017-05-22_11-32-22_iOS_accelerometer.json');
    logger_1  = strcat(pwd,'\Res\2017-05-22_11-32-22_iOS\2017-05-22_11-32-22_iOS_angles.json');

    set = [0 -0.9 0];
    step_adj = 4.5;

    show = imread ('IT_GYM_Office.png');
    mask  = imread('mask_IT_GYM.png');
    masktable = load('masktable_IT_GYM.out');
    pixelSize = 0.007;
%     Information
%  6-15 sec - 12 steps 
% 16-23 sec -  9 steps
% 28-36 sec - 10 steps
% 38-46 sec - 12 steps
%}
%{
% 2017-05-26_10-12-38 IT-GYM
    ac_1 = strcat(pwd,'\Res\2017-05-26_10-12-38_iOS\2017-05-26_10-12-38_iOS_accelerometer.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_10-12-38_iOS\2017-05-26_10-12-38_iOS_angles.json');
    
    set = [0 -0.3 0];
    step_adj = 2.2;
    
    show = imread ('IT_GYM_Office.png');
    mask  = imread('mask_IT_GYM.png');
    masktable = load('masktable_IT_GYM.out');
    pixelSize = 0.007;
%   Information
%  1-9 sec - 10 steps
% 10-15 sec - 8 steps
% 20-23 sec - 4 steps
% 26-29 sec - 4 steps
% 34-37 sec - 4 steps
% 41-44 sec - 6 steps
% 48-53 sec - 7 steps
% 55-62 sec - 10 steps
%}
%{
% 2017-05-26_10-14-20 IT-GYM
    ac_1 = strcat(pwd,'\Res\2017-05-26_10-14-20_iOS\2017-05-26_10-14-20_iOS_accelerometer.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_10-14-20_iOS\2017-05-26_10-14-20_iOS_angles.json'); 
        
    set = [0 -0.6 0];
    step_adj = 2.5;
        
    show = imread ('IT_GYM_Office.png');
    mask  = imread('mask_IT_GYM.png');
    masktable = load('masktable_IT_GYM.out');
    pixelSize = 0.007;
%      Information
%  3-7 sec - 5 steps
% 11-16 sec - 2 steps
% 20-24 sec - 7 steps
% 31-34 sec - 7 steps 
% 36-42 sec - 8 steps
% 45-51 sec - 8 steps
%}
%{
 %2017-05-26_10-15-57 IT-GYM
    ac_1 = strcat(pwd,'\Res\2017-05-26_10-15-57_iOS\2017-05-26_10-15-57_iOS_accelerometer.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_10-15-57_iOS\2017-05-26_10-15-57_iOS_angles.json');

    set = [7 -0.5 0];
    step_adj = 1.5;

    show = imread ('IT_GYM_Office.png');
    mask  = imread('mask_IT_GYM.png');
    masktable = load('masktable_IT_GYM.out');
    pixelSize = 0.007;
%      Information
%  1- 9 sec - 10 steps
% 15-18 sec - 5 steps
% 22-25 sec - 5 steps
%}
%{
% 2017-05-26_10-17-06 IT-GYM
    ac_1 = strcat(pwd,'\Res\2017-05-26_10-17-06_iOS\2017-05-26_10-17-06_iOS_accelerometer.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_10-17-06_iOS\2017-05-26_10-17-06_iOS_angles.json');
    
    set = [7.5 -5.5 0];
    step_adj = 2.1;
    
    show = imread ('IT_GYM_Office.png');
    mask  = imread('mask_IT_GYM.png');
    masktable = load('masktable_IT_GYM.out');
    pixelSize = 0.007;
%   Information
% 1- 6 sec - 9 steps
% 9-14 sec - 10 steps
%}
%{
% 2017-05-26_08-05-46 DEVS
    ac_1 = strcat(pwd,'\Res\2017-05-26_08-05-46_iOS_DEV\2017-05-26_08-05-46_iOS_accelerometer.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_08-05-46_iOS_DEV\2017-05-26_08-05-46_iOS_angles.json');

    set = [0.5 -10.3 0];
    step_adj = 5;

    pixelSize = 0.0062;
    show = imread ('KAA SOLUTIONS_Office.png');
    mask  = imread('mask_KAA SOLUTIONS.png');
    masktable = load('masktable_KAA_Solutions.out');
%       INFORMATION
%  6-13 sec - 11 steps
% 14-18 sec -  7 steps
%}
%{
% 2017-05-26_08-06-59 DEVS
    ac_1 = strcat(pwd,'\Res\2017-05-26_08-06-59_iOS_DEV\2017-05-26_08-06-59_iOS_accelerometer.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_08-06-59_iOS_DEV\2017-05-26_08-06-59_iOS_angles.json');
    
    set = [5.5 -1 0];
    step_adj = 3;
    
    show = imread ('KAA SOLUTIONS_Office.png');
    mask  = imread('mask_KAA SOLUTIONS.png');
    masktable = load('masktable_KAA_Solutions.out');
    pixelSize = 0.0062;
%    INFORMATION
%   6-11 sec - 8 steps
%  13-20 sec - 11 steps
%}
%{
% 2017-05-26_08-08-18 DEVS
    ac_1 = strcat(pwd,'\Res\2017-05-26_08-08-18_iOS_DEV\2017-05-26_08-08-18_iOS_accelerometer.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_08-08-18_iOS_DEV\2017-05-26_08-08-18_iOS_angles.json');

    set = [4 -11.3 0];
    step_adj = 4;

    show = imread ('KAA SOLUTIONS_Office.png');
    mask  = imread('mask_KAA SOLUTIONS.png');
    masktable = load('masktable_KAA_Solutions.out');
    pixelSize = 0.0062;
% INFORMATION
%  5-10 - 6 steps
% 10-16 - 9 steps
%}
%{
% 2017-05-26_08-09-13 DEVS
    ac_1 = strcat(pwd,'\Res\2017-05-26_08-09-13_iOS_DEV\2017-05-26_08-09-13_iOS_accelerometer.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_08-09-13_iOS_DEV\2017-05-26_08-09-13_iOS_angles.json');

    set = [0.5 -0.5 0];
    step_adj = 2.0;

    show = imread ('KAA SOLUTIONS_Office.png');
    mask  = imread('mask_KAA SOLUTIONS.png');
    masktable = load('masktable_KAA_Solutions.out');
    pixelSize = 0.0062;
%   INFORMATION
%  6-11 - 9 steps
% 12-16 - 7 steps
%}
%{
% 2017-05-26_08-11-01 DEVS
    ac_1 = strcat(pwd,'\Res\2017-05-26_08-11-01_iOS_DEV\2017-05-26_08-11-01_iOS_accelerometer.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_08-11-01_iOS_DEV\2017-05-26_08-11-01_iOS_angles.json');
    
    set = [0.5 -0.5 0];
    step_adj = 2.0;    
    
    show = imread ('KAA SOLUTIONS_Office.png');
    mask  = imread('mask_KAA SOLUTIONS.png');
    masktable = load('masktable_KAA_Solutions.out');
    pixelSize = 0.0062;
% INFORMATION
% STATIONARY
%}
%{
% 2017-05-26_08-12-39 DEVS
    ac_1 = strcat(pwd,'\Res\2017-05-26_08-12-39_iOS_DEV\2017-05-26_08-12-39_iOS_accelerometer.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_08-12-39_iOS_DEV\2017-05-26_08-12-39_iOS_angles.json');

    set = [0.5 -0.5 0];
    step_adj = 2.0;

    show = imread ('KAA SOLUTIONS_Office.png');
    mask  = imread('mask_KAA SOLUTIONS.png');
    masktable = load('masktable_KAA_Solutions.out');

% INFORMATION
% STATIONARY
%}
%{
% 2017-05-26_08-15-23 DEVS
    ac_1 = strcat(pwd,'\Res\2017-05-26_08-15-23_iOS_DEV\2017-05-26_08-15-23_iOS_accelerometer.json');
	logger_1  = strcat(pwd,'\Res\2017-05-26_08-15-23_iOS_DEV\2017-05-26_08-15-23_iOS_angles.json');

    set = [0.5 -0.5 0];
    step_adj = 2.0;

    show = imread ('KAA SOLUTIONS_Office.png');
    mask  = imread('mask_KAA SOLUTIONS.png');
    masktable = load('masktable_KAA_Solutions.out');
    pixelSize = 0.0062;
% INFORMATION
% STATIONARY
%} 
%% ========== Settings ========== %%
settings.meshSteps = [0.3 0.3];
settings.samplePeriod = 0.033;
settings.t_show = 0;
settings.stationaryIndicator = 1.035;
settings.cutoffLow = 2.2;
settings.cutoffHigh = 0.8;
settings.MapOrientationAngle = 20;
%% ========== Initializations ========== %%
pos_prev = set;
posprev = set;
objPlotter = Plotter();
objReader = Reader('accelPath', ac_1, 'loggerPath', logger_1, 'isPrintable', 0);
objStep = Step('samplePeriod', settings.samplePeriod , 'cutoffHigh', settings.cutoffHigh , 'cutoffLow', settings.cutoffLow , 'stationaryIndicator', settings.stationaryIndicator);
objConvertor = Convertor();
%% ========== Plotting Map ========== %%
objPlotter.fPlotRoomPixels(show, ac_1);
%% ========== Read Logs ========== %%
objReader.fReadAccel();
accel = objReader.accel;

objReader.fReadAngles();
anglesLogger = objReader.anglesLogger;

%% GENERAL CYCLE %%
for t = 2:length(accel) 
%% ========== Step Detection ========== %%
    if (t > 24)
        stationary_ar = objStep.fDetectSteps(accel( t-24 : t));
        stationary = stationary_ar(end);
    else
        stationary = 1;
    end
%% ========== AHRS Correction ========== %%  
    objConvertor.angles = [[anglesLogger(t).x]; [anglesLogger(t).y]; -[anglesLogger(t).z] - 180 + settings.MapOrientationAngle]';    
%% ========== From local to Global ========== %%
    if (t > 24)
        [b, a] = butter(8, (2 * settings.cutoffLow)/(1/settings.samplePeriod), 'low' );
        accelX = filtfilt(b, a, [accel( t-24 : t).x]);
        accelY = filtfilt(b, a, [accel( t-24 : t).y]);
        accelZ = filtfilt(b, a, [accel( t-24 : t).z]);
        accelX = accelX(end);
        accelY = accelY(end);
        accelZ = accelZ(end);
    else
        accelX = accel(t).x;
        accelY = accel(t).y;
        accelZ = accel(t).z;        
    end
    
    objConvertor.quaternion = objConvertor.fGetQuaterionFromAngles();
    accGlobal = objConvertor.fFromLocalToGlobal([accelX; accelY; accelZ]');

    %% ========== ZUPT ========== %%
        if stationary == 1 
            vel = [0 0 0];
        else
            vel(1,1) = vel(1,1) + 9.8 * accGlobal(1) * settings.samplePeriod;
            vel(1,2) = vel(1,2) + 9.8 * accGlobal(2) * settings.samplePeriod;
            vel(1,3) = vel(1,3) + 9.8 * accGlobal(3) * settings.samplePeriod;
        end  
%%  ========== TRACKER ========== %%
    pos = pos_prev + step_adj .* vel * settings.samplePeriod;
    coordInPixels = Checker.fCorrectIfIsNotOnMap(mask, ceil([pos(1), -pos(2)] / pixelSize));
    pos(1) = coordInPixels(1) * pixelSize; pos(2) = - coordInPixels(2) * pixelSize;

    if Checker.fCheck_obst(mask, ceil([pos_prev(1) -pos_prev(2)] / pixelSize), ceil([ pos(1) , -pos(2)] / pixelSize))
        pos(1) = pos_prev(1);
        pos(2) = pos_prev(2) + step_adj * vel(2) * settings.samplePeriod;
        coordInPixels = Checker.fCorrectIfIsNotOnMap(mask, ceil([pos(1), -pos(2)] / pixelSize));
        pos(1) = coordInPixels(1) * pixelSize; pos(2) = -coordInPixels(2) * pixelSize;
    	if Checker.fCheck_obst(mask, ceil([pos_prev(1) -pos_prev(2)]  / pixelSize), ceil([ pos(1) , -pos(2)] / pixelSize))
            pos(1) = pos_prev(1) + step_adj * vel(1) * settings.samplePeriod;
            pos(2) = pos_prev(2);
            coordInPixels = Checker.fCorrectIfIsNotOnMap(mask, ceil([pos(1), -pos(2)] / pixelSize));
            pos(1) = coordInPixels(1) * pixelSize; pos(2) = - coordInPixels(2) * pixelSize;
            if Checker.fCheck_obst(mask, ceil([pos_prev(1) -pos_prev(2)]  / pixelSize), ceil([ pos(1) , -pos(2)] / pixelSize))
                pos = pos_prev + step_adj .* vel * settings.samplePeriod;                        % integrate velocity to yield position
                coordInPixels = Checker.fCorrectIfIsNotOnMap(mask, ceil([pos(1), -pos(2)] / pixelSize));
                pos(1) = coordInPixels(1) * pixelSize; pos(2) = - coordInPixels(2) * pixelSize;
            end
        end
    end
    %}
   if settings.t_show - accel(t).timestamp < 0
    coordInMeters = Checker.fMeshCorrector([pos(1) -pos(2)], settings.meshSteps, size(mask,1) * pixelSize, masktable);
    pos(1) = coordInMeters(1); pos(2) = - coordInMeters(2);
    objPlotter.fPlotSimpleApproach([pos(1) -pos(2)]/pixelSize, [posprev(1) -posprev(2)]/pixelSize);
    posprev = pos;
    settings.t_show = settings.t_show + 1;
    end
    pos_prev = pos;    
% ======================================================================= %
end



