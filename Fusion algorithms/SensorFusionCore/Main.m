clear all;
close all;
clc;
addpath('IMU');
addpath('Res');
addpath('Map');
%% Initializations
objPlotter = Plotter();
MapOrientationAngle = 20;
meshSteps = [0.3 0.3];


%% ============== Pathes  ============== %%
%{
% 2017-05-22_11-32-22 IT-GYM
    ac_1 = strcat(pwd,'\Res\2017-05-22_11-32-22_iOS\2017-05-22_11-32-22_iOS_accelerometer.json');
    gyr_1  = strcat(pwd,'\Res\2017-05-22_11-32-22_iOS\2017-05-22_11-32-22_iOS_gyroscope.json');
    logger_1  = strcat(pwd,'\Res\2017-05-22_11-32-22_iOS\2017-05-22_11-32-22_iOS_angles.json');

    X_coord_prev = 0; Y_coord_prev = -0.9; accelerationX = 0; accelerationY = 0;
    stepLength = 0.6;

    set = [0 -0.9 0];
    step_adj = 4;

    show = imread ('IT_GYM_Office.png');
    mask  = imread('mask_IT_GYM.png');
    masktable = load('masktable_IT_GYM.out');
    pixelSize = 0.007;
%     Information
%  6-15 sec - 12 steps 
% 16-23 sec -  9 steps
% 28-36 sec - 10 steps
% 38-46 sec - 12 steps
%}
%
% 2017-05-26_10-12-38 IT-GYM
    ac_1 = strcat(pwd,'\Res\2017-05-26_10-12-38_iOS\2017-05-26_10-12-38_iOS_accelerometer.json');
    gyr_1  = strcat(pwd,'\Res\2017-05-26_10-12-38_iOS\2017-05-26_10-12-38_iOS_gyroscope.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_10-12-38_iOS\2017-05-26_10-12-38_iOS_angles.json');
    
    X_coord_prev = 0; Y_coord_prev = -0.3; accelerationX = 0; accelerationY = 0;
    stepLength = 0.6;
    
    set = [0 -0.3 0];
    step_adj = 2;
    
    show = imread ('IT_GYM_Office.png');
    mask  = imread('mask_IT_GYM.png');
    masktable = load('masktable_IT_GYM.out');
    pixelSize = 0.007;
%   Information
%  1-9 sec - 10 steps
% 10-15 sec - 8 steps
% 20-23 sec - 4 steps
% 26-29 sec - 4 steps
% 34-37 sec - 4 steps
% 41-44 sec - 6 steps
% 48-53 sec - 7 steps
% 55-62 sec - 10 steps
%}
%{
% 2017-05-26_10-14-20 IT-GYM
    ac_1 = strcat(pwd,'\Res\2017-05-26_10-14-20_iOS\2017-05-26_10-14-20_iOS_accelerometer.json');
    gyr_1  = strcat(pwd,'\Res\2017-05-26_10-14-20_iOS\2017-05-26_10-14-20_iOS_gyroscope.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_10-14-20_iOS\2017-05-26_10-14-20_iOS_angles.json'); 
    
    X_coord_prev = 0; Y_coord_prev = -0.6; accelerationX = 0; accelerationY = 0;
    stepLength = 0.6;
        
    set = [0 -0.6 0];
    step_adj = 2.5;
        
    show = imread ('IT_GYM_Office.png');
    mask  = imread('mask_IT_GYM.png');
    masktable = load('masktable_IT_GYM.out');
    pixelSize = 0.007;
%      Information
%  3-7 sec - 5 steps
% 11-16 sec - 2 steps
% 20-24 sec - 7 steps
% 31-34 sec - 7 steps 
% 36-42 sec - 8 steps
% 45-51 sec - 8 steps
%}
%{
 %2017-05-26_10-15-57 IT-GYM
    ac_1 = strcat(pwd,'\Res\2017-05-26_10-15-57_iOS\2017-05-26_10-15-57_iOS_accelerometer.json');
    gyr_1  = strcat(pwd,'\Res\2017-05-26_10-15-57_iOS\2017-05-26_10-15-57_iOS_gyroscope.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_10-15-57_iOS\2017-05-26_10-15-57_iOS_angles.json');

    X_coord_prev = 7; Y_coord_prev = -0.5; accelerationX = 0; accelerationY = 0;
    stepLength = 0.6;

    set = [7 -0.5 0];
    step_adj = 1.5;

    show = imread ('IT_GYM_Office.png');
    mask  = imread('mask_IT_GYM.png');
    masktable = load('masktable_IT_GYM.out');
    pixelSize = 0.007;
%      Information
%  1- 9 sec - 10 steps
% 15-18 sec - 5 steps
% 22-25 sec - 5 steps
%}
%{
% 2017-05-26_10-17-06 IT-GYM
    ac_1 = strcat(pwd,'\Res\2017-05-26_10-17-06_iOS\2017-05-26_10-17-06_iOS_accelerometer.json');
    gyr_1  = strcat(pwd,'\Res\2017-05-26_10-17-06_iOS\2017-05-26_10-17-06_iOS_gyroscope.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_10-17-06_iOS\2017-05-26_10-17-06_iOS_angles.json');
    
    X_coord_prev = 7.5; Y_coord_prev = -5.5; accelerationX = 0; accelerationY = 0;
    stepLength = 0.6;
        
    set = [7.5 -5.5 0];
    step_adj = 1.9;
    
    show = imread ('IT_GYM_Office.png');
    mask  = imread('mask_IT_GYM.png');
    masktable = load('masktable_IT_GYM.out');
    pixelSize = 0.007;
%   Information
% 1- 6 sec - 9 steps
% 9-14 sec - 10 steps
%}
%{
% 2017-05-26_08-05-46 DEVS
    ac_1 = strcat(pwd,'\Res\2017-05-26_08-05-46_iOS_DEV\2017-05-26_08-05-46_iOS_accelerometer.json');
    gyr_1  = strcat(pwd,'\Res\2017-05-26_08-05-46_iOS_DEV\2017-05-26_08-05-46_iOS_gyroscope.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_08-05-46_iOS_DEV\2017-05-26_08-05-46_iOS_angles.json');

    X_coord_prev = 0.5; Y_coord_prev = -10.3; accelerationX = 0; accelerationY = 0;
    stepLength = 0.8;

    set = [0.5 -10.3 0];
    step_adj = 2.8;

    pixelSize = 0.0062;
    show = imread ('KAA SOLUTIONS_Office.png');
    mask  = imread('mask_KAA SOLUTIONS.png');
    masktable = load('masktable_KAA_Solutions.out');
%       INFORMATION
%  6-13 sec - 11 steps
% 14-18 sec -  7 steps
%}
%{
% 2017-05-26_08-06-59 DEVS
    ac_1 = strcat(pwd,'\Res\2017-05-26_08-06-59_iOS_DEV\2017-05-26_08-06-59_iOS_accelerometer.json');
    gyr_1  = strcat(pwd,'\Res\2017-05-26_08-06-59_iOS_DEV\2017-05-26_08-06-59_iOS_gyroscope.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_08-06-59_iOS_DEV\2017-05-26_08-06-59_iOS_angles.json');
    
    X_coord_prev = 5.5; Y_coord_prev = -1; accelerationX = 0; accelerationY = 0;
    stepLength = 0.6;

    set = [5.5 -1 0];
    step_adj = 2.6;
    
    show = imread ('KAA SOLUTIONS_Office.png');
    mask  = imread('mask_KAA SOLUTIONS.png');
    masktable = load('masktable_KAA_Solutions.out');
    pixelSize = 0.0062;
%    INFORMATION
%   6-11 sec - 8 steps
%  13-20 sec - 11 steps
%}
%{
% 2017-05-26_08-08-18 DEVS
    ac_1 = strcat(pwd,'\Res\2017-05-26_08-08-18_iOS_DEV\2017-05-26_08-08-18_iOS_accelerometer.json');
    gyr_1  = strcat(pwd,'\Res\2017-05-26_08-08-18_iOS_DEV\2017-05-26_08-08-18_iOS_gyroscope.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_08-08-18_iOS_DEV\2017-05-26_08-08-18_iOS_angles.json');

    X_coord_prev = 4; Y_coord_prev = -11.3; accelerationX = 0; accelerationY = 0;
    stepLength = 0.9;

    set = [4 -11.3 0];
    step_adj = 2.7;

    show = imread ('KAA SOLUTIONS_Office.png');
    mask  = imread('mask_KAA SOLUTIONS.png');
    masktable = load('masktable_KAA_Solutions.out');
    pixelSize = 0.0062;
% INFORMATION
%  5-10 - 6 steps
% 10-16 - 9 steps
%}
%{
% 2017-05-26_08-09-13 DEVS
    ac_1 = strcat(pwd,'\Res\2017-05-26_08-09-13_iOS_DEV\2017-05-26_08-09-13_iOS_accelerometer.json');
    gyr_1  = strcat(pwd,'\Res\2017-05-26_08-09-13_iOS_DEV\2017-05-26_08-09-13_iOS_gyroscope.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_08-09-13_iOS_DEV\2017-05-26_08-09-13_iOS_angles.json');

    X_coord_prev = 0.5; Y_coord_prev = -0.5; accelerationX = 0; accelerationY = 0;
    stepLength = 0.6;

    set = [0.5 -0.5 0];
    step_adj = 2.0;

    show = imread ('KAA SOLUTIONS_Office.png');
    mask  = imread('mask_KAA SOLUTIONS.png');
    masktable = load('masktable_KAA_Solutions.out');
    pixelSize = 0.0062;
%   INFORMATION
%  6-11 - 9 steps
% 12-16 - 7 steps
%}
%{
% 2017-05-26_08-11-01 DEVS
    ac_1 = strcat(pwd,'\Res\2017-05-26_08-11-01_iOS_DEV\2017-05-26_08-11-01_iOS_accelerometer.json');
    gyr_1  = strcat(pwd,'\Res\2017-05-26_08-11-01_iOS_DEV\2017-05-26_08-11-01_iOS_gyroscope.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_08-11-01_iOS_DEV\2017-05-26_08-11-01_iOS_angles.json');
    
    X_coord_prev = 0.5; Y_coord_prev = -0.5; accelerationX = 0; accelerationY = 0;
    stepLength = 0.6;
    
    set = [0.5 -0.5 0];
    step_adj = 2.0;    
    
    show = imread ('KAA SOLUTIONS_Office.png');
    mask  = imread('mask_KAA SOLUTIONS.png');
    masktable = load('masktable_KAA_Solutions.out');
    pixelSize = 0.0062;
% INFORMATION
% STATIONARY
%}
%{
% 2017-05-26_08-12-39 DEVS
    ac_1 = strcat(pwd,'\Res\2017-05-26_08-12-39_iOS_DEV\2017-05-26_08-12-39_iOS_accelerometer.json');
    gyr_1  = strcat(pwd,'\Res\2017-05-26_08-12-39_iOS_DEV\2017-05-26_08-12-39_iOS_gyroscope.json');
    logger_1  = strcat(pwd,'\Res\2017-05-26_08-12-39_iOS_DEV\2017-05-26_08-12-39_iOS_angles.json');

    X_coord_prev = 0.5; Y_coord_prev = -0.5; accelerationX = 0; accelerationY = 0;
    stepLength = 0.6;

    set = [0.5 -0.5 0];
    step_adj = 2.0;

    show = imread ('KAA SOLUTIONS_Office.png');
    mask  = imread('mask_KAA SOLUTIONS.png');
    masktable = load('masktable_KAA_Solutions.out');

% INFORMATION
% STATIONARY
%}
%{
% 2017-05-26_08-15-23 DEVS
    ac_1 = strcat(pwd,'\Res\2017-05-26_08-15-23_iOS_DEV\2017-05-26_08-15-23_iOS_accelerometer.json');
    gyr_1  = strcat(pwd,'\Res\2017-05-26_08-15-23_iOS_DEV\2017-05-26_08-15-23_iOS_gyroscope.json');
	logger_1  = strcat(pwd,'\Res\2017-05-26_08-15-23_iOS_DEV\2017-05-26_08-15-23_iOS_angles.json');

    X_coord_prev = 0.5; Y_coord_prev = -0.5; accelerationX = 0; accelerationY = 0;
    stepLength = 0.6;

    set = [0.5 -0.5 0];
    step_adj = 2.0;

    show = imread ('KAA SOLUTIONS_Office.png');
    mask  = imread('mask_KAA SOLUTIONS.png');
    masktable = load('masktable_KAA_Solutions.out');
    pixelSize = 0.0062;
% INFORMATION
% STATIONARY
%} 
%% ============== Reading sensor data ============== %%
objReader = Reader('accelPath', ac_1, 'gyroPath' , gyr_1, 'loggerPath', logger_1);

objReader.isPrintable = 0;
objReader.fReadAccel();
accel = objReader.accel;

objReader.isPrintable = 0; 
objReader.fReadGyro();
gyro = objReader.gyro;

objReader.isPrintable = 0;
objReader.fReadAngles();
anglesLogger = objReader.anglesLogger;
%% ============== IMU processing ============== %%
samplePeriod = 0.033;
stationaryIndicator = 0.035;
cutoffLow = 2.2;
cutoffHigh = 0.8;

objStep = Step('samplePeriod', samplePeriod , 'cutoffHigh', cutoffHigh , 'cutoffLow', cutoffLow , 'stationaryIndicator', stationaryIndicator);
stationary = (objStep.fDetectSteps(accel))';

%% TEST
[b, a] = butter(8, (2 * cutoffLow)/(1/samplePeriod), 'low' );
accelX = filtfilt(b, a, [accel.x]);
accelY = filtfilt(b, a, [accel.y]);
accelZ = filtfilt(b, a, [accel.z]);
%{
                 figure('units','normalized','outerposition',[0 0 1 1])
                 subplot(3,1,1)
                 plot([accel.timestamp], accelX', 'r', [accel.timestamp], [accel.x], 'g' );
                 title('AX filtered (time domain)' );
                 grid on;
                 grid minor;
                 subplot(3,1,2)
                 plot([accel.timestamp], accelY', 'r', [accel.timestamp], [accel.y], 'g'  );
                 title('AY filtered (time domain)' );
                 grid on;
                 grid minor;
                 subplot(3,1,3)
                 plot([accel.timestamp], accelZ', 'r', [accel.timestamp], [accel.z], 'g' );
                 title('AZ filtered (time domain)' );
                 grid on;
                 grid minor;
                 xlabel('Time (s)' );
                 ylabel('Acceleration (g)' );
                 legend('X' , 'Y' , 'Z' );
                 grid on;
                 grid minor;
                 pause;
                 close;
                 
                 figure('units','normalized','outerposition',[0 0 1 1])
                 delta_f = 1 / (length(accel) * samplePeriod);
                 f = 0: delta_f: (1/samplePeriod - delta_f);
                 subplot(3,1,1)
                 plot (f, abs(fft([gyro.x])) / length(gyro), 'r', 'LineWidth' , 2);                
                 title('\omega X (frequency domain)' );
                 grid on;
                 grid minor;
                 xlabel('Frequency (Hz)' );
                 ylabel('Magnitude (g)' );
                 
                 subplot(3,1,2)
                 plot (f, abs(fft([gyro.y])) / length(gyro), 'r', 'LineWidth' , 2);                
                 title('\omega Y (frequency domain)' );
                 grid on;
                 grid minor;
                 xlabel('Frequency (Hz)' );
                 ylabel('Magnitude (g)' );
                 
                 subplot(3,1,3)
                 plot (f, abs(fft([gyro.z])) / length(gyro), 'r', 'LineWidth' , 2);                
                 title('\omega Z (frequency domain)' );
                 grid on;
                 grid minor;
                 xlabel('Frequency (Hz)' );
                 ylabel('Magnitude (g)' );

                 pause;
                 close;            
%}             

[b, a] = butter(8, (2 * cutoffLow)/(1/samplePeriod), 'low' );
gyroX = filtfilt(b, a, [gyro.x]);
gyroY = filtfilt(b, a, [gyro.y]);
gyroZ = filtfilt(b, a, [gyro.z]);
%{
                 figure('units','normalized','outerposition',[0 0 1 1])
                 subplot(3,1,1)
                 plot([accel.timestamp], gyroX', 'r', [accel.timestamp], [gyro.x], 'g'  );
                 title('\omega X filtered (time domain)' );
                 grid on;
                 grid minor;
                 xlabel('Time (s)' );
                 ylabel('Angular speed (g)' );

                 subplot(3,1,2)
                 plot([accel.timestamp], gyroY', 'r', [accel.timestamp], [gyro.y], 'g'  );
                 title('\omega Y filtered (time domain)' );
                 grid on;
                 grid minor;
                 xlabel('Time (s)' );
                 ylabel('Angular speed (g)' );

                 subplot(3,1,3)
                 plot([accel.timestamp], gyroZ', 'r', [accel.timestamp], [gyro.z], 'g'  );
                 title('\omega Z (time domain)' );                 
                 grid on;
                 grid minor;
                 xlabel('Time (s)' );
                 ylabel('Angular speed (g)' );

                 pause;
                 close;
%}
               
for i = 1 : length (accelX)
    accel(i).x = accelX(i);
    accel(i).y = accelY(i);
    accel(i).z = accelZ(i);
     gyro(i).x = gyroX(i);
    gyro(i).y = gyroY(i);
    gyro(i).z = gyroZ(i);    
end

%% ============== AHRS evaluation ============== %%
euler = zeros(length(accel), 3);

objMadgwickAHRS = MadgwickAHRS('SamplePeriod', samplePeriod, 'Beta', 1);
% Initial adjustment
for t = 1:200
    objMadgwickAHRS.fUpdateIMU ( [gyro(t).x gyro(t).y  gyro(t).z], [accel(t).x accel(t).y accel(t).z] );
end
% For all data
objConvertor = Convertor('quaternion', [0 0 0 0]);
for t = 1:length(accel) 
    if(stationary(t)) 
        objMadgwickAHRS.Beta = 1; 
    else 
        objMadgwickAHRS.Beta = 0; % Follow the gyroscope
    end
    objMadgwickAHRS.fUpdateIMU ( [gyro(t).x gyro(t).y  gyro(t).z], [accel(t).x accel(t).y accel(t).z] );
    objConvertor.quaternion = objMadgwickAHRS.Quaternion;
    euler(t,:) = objConvertor.fGetAnglesFromQuaternion();
end

% Angle correction and filtration
for i = 1 : length(euler)
    euler(i,1) = euler(i,1)+ 180;
    euler(i,2) = -euler(i,2);
    euler(i,3) = euler(i,3) - 140;
end
%euler = fRoughFilterDegMod(euler);
objPlotter.PlotAngles([accel.timestamp], euler, [[anglesLogger.x]; [anglesLogger.y]; -[anglesLogger.z] - 180 + MapOrientationAngle]' );


% USE ANGLES DETERMINED BY MADGWICK METHOD
%objConvertor.angles = euler;
%objConvertor.quaternion = objConvertor.fGetQuaterionFromAngles();
% USE ANGLES DETERMINED BY LOGGER
 objConvertor.angles = [[anglesLogger.x]; [anglesLogger.y]; -[anglesLogger.z] - 180 + MapOrientationAngle]';
 objConvertor.quaternion = objConvertor.fGetQuaterionFromAngles();
%% ============== FROM LOCAL CS TO GLOBAL CS ============== %%
accLocal = [accel.x; accel.y; accel.z]';
accGlobal = objConvertor.fFromLocalToGlobal(accLocal);
accGlobal(:,3) = accGlobal(:,3) - 1;
%objPlotter.fPlotAccelGlobalAndLocal([accel.timestamp], accLocal, accGlobal);
%% ============== ZUPT ============== %%
objZUPT = ZUPT ('accel', accGlobal .* 9.8, 'stat', stationary, 'samplePeriod', samplePeriod);
velGlobal = objZUPT.fGetVelocities( );
% objPlotter.fPlotGlobalVelocity([accel.timestamp], velGlobal)
%% ============== CONSTANT LENGTH UPDATE  ============== %%

%{
objPlotter.fPlotRoomPixels(show, ac_1);
coordInPixels = Checker.fCorrectIfIsNotOnMap(mask, ceil([X_coord_prev, -Y_coord_prev] / pixelSize)); %(Initialization)
X_coord_prev = coordInPixels(1) * pixelSize; Y_coord_prev = - coordInPixels(2) * pixelSize;



for t = 1 : length(accGlobal)
    if stationary(t) == 0
        accelerationX = accelerationX + accGlobal(t,1);
        accelerationY = accelerationY + accGlobal(t,2);
    elseif accelerationX ~= 0 && accelerationY ~= 0      
        accelerationAngle = atan2(accelerationY, accelerationX);
        X_coord = X_coord_prev + stepLength * cos(accelerationAngle);
        Y_coord = Y_coord_prev + stepLength * sin(accelerationAngle);
        
        
        coordInMeters = Checker.fCorrectCoordIfWall(mask, [X_coord_prev, -Y_coord_prev], [X_coord, -Y_coord], pixelSize, masktable, meshSteps )
        
        
%         if fCheck_obst(mask , ceil([X_coord_prev -Y_coord_prev ]/pixelSize) , ceil([X_coord -Y_coord]/pixelSize) )
%             X_coord = X_coord_prev;
%             Y_coord = Y_coord_prev + stepLength * sin(accelerationAngle);
%             if fCheck_obst(mask , ceil([X_coord_prev -Y_coord_prev ]/pixelSize) , ceil([X_coord -Y_coord]/pixelSize) )
%                 X_coord = X_coord_prev + stepLength * cos(accelerationAngle);
%                 Y_coord = Y_coord_prev;
%                if fCheck_obst(mask , ceil([X_coord_prev -Y_coord_prev ]/pixelSize) , ceil([X_coord -Y_coord]/pixelSize) )
%                    accelerationAngle = atan2(accelerationY , accelerationX);
%                    X_coord = X_coord_prev + stepLength * cos(accelerationAngle);
%                    Y_coord = Y_coord_prev + stepLength * sin(accelerationAngle);
%                end
%            end
%        end     
%        objPlotter.fPlotSimpleApproach([X_coord -Y_coord]/pixelSize, [X_coord_prev -Y_coord_prev]/pixelSize);        
%        pause(0.1);
%        X_coord_prev = X_coord;
%        Y_coord_prev = Y_coord;
%        
%        accelerationX = 0;
%        accelerationY = 0;   
%    end
%end
%}

%% ============== Direct trajectory ============== %%
objPlotter.fPlotRoomPixels(show, ac_1);
objParticleFilter = ParticleFilter();
t_show = 0;
 
pos = zeros(size(velGlobal));
pos(1,:) = set;
posprev = set;
coordInPixels = Checker.fCorrectIfIsNotOnMap(mask, ceil([pos(1,1), -pos(1,2)] / pixelSize));
pos(1,1) = coordInPixels(1) * pixelSize; pos(1,2) = - coordInPixels(2) * pixelSize;

for t = 2:length(pos) 
    pos(t,:) = pos(t-1,:) + step_adj .* velGlobal(t,:) * samplePeriod;                        % integrate velocity to yield position
    coordInPixels = Checker.fCorrectIfIsNotOnMap(mask, ceil([pos(t,1), -pos(t,2)] / pixelSize));
    pos(t,1) = coordInPixels(1) * pixelSize; pos(t,2) = - coordInPixels(2) * pixelSize;
    if Checker.fCheck_obst(mask, ceil([pos(t-1,1) -pos(t-1,2)] / pixelSize), ceil([ pos(t,1) , -pos(t,2)] / pixelSize))
        pos(t,1) = pos(t-1,1);
        pos(t,2) = pos(t-1,2) + step_adj .* velGlobal(t,2) * samplePeriod;
        coordInPixels = Checker.fCorrectIfIsNotOnMap(mask, ceil([pos(t,1), -pos(t,2)] / pixelSize));
        pos(t,1) = coordInPixels(1) * pixelSize; pos(t,2) = - coordInPixels(2) * pixelSize;
        if Checker.fCheck_obst(mask, ceil([pos(t-1,1) -pos(t-1,2)]  / pixelSize), ceil([ pos(t,1) , -pos(t,2)] / pixelSize))
            pos(t,1) = pos(t-1,1) + step_adj .* velGlobal(t,1) * samplePeriod;
            pos(t,2) = pos(t-1,2);
            coordInPixels = Checker.fCorrectIfIsNotOnMap(mask, ceil([pos(t,1), -pos(t,2)] / pixelSize));
            pos(t,1) = coordInPixels(1) * pixelSize; pos(t,2) = - coordInPixels(2) * pixelSize;
            if Checker.fCheck_obst(mask, ceil([pos(t-1,1) -pos(t-1,2)]  / pixelSize), ceil([ pos(t,1) , -pos(t,2)] / pixelSize))
                pos(t,:) = pos(t-1,:) + step_adj .* velGlobal(t,:) * samplePeriod;                        % integrate velocity to yield position
                coordInPixels = Checker.fCorrectIfIsNotOnMap(mask, ceil([pos(t,1), -pos(t,2)] / pixelSize));
                pos(t,1) = coordInPixels(1) * pixelSize; pos(t,2) = - coordInPixels(2) * pixelSize;
            end
        end
    end

    if t_show - accel(t).timestamp < 0
%    coordInMeters = Checker.fMeshCorrector([pos(t,1) -pos(t,2)], meshSteps, size(mask,1) * pixelSize, masktable);
%    pos(t,1) = coordInMeters(1); pos(t,2) = - coordInMeters(2);
    objPlotter.fPlotSimpleApproach([pos(t,1) -pos(t,2)]/pixelSize, [posprev(1) -posprev(2)]/pixelSize);
    posprev = pos(t,:);
    t_show = t_show + 1;
    end
end
%}

%% ============== PARTICLE FILTER (2D) ============== %%
%{
% Settings
partNumb = 100; %% Number of particles
pixelSize = 0.007;
COV_MODEL = [0.01 0.01];                                     %% Covariance of the model
Z_init = [0 -0.5];                                          %% True initial position (Alex`s BECAONS FORMAT: X, Y)
% Load files
mask  = imread('Office map - binary_mask.bmp');
Z_temp = load(strcat(pwd,'\Res\output.mat'));                        %% Alex readings from BEACONS
Z_temp = Z_temp.output;
Z_temp(:,2) = -Z_temp(:,2);
Z_temp = [[Z_temp(1,1:2) 0]; Z_temp ];
Z(:,1) = interp1( Z_temp(:,3) , Z_temp(:,1) , [accel.timestamp]);
Z(:,2) = interp1( Z_temp(:,3) , Z_temp(:,2) , [accel.timestamp]);
XYideal = importdata('ideal_trajectory.txt'); 
%GENERATION
Coord = zeros(partNumb, 2);
Coord_prev_step = zeros(partNumb, 2);
Weight = zeros(partNumb, 2);
Weight_norm = zeros(partNumb,1);
CoordAv = zeros(numel(accel),2);
%objPlotter.fPlotRoomMeters();                          %% Plotter in meters
objPlotter.fPlotRoomPixels(mask);
objParticleFilter = ParticleFilter();
%% Initialization
for i = 1 : partNumb
    Coord(i,1) = Z_init(1,1) + sqrt(COV_MODEL(1,1)) * randn;
    Coord(i,2) = Z_init(1,2) + sqrt(COV_MODEL(1,2)) * randn;
end
Coord_prev_step = Coord;
Coord_Resample = Coord;
Weight_Low = Z_init;
Weight_High = Z_init;

%% Filter itself
for t = 2 : length(accel)   
    %% Model
    if ~stationary(t,1)
        for i = 1:partNumb
            Coord_prev_step(i,:) = [Coord(i,1) Coord(i,2)];          %% Temporary storage for previous coordinates of points
            CoordAv(t-1,1) + (accel(t).timestamp - accel(t-1).timestamp) .* velGlobal(t,1)   
            
            Coord(i,1) = Coord(i,1) + (accel(t).timestamp - accel(t-1).timestamp) .* velGlobal(t,1) + (COV_MODEL(1,1)^0.5) .* randn; 
            Coord(i,2) = Coord(i,2) + (accel(t).timestamp - accel(t-1).timestamp) .* velGlobal(t,2) + (COV_MODEL(1,2)^0.5) .* randn;
            %% Converting from m to pixels
            [Coord(i, :), Coord_prev_step(i,:)] = objParticleFilter.fCheckPixels(Coord(i, :), Coord_prev_step(i,:), pixelSize);
            %% Crashing particle at the wall 
            Weight_norm(i) = objParticleFilter.fGetWeights(Coord_prev_step(i,:), Coord(i,:), pixelSize, COV_MODEL(1,:), [Z(t,1), Z(t,2)], mask, 'IMU', [ CoordAv(t-1,2) + (accel(t).timestamp - accel(t-1).timestamp) .* velGlobal(t,2)]);          
        end
        Weight_norm = Weight_norm(:)./sum(Weight_norm); % Normalize weight
        %% Resampling wheel algorithm
        [Coord_Resample Weight_Resample] = objParticleFilter.fResample(Coord, Weight_norm, partNumb);
        Weight_norm_Resample = Weight_Resample(:)./sum(Weight_Resample); % Normalize weight
 
    %% Plot Preparation
         Weight_High = intersect (Coord_Resample, Coord, 'rows');
        Weight_Low = setdiff(Coord, Coord_Resample, 'rows');
        if (size(Weight_Low) == 0)
            Weight_Low = [0 0];
        end
    end
    CoordAv(t,:) = [mean(Weight_High(:,1)) mean(Weight_High(:,2))];            % Metrics
%   objPlotter.funcCurrentParticles(Weight_High , Weight_Low , Z (t, :) , CoordAv (t, :));  % Plot iteration in m/s
    % Ideal trajectory coordinates
    idealCoord(1,1) = interp1(XYideal(:,1) , XYideal(:,2) , accel(t).timestamp);
    idealCoord(1,2) = interp1(XYideal(:,1) , XYideal(:,3) , accel(t).timestamp);

    objPlotter.fCurrentParticles([Weight_High(:,1) -Weight_High(:,2)] / pixelSize , [Weight_Low(:,1) -Weight_Low(:,2)] / pixelSize , [Z(t, 1) -Z(t, 2)] / pixelSize , [CoordAv(t, 1) -CoordAv(t, 2)] / pixelSize, [idealCoord(1,1) -idealCoord(1,2)] / pixelSize); % Plot Iteration in pixels    
 
Coord = Coord_Resample;
end
%}