
%% Depicting avgRSSI plot
figure(figN);
hold on;
% Depicting points dotted lines
for p=1:pointsN
    plot([p p], [0 beaconsN+1], 'k:');
    % Depicting points for beacons with packets in record
    for bN=1:beaconsN
        beacNcalibrOK{bN,p} = struct('RSSI_TH', 'NO', 'D_TH', 'NO');
        if avgRssi(bN,p)~=0
            if avgRssi(bN,p) > (k_coef * 10*log10(beacDist(bN,p)) + b_coef)
                plot(p, bN, '.', 'Marker', 'p', 'MarkerSize', 10, 'LineWidth', 1,...
                     'Color', 'Blue', 'MarkerFaceColor', 'Blue');
                beacNcalibrOK{bN,p}.RSSI_TH = 'YES';
            else
                plot(p, bN, '.', 'Marker', '.', 'MarkerSize', 20,...
                     'LineWidth', 1, 'Color', 'Red');
            end
            text(p-0.3, bN+.4, sprintf('%.1f', avgRssi(bN,p)), 'Color', 'black') ;
        end        
    end
end
% Editing the figure
title(['Averaged RSSI values for beacons calibration data in ' logFileName]);
xlabel('Number of calibration position');
ylabel('Beacons number');
ax = gca;
ax.XTick = 0:pointsN+1;
ax.YTick = 0:beaconsN+1;
xlim([0 pointsN+1]);
ylim([0 beaconsN+1]);
ax.YGrid = 'on';
ax.GridLineStyle = '--';
ax.GridAlpha = 0.5;
box on;
figN = figN + 1;


%% Depicting Distance2Beacons plot
figure(figN);
hold on;
% Depicting points dotted lines
for p=1:pointsN
    plot([p p], [0 beaconsN+1], 'k:');
    % Depicting points for beacons with packets in record
    for bN=1:beaconsN
        if beacDist(bN,p)~=0
            if beacDist(bN,p) < dTH
                 plot(p, bN, '.', 'Marker', 'p', 'MarkerSize', 10, 'LineWidth', 1,...
                     'Color', 'Blue', 'MarkerFaceColor', 'Blue');
                beacNcalibrOK{bN,p}.D_TH = 'YES'; 
            else
                plot(p, bN, '.', 'Marker', '.', 'MarkerSize', 20,...
                     'LineWidth', 1, 'Color', 'Red');
            end
            text(p-0.3, bN+.4, sprintf('%.1f', beacDist(bN,p)), 'Color', 'black') ;
        end       
    end
end
% Editing the figure
title(['Distances from user device to beacons, log - ' logFileName]);
xlabel('Number of calibration position');
ylabel('Beacons number');
ax = gca;
ax.XTick = 0:pointsN+1;
ax.YTick = 0:beaconsN+1;
xlim([0 pointsN+1]);
ylim([0 beaconsN+1]);
ax.YGrid = 'on';
ax.GridLineStyle = '--';
ax.GridAlpha = 0.5;
box on;
figN = figN + 1;


%% Depicting packets number plot
figure(figN);
hold on;
% Depicting points dotted lines
for p=1:pointsN
    plot([p p], [0 beaconsN+1], 'k:');
    % Depicting points for beacons with packets in record
    for bN=1:beaconsN
        if packetsCount(bN,p)~=0
            if strcmp(beacNcalibrOK{bN,p}.RSSI_TH, 'YES') &&...
               strcmp(beacNcalibrOK{bN,p}.D_TH, 'YES')
                 plot(p, bN, '.', 'Marker', 'p', 'MarkerSize', 10, 'LineWidth', 1,...
                     'Color', 'Blue', 'MarkerFaceColor', 'Blue');
            else
                plot(p, bN, '.', 'Marker', '.', 'MarkerSize', 20, 'LineWidth', 1, 'Color', 'Red');
            end
            text(p-0.3, bN+.4, num2str(packetsCount(bN,p)), 'Color', 'black') ;
        end        
    end
end
% Editing the figure
title(['Available packets from beacons for ' logFileName]);
xlabel('Number of calibration position');
ylabel('Beacons number');
ax = gca;
ax.XTick = 0:pointsN+1;
ax.YTick = 0:beaconsN+1;
xlim([0 pointsN+1]);
ylim([0 beaconsN+1]);
ax.YGrid = 'on';
ax.GridLineStyle = '--';
ax.GridAlpha = 0.5;
box on;
figN = figN + 1;