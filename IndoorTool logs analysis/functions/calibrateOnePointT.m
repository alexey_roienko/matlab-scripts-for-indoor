%% Function performs calibration for a beacon having averaged RSSI value 
%  just for one point. Suppose that DAMP parameter is fixed and equal to
%  the initial value. Estimate TXpower parameter only.
%  "distance" should be in dBs, i.e. 10*log10(distance)

function output = calibrateOnePointT(distance, av_rssi, init_damp)

% Wrong data case
if distance < 0
    output = -1;
end

% Find TXpower from the rssi formula
output = av_rssi + init_damp * distance;

