%% Function performs calibration for a beacon having averaged RSSI value 
%  just for one point. Suppose that TXpower parameter is fixed and equal to
%  the initial value. Estimate DAMP parameter only.
%  Note that "dist_vect" should be in dBs! I.e. 10*log10(distance)

function [TXpower, damp] = calibrateLSM(dist_vect, av_rssi_vect)

% Wrong data case
if length(dist_vect) ~= length(av_rssi_vect)
    warning('Number of distance points does not coincide with the number of avRSSI points!');
    TXpower = 0;
    damp = 0;
else
    N = length(dist_vect);
    sum_d      = sum(dist_vect); 
    sum_rssi   = sum(av_rssi_vect);
    sum_d_rssi = sum(av_rssi_vect.*dist_vect);
    sum_d2     = sum(dist_vect.^2);
    
    c1 = N * sum_d_rssi - sum_d * sum_rssi;
    c2 = N * sum_d2 - sum_d^2;
    
    a = c1/c2;
    b = (sum_rssi - a*sum_d) / N;
    
    if -a > 5
        damp = 4.5;
    elseif -a < 1
        damp = 1.5;
    else
        damp = -a;
    end
    
    if b < -90
        TXpower = -75;
    elseif b > -40
        TXpower = -63;
    else
        TXpower = b;
    end    
    
%     damp = -a;
%     TXpower = b;
end

