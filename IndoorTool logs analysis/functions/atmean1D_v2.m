function output = atmean1D_v2(input, tr_par)

%input - denotes 1D signal to be filtered
%tr_par - trimming parameter in percents

N = length(input);
trNumb = round(N * tr_par/100);

temp = sort(input);
if trNumb~=0
    output = mean(temp(trNumb : N-trNumb+1));
else
    output = mean(temp);
end
