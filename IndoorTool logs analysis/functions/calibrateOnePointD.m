%% Function performs calibration for a beacon having averaged RSSI value 
%  just for one point. Suppose that TXpower parameter is fixed and equal to
%  the initial value. Estimate DAMP parameter only.

function output = calibrateOnePointD(distance, av_rssi, txPower)

% Wrong data case
if (distance == 1) || (distance < 0)
    output = -1;    
end

% Find damp from the rssi formula
output = (txPower - av_rssi) / (10 * log10(distance));

