%% Extraction of calibration point positions as well as beacons data for 
%  corresponding points from the specified Indoor Tool JSON log-file

function [points, beacons] = read_IT_json_log(fName)


data    = loadjson(fName);
pointsN = length(data);
points  = cell(1, pointsN);
beacons = cell(1, pointsN);
for p=1:pointsN
    points{p} = struct('x', data{p}.aUserPositionX, 'y', data{p}.aUserPositionY);
    beacons{p} = data{p}.beaconsLog;
end







