%% Function extracts the RSSI values and timestamps from the input data matrix
%  which is had got from downloading a json log-file.

function [rssi, packets] = get_data_BLE(data, beacMAClist)

persistent figN
if isempty(figN)
    figN = 1;
end

beaconsN = length(beacMAClist);
buffer   = zeros(beaconsN, 1);
counter  = zeros(beaconsN, 1);
N        = length(data);

% Investigations %
% beacons_packets = zeros(N, beaconsN);

%% Cycle over each packet in input "data"
for n=1:N
    currentMAC = data{n}.mac;
    for bN=1:beaconsN
        if strcmp(currentMAC, beacMAClist{bN})
            buffer(bN)  = buffer(bN) + data{n}.rssi;
            counter(bN) = counter(bN) + 1;
%             beacons_packets(counter(bN), bN) = data{n}.rssi;
            break;
        end
    end
end


%% Calculate average RSSI value for each beacon in considered calibration position
rssi    = zeros(beaconsN, 1);
packets = counter;
for bN=1:beaconsN
    if counter(bN)~=0
        rssi(bN) = buffer(bN)/packets(bN);
        %rssi(bN) = atmean1D_v2(beacons_packets(1:counter(bN), bN), 49);
    end
end

% Investigation %
% figure(figN);
% hold on;
% for bN=1:beaconsN
%     atm_value = atmean1D_v2(beacons_packets(1:counter(bN), bN), 49);
%     plot(1:counter(bN), beacons_packets(1:counter(bN), bN));
%     plot(1:counter(bN), rssi(bN)*ones(counter(bN),1));
%     plot(1:counter(bN), atm_value*ones(counter(bN),1));
% end
% figN = figN + 1;
% pause;


