%% Function calculates distance to each beacon for corresponding position 
%  of calibration point

function beacDist = dist2beac(beaconsParams, points, userHeight)

pointsN  = length(points);
beaconsN = length(beaconsParams);
beacDist = zeros(beaconsN, pointsN);

for p=1:pointsN
    for bN=1:beaconsN
        beacDist(bN, p) = ((points{p}.x - beaconsParams{bN}.x)^2 + ...
                           (points{p}.y - beaconsParams{bN}.y)^2 + ...
                           (userHeight  - beaconsParams{bN}.z)^2)^.5;
    end
end
