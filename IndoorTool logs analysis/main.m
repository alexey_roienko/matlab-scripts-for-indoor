%% Script processes the calibration log from Indoor Tool and depicts 
%  information about:
%  1. Averaged RSSI values for each beacon for each calibration position;
%  2. Distance to each beacon from each calibration position;
%  3. Number of packets for each beacon for each calibration position as
%  well as the calibration can be done or not for each beacon in each
%  calibration position.  

clc
close all;
clear;

%% Initialization
addpath([pwd '/logs/'], [pwd '/beacons_data/'], [pwd '/functions/'], [pwd '/map_data/']);
figN = 1;
beaconsMACs   = readBeaconsJson('/beacons_data/', 'MAC');
beaconsParams = readBeaconsJson('/beacons_data/', 'beacons_params');
beaconsN      = length(beaconsMACs);
% RSSI threshold init block - averaged values
% k_coef = -2.021;    b_coef = -74.21;     %Kaa beacon#2
k_coef_array = [-1.348 -1.231 -2.15];      %IT-Jim beacons #1, 6, 10
b_coef_array = [-82.277 -76.732 -73.826];
k_coef = k_coef_array(1); %mean(k_coef_array);
b_coef = b_coef_array(1); %mean(b_coef_array);
% Distance threshold init block
dTH = 6;
% Height of the user's device
userHeight = 1.5;
% DAMP initial value for 1 point calibration
init_damp = 2.2;


%% Read log JSON-file
f_content = dir([pwd '/logs/']);
f_start = 3;
logFileName = f_content(f_start).name;

disp(['Processing ' logFileName ' ...']);
[points, beaconCells] = read_IT_json_log(logFileName);
pointsN = length(points);


%% Estimating distances to beacons
beacDist = dist2beac(beaconsParams, points, userHeight);


%% Cycle over each calibration position
avgRssi      = zeros(beaconsN, pointsN);
packetsCount = zeros(beaconsN, pointsN);
for p=1:pointsN    
    [avgRssi(:,p), packetsCount(:,p)] = get_data_BLE(beaconCells{p}, beaconsMACs);
end

beacNcalibrOK = cell(beaconsN, pointsN);

main_depicting_plots;


%% Calibration using LSM; for correct cases
beacons_calibr_data = cell(beaconsN,1);
for bN=1:beaconsN
    beacon_data = zeros(pointsN, 2);
    points = 0;
    for p=1:pointsN
        if strcmp(beacNcalibrOK{bN,p}.D_TH, 'YES') && ...
           strcmp(beacNcalibrOK{bN,p}.RSSI_TH, 'YES')
            
            points = points + 1;
            beacon_data(points, 1) = beacDist(bN,p);
            beacon_data(points, 2) = avgRssi(bN,p);            
        end
    end
    beacons_calibr_data{bN} = beacon_data(1:points, 1:2);
    beacon_data(1:points, 1) = 10*log10(beacon_data(1:points, 1));
    if points > 1
        [tx, da] = calibrateLSM(beacon_data(1:points,1), beacon_data(1:points,2));
        disp(['Beacon #' num2str(bN) ': TX=' sprintf('%.2f', tx) ', damp=' sprintf('%.2f', da)])
    elseif points==0
        disp(['Beacon #' num2str(bN) ' has no calibration points'])
    else
        tx = calibrateOnePointT(beacon_data(1:points,1), beacon_data(1:points,2), init_damp);
        disp(['Beacon #' num2str(bN) ' has only 1 calibr. point: TX=' ...
               sprintf('%.2f', tx) ', damp=' sprintf('%.1f', init_damp)])
    end
%     pause;
end










